//
//  JsonWebToken.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 26/11/18.
//  Copyright © 2018 CoopDaquilema. All rights reserved.
//

import Foundation
import JWT
class JsonWebToken {
    
    static func  crearJWT(contenido:String, firma:String) -> String {
        
        var json: String = ""
        json = JWT.encode(claims: ["Contenido": contenido], algorithm: .hs256(firma.data(using: .utf8)!))
        return json
        
    }
    static func leerJWT() -> Void {
        
    }
}
