//
//  Temporizador.swift
//  DaquiApp
//
//  Created by Fredy on 28/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
class Temporizador  {
    var second = 0
    var timer = Timer()
    var timerIso = false
    
    func Inicio() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector (updateTimer), userInfo: nil, repeats: true)
    }
    func Parar() {
        timer.invalidate()
        self.timerIso = false
        second = 0
    }
    
    @objc func updateTimer () -> Bool {
        self.second = self.second + 1
        if second < 60 {
            
            self.timerIso = true
        }
        return self.timerIso
    }
}
