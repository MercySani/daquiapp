import UIKit

class TimerApplication: UIApplication {

    //el tiempo de espera en segundos, después del cual debería realizar acciones personalizadas
    //como desconectar al usuario
    private var timeoutInSeconds: TimeInterval {
        // 5 minutes
        return 5 * 60
        
    }

    private var idleTimer: Timer?

    // actualiza mediante la interaccion del usuario
    private func resetIdleTimer() {
        if let idleTimer = idleTimer {
            idleTimer.invalidate()
        }

        idleTimer = Timer.scheduledTimer(timeInterval: timeoutInSeconds,
                                         target: self,
                                         selector: #selector(TimerApplication.timeHasExceeded),
                                         userInfo: nil,
                                         repeats: false
        )
    }

    // si el temporizador alcanza este limite permitido se lanza esta notificacion.
    @objc private func timeHasExceeded() {
        NotificationCenter.default.post(name: .appTimeout, object: nil )
    }

    override func sendEvent(_ event: UIEvent) {

        super.sendEvent(event)

        if idleTimer != nil {
            self.resetIdleTimer()
        }

        if let touches = event.allTouches {
            for touch in touches where touch.phase == UITouchPhase.began {
                self.resetIdleTimer()
            }
        }
    }
}
extension Notification.Name {
    
    static let appTimeout = Notification.Name("appTimeout")
    
}
