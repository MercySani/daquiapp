//
//  DelegateOTP.swift
//  DaquiApp
//
//  Created by Fredy on 11/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
protocol OTPDelegate {
    func inserOTP(value: String)
}
