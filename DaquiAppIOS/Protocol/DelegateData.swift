
//
//  DelegateData.swift
//  DaquiApp
//
//  Created by Desarrollador on 30/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation

protocol PopUpDelegate {
    func seleccionString(value: String, tipoSelection: String, tipoCuenta:String, saldoDisponible: String)
}
