//
//  ConsultaViewController.swift
//  DaquiApp
//
//  Created by Desarrollador on 17/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class ConsultaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var usuario = UsuarioModel()
    var teams: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        usuario = (tabBarController as! TabBarViewController).usuario
        
        teams = ["Atletico de Madrid", "Barcelona", "Deportivo de la Coruña", "Las Palmas", "Malaga", "Rayo Vallecano"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "mycell")
        cell.textLabel?.text  = teams[indexPath.row]
        
        cell.imageView!.image = UIImage(named: "isotipo coac daquilema")!
        cell.detailTextLabel?.text = "Detalles"
        return cell
    }
    
}
