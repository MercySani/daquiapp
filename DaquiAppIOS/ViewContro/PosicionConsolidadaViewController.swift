//
//  swPosicionConsolidada.swift
//  DaquiApp
//
//  Created by Fredy on 14/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//


import UIKit
import Foundation

class PosicionConsolidadaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    var usuario: UsuarioModel = UsuarioModel()
    var posicionConsolidada: [PosicionConsolidadaModel] = []
    var noCertificados: [PosicionConsolidadaModel] = []
    var certificados: [PosicionConsolidadaModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var segmenControl: UISegmentedControl!
    lazy var refreshControl: UIRefreshControl = {
        let refresControl = UIRefreshControl()
        refresControl.addTarget(self, action: #selector(PosicionConsolidadaViewController.actualizarDatos(_:)), for: .valueChanged)
        refresControl.tintColor = UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        return refresControl
    }()
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usuario = (tabBarController as! TabBarViewController).usuario
        print("Usuario --> \(usuario)")
        if(usuario.strNombreUser == _CONST.USUARIO_DEMO){
            definirTiposSaldos(posicionCon: _DEMO.posicionConsolidad())
            self.posicionConsolidada.removeAll()
            self.posicionConsolidada = self.noCertificados
            self.tableView.reloadData()
            
        }else{
            posicionConsolidada(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_CEDULA, usuario: usuario.strIdentificacion, token:usuario.strToken)
        }
        self.tableView.addSubview(self.refreshControl)
        
        meessageWelcome(usuarioModel: usuario)
    }
    
    func meessageWelcome(usuarioModel: UsuarioModel) {
        let hora = Calendar.current.component(.hour, from: Date())
        let nombre = Utilidades.separarNombre(fullName: usuarioModel.strNombreCompleto)
            if(usuarioModel.strMensajeError.isNotEmpty){
                Toast(text: Utilidades.saludoDadoHora(hora: hora) + ", " + nombre + ". " + usuarioModel.strMensajeError, delay: 0 , duration: 5 ).show()
                let apariencia = ToastView.appearance()
                apariencia.backgroundColor = UIColor.blue
                apariencia.textColor = UIColor.white
                apariencia.font = UIFont.boldSystemFont(ofSize: 14)
                apariencia.cornerRadius = 14
                apariencia.bottomOffsetPortrait = CGFloat(60)
                
                
            }else{
                Toast(text: Utilidades.saludoDadoHora(hora: hora) + ", " + nombre, delay: 0 , duration: 5 ).show()
                let apariencia = ToastView.appearance()
                apariencia.backgroundColor = UIColor.blue
                apariencia.textColor = UIColor.white
                apariencia.font = UIFont.boldSystemFont(ofSize: 14)
                apariencia.cornerRadius = 14
                apariencia.bottomOffsetPortrait = CGFloat(60)
        }
    }
    
    @objc func actualizarDatos (_ refresControl: UIRefreshControl){
        posicionConsolidada.removeAll()
        noCertificados.removeAll()
        certificados.removeAll()
        refresControl.endRefreshing()
        posicionConsolidada(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_CEDULA, usuario: usuario.strIdentificacion,  token:usuario.strToken)
        segmenControl.selectedSegmentIndex = 0
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.posicionConsolidada.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cuentaCell") as! GenericoTableViewCell
        cell.titulo.text = self.posicionConsolidada[indexPath.row].Descripcion
        let auxCuenta = NSMutableAttributedString()
        auxCuenta
            .bold("Cuenta: ")
            .normal(self.posicionConsolidada[indexPath.row].Cuenta)
        
        cell.detalle1.attributedText = auxCuenta
        
        let auxSaldoEfectivo = NSMutableAttributedString()
        auxSaldoEfectivo .bold("Saldo disponible: ")
                        .normal("$\(self.posicionConsolidada[indexPath.row].Saldoefectivo)")
        cell.detalle2.attributedText = auxSaldoEfectivo
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "mostrarTransacciones", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination =  segue.destination as? TransaccionCuentaViewController {
            destination.numeroCuenta = posicionConsolidada[(tableView.indexPathForSelectedRow?.row)!].Cuenta
        }
    }
    
    func posicionConsolidada( identificacion: String, tipoIdentificacion: String, usuario: String, token: String){
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/posicionConsolidada/\(_CONST.CANAL_DES)/\(identificacion)/\(tipoIdentificacion)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.posicionConsolidada = try JSONDecoder().decode([PosicionConsolidadaModel].self , from: data)
                    if  self.posicionConsolidada.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            
                            self.definirTiposSaldos(posicionCon: self.posicionConsolidada)
                            self.posicionConsolidada.removeAll()
                            self.posicionConsolidada = self.noCertificados
                            self.tableView.reloadData()
                        }
                    }else{
                        DispatchQueue.main.async {
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                    
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                               Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                        DispatchQueue.main.async {
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_PP_LISTA_VACIA, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir Posicion Consolidada:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    func definirTiposSaldos(posicionCon: [PosicionConsolidadaModel]) -> (){
        
        for pc: PosicionConsolidadaModel in posicionCon {
            let indexStartOfText = pc.Cuenta.index(pc.Cuenta.startIndex, offsetBy: _CONST.N_PREF_CNTA_FIN)
            let prefijo = String(pc.Cuenta[...indexStartOfText])
            for cnta in _CONST.CNTA_CERTIFICADOS {
                if prefijo == cnta{
                    self.certificados.append(pc)
                }else{
                    self.noCertificados.append(pc)
                }
            }
        }
    }
    
    @IBAction func sc_actualizarTipoSaldo(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
            case 0:
                
                self.posicionConsolidada.removeAll()
                self.posicionConsolidada = self.noCertificados
                self.tableView.reloadData();
            case 1:
                
                self.posicionConsolidada.removeAll()
                self.posicionConsolidada = self.certificados
                self.tableView.reloadData();
            default: break;
        }
    }
    
    
}

