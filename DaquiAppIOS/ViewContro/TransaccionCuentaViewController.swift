//
//  TransaccionesCuentasSWController.swift
//  DaquiApp
//
//  Created by Fredy on 17/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
import UIKit

class TransaccionCuentaViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var transaccionTableView: UITableView!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var definirFechaBtn: UIBarButtonItem!
    @IBOutlet weak var fechaTextField: UITextField!
    @IBOutlet weak var consultarBtn: UIButton!
    let objActivity = activityIndicatorVC()
    var numeroCuenta: String = String()
    var fechaTransaccion: String = String()
    var usuario: UsuarioModel = UsuarioModel()
    var transaccionesCuenta: [TransaccionCuentaModel] = []
    
    let fechaDatePicker = UIDatePicker()
    let date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        usuario = (tabBarController as! TabBarViewController).usuario
      
        
        
        //consultar las ultimas transacciones
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateStyle = .short
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        let dateFormato = dateStringFormatter.date(from: dateStringFormatter.string(from: self.date))
        let monthsToAdd = _CONST.ULTIMAS_TRANSACCIONES
        
        let newDate = Calendar.current.date(byAdding: .day, value: monthsToAdd, to: dateFormato!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = _CONST.YYYY_MM_DD
        
        //fin consultar
        
        guard usuario.strNombreUser == _CONST.USUARIO_DEMO else {
            consultaTransaccionesCuentas(identificacion: usuario.strIdentificacion, idTipoIdentificacion: _CONST.TI_CEDULA, numeroCuenta: numeroCuenta, fecha: dateFormatter.string(from: newDate!), token: usuario.strToken)
            return
        }
    }
    
   func createDatePicker() {
        
        // toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        // done button for toolbar
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([done], animated: false)
        fechaTextField.inputAccessoryView = toolbar
        fechaTextField.inputView = fechaDatePicker
        // format picker for date
        fechaDatePicker.datePickerMode = .date
    }
    
    @objc func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        fechaTransaccion = dateFormatter.string(from: fechaDatePicker.date)
        
        consultaTransaccionesCuentas(identificacion: usuario.strIdentificacion, idTipoIdentificacion: _CONST.TI_CEDULA, numeroCuenta: numeroCuenta, fecha: fechaTransaccion, token: usuario.strToken)
        self.view.endEditing(true)
    } 
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.transaccionesCuenta.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transaccionCell") as! GenericoTableViewCell;
        
        cell.titulo.textColor = UIColor(red: 0.39, green: 0, blue: 0, alpha: 1)
        
        if self.transaccionesCuenta[indexPath.row].Depositos == "0.00"{
            
            let auxValor = NSMutableAttributedString()
            auxValor .bold(" -  ")
            .normal("$\(self.transaccionesCuenta[indexPath.row].Retiros) ")
            
            cell.titulo.attributedText = auxValor
        }else{
            let auxValor = NSMutableAttributedString()
            auxValor .bold(" +  ")
                .normal("$\(self.transaccionesCuenta[indexPath.row].Depositos) ")
             
            cell.titulo.attributedText = auxValor
        }
        
        
        
        let auxTransaccion = NSMutableAttributedString()
        auxTransaccion .bold("Detalle: ") .normal(self.transaccionesCuenta[indexPath.row].Transaccion)
        cell.detalle1.attributedText = auxTransaccion
        
        let auxFechaContable = NSMutableAttributedString()
        auxFechaContable .bold("") .normal(self.transaccionesCuenta[indexPath.row].FContable)
        cell.detalle2.attributedText = auxFechaContable
        
        let auxContable = NSMutableAttributedString()
        auxContable .bold("") .normal("[$\(self.transaccionesCuenta[indexPath.row].Contable)]")
        cell.detalle3.attributedText = auxContable
        return cell
    }
    
    @IBAction func consultarBarButton(_ sender: UIBarButtonItem) {
        let fechaPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "FechaPopupViewController") as! FechaPopupViewController
        fechaPopupViewController.popUpDelegate = self
        fechaPopupViewController.tipoSeleccion = "FechaTransaccion"
        self.present(fechaPopupViewController, animated: true)
    }
    
    
    func consultaTransaccionesCuentas( identificacion: String, idTipoIdentificacion: String, numeroCuenta: String, fecha: String, token:String){
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/consultaTransaccionesCuentas/\(_CONST.CANAL_DES)/\(identificacion)/\(idTipoIdentificacion)/\(numeroCuenta)/\(fecha)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.transaccionesCuenta = try JSONDecoder().decode([TransaccionCuentaModel].self , from: data)
                    if  self.transaccionesCuenta.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            self.transaccionTableView.reloadData()
                            
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_TRAN_CNTA_SIN + fecha , view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                  Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                    }
                }
            }else if let error = error{
                print("Error al consumir Transaccion de Cuentas:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_TRAN_CNTA_SIN, view: self)
                }
            }
            }.resume()
        
    }
    @IBAction func btnAnalisisEgresos(_ sender: Any) {
        performSegue(withIdentifier: "analisisEgresoSegue", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination =  segue.destination as? AnalisisEgresoViewController {
            destination.numeroCuenta = self.numeroCuenta
        }
    }
}

extension TransaccionCuentaViewController: PopUpDelegate {
    func seleccionString(value: String, tipoSelection: String, tipoCuenta: String, saldoDisponible: String) {
        fechaTransaccion = value;
        DispatchQueue.main.async {
            self.transaccionesCuenta.removeAll()
            self.transaccionTableView.reloadData()
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        consultaTransaccionesCuentas(identificacion: usuario.strIdentificacion, idTipoIdentificacion: _CONST.TI_CEDULA, numeroCuenta: numeroCuenta, fecha: fechaTransaccion, token: usuario.strToken)
    }
    
  
}
