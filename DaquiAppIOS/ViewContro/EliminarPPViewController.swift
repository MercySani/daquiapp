//
//  EliminarPPViewController.swift
//  DaquiApp
//
//  Created by Fredy on 12/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class EliminarPPViewController: UIViewController {
 
    var usuario: UsuarioModel = UsuarioModel()
    
    @IBOutlet weak var idPP: UILabel!
    @IBOutlet weak var nombreServicio: UILabel!
    @IBOutlet weak var diaDebito: UILabel!
    @IBOutlet weak var cuentaDebito: UILabel!
    @IBOutlet weak var fechaFinalizacion: UILabel!
    @IBOutlet weak var fechaInicio: UILabel!
    //fin variables de vista
    @IBOutlet weak var btnEliminar: UIButton!
    
    var pagoProgramado: [LisCueXNoSocioSRModel] = []
    let objActivity = activityIndicatorVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        usuario = (tabBarController as! TabBarViewController).usuario
        btnEliminar.layer.cornerRadius = _CONST.CORNER_RADIUS
        
        for items in pagoProgramado {
            let auxID = NSMutableAttributedString()
            auxID
                .bold("ID: ")
                .normal("\(items.id)")
            idPP.attributedText = auxID
            
            let auxNomSer = NSMutableAttributedString()
            auxNomSer
                .bold("Nombre Servicio: ")
                .normal("\(items.servicio.nombre)")
            nombreServicio.attributedText = auxNomSer
            
            let auxFIni = NSMutableAttributedString()
            auxFIni
                .bold("Fecha Inicio: ")
                .normal("\(items.fInicio)")
            fechaInicio.attributedText = auxFIni
            
            let auxFFin = NSMutableAttributedString()
                auxFFin
                    .bold("Fecha Finalización: ")
                    .normal("\(items.fFinalizacion)")
            fechaFinalizacion.attributedText = auxFFin
            
            let auxCDebito = NSMutableAttributedString()
                auxCDebito
                .bold("Cuenta de Débito: ")
                .normal("\(items.cuentaDebito)")
            cuentaDebito.attributedText = auxCDebito
            
            let auxdDebito = NSMutableAttributedString()
                auxdDebito
                    .bold("Día de pago: ")
                    .normal("\(items.dia)")
            diaDebito.attributedText = auxdDebito
            
           
        }
        
    }
    
   
    
    @IBAction func eliminarPagoProgramado(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        self.enviarCodigoOPT(userName: usuario.strNombreUser, identificacion: usuario.strIdentificacion, token: usuario.strToken)
    
    }
    
    //enviar OTP
    //esta funcion solo envia el OTP, para utilizar este funcion cualquiera de las dos variables deben estar estar correctos.
    func enviarCodigoOPT(userName: String, identificacion: String, token: String)   {
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = [ "strUsername": userName,
                                    "strIdentificacion": identificacion,
                                    "strToken": token,
                                    
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                    "strObservacion": _CONST.CANAL_DES
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/procesoEnvioOTP")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    let otpRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  otpRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK {
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            
                            let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
                            otpViewController.popUpOTPDelegate = self
                            self.present(otpViewController, animated: true)
                        }
                    }else{
                        if otpRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: otpRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_OTP_NOK , view: self)
                            }
                        }
                    }
                } catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                }
            }else if let error = error{
                print("Error al consumir Envio OTP :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    
    //combinar
    func mergePagoProgramado( id: Int , servicioID: String, personaID: Int, noSocio: String, idAgencia: Int, fInicio: String, fFinalizacion: String, dia: Int, cuentaDebito: String, montoMaximo: Int, referencia: String, datoAdicional: String, codigoCanalInterno: String, estado: Int, usuarioCreacion: String, usuarioActualizacion: String, identificacion: String, tipoIdentificacion: String, codigoOTP:String , token: String )->Void{
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = [ "id": id,
                                    "servicio": [
                                        "id": servicioID],
                                    "persona":  ["identificacion": identificacion, "idTipoIdentificacion": tipoIdentificacion, "socio" : noSocio],
                                    
                                    "idAgencia": idAgencia,
                                    "fInicio": fInicio,
                                    "fFinalizacion": fFinalizacion,
                                    "dia": dia,
                                    "cuentaDebito": cuentaDebito,
                                    "montoMaximo": montoMaximo,
                                    "referencia": referencia,
                                    "datoAdicional": datoAdicional,
                                    "codigoCanalInterno": codigoCanalInterno,
                                    "estado": estado,
                                    "usuarioCreacion": usuarioCreacion,
                                    "usuarioActualizacion": usuarioActualizacion,
                                    "identificacion": identificacion,
                                    
                                    "strCodigoOtp": codigoOTP,
                                    "strToken": token,
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/combinarPagoProgramadoSR")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { data, response, error in
            if let data = data{
                do{
                    let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                    
                    if  mensajeRespuesta.strCodigoError == _CONST.CODIGO_RETORNO_OK {
                        
                        DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            let alertControler = UIAlertController(title: _CONST.AVISO, message: _CONST.PP_MEN_ELIMINAR_OK, preferredStyle: .alert)
                            let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
                            { (action:UIAlertAction!) in
                                
                                self.performSegue(withIdentifier: "unwindSegue", sender: self)
                            }
                            alertControler.addAction(aceptarAccion)
                            self.present(alertControler, animated: true, completion: nil)
                        }
                    }else{
                            if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                                }
                            }else if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_SEG{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                                }
                            }else{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                                }
                            }
                    }
                }catch {
                    do{
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                    
                    
                }
            }else if let error = error{
                print("Error al consumir Eliminar Pago Programado :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir Eliminar Pago Programado")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    
    
    //fin combinar
    
    

}

extension EliminarPPViewController: OTPDelegate{
    func inserOTP(value: String) {
        
        for items in self.pagoProgramado {
          if(items.datoAdicional == ""){
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
                mergePagoProgramado( id: items.id , servicioID: items.servicio.id, personaID: items.persona.id, noSocio: items.persona.socio, idAgencia: items.idAgencia, fInicio: items.fInicio, fFinalizacion: items.fFinalizacion, dia: items.dia, cuentaDebito: items.cuentaDebito, montoMaximo: items.montoMaximo, referencia: items.referencia, datoAdicional: _CONST.DATO_ADICIONAL , codigoCanalInterno: items.codigoCanalInterno  , estado: _CONST.ESTADO_PP_ELIMINAR, usuarioCreacion: items.usuarioCreacion, usuarioActualizacion: items.usuarioActualizacion, identificacion: items.persona.identificacion, tipoIdentificacion: items.persona.idTipoIdentificacion, codigoOTP: value, token: usuario.strToken)
            }else{
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            mergePagoProgramado( id: items.id , servicioID: items.servicio.id, personaID: items.persona.id, noSocio: items.persona.socio, idAgencia: items.idAgencia, fInicio: items.fInicio, fFinalizacion: items.fFinalizacion, dia: items.dia, cuentaDebito: items.cuentaDebito, montoMaximo: items.montoMaximo, referencia: items.referencia, datoAdicional: items.datoAdicional! , codigoCanalInterno: items.codigoCanalInterno  , estado: _CONST.ESTADO_PP_ELIMINAR, usuarioCreacion: items.usuarioCreacion, usuarioActualizacion: items.usuarioActualizacion, identificacion: items.persona.identificacion, tipoIdentificacion: items.persona.idTipoIdentificacion, codigoOTP: value, token: usuario.strToken)
            }
        }
    }
    
    
}


