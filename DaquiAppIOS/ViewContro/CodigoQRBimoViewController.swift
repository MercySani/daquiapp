//
//  CodigoQRBimoViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 04/06/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import Foundation
import UIKit

class CodigoQRBimoViewController: UIViewController{
    
    var usuario: UsuarioModel = UsuarioModel()
    @IBOutlet weak var image: UIImageView?
    @IBOutlet weak var lblCelular: UILabel!
    @IBOutlet weak var btnSalir: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("usuario -> \(usuario)")
        lblCelular.text = usuario.strTelefono
        image?.image = Utilidades.generateQRCode(from: usuario.strTelefono)
        
        self.btnSalir.layer.cornerRadius = _CONST.CORNER_RADIUS
        
    }
    

    @IBAction func accionSalir(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
