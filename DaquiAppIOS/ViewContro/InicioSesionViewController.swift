//
//  InicioSesionViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 03/01/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//
import LocalAuthentication
import UIKit

class InicioSesionViewController: UIViewController, UITextFieldDelegate{
    
    //variables de indicador de actividades
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let objActivity = activityIndicatorVC()
    var usuario = UsuarioModel()
    var nombreUsuario :String = ""
    
    @IBOutlet weak var btnHuella: UIButton!
    @IBOutlet weak var btnUsuario: UIButton!
    
    @IBOutlet weak var contenedorHuella: UIView!
    @IBOutlet weak var contenedorUsuario: UIView!
    
    @IBAction func authWithTouchID(_ sender: Any) {
         self.biometriaLocalConsulta()
       }
       
       override func viewDidLoad() {
           super.viewDidLoad()
       
         self.navigationController?.navigationBar.tintColor =
           UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
           let backItem = UIBarButtonItem()
           backItem.title = "Menú"
           navigationItem.backBarButtonItem = backItem
    
        contenedorHuella.layer.borderColor=UIColor.white.cgColor
        
        contenedorHuella.layer.borderWidth=1
        
        contenedorUsuario.layer.borderColor=UIColor.white.cgColor
        contenedorUsuario.layer.borderWidth=1
       }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           navigationController?.setNavigationBarHidden(false, animated: animated)
       }

       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           navigationController?.setNavigationBarHidden(false, animated: animated)
       }
       
       func showAlertController(_ message: String) {
           let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
           alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
           present(alertController, animated: true, completion: nil)
       }
    
    func huellaValidacion()-> Void{
        // 1
        let context = LAContext()
        var error: NSError?
        
        // 2
        // verificar si Touch ID está disponible
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            // 3
            let reason = "Autenticar con Touch ID"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply:
                {(success, error) in
                    // 4
                    if success {
                        self.biometriaLocalLogin(usuario:self.nombreUsuario)
                    }
                    else {
                     DispatchQueue.main.async {
                         self.objActivity.hideActivityIndicator(uiView: self.view)
                         Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "Error de autenticación de identificación táctil", view: self)
                     }
                    }
            })
        }// 5
        else {
            showAlertController("Touch ID no disponible")
        }
    }
    
    
    func biometriaLocalConsulta()-> Void{
        let strAction="Q"
        let session = URLSession(
        configuration: URLSessionConfiguration.ephemeral,
        delegate: NSURLSessionPinningDelegate(),
        delegateQueue: nil)
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        let version: String = UIDevice.current.model+"_"+UIDevice.current.systemName+"_" + UIDevice.current.systemVersion+"|"+appVersion
        
        let json: [String: Any] = ["strAccion": strAction,
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                    "strCanalDescripcionAdicional": _CONST.CANAL_DES ,
                                    "strDetalleDispositivo": version ,
                                    "strOrigenIdentificador": macAddress?.description ?? "",
                                    "strToken":"",
                                    "strNombreUser": "",
                                    ]
    
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/biometrialocal/general")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
    
        session.dataTask(with: request) { data, response, error in
            if let data = data, let _ = response as? HTTPURLResponse
            {
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(ResultadoRestful.self , from: data)
                    self.nombreUsuario=mensajeRespuestaData.strNombreUser
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                    }
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK{
                        DispatchQueue.main.async {
                            self.huellaValidacion()
                        }
                    }else if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_BIOMETRIA_NO_REGISTRADO{
                        DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "segueLoginRegistroHuella", sender: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir BiometriaLocal consulta :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                print("no se obtuno nigun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }.resume()
    }
    
    
    func biometriaLocalLogin(usuario: String) -> Void{
        //https://stackoverflow.com/questions/34223291/ios-certificate-pinning-with-swift-and-nsurlsession
       
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let version: String = UIDevice.current.model+"_"+UIDevice.current.systemName+"_" + UIDevice.current.systemVersion+"|"+appVersion
        
        
        let json: [String: Any] = ["strAccion": "L",
                                   "strNombreUser": usuario,
                                   "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                   "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                   "strOrigenIdentificador": macAddress?.description ?? "",
                                   "strDetalleDispositivo": version,
                                   "strToken":""]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/biometrialocal/general")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, let _ = response as? HTTPURLResponse {
                    do {
                        let usuarioData = try JSONDecoder().decode(UsuarioModel.self, from: data)
                        self.usuario = usuarioData
                        
                        if self.usuario.strCodigoError == _CONST.CODIGO_LOGUEO_OK{
                             DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            }
                            if(self.usuario.strVerificarExistenciaUCSOrigen){
                                if self.usuario.strUserEstado == _CONST.COD_ESTADO_CAMIBIO_CLAVE{
                                    
                                    if(self.usuario.strIdentificacion == self.usuario.strNombreUser){
                                        DispatchQueue.main.async {
                                            self.objActivity.hideActivityIndicator(uiView: self.view)
                                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_CEDULA_IGUAL_USUARIO, view: self)
                                        }
                                    }else{
                                        DispatchQueue.main.async {
                                        
                                        let cambioClaveViewController = self.storyboard?.instantiateViewController(withIdentifier: "CambioClaveViewController") as!  CambioClaveViewController
                                        cambioClaveViewController.usuario = self.usuario
                                        
                                        self.present(cambioClaveViewController, animated: true)
                                    }}
                                }else{
                                    
                                    //el mensaje va en vacio xq se modifica en el metodo el mensaje BOLD
                                   
                                    DispatchQueue.main.async {
                                        
                                         self.performSegue(withIdentifier: "segueHuella", sender: self)
                                        //self.usuarioField.text = ""
                                        //self.contrasenaField.text = ""
                                    }
                                }
                            }else{
                            self.enviarCodigoOPT(userName: self.usuario.strNombreUser, identificacion: self.usuario.strIdentificacion, token: self.usuario.strToken)
                            }
                            
                        }else{
                            if(self.usuario.strCodigoError == _CONST.CODIGO_ACTULIZACION){
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeActualizacion(titulo: "", mensaje: "", view: self)
                                }
                             
                            }else{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: self.usuario.strMensajeError, view: self)
                                }
                            }
                            }
                            
                    }  catch let error as NSError {
                        print("Error al leer JSON: \(error)")
                        
                        do {
                            var mensajeMode: MensajeModel = MensajeModel()
                            let mensajeData = try JSONDecoder().decode(MensajeModel.self, from: data)
                             mensajeMode = mensajeData
                            
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeMode.strMensajeError, view: self)
                            }
                        } catch _ as NSError {
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                            }
                        }
                    }
            }else if let error = error{
                print("Error al consumir logeoUsuario:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir logeoUsuario: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }.resume()
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueHuella"{
            let tabBarViewController = segue.destination as! TabBarViewController
            tabBarViewController.usuario = self.usuario
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    //enviar OTP
    //esta funcion solo envia el OTP, para utilizar este funcion cualquiera de las dos variables deben estar estar correctos.
    func enviarCodigoOPT(userName: String, identificacion: String, token:String)   {
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = [ "strUsername": userName,
                                    "strIdentificacion": identificacion,
                                    "strToken": token,
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                    "strObservacion": _CONST.CANAL_DES]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/procesoEnvioOTP")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let otpRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    
                    if  otpRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK {
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
                            otpViewController.popUpOTPDelegate = self
                            self.present(otpViewController, animated: true)
                        }
                    }else{
                        if otpRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: otpRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_OTP_NOK , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir Envio OTP :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    func verificarOTP(identificacion: String , codigoOtp: String, token: String)-> Void{
        
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = ["strIdentificacion": identificacion,
                                   "strOTP": codigoOtp,
                                   "strToken": token,
                                   "strCanal": _CONST.CANAL_DES,
                                   "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                   "strOrigenIdentificador": macAddress?.description ?? ""]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/verificarOTP")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { data, response, error in
            if let data = data{
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK{
                        
                        if self.usuario.strUserEstado == _CONST.COD_ESTADO_CAMIBIO_CLAVE{
                            if(self.usuario.strIdentificacion == self.usuario.strNombreUser){
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                     Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_CEDULA_IGUAL_USUARIO, view: self)
                                }
                            }else{
                                print("Llego 4 /()")
                            DispatchQueue.main.async {
                                 self.objActivity.hideActivityIndicator(uiView: self.view)
                                let cambioClaveViewController = self.storyboard?.instantiateViewController(withIdentifier: "CambioClaveViewController") as!  CambioClaveViewController
                                cambioClaveViewController.usuario = self.usuario
                                /*cambioClaveViewController.claveLogin = self.contrasenaField.text!
                                cambioClaveViewController.userLogin = self.usuarioField.text!*/
                                
                                self.present(cambioClaveViewController, animated: true)
                                }
                            }
                        }else{
                        
                             print("Llego 3 /()")
                        if mensajeRespuestaData.strValorResultado != "" {
                             self.usuario.strToken = mensajeRespuestaData.strValorResultado
                        }
                        
                            DispatchQueue.main.async {
                                                   self.objActivity.hideActivityIndicator(uiView: self.view)
                                                   //self.usuarioField.text = ""
                                                   //self.contrasenaField.text = ""
                                                   
                                                   }
                    
                        Utilidades.mensajeSugerencia(titulo: "", mensaje: "", view: self)
                            
                       
                        }
                    }else{
                        if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                print("Llego 1 /()")
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }else if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_USERNAME{
                            
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                print("Llego 2 /()")
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            print("Llego 5 /()")
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir verificar OTP:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir verificarOTP: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }

    func cerrarSesion(identificacion: String , token: String, opcion: Int)-> Void{
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = ["strIdentificacion": identificacion,
                                   "strToken": token]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/cerrarSesion")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { data, response, error in
            
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK{
                        
                        
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                        if (opcion == _CONST.OP_CERRAR_ENVIO){
                            
                            DispatchQueue.main.async{
                                let alertControl = UIAlertController(title: _CONST.AVISO, message: mensajeRespuestaData.strMensajeError , preferredStyle: .alert)
                                let actionM = UIAlertAction(title: _CONST.ACEPTAR, style: .default) { (action:UIAlertAction) in
                                    self.enviarCodigoOPT(userName: self.usuario.strNombreCompleto, identificacion: self.usuario.strIdentificacion, token: self.usuario.strToken)
                                }
                                alertControl.addAction(actionM)
                                self.present(alertControl, animated: true, completion: nil)
                            }
                        }else{
                             DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }
                    }else{
                        if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir cerrar sesion:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir cerrar sesion: No se obtuvo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    
    
    
    @IBAction func unwindInicioSesion(_ sender: UIStoryboardSegue) {
       
    }
    
}
extension InicioSesionViewController: OTPDelegate{
    func inserOTP(value: String) {
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        verificarOTP(identificacion: usuario.strIdentificacion, codigoOtp: value, token: usuario.strToken)
    }
}
