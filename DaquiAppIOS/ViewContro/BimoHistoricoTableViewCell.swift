//
//  BimoHistoricoTableViewCell.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 11/05/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import UIKit

class BimoHistoricoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblReferencia: UILabel!
    @IBOutlet weak var lblFecha: UILabel!
    @IBOutlet weak var lblMonto: UILabel!
    @IBOutlet weak var imagenEstado: UIImageView!
    
    static let identificador = "historico"
    private var title: String = ""
    
    static func nib() -> UINib{
           return UINib (nibName: "BimoHistoricoTableViewCell",bundle:nil)
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblNombre.text = "eje1"
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(witch historico: ListaDePagosBimoModel){
       
        lblReferencia.text = historico.reference
        lblFecha.text = historico.fecha
        lblMonto.text = "$" + String(historico.amount.value)
        if historico.criterio == _CONST.COBRO_BIMO {
             lblNombre.text = historico.nameDst
            if historico.estado == "Pendiente"{
               imagenEstado.image = UIImage(named: "cobroPendiente")
            }
            if historico.estado == "Pagados"{
                imagenEstado.image = UIImage(named: "cobroRealizado")
            }
            if historico.estado == "Rechazados"{
                imagenEstado.image = UIImage(named: "cobroRechazado")
            }
             if historico.estado == "Expirado"{
                 imagenEstado.image = UIImage(named: "cobroRechazado")
            }
        }else{
             lblNombre.text = historico.nameOrig
            if historico.estado == "Pendiente"{
              imagenEstado.image = UIImage(named: "pagoPendiente")
            }
            if historico.estado == "Pagados"{
               imagenEstado.image = UIImage(named: "pagoRealizado")
            }
            if historico.estado == "Rechazados"{
                imagenEstado.image = UIImage(named: "pagoRechazado")
            }
             if historico.estado == "Expirado"{
                imagenEstado.image = UIImage(named: "pagoRechazado")
            }
        }
       
    }
 
}
