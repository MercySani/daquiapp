//
//  PopUpOTPViewController.swift
//  DaquiApp
//
//  Created by Fredy on 11/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit
import  Foundation

class PopUpOTPViewController: UIViewController {

    var popUpOTPDelegate: OTPDelegate?
    
    @IBOutlet weak var btnAceptar: UIButton!
    @IBOutlet weak var btnCerrar: UIButton!
    @IBOutlet weak var btnBorrar: UIButton!
    @IBOutlet weak var btn0: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn6: UIButton!
    @IBOutlet weak var btn7: UIButton!
    @IBOutlet weak var btn8: UIButton!
    @IBOutlet weak var btn9: UIButton!
    
    @IBOutlet weak var txtOTP: UITextField!
    var actualNum: Int = 0
    var auxCero: String = "0"
   
    @IBAction func touchTextBox(_ sender: Any) {
        //DispatchQueue.main.async {
                      self.view.endEditing(true)
          //        }
    }
    
    @IBAction func numeroOTP(_ sender: UIButton) {
        
       
        
        if((txtOTP.text?.count)! < 6 ){
            
            txtOTP.text = txtOTP.text! + (sender.titleLabel?.text)!
            
        }
        
    }
    
    @IBAction func aceptar(_ sender: Any) {
        guard !(txtOTP.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_OTP  , view: self)
            }
            return
        }
        popUpOTPDelegate?.inserOTP(value: txtOTP.text!)
        dismiss(animated: true)
    }
    
    @IBAction func cancelar(_ sender: Any) {
        dismiss(animated: true)
    }
    
    
    @IBAction func borrar(_ sender: Any) {
        guard !(txtOTP.text!.isEmpty) else {
            return
        }
        
        let aux: String = txtOTP.text!
        txtOTP.text = String(aux.dropLast())
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        txtOTP.text = ""
        self.view.endEditing(true)
        
        btnCerrar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnAceptar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnBorrar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btn0.layer.cornerRadius = _CONST.CORNER_RADIUS
        btn1.layer.cornerRadius = _CONST.CORNER_RADIUS
        btn2.layer.cornerRadius = _CONST.CORNER_RADIUS
        btn3.layer.cornerRadius = _CONST.CORNER_RADIUS
        btn4.layer.cornerRadius = _CONST.CORNER_RADIUS
        btn5.layer.cornerRadius = _CONST.CORNER_RADIUS
        btn6.layer.cornerRadius = _CONST.CORNER_RADIUS
        btn7.layer.cornerRadius = _CONST.CORNER_RADIUS
        btn8.layer.cornerRadius = _CONST.CORNER_RADIUS
        btn9.layer.cornerRadius = _CONST.CORNER_RADIUS
        randomBtn()
    }
    
    func randomBtn(){
        var arrayNumber: [Int] = []
        arrayNumber = self.generarNumero(from: 0, to: 9, qut: nil)
        for item in 0...arrayNumber.count{
            
             switch item {
             case 0:
             if (btn0.titleLabel?.text == nil) {
             btn0.setTitle("\(arrayNumber[item])", for: .normal)
             }
             break
             case 1:
             if (btn1.titleLabel?.text == nil) {
             btn1.setTitle("\(arrayNumber[item])", for: .normal)
             }
             break
             case 2:
             if (btn2.titleLabel?.text == nil) {
             btn2.setTitle("\(arrayNumber[item])", for: .normal)
             }
             break
             case 3:
             if (btn3.titleLabel?.text == nil) {
             btn3.setTitle("\(arrayNumber[item])", for: .normal)
             }
             break
             case 4:
             if (btn4.titleLabel?.text == nil) {
             btn4.setTitle("\(arrayNumber[item])", for: .normal)
             }
             break
             case 5:
             if (btn5.titleLabel?.text == nil) {
             btn5.setTitle("\(arrayNumber[item])", for: .normal)
             }
             break
             case 6:
             if (btn6.titleLabel?.text == nil) {
             btn6.setTitle("\(arrayNumber[item])", for: .normal)
             }
             break
             case 7:
             if (btn7.titleLabel?.text == nil) {
             btn7.setTitle("\(arrayNumber[item])", for: .normal)
             }
             break
             case 8:
             if (btn8.titleLabel?.text == nil) {
             btn8.setTitle("\(arrayNumber[item])", for: .normal)
             }
             break
             case 9:
             if (btn9.titleLabel?.text == nil) {
             btn9.setTitle("\(arrayNumber[item])", for: .normal)
             }
             break
             default:
             break
             }
        }
   
    }
    func generarNumero(from:Int, to:Int, qut: Int?)->[Int]{
        
        var myRandomNumbers = [Int]()
        var numberOfNumbers = qut
        
        let lower = UInt32(from)
        let higher = UInt32(to+1)
        
        if numberOfNumbers == nil || numberOfNumbers! > (to-from) + 1
        {
            numberOfNumbers = (to-from) + 1
        }
        
        while myRandomNumbers.count != numberOfNumbers
        {
            let myNumber = arc4random_uniform(higher - lower) + lower
            
            if !myRandomNumbers.contains(Int(myNumber))
            {
                myRandomNumbers.append(Int(myNumber))
                
            }
        }
        return myRandomNumbers
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
   
}
