//
//  HabilitarUsuarioViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 13/12/2019.
//  Copyright © 2019 CoopDaquilema. All rights reserved.
//

import UIKit
import Foundation
class HabilitarUsuarioViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var identificacionField: UITextField!
    
    
    @IBOutlet weak var emailField: UITextField!
    
    
    @IBOutlet weak var celularField: UITextField!
    

    @IBOutlet weak var continuarBtn: UIButton!
    
    
    let objActivity = activityIndicatorVC()
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        identificacionField.delegate = self
        celularField.delegate=self
        self.navigationController?.navigationBar.tintColor =
        UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        continuarBtn.layer.cornerRadius = _CONST.CORNER_RADIUS
    }
    
    
    @IBAction func btnCancelar(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Something Else"
        navigationItem.backBarButtonItem = backItem // This will show in the next view controller being pushed
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var tipoCaracter: CharacterSet = CharacterSet()
        var characterSet: CharacterSet = CharacterSet()
        var retorno: Bool = false
        if textField.tag == 1{
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 15
            }
        }else if textField.tag == 2 {
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 10
            }
        }
        
        
        return retorno
    }

    
    @IBAction func modificarClave(_ sender: Any) {
        if !self.identificacionField.text!.isEmpty  &&
            !self.celularField.text!.isEmpty  && !self.emailField.text!.isEmpty{
            
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            buscarSocio(identificacion:self.identificacionField.text!, celular:self.celularField.text!, email: self.emailField.text!)
        }else{
            Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.SIM_CRE_MSJ_CAMPOS, view: self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    func buscarSocio(identificacion: String, celular: String, email: String)-> Void{
        let strAction="CONSULTAR"
        let session = URLSession(
        configuration: URLSessionConfiguration.ephemeral,
        delegate: NSURLSessionPinningDelegate(),
        delegateQueue: nil)
        
        let json: [String: Any] = ["strAccion": strAction,
        "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
        "strObservacion": _CONST.CANAL_DES ,
        "strCelular": celular ,
        "strEmail": email,
        "strIdentificacion":identificacion]
    
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/habilitacionUsuario/general")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
    
        session.dataTask(with: request) { data, response, error in
            if let data = data, let _ = response as? HTTPURLResponse
            {
                do{
                    print("Ingreso 1")
                    let mensajeRespuestaData = try JSONDecoder().decode(MensajeHabilitarUsuarioModel.self , from: data)
                    print("mensajeRespuestaData-->  \(mensajeRespuestaData) ")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                    }
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK{
                        
                        DispatchQueue.main.async {/*
                            self.dismiss(animated: true)
                            let terminosHabilitarUsrViewController =  self.storyboard?.instantiateViewController(withIdentifier: "TerminosHabilitarUsrViewController") as! TerminosHabilitarUsrViewController
                            self.present(terminosHabilitarUsrViewController, animated: true)*/
                            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TerminosHabilitarUsrViewController") as? TerminosHabilitarUsrViewController
                            vc?.mensajeRepuesta=mensajeRespuestaData
                            vc?.identificacion=identificacion
                            vc?.celular=celular
                            
                            self.navigationController?.pushViewController(vc!, animated: true)
                        }
                        
                        
                        
                    }else if mensajeRespuestaData.strCodigoError=="-2"{
                          DispatchQueue.main.async {
                               self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSimpleUrl(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError,
                                url: mensajeRespuestaData.strEmail, view: self)
                        }
                        
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir HabilitarUsuario consulta :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                print("no se obtuno nigun dto")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }.resume()
        
        
    }
    
}
