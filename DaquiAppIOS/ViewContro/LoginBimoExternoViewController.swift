//
//  LoginBimoExternoViewController.swift
//  DaquiAppIOS
//
//  Created by MacBookPro on 6/1/20.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//
import SwiftyXMLParser
import Foundation
import UIKit

class LoginBimoExternoViewController: UIViewController, UITextFieldDelegate {
 
    
    let auxDireccion = 1
    var celularUsuario :String = ""
    var usuario = UsuarioModel()
    //variables de indicador de actividades
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    @IBOutlet weak var image: UIImageView!
    let objActivity = activityIndicatorVC()
    
    var posicionConsolidada: [PosicionConsolidadaModel] = []
    var posicionConsolidadaAuxiliar: [PosicionConsolidadaModel] = []
    var posicionConsolidadaString: PosicionConsolidadaModel = PosicionConsolidadaModel()
       
    var cuentaSeleccionada = ""
    var auxCount = 0
    var tieneBilleteraBimo = 0
    var cuentaBimo = ""
    var saldoCuentaBimo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard !Seguridad.verifyJailbreak() else {
                  DispatchQueue.main.async {
                      
                      let alertController = UIAlertController(title: _CONST.ALERTA, message: _CONST.MSJ_SEGURIDAD , preferredStyle: .alert)
                      let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
                      { (action:UIAlertAction!) in
                          exit(0)
                      }
                      alertController.addAction(aceptarAccion)
                      self.present(alertController, animated: true, completion: nil)
                  }
                  return
              }
              
              self.contrasenaField.text = ""
              contrasenaField.delegate = self
        celularField.text = celularUsuario
        celularField.isEnabled=false
              //Borde Redondeado botones
              btnSalir.layer.cornerRadius = _CONST.CORNER_RADIUS
              btnAcceder.layer.cornerRadius = _CONST.CORNER_RADIUS
        image?.image = Utilidades.generateQRCode(from: celularUsuario)
        usuario.strBilleteraMovil=celularUsuario
        
        self.navigationController?.navigationBar.tintColor =
        UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
    }
    
    @IBOutlet weak var celularField: UITextField!
    @IBOutlet weak var contrasenaField: UITextField!
    @IBOutlet weak var btnSalir: UIButton!
    @IBOutlet weak var btnAcceder: UIButton!
    
    func hideKeyboard()  {
        celularField.resignFirstResponder()
        contrasenaField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyboard()
        return true
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
              contrasenaField.resignFirstResponder()
              self.view.endEditing(true)
    }
    
    @IBAction func recuperarContraseñaBimo(_ sender: Any) {

        let recuperarClaveBimoExternoViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecuperarClaveBimoExternoViewController") as! RecuperarClaveBimoExternoViewController
        //dismiss(animated: true)
        present(recuperarClaveBimoExternoViewController, animated: true)
    }
    
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            var tipoCaracter: CharacterSet = CharacterSet()
            var characterSet: CharacterSet = CharacterSet()
            var retorno: Bool = false
            
                tipoCaracter = CharacterSet.decimalDigits
                characterSet = CharacterSet(charactersIn: string)
                retorno = tipoCaracter.isSuperset(of: characterSet)
                if(retorno){
                    guard let text = textField.text else { return true }
                    let count = text.count + string.count - range.length
                    retorno = count <= 10
                }
           
            return retorno
        }
   
    @IBAction func accionIngresar(_ sender: Any) {
              guard !((self.celularField.text == _CONST.USUARIO_DEMO) && (self.contrasenaField.text == _CONST.PASSWORD_DEMO)) else {
                  DispatchQueue.main.async {
                      self.usuario.strCodigoError = _CONST.CODIGO_OK
                      self.usuario.strMensajeError = _CONST.MSJ_DEFAULT_OK
                      self.usuario.strEMail = _CONST.EMAIL_DEMO
                      self.usuario.strNombreCompleto = _CONST.NOMBRECOMPLETO_DEMO
                      self.usuario.strNombreUser = _CONST.USUARIO_DEMO
                      self.usuario.strTelefono = _CONST.TELEFONO_DEMO
                      self.usuario.strIdentificacion = "0000000000"
                      self.performSegue(withIdentifier: "segueBimoPrincipal", sender: nil)
        
                      self.celularField.text = ""
                      self.contrasenaField.text = ""
                  }
                  return
              }
              
        guard  !(Utilidades.tieneCaracteresEspeciales(string: self.celularField.text!)) else{
                  DispatchQueue.main.async {
                      Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_CARACTERES_ESPECIALES, view: self)
                  }
                  return
              }
              if !self.celularField.text!.isEmpty && !self.contrasenaField.text!.isEmpty {
              //indicator de actividades
              DispatchQueue.main.async {
                  self.objActivity.showActivityIndicator(uiView: self.view)
              }
              // fin indicador de actividades
              logeoUsuarioBimoExterno(celular: self.celularField.text!, contrasena: self.contrasenaField.text!)
              }else{
                  Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.ERR_C_VACIOS, view: self)
              }
    }
    
    func logeoUsuarioBimoExterno(celular: String, contrasena: String) -> Void{
 //https://stackoverflow.com/questions/34223291/ios-certificate-pinning-with-swift-and-nsurlsession
       
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let version: String = UIDevice.current.model+"_"+UIDevice.current.systemName+"_" + UIDevice.current.systemVersion+"|"+appVersion
        var firma: String = ""
        var jwt:String = ""
        for _ in 1...7 {
            firma = firma + celular
        }
        jwt = JsonWebToken.crearJWT(contenido: HASH.MD5(string: contrasena), firma: firma)
        let strAction="L"
        let json: [String: Any] = ["strAccion": strAction,
                                "strNombreUser": celular,
                                "strPassword": jwt ,
                                "strCanalSeguridad": _CONST.CANAL_SEG_IOS_BIMO,
                                "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                "strOrigenIdentificador": macAddress?.description ?? "",
                                "strDetalleDispositivo": version,
                                "strToken":"",
                                "strSistema": _CONST.SISTEMA_BIMO_EXTERNO,
                                "strIdentificacion":"",
                                "strObservacion":""]
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/bimo/generalexterno")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, let _ = response as? HTTPURLResponse {
                    do {
                        
                        let usuarioData = try JSONDecoder().decode(UsuarioModel.self, from: data)
                        self.usuario = usuarioData
                        if self.usuario.strCodigoError == _CONST.CODIGO_LOGUEO_OK{
                             DispatchQueue.main.async {
                               print("self.usuario \(self.usuario)")
                                self.accionBimo(strTrama: _CONST.VALIDA_CAMPO_RQ, telefonoOrigen:  self.usuario.strTelefono, identificacion:  self.usuario.strTelefono, strFiltro: "", strCriterio:"" ,token: self.usuario.strToken)
                                self.accionBimo(strTrama: _CONST.CONSULTA_TEL_CUENTA_RQ, telefonoOrigen: self.usuario.strTelefono, identificacion: self.usuario.strIdentificacion, strFiltro: _CONST.TELEFONO_BIMO, strCriterio: self.usuario.strTelefono, token: self.usuario.strToken)
                               self.contrasenaField.text = ""
                            }
                        }else{
                            if(self.usuario.strCodigoError == _CONST.CODIGO_ACTULIZACION){
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeActualizacion(titulo: "", mensaje: "", view: self)
                                }
                             
                            }else{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: self.usuario.strMensajeError, view: self)
                                }
                            }
                            }
                            
                    }  catch let error as NSError {
                        print("Error al leer JSON: \(error)")
                        
                        do {
                            var mensajeMode: MensajeModel = MensajeModel()
                            let mensajeData = try JSONDecoder().decode(MensajeModel.self, from: data)
                             mensajeMode = mensajeData
                            
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeMode.strMensajeError, view: self)
                            }
                        } catch _ as NSError {
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                            }
                        }
                    }
            }else if let error = error{
                print("Error al consumir logeoUsuarioBimo:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir logeoUsuarioBimo: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }.resume()
        
    }
    
    //para Buscar la cuenta
    func posicionConsolidada( identificacion: String, tipoIdentificacion: String, usuario: String, token: String){
        print("posicionConsolidada------")
        print("identificacion \(identificacion)")
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/posicionConsolidada/\(_CONST.CANAL_DES)/\(identificacion)/\(tipoIdentificacion)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS_BIMO)"
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
            session.dataTask(with: url){(data, response, error) in
            let response = response as? HTTPURLResponse

            if let data = data,
                response?.statusCode==200{
                do{
                    self.posicionConsolidada = try JSONDecoder().decode([PosicionConsolidadaModel].self , from: data)
                    
                    if  self.posicionConsolidada.count != _CONST.LISTA_VACIA{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            for item in self.posicionConsolidada{
                                if(item.Producto == _CONST.CNTA_DEBITO_VISTA  || item.Producto == _CONST.CNTA_DEBITO_KULLKIMIRAK || item.Producto == _CONST.CNTA_DEBITO_BASICA){
                                    self.posicionConsolidadaString.Cuenta = item.Cuenta
                                    self.posicionConsolidadaString.Descripcion = item.Descripcion
                                    self.posicionConsolidadaString.GProducto = item.GProducto
                                    self.posicionConsolidadaString.Producto = item.Producto
                                    self.posicionConsolidadaString.Saldoaportacion = item.Saldoaportacion
                                    self.posicionConsolidadaString.Saldobloqueado = item.Saldobloqueado
                                    self.posicionConsolidadaString.Saldocontable = item.Saldocontable
                                    self.posicionConsolidadaString.Saldoefectivo = item.Saldoefectivo
                                    self.posicionConsolidadaAuxiliar.append(self.posicionConsolidadaString)
                                   print("self.posicionConsolidadaString------ \(self.posicionConsolidadaString)")
                                }
                            }
                            if self.posicionConsolidadaAuxiliar.count != _CONST.LISTA_VACIA{
                                print("self.posicionConsolidadaAuxiliar.count  \(self.posicionConsolidadaAuxiliar.count )")
                                self.performSegue(withIdentifier: "segueBimoPrincipal", sender: self)
                            }
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                        }
                    }
                    
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                  Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
                }
            }else if let error = error{
                print("Error al consumir Posicion Consolidada:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
    }
    
      override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "segueBimoPrincipal"{
               if let destino = segue.destination as?  BillteraMovilViewController {
                print("se va self.posicionConsolidadaAuxiliar \(self.posicionConsolidadaAuxiliar.count)")
                   destino.estadoUsuario = tieneBilleteraBimo
                   destino.posicionConsolidadaAuxiliar = self.posicionConsolidadaAuxiliar
                   destino.cuentaBimo = self.cuentaBimo
                   destino.usuario = self.usuario
                   destino.origen = _CONST.BIMO_ORIGEN_EXTERNO
               }
               
           }
       }
 
    func accionBimo(strTrama: String, telefonoOrigen: String, identificacion: String,strFiltro: String, strCriterio: String , token: String)-> Void{
        print("accionBimo---")
           let session = URLSession(
                      configuration: URLSessionConfiguration.ephemeral,
                      delegate: NSURLSessionPinningDelegate(),
                      delegateQueue: nil)
           
           let json: [String: Any] = [ "strIdentificacion": identificacion,
                                       "strToken": token,
                                       "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                       "strCanalSeguridad": _CONST.CANAL_SEG_IOS_BIMO,
                                       "strTrama": strTrama,
                                       "strTelefonoOrigen": telefonoOrigen,
                                       "strTelefonoDestino": "",
                                       "strDescripcion": "",
                                       "strMonto": "",
                                       "strAccion": "",
                                       "strFiltro": strFiltro,
                                       "strCriterio": strCriterio,
                                       "strNumeroCuenta": "",
                                       "strEndToEnd": ""
                                       
           ]
           
           let jsonData = try? JSONSerialization.data(withJSONObject: json)
           let url = URL(string: "\(_CONST.DIRECCION)/bimo/general")!
           var request = URLRequest(url: url)
           request.httpMethod = "POST"
           request.httpBody = jsonData
           session.dataTask(with: request) { data, response, error in
           if let data = data, let response = response as? HTTPURLResponse, response.statusCode==202{
               do{
                   let mensajeRespuestaData = try JSONDecoder().decode(BimoResultado.self , from: data)
                   if  mensajeRespuestaData.strCodigo == _CONST.CODIGO_OK{
                       
                       DispatchQueue.main.sync {
                           let xml = try! XML.parse(mensajeRespuestaData.strXML)
                           print(mensajeRespuestaData)
                           if let codResultado = xml["BimoXml", "codResultado"].text {
                               if strTrama == _CONST.VALIDA_CAMPO_RQ {
                                   DispatchQueue.main.async {
                                          self.objActivity.hideActivityIndicator(uiView: self.view)
                                       
                                       if codResultado == _CONST.CODIGO_OK{
                                           print("tiene billera ")
                                           self.tieneBilleteraBimo = 1
                                           //self.buscarCuentaBimo()
                                        self.posicionConsolidada(identificacion: identificacion, tipoIdentificacion: _CONST.TI_CEDULA, usuario: identificacion, token: token)
                                        
                                       }else  if codResultado == _CONST.BIMO_500{
                                           print("NO tiene billera ")
                                        Utilidades.mensajeSimple(titulo: "Aviso", mensaje: "No posee billetera ", view: self)
                                       }else  if codResultado == _CONST.BIMO_501{
                                           print("billera bloqueada")
                                            self.tieneBilleteraBimo = 3
                                            self.posicionConsolidada(identificacion: identificacion, tipoIdentificacion: _CONST.TI_CEDULA, usuario: identificacion, token: token)
                                          // self.buscarCuentaBimo()
                                           
                                       }else{
                                           let mensajeError = xml["BimoXml", "mensajeResultado"].text
                                           Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeError! , view: self)
                                       }
                                   }
                               }
                               if strTrama == _CONST.CONSULTA_TEL_CUENTA_RQ {
                                 if codResultado == _CONST.CODIGO_OK{
                                   DispatchQueue.main.async {
                                       self.objActivity.hideActivityIndicator(uiView: self.view)
                                       let cadenaCuenta: [String] = xml["BimoXml", "codResultadoAdicional"].text!.components(separatedBy: "|")
                                       self.cuentaBimo  = cadenaCuenta[3]
                                   }
                                }
                               }
                              
                           }
                       }
                   }else{
                       if mensajeRespuestaData.strCodigo == _CONST.CODIGO_ERROR_TOKEN{
                           DispatchQueue.main.async {
                               self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensaje, view: self)
                           }
                       }else{
                           DispatchQueue.main.async {
                               self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensaje , view: self)
                           }
                       }
                   }
               }catch let error as NSError{
                   print("Error al leer JSON: \(error)")
                   DispatchQueue.main.async {
                       self.objActivity.hideActivityIndicator(uiView: self.view)
                       Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                   }
               }
           }else if let error = error{
               print("Error al consumir cerrar sesion:error: \(error)")
               if let error = error as NSError?, error.domain == NSURLErrorDomain{
                   if error.code == -1001 {
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                       }
                   } else if error.code == -1009 {
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                       }
                   }else{
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                       }
                   }
               }else{
                   DispatchQueue.main.async {
                       self.objActivity.hideActivityIndicator(uiView: self.view)
                       Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                   }
               }
           }else{
               //No se obtubo ningun dato
               DispatchQueue.main.async {
                   self.objActivity.hideActivityIndicator(uiView: self.view)
                   Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
               }
           }
           }.resume()
       
       }
    
    @IBAction func unwindLoginBimoExterno(_ sender: UIStoryboardSegue) {
       
    }
}
