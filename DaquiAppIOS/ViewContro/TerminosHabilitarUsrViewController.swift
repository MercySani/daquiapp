//
//  TerminosHabilitarUsrViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 14/12/2019.
//  Copyright © 2019 CoopDaquilema. All rights reserved.
//

import Foundation
import UIKit

class TerminosHabilitarUsrViewController: UIViewController, UITextFieldDelegate {
    var mensajeRepuesta : MensajeHabilitarUsuarioModel? = nil
    var identificacion :String = ""
    var celular :String = ""
    let objActivity = activityIndicatorVC()
    
    
    @IBOutlet weak var swtAceptar: UISwitch!
    
    @IBOutlet weak var btnAceptarTerminos: UIButton!
    
    @IBOutlet weak var btnCancelar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.navigationController?.navigationBar.tintColor =
               UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
               
        btnAceptarTerminos.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnCancelar.layer.cornerRadius = _CONST.CORNER_RADIUS
      
    }
    
    
    
    
    @IBAction func aceptarTerminos(_ sender: Any) {
        if(swtAceptar.isOn){
            mostrarDialogoOTP()
        }else{
            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_ACEPTAR_CONDICIONES , view: self)
        }
       
    }
    
    
    @IBAction func cancelarTerminos(_ sender: Any) {
        navigationController?.popViewController(animated: true)
               dismiss(animated: true, completion: nil)
        //navigationController?.popToRootViewController(animated: true)
    }
    
    func mostrarDialogoOTP() -> Void{
        DispatchQueue.main.async {
            self.objActivity.hideActivityIndicator(uiView: self.view)
            
            let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
            otpViewController.popUpOTPDelegate = self as? OTPDelegate
            self.present(otpViewController, animated: true)
        }
    }
    
    func aceptarTerminosCondiciones(identificacion: String, celular:String, otp: String, mensajeRepuesta : MensajeHabilitarUsuarioModel )-> Void{
        
        let strAction="HABILITAR"
        let session = URLSession(
        configuration: URLSessionConfiguration.ephemeral,
        delegate: NSURLSessionPinningDelegate(),
        delegateQueue: nil)
        
        let json: [String: Any] = ["strAccion": strAction,
        "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
        "strOTP": otp ,
        "strEmail":mensajeRepuesta.strEmail,
        "strCelular":celular,
        "strIdentificacion":identificacion,
        "strObservacion": _CONST.CANAL_DES,
        "strInsertarEmail": mensajeRepuesta.strInsertarEmail ]
    
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/habilitacionUsuario/general")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
    
        session.dataTask(with: request) { data, response, error in
            if let data = data, let _ = response as? HTTPURLResponse
            {
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                    }
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK{
                        
                        /*Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)*/
                        DispatchQueue.main.async {
                            self.dismiss(animated: true, completion: nil)
                            
                            self.performSegue(withIdentifier: "segueHabilitacionCorrec", sender: self)
                            
                        }
                        
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir HabilitarUsuario consulta :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                print("no se obtuno nigun dto")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }.resume()
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueHabilitacionCorrec" {
            if let destinationVC = segue.destination as? HabilitacionCorrectaViewController {
                destinationVC.mensajeCorreo = mensajeRepuesta!.strEmail
            }
        }
    }
    
    
}


extension TerminosHabilitarUsrViewController: OTPDelegate{
    func inserOTP(value: String) {
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        print("enviando opt...")
        print("\(self.identificacion) --  \(self.celular) --- \(self.mensajeRepuesta)")
        
        aceptarTerminosCondiciones(identificacion: identificacion,  celular: celular, otp: value, mensajeRepuesta:mensajeRepuesta!)
    }
}


