//
//  MisPagosPendientesTableViewCell.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 07/05/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import UIKit

protocol MisPagosPendientesDelegate: AnyObject{
    func didTapButton(with title: String)
}

class MisPagosPendientesTableViewCell: UITableViewCell {

    weak var delegate: MisPagosPendientesDelegate?
    
    var alertsDelegate : alerts?
    
    static let identificador = "MisPagosPendientesTableViewCell"
    
    static func nib() -> UINib{
        return UINib (nibName: "MisPagosPendientesTableViewCell",bundle:nil)
    }
    
    @IBOutlet var btnAceptar: UIButton!
    @IBOutlet var btnRechazar: UIButton!
    @IBOutlet var lblPago: UILabel!
    @IBOutlet weak var lblRefencia: UILabel!
    @IBOutlet weak var lblFecha: UILabel!
    @IBOutlet weak var lblMonto: UILabel!
    
    var monto = ""
    var descripcion = ""
    var endToEnd = ""
    var telefonoDestino = ""
    
    private var title: String = ""
    
    @IBAction func didTapButton(){
        delegate?.didTapButton(with: title)
    }
    
    @IBAction func aceptarPago(){
        
        alertsDelegate?.mostrarAlerta(title: "PAGAR", message: "¿Desea aceptar este pago?",monto: self.monto, descripcion: self.descripcion, endToEnd: self.endToEnd, telefonoDestino: self.telefonoDestino)
    }
    
    @IBAction func rechazarPago(){
        
        alertsDelegate?.mostrarAlerta(title: "RECHAZAR", message: "¿Desea rechazar este pago?", monto: self.monto, descripcion: self.descripcion, endToEnd: self.endToEnd, telefonoDestino: self.telefonoDestino)
    }
    
    func configure(witch pago: PagoBimoModel){
       
        lblPago.text = pago.nameOrig
        lblRefencia.text = pago.reference
        lblFecha.text =  String(pago.date.year) + "-" + String(pago.date.month) + "-" + String(pago.date.day)
        lblMonto.text = "$"+String(pago.amount.value)
         
        self.monto = String(pago.amount.value)
        self.descripcion = pago.reference
        self.endToEnd = pago.endToEnd
        self.telefonoDestino = pago.phoneOrig
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
