//
//  PagoProgramadoViewController.swift
//  DaquiApp
//
//  Created by Fredy on 11/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit
import  Foundation

class PagoProgramadoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    var usuario: UsuarioModel = UsuarioModel()
    @IBOutlet weak var tableView: UITableView!
    let objActivity = activityIndicatorVC()
    
    var listaCuentasXNoSocio: [LisCueXNoSocioSRModel] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.reloadData()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.navigationController?.navigationBar.tintColor =
        UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        let backItem = UIBarButtonItem()
        backItem.title = "Menu"
        navigationItem.backBarButtonItem = backItem
        
        usuario = (tabBarController as! TabBarViewController).usuario
       guard usuario.strNombreUser == _CONST.USUARIO_DEMO else {
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            listaCuentasPoridetificacionSR(identificacion: usuario.strIdentificacion, contrasena: usuario.strIdentificacion, token: usuario.strToken)
            // Bar Item Button
        return
        }

    
    
    }
    
    @IBAction func insertarPP(_ sender: UIBarButtonItem) {
        let backItem = UIBarButtonItem()
        backItem.title = "Atras"
        navigationItem.backBarButtonItem = backItem
        
        performSegue(withIdentifier: "InsertarPPViewController", sender: self)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func unwindSegue(_ sender: UIStoryboardSegue) {
        listaCuentasXNoSocio.removeAll()
        tableView.reloadData()
        listaCuentasPoridetificacionSR(identificacion: usuario.strIdentificacion, contrasena: usuario.strIdentificacion, token: usuario.strToken)
    }
    
    
    func listaCuentasPoridetificacionSR(identificacion: String, contrasena: String, token: String)->Void{
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let urlString = "\(_CONST.DIRECCION)/listaCuentasMatriculadasSocioSR/\(_CONST.TI_CEDULA)/\(identificacion)/\(contrasena)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let _ = response as? HTTPURLResponse
                {
                do{
                    self.listaCuentasXNoSocio = try JSONDecoder().decode([LisCueXNoSocioSRModel].self , from: data)
                    if  self.listaCuentasXNoSocio.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            self.tableView.reloadData()
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_PP_LISTA_VACIA, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir PP Lista Cuentas Por No Socio:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listaCuentasXNoSocio.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PagoProgramadoCell") as! GenericoTableViewCell
        
        let formattedString = NSMutableAttributedString()
        formattedString
            .bold("ID: ")
            .normal("\(String(self.listaCuentasXNoSocio[indexPath.row].id))")
        
        
        cell.detalle1.attributedText = formattedString
        
        let serFormat = NSMutableAttributedString()
        serFormat
        .bold("Servicio: ")
        .normal("\(self.listaCuentasXNoSocio[indexPath.row].servicio.nombre)")
        cell.detalle2.attributedText = serFormat
        
        let fInicio = NSMutableAttributedString()
        fInicio
        .bold("Fecha Inicio: ")
        .normal("\(self.listaCuentasXNoSocio[indexPath.row].fInicio)")
        cell.detalle3.attributedText = fInicio
        
        let fFin = NSMutableAttributedString()
        fFin
            .bold("Fecha Fin: ")
            .normal("\(self.listaCuentasXNoSocio[indexPath.row].fFinalizacion)")
        cell.detalle4.attributedText = fFin
        
        let cDebito = NSMutableAttributedString()
        cDebito
            .bold("Cuenta Débito: ")
            .normal("\(self.listaCuentasXNoSocio[indexPath.row].cuentaDebito)")
        cell.detalle5.attributedText = cDebito
        
        let dDebito = NSMutableAttributedString()
        dDebito
            .bold("Día de pago: ")
            .normal("\( String(self.listaCuentasXNoSocio[indexPath.row].dia)) ")
        cell.detalle6.attributedText = dDebito
        
        let auxReferencia = NSMutableAttributedString()
        auxReferencia
            .bold("Referencia: ")
            .normal("\( String(self.listaCuentasXNoSocio[indexPath.row].referencia)) ")
        cell.detalle7.attributedText = auxReferencia
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "eliminarPagoProgramado", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as?  EliminarPPViewController {
            destination.pagoProgramado = [listaCuentasXNoSocio[(tableView.indexPathForSelectedRow?.row)!]]
          
        }
        
    }
    
    
}

