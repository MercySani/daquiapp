//
//  BilleteraMovilViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 07/05/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import UIKit
import SwiftyXMLParser

class CellOpciones: UITableViewCell{
    
}

class BillteraMovilViewController: UIViewController, UITextFieldDelegate {
    
    // botones
    @IBOutlet weak var btnRecargar: UIButton!
    @IBOutlet weak var btnCobrar: UIButton!
    @IBOutlet weak var btnPagar: UIButton!
    @IBOutlet weak var btnVerPagosPendientes: UIButton!
    @IBOutlet weak var btnOpciones: UIButton!
    @IBOutlet weak var btnSalir: UIButton!
    @IBOutlet weak var viewBtnCobrar: UIView!
    @IBOutlet weak var viewBtnPagar: UIView!
    
    // contenedores
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    
    @IBOutlet weak var tablaHistoricos: UITableView!
    @IBOutlet weak var lblSaldoCuentaBimo: UILabel!
    @IBOutlet weak var imagenBillteraBimo: UIImageView!
    
    // txt
    @IBOutlet weak var txtMonto: UITextField!
    @IBOutlet weak var lblNumeroCuenta: UILabel!
    
    // variables
    var usuario: UsuarioModel = UsuarioModel()
    let objActivity = activityIndicatorVC()
    var listaHistoricos: [ListaDePagosBimoModel] = []
    var listaPagosPendientes: [PagoBimoModel] = []
    let transparentView = UIView()
    let subMenuOpciones = UITableView()
    var listaOpciones = [""]
    var posicionConsolidada: [PosicionConsolidadaModel] = []
    var posicionConsolidadaAuxiliar: [PosicionConsolidadaModel] = []
    var estadoUsuario = 0
    var selectedButton = UIButton()
    var posicionConsolidadaString: PosicionConsolidadaModel = PosicionConsolidadaModel()
    
    var cuentaBimo = ""
    var saldoCuentaBimo = ""
    var mensajeAlerta: String?
    var monto: Int = 0
    var accion: String?
    var origen: String?
    
    var identificacionAux: String?
    var canalAux: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("pagina principal---------------------------")
        print("self.posicionConsolidadaAuxiliar \(self.posicionConsolidadaAuxiliar)")
        print("self.cuentaBimo     \(self.cuentaBimo)")
        if origen == _CONST.BIMO_ORIGEN_EXTERNO{
            canalAux = _CONST.CANAL_SEG_IOS_BIMO
            usuario.strBilleteraMovil = usuario.strTelefono
            identificacionAux = usuario.strBilleteraMovil
        }else{
            canalAux = _CONST.CANAL_SEG_IOS
            identificacionAux = usuario.strIdentificacion
        }
        
        if(usuario.strNombreUser == _CONST.USUARIO_DEMO){
            self.estadoUsuario = 1
        }else{
            DispatchQueue.main.async {
                       self.objActivity.showActivityIndicator(uiView: self.view)
            }
            self.accionBimo(strTrama: _CONST.VALIDA_CAMPO_RQ, telefonoOrigen: usuario.strBilleteraMovil, identificacion: usuario.strIdentificacion, strFiltro: "", strCriterio:"" ,token: usuario.strToken)
            self.accionBimo(strTrama: _CONST.CONSULTA_TEL_CUENTA_RQ, telefonoOrigen: usuario.strBilleteraMovil, identificacion: usuario.strIdentificacion, strFiltro: _CONST.TELEFONO_BIMO, strCriterio:usuario.strBilleteraMovil, token: usuario.strToken)
            
            cargarDatos()
        }
      
        ponerBordes()
        
        self.tablaHistoricos.register(BimoHistoricoTableViewCell.nib(),forCellReuseIdentifier: BimoHistoricoTableViewCell.identificador)
        self.tablaHistoricos.dataSource = self
        self.tablaHistoricos.delegate = self
        self.tablaHistoricos.tableFooterView = UIView()
        
        menuEstadoUsuario()
        
        self.lblNumeroCuenta.text = cuentaBimo
        self.subMenuOpciones.register(CellOpciones.self ,forCellReuseIdentifier: "opcion")
        
        subMenuOpciones.delegate = self
        subMenuOpciones.dataSource = self
        subMenuOpciones.tableFooterView = UIView()
        
        self.txtMonto.delegate = self
        
    }
    
    func ponerBordes(){
        
              btnRecargar.layer.cornerRadius = _CONST.CORNER_RADIUS
              btnPagar.layer.cornerRadius = _CONST.CORNER_RADIUS
              btnCobrar.layer.cornerRadius = _CONST.CORNER_RADIUS
              btnVerPagosPendientes.layer.cornerRadius = _CONST.CORNER_RADIUS
              btnSalir.layer.cornerRadius = _CONST.CORNER_RADIUS
              viewBtnCobrar.layer.cornerRadius = _CONST.CORNER_RADIUS
              viewBtnPagar.layer.cornerRadius = _CONST.CORNER_RADIUS
              self.view1.layer.cornerRadius = _CONST.CORNER_RADIUS
              self.view1.layer.borderWidth = _CONST.BORDER_WIDTH
              self.view1.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
             
              self.view2.layer.cornerRadius = _CONST.CORNER_RADIUS
              self.view2.layer.borderWidth = _CONST.BORDER_WIDTH
              self.view2.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
              
              self.view3.layer.cornerRadius = _CONST.CORNER_RADIUS
              self.view3.layer.borderWidth = _CONST.BORDER_WIDTH
              self.view3.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
    }
    
    func menuEstadoUsuario(){
      
        if self.estadoUsuario == 1{
            listaOpciones = ["Bloquear", "Desafiliar", "Mi QR"]
            self.imagenBillteraBimo.isHidden = true
            self.view1.isHidden = false
            self.view2.isHidden = false
            self.view3.isHidden = false
            self.btnVerPagosPendientes.isHidden = false
        }else {
            listaOpciones = ["Desbloquear", "Mi QR"]
            self.view1.isHidden = false
            self.view2.isHidden = true
            self.view3.isHidden = true
            self.btnVerPagosPendientes.isHidden = true
            self.imagenBillteraBimo.isHidden = false
        }
         if origen == _CONST.BIMO_ORIGEN_EXTERNO{
            listaOpciones.append("Mis Datos")
            self.btnSalir.isHidden = false
         }else{
            self.btnSalir.isHidden = true
         }
         self.tablaHistoricos.reloadData()
         self.subMenuOpciones.reloadData()
    }
    
    func cargarDatos(){
        self.posicionConsolidada(identificacion: self.identificacionAux!, tipoIdentificacion: _CONST.TI_CEDULA, usuario: self.identificacionAux!, token: usuario.strToken)
                      
          self.accionBimo(strTrama: _CONST.LISTA_HISTORICOS, telefonoOrigen: usuario.strBilleteraMovil, identificacion: self.identificacionAux!, strFiltro: _CONST.TELEFONO_BIMO, strCriterio:usuario.strBilleteraMovil, token: usuario.strToken)
           buscarSaldo()
         
           self.lblNumeroCuenta.reloadInputViews()
           self.lblSaldoCuentaBimo.reloadInputViews()
           self.tablaHistoricos.reloadData()
         }
    
    func buscarSaldo(){
        print("buscar saldo----")
        print("self.cuentaBimo     \(self.cuentaBimo)")
         for item in self.posicionConsolidadaAuxiliar{
             if item.Cuenta == self.cuentaBimo{
                self.lblSaldoCuentaBimo.text = "$" + item.Saldoefectivo
            }
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           txtMonto.resignFirstResponder()
           self.view.endEditing(true)
    }
    
    @IBAction func accionRecargar(_ sender: Any) {
        menuEstadoUsuario()
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        cargarDatos()
    }
    
    @IBAction func accionCobrar(_ sender: Any) {
        guard !(txtMonto.text!.isEmpty) else {
        DispatchQueue.main.async {
           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_MONTO , view: self)
         }
        return
        }
        accion = "Cobrar"
       performSegue(withIdentifier:"segueAccion", sender: self)
    }
    
    @IBAction func accionPagar(_ sender: Any) {
        guard !(txtMonto.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_MONTO , view: self)
            }
            return
        }
        accion = "Pagar"
        performSegue(withIdentifier:"segueAccion", sender: self)
    }
    
    
    @IBAction func accionVerPagos(_ sender: Any) {
      performSegue(withIdentifier:"seguePagosPendientes", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "seguePagosPendientes" {
                   if let destino = segue.destination as? MisPagosPendientesViewController{
                    destino.usuario = usuario
                    destino.listaPagos = self.listaPagosPendientes
                    destino.origen = self.origen
                   }
        }
        if segue.identifier == "segueAccion" {
            if let destino = segue.destination as? TransaccionBimoViewController{
                destino.usuario = usuario
                destino.lblTitulo = accion
                destino.lblMonto = txtMonto.text
                destino.strCriterio = "DIRECTO"
                destino.origen = self.origen
            }
        }
        
        if segue.identifier == "segueModalAdmin" {
            if let destino = segue.destination as? AlertBimoViewController{
                 destino.usuario = usuario
                 destino.strTrama = _CONST.BLOCK_USER_RQ
                 destino.origen = self.origen
                if mensajeAlerta == "Bloquear" {
                    destino.variableMensaje = "¿Desea bloquear su cuenta bimo? "
                    destino.strAccion = "BLOQUEO"
                }
                if mensajeAlerta == "Desbloquear" {
                    destino.variableMensaje = "¿Desea desbloquear su cuenta bimo? "
                    destino.strAccion = "DESBLOQUEO"
                }
                if mensajeAlerta == "Desafiliar" {
                    destino.variableMensaje = "¿Desea desafiliar su cuenta bimo? "
                    destino.strAccion = "DESENROLAMIENTO"
                   
                }
                
            }
        }
        
        if segue.identifier == "segueCodigoQR"{
            if let destino = segue.destination as? CodigoQRBimoViewController{
                destino.usuario = usuario
            }
        }
        
        if segue.identifier == "segueMisDatos"{
            if let destino = segue.destination as? MisDatosBimoViewController{
                destino.usuario = self.usuario
                destino.cuenta = self.cuentaBimo
            }
        }
    }
    
    func agregarTransparencia(frames: CGRect){
        print("self.listaOpciones.count -->> \(self.listaOpciones.count)")
        print("self.subMenuOpciones.rowHeight-->> \(self.subMenuOpciones.rowHeight)")
        self.subMenuOpciones.rowHeight = 48.00
        print("self.subMenuOpciones.rowHeight-->> \(self.subMenuOpciones.rowHeight)")
        let ventana = UIApplication.shared.keyWindow
        transparentView.frame = ventana?.frame ?? self.view.frame
        self.view.addSubview(transparentView)
        subMenuOpciones.frame = CGRect(x: frames.origin.x , y: frames.origin.y + frames.height, width: frames.width, height: 0)
        self.view.addSubview(subMenuOpciones)
        subMenuOpciones.layer.cornerRadius = 5
        transparentView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        
        let tapGesto = UITapGestureRecognizer(target: self, action: #selector(removeTransparentView))
        transparentView.addGestureRecognizer(tapGesto)
        transparentView.alpha = 0
        UIView.animate(withDuration: 0.4, delay: 0.0,usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0.5
            self.subMenuOpciones.frame = CGRect(x: frames.origin.x , y: frames.origin.y + frames.height, width: frames.width, height: CGFloat(self.subMenuOpciones.rowHeight*CGFloat(self.listaOpciones.count+1)))
        }, completion: nil)
    }
    
    @objc func removeTransparentView(){
        let frames = selectedButton.frame
        UIView.animate(withDuration: 0.4, delay: 0.0,usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transparentView.alpha = 0
            self.subMenuOpciones.frame = CGRect(x: frames.origin.x , y: frames.origin.y + frames.height, width: frames.width, height: 0)
        }, completion: nil)
    }
    
    
    @IBAction func abrirOpciones(_ sender: Any) {
        agregarTransparencia(frames: btnOpciones.frame)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    @IBAction func unwindBilleteraMovil(_ sender: UIStoryboardSegue) {
       self.txtMonto.text = ""
        if sender.source is AlertBimoViewController{
            if let senderVC = sender.source as? AlertBimoViewController{
                estadoUsuario = senderVC.estadoUsuario
                if estadoUsuario == 1 {
                    cargarDatos()
                }
            }
        }
        viewDidLoad()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
         var retorno: Bool = false
         if textField.tag == 0{
             if(Utilidades.validarNumero(textField: textField, range: range,string: string)){
                 guard let text = textField.text else { return true }
                 let count = text.count + string.count - range.length
                 retorno = count <= 5
             }
         }
         
         return retorno
        
        /*if let digito = Int(string){
            monto = monto * 10 + digito
            txtMonto.text = actualizarMonto()
            
        }
        
        return false*/
     }
    
    func actualizarMonto() -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currency
        let amount  = Double(monto/100) + Double(monto%100)/100
        return formatter.string(from: NSNumber(value: amount))
    }
    
    @IBAction func accionSalir(_ sender: Any) {
        print("accionSalir------")
        self.performSegue(withIdentifier: "unwindSeguePortal", sender: view)
    }
    
    
}

extension BillteraMovilViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRow = 1
        switch tableView {
        case subMenuOpciones:
            numberOfRow = listaOpciones.count
        case tablaHistoricos:
            numberOfRow = listaHistoricos.count
        default: print("hay error en numberOfRowsInSection")
        }
        return numberOfRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        switch tableView {
        case subMenuOpciones:
            cell = tableView.dequeueReusableCell(withIdentifier: "opcion", for: indexPath)
            cell.textLabel?.text = listaOpciones[indexPath.row]
        case tablaHistoricos:
           let celda = tableView.dequeueReusableCell(withIdentifier: BimoHistoricoTableViewCell.identificador, for: indexPath) as! BimoHistoricoTableViewCell
            celda.configure(witch: listaHistoricos[indexPath.row])
            cell = celda
        default: print("hay error en numberOfRowsInSection")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        selectedButton.setTitle(listaOpciones[indexPath.row], for: .normal)
        mensajeAlerta = listaOpciones[indexPath.row]
        if mensajeAlerta == "Mi QR"{
            performSegue(withIdentifier:"segueCodigoQR", sender: self)
        }else if mensajeAlerta == "Mis Datos"{
            performSegue(withIdentifier:"segueMisDatos", sender: self)
        }else{
            performSegue(withIdentifier:"segueModalAdmin", sender: self)
        }
        
        removeTransparentView()
    }
    
    func accionBimo(strTrama: String, telefonoOrigen: String, identificacion: String,strFiltro: String, strCriterio: String , token: String)-> Void{
        let session = URLSession(
                   configuration: URLSessionConfiguration.ephemeral,
                   delegate: NSURLSessionPinningDelegate(),
                   delegateQueue: nil)
        
        let json: [String: Any] = [ "strIdentificacion": identificacion,
                                    "strToken": token,
                                    "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                    "strCanalSeguridad": canalAux,
                                    "strTrama": strTrama,
                                    "strTelefonoOrigen": telefonoOrigen,
                                    "strTelefonoDestino": "",
                                    "strDescripcion": "",
                                    "strMonto": "",
                                    "strAccion": "",
                                    "strFiltro": strFiltro,
                                    "strCriterio": strCriterio,
                                    "strNumeroCuenta": "",
                                    "strEndToEnd": ""
                                    
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/bimo/general")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { data, response, error in
        if let data = data, let response = response as? HTTPURLResponse, response.statusCode==202{
            do{
                let mensajeRespuestaData = try JSONDecoder().decode(BimoResultado.self , from: data)
                if  mensajeRespuestaData.strCodigo == _CONST.CODIGO_OK{
                                     DispatchQueue.main.sync {
                                         let xml = try! XML.parse(mensajeRespuestaData.strXML)
                                         
                                         if let codResultado = xml["BimoXml", "codResultado"].text {
                                             if strTrama == _CONST.VALIDA_CAMPO_RQ {
                                                DispatchQueue.main.async {
                                                     self.objActivity.hideActivityIndicator(uiView: self.view)
                                                     if codResultado == _CONST.CODIGO_OK{
                                                        self.estadoUsuario = 1
                                                         self.cargarDatos()
                                                     }
                                                     if codResultado == _CONST.BIMO_500{
                                                         self.posicionConsolidada(identificacion: identificacion, tipoIdentificacion: _CONST.TI_CEDULA, usuario: identificacion, token: token)
                                                        self.estadoUsuario = 2
                                                     }
                                                     if codResultado == _CONST.BIMO_501{
                                                         self.estadoUsuario = 3
                                                         self.cargarDatos()
                                                     }
                                                }
                                                 
                                             }
                                            if strTrama == _CONST.CONSULTA_TEL_CUENTA_RQ {
                                                DispatchQueue.main.async {
                                                     self.objActivity.hideActivityIndicator(uiView: self.view)
                                                     let cadenaCuenta: [String] = xml["BimoXml", "codResultadoAdicional"].text!.components(separatedBy: "|")
                                                     self.cuentaBimo  = cadenaCuenta[3]
                                                    self.lblNumeroCuenta.text = self.cuentaBimo
                                                    self.tablaHistoricos.reloadData()
                                                }
                                                
                                             }
                                             if strTrama == _CONST.LISTA_HISTORICOS {
                                                 do {
                                                     if codResultado == _CONST.CODIGO_OK{
                                                          let jsonData = Data(xml["BimoXml", "mensajeResultado"].text!.utf8)
                                                         self.listaHistoricos =  try JSONDecoder().decode([ListaDePagosBimoModel].self , from: jsonData)
                                                       
                                                        DispatchQueue.main.async {
                                                             self.objActivity.hideActivityIndicator(uiView: self.view)
                                                             self.tablaHistoricos.reloadData()
                                                        }
                                                     }else{
                                                         let mensajeError = xml["BimoXml", "mensajeResultado"].text
                                                         DispatchQueue.main.async {
                                                             self.objActivity.hideActivityIndicator(uiView: self.view)
                                                             Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeError! , view: self)
                                                         }
                                                     }
                                                     
                                                 } catch {
                                                     print(error.localizedDescription)
                                                 }
                                                 
                                             }
                                         }
                                     }
                }else{
                    if mensajeRespuestaData.strCodigo == _CONST.CODIGO_ERROR_TOKEN{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            if self.origen == _CONST.BIMO_ORIGEN_EXTERNO{
                                Utilidades.mensajeSesionBimo(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strCodigo , view: self)
                            }else{
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strCodigo , view: self)
                            }
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensaje , view: self)
                        }
                    }
                }
            }catch let error as NSError{
                print("Error al leer JSON: \(error)")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }else if let error = error{
            print("Error al consumir cerrar sesion:error: \(error)")
            if let error = error as NSError?, error.domain == NSURLErrorDomain{
                if error.code == -1001 {
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                    }
                } else if error.code == -1009 {
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }else{
            //No se obtubo ningun dato
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
        }
        }.resume()
    
    }
    
   //para Buscar la cuenta
   func posicionConsolidada( identificacion: String, tipoIdentificacion: String, usuario: String, token: String){
       
       print("posicion consolidadaa-----")
       let session = URLSession(
           configuration: URLSessionConfiguration.ephemeral,
           delegate: NSURLSessionPinningDelegate(),
           delegateQueue: nil)
       
    let urlString = "\(_CONST.DIRECCION)/posicionConsolidada/\(_CONST.CANAL_DES)/\(self.identificacionAux!)/\(tipoIdentificacion)/\(usuario)/\(token)/\(self.canalAux!)"
       guard let url = URL(string: urlString) else {
           
           DispatchQueue.main.async {
               self.objActivity.hideActivityIndicator(uiView: self.view)
               Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
           }
           return}
           session.dataTask(with: url){(data, response, error) in
           let response = response as? HTTPURLResponse

           if let data = data,
               response?.statusCode==200{
               do{
                   self.posicionConsolidada = try JSONDecoder().decode([PosicionConsolidadaModel].self , from: data)
                   
                   if  self.posicionConsolidada.count != _CONST.LISTA_VACIA{
                       
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           
                           for item in self.posicionConsolidada{
                               if(item.Producto == _CONST.CNTA_DEBITO_VISTA  || item.Producto == _CONST.CNTA_DEBITO_KULLKIMIRAK || item.Producto == _CONST.CNTA_DEBITO_BASICA){
                                   
                                   self.posicionConsolidadaString.Cuenta = item.Cuenta
                                   self.posicionConsolidadaString.Descripcion = item.Descripcion
                                   self.posicionConsolidadaString.GProducto = item.GProducto
                                   self.posicionConsolidadaString.Producto = item.Producto
                                   self.posicionConsolidadaString.Saldoaportacion = item.Saldoaportacion
                                   self.posicionConsolidadaString.Saldobloqueado = item.Saldobloqueado
                                   self.posicionConsolidadaString.Saldocontable = item.Saldocontable
                                   self.posicionConsolidadaString.Saldoefectivo = item.Saldoefectivo
                                   self.posicionConsolidadaAuxiliar.append(self.posicionConsolidadaString)
                               }
                           }
                           
                       }
                   }else{
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                       }
                   }
                   
               }catch{
                   do {
                       let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                       
                       if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                           DispatchQueue.main.async {
                               self.objActivity.hideActivityIndicator(uiView: self.view)
                                 if self.origen == _CONST.BIMO_ORIGEN_EXTERNO{
                                     Utilidades.mensajeSesionBimo(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strCodigoError , view: self)
                                 }else{
                                     Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strCodigoError , view: self)
                                 }
                           }
                       }else{
                           DispatchQueue.main.async {
                               self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                           }
                       }
                   }catch let error as NSError{
                   print("Error al leer JSON: \(error)")
                   DispatchQueue.main.async {
                       self.objActivity.hideActivityIndicator(uiView: self.view)
                       Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                   }
               }
               }
           }else if let error = error{
               print("Error al consumir Posicion Consolidada:error: \(error)")
               if let error = error as NSError?, error.domain == NSURLErrorDomain{
                   if error.code == -1001 {
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                       }
                   } else if error.code == -1009 {
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                       }
                   }else{
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                       }
                   }
               }else{
                   DispatchQueue.main.async {
                       self.objActivity.hideActivityIndicator(uiView: self.view)
                       Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                   }
               }
           }else{
               //No se obtubo ningun dato
               DispatchQueue.main.async {
                   self.objActivity.hideActivityIndicator(uiView: self.view)
                   Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
               }
           }
           }.resume()
   }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if self.isMovingFromParentViewController {
            print("atrasss")
        }
    }
}

extension BillteraMovilViewController: UITableViewDelegate{
    
}

