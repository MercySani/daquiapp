//
//  AutorizacionHuellaViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 03/01/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import UIKit
class AutorizacionHuellaViewController: UIViewController {
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let objActivity = activityIndicatorVC()
    var usuario: UsuarioModel = UsuarioModel()
    
    @IBOutlet weak var swtAutorizacion: UISwitch!
    
    @IBOutlet weak var btnAceptarAutorizacion: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor =
               UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
    }
    
    
    @IBAction func aceptarAutorizacion(_ sender: Any) {
        if(swtAutorizacion.isOn){
            let macAddress = UIDevice.current.identifierForVendor?.uuidString
           biometriaLocalMerge(iMei: macAddress?.description ?? "")
        }else{
            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_ACEPTAR_CONDICIONES , view: self)
        }
       
    }
    
    
    @IBAction func cancelarAutorizacion(_ sender: Any) {
         self.performSegue(withIdentifier: "unwindInicioSesion", sender: self)
    }
    
    func biometriaLocalMerge(iMei: String)-> Void{
         let strAction="M"
         let session = URLSession(
         configuration: URLSessionConfiguration.ephemeral,
         delegate: NSURLSessionPinningDelegate(),
         delegateQueue: nil)
         let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
         let version: String = UIDevice.current.model+"_"+UIDevice.current.systemName+"_" + UIDevice.current.systemVersion+"|"+appVersion
         
         let json: [String: Any] = ["strAccion": strAction,
                                     "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                     "strCanalDescripcionAdicional": _CONST.CANAL_DES ,
                                     "strDetalleDispositivo": version ,
                                     "strOrigenIdentificador": iMei,
                                     "strToken":usuario.strToken,
                                     "strNombreUser": usuario.strNombreUser,
                                     ]
     
         
         let jsonData = try? JSONSerialization.data(withJSONObject: json)
         let url = URL(string: "\(_CONST.DIRECCION)/biometrialocal/general")!
         var request = URLRequest(url: url)
         request.httpMethod = "POST"
         request.httpBody = jsonData
         
     
         session.dataTask(with: request) { data, response, error in
             if let data = data, let _ = response as? HTTPURLResponse
             {
                 do{
                     let mensajeRespuestaData = try JSONDecoder().decode(ResultadoRestful.self , from: data)
                     DispatchQueue.main.async {
                         self.objActivity.hideActivityIndicator(uiView: self.view)
                     }
                     if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK{
                          DispatchQueue.main.async {
                            
                            self.performSegue(withIdentifier: "unwindInicioSesion", sender: self)
                        }
                     }else{
                         DispatchQueue.main.async {
                             self.objActivity.hideActivityIndicator(uiView: self.view)
                             Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                         }
                     }
                 }catch let error as NSError{
                     print("Error al leer JSON: \(error)")
                     DispatchQueue.main.async {
                         self.objActivity.hideActivityIndicator(uiView: self.view)
                         Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                     }
                 }
             }else if let error = error{
                 print("Error al consumir BiometriaLocal consulta :error: \(error)")
                 if let error = error as NSError?, error.domain == NSURLErrorDomain{
                     if error.code == -1001 {
                         DispatchQueue.main.async {
                             self.objActivity.hideActivityIndicator(uiView: self.view)
                             Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                         }
                     } else if error.code == -1009 {
                         DispatchQueue.main.async {
                             self.objActivity.hideActivityIndicator(uiView: self.view)
                             Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                         }
                     }else{
                         DispatchQueue.main.async {
                             self.objActivity.hideActivityIndicator(uiView: self.view)
                             Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                         }
                     }
                 }else{
                     DispatchQueue.main.async {
                         self.objActivity.hideActivityIndicator(uiView: self.view)
                         Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                     }
                 }
             }else{
                 //No se obtubo ningun dato
                 print("no se obtuno nigun dato")
                 DispatchQueue.main.async {
                     self.objActivity.hideActivityIndicator(uiView: self.view)
                     Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                 }
             }
         }.resume()
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
}
