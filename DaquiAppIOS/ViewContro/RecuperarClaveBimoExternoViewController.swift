//
//  RecuperarClaveExternoViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 7/9/18.
//  Copyright © 2018 CoopDaquilema. All rights reserved.
//

import Foundation
import UIKit
class RecuperarClaveBimoExternoViewController: UIViewController, UITextFieldDelegate {
    
    //variables de indicador de actividades
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let objActivity = activityIndicatorVC()
     var usuarioRespuesta = ResultadoRestful()
    @IBOutlet weak var btnCancelar: UIButton!
    @IBOutlet weak var btnContinuar: UIButton!
    
    @IBOutlet weak var identificacionField: UITextField!
     @IBOutlet weak var celularField: UITextField!
    var usuario: UsuarioModel = UsuarioModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.endEditing(true)
        identificacionField.delegate = self
        celularField.delegate = self
        btnContinuar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnCancelar.layer.cornerRadius = _CONST.CORNER_RADIUS
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var tipoCaracter: CharacterSet = CharacterSet()
        var characterSet: CharacterSet = CharacterSet()
        var retorno: Bool = false
        if textField.tag == 1{
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 10
            }
        }
        if textField.tag == 2{
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 10
            }
        }
        
        return retorno
    }
    //Para ocultar el teclado al hacer touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func cancelar(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func continuar(_ sender: Any) {
       guard !(identificacionField.text!.isEmpty) else {
         DispatchQueue.main.async {
            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_C_VACIOS , view: self)
          }
         return
         }
        
         guard (identificacionField.text!.count == 10) else {
         DispatchQueue.main.async {
            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_BIMO_ERROR_IDENTIFICACION_LONGITUD , view: self)
          }
         return
         }
        
        guard !(celularField.text!.isEmpty) else {
         DispatchQueue.main.async {
            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_C_VACIOS , view: self)
          }
         return
         }
        
         guard (celularField.text!.count == 10) else {
         DispatchQueue.main.async {
            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_BIMO_ERROR_CELULAR_LONGITUD , view: self)
          }
         return
         }
       DispatchQueue.main.async { self.objActivity.showActivityIndicator(uiView: self.view)
                }
       recuperarClaveBimoExterno(identificacion: self.identificacionField.text!,celular: self.celularField.text!)
            
    }
    func recuperarClaveBimoExterno(identificacion: String,celular: String)-> Void{
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        let version: String = UIDevice.current.model+"_"+UIDevice.current.systemName+"_" + UIDevice.current.systemVersion+"|"+appVersion
        let strAction="QRC"
        let json: [String: Any] = ["strAccion": strAction,
                                "strNombreUser": celular,
                                "strPassword": "" ,
                                "strCanalSeguridad": _CONST.CANAL_SEG_IOS_BIMO,
                                "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                "strOrigenIdentificador": macAddress?.description ?? "",
                                "strDetalleDispositivo": version,
                                "strToken":"",
                                "strSistema": _CONST.SISTEMA_BIMO_EXTERNO,
                                "strIdentificacion": identificacion,
                                "strObservacion": _CONST.CANAL_DES]
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/bimo/generalexterno")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
    
        session.dataTask(with: request) { data, response, error in
            if let data = data, let _ = response as? HTTPURLResponse {
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(ResultadoRestful.self , from: data)
                    print(mensajeRespuestaData.strCodigoError)
                    self.usuarioRespuesta = mensajeRespuestaData
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_OK{
                           print(mensajeRespuestaData.strCodigoError)

                        
                        DispatchQueue.main.async { self.objActivity.hideActivityIndicator(uiView: self.view)
                            let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
                            otpViewController.popUpOTPDelegate = self
                            self.present(otpViewController, animated: true)
                        }
                        
                    }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir buscarUsuario:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
    }
    
  func verificarOTP(celular: String , codigoOtp: String, token: String)-> Void{
       
       let macAddress = UIDevice.current.identifierForVendor?.uuidString
       
       let session = URLSession(
           configuration: URLSessionConfiguration.ephemeral,
           delegate: NSURLSessionPinningDelegate(),
           delegateQueue: nil)
       
       let strAction="OTP"
          let json: [String: Any] = ["strAccion": strAction,
                                  "strNombreUser": celular,
                                  "strPassword": "" ,
                                  "strCanalSeguridad": _CONST.CANAL_SEG_IOS_BIMO,
                                  "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                  "strOrigenIdentificador": macAddress?.description ?? "",
                                  "strDetalleDispositivo": "",
                                  "strToken":"",
                                  "strSistema": _CONST.SISTEMA_BIMO_EXTERNO,
                                  "strIdentificacion": "",
                                  "strObservacion": _CONST.CANAL_DES,
                                  "strOTP": codigoOtp]
    
      
       let jsonData = try? JSONSerialization.data(withJSONObject: json)
       let url = URL(string: "\(_CONST.DIRECCION)/bimo/generalexterno")!
       var request = URLRequest(url: url)
       request.httpMethod = "POST"
       request.httpBody = jsonData
       session.dataTask(with: request) { data, response, error in
           if let data = data{
               do{
                   let mensajeRespuestaData = try JSONDecoder().decode(ResultadoRestful.self , from: data)
                   
                   if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_OK{
                    self.usuarioRespuesta.strToken = mensajeRespuestaData.strToken
                           DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                               let definirClaveBimoExternoViewController = self.storyboard?.instantiateViewController(withIdentifier: "DefinirClaveBimoExternoViewController") as!  DefinirClaveBimoExternoViewController
                               definirClaveBimoExternoViewController.usuario = self.usuarioRespuesta
                              definirClaveBimoExternoViewController.recuperar = "true";
                            
                               self.present(definirClaveBimoExternoViewController, animated: true)
                               }
                   }else{
                       if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                           
                           DispatchQueue.main.async {
                               self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                           }
                       }else if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_USERNAME{
                           
                           DispatchQueue.main.async {
                               self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                           }
                       }else{
                           DispatchQueue.main.async {
                               self.objActivity.hideActivityIndicator(uiView: self.view)
                              Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                           }
                       }
                   }
               }catch let error as NSError{
                   print("Error al leer JSON: \(error)")
                   DispatchQueue.main.async {
                       self.objActivity.hideActivityIndicator(uiView: self.view)
                       Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                   }
               }
           }else if let error = error{
               print("Error al consumir verificar OTP:error: \(error)")
               if let error = error as NSError?, error.domain == NSURLErrorDomain{
                   if error.code == -1001 {
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                       }
                   } else if error.code == -1009 {
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                       }
                   }else{
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                       }
                   }
               }else{
                   DispatchQueue.main.async {
                       self.objActivity.hideActivityIndicator(uiView: self.view)
                       Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                   }
               }
           }else{
               print("Error al consumir verificarOTP: No se obtubo ningun dato")
               DispatchQueue.main.async {
                   self.objActivity.hideActivityIndicator(uiView: self.view)
                   Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
               }
           }
           }.resume()
       
   }
    
}
extension RecuperarClaveBimoExternoViewController: OTPDelegate{
    func inserOTP(value: String) {
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        verificarOTP(celular: self.celularField.text!, codigoOtp: value, token: usuarioRespuesta.strToken)
    }
}

