//
//  DefinirClaveBimoExternoViewController.swift
//  DaquiAppIOS
//
//  Created by MacBookPro on 6/1/20.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import Foundation
import UIKit

class DefinirClaveBimoExternoViewController: UIViewController, UITextFieldDelegate{
    
   var usuario: ResultadoRestful = ResultadoRestful()
    var recuperar : String = ""
     var strAction = ""
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let objActivity = activityIndicatorVC()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.endEditing(true)
        cancelar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnModificar.layer.cornerRadius = _CONST.CORNER_RADIUS
        titulo.text="Definir Clave Bimo"
        mensaje.text="Por su seguridad defina una contraseña de 6 dígitos"
    }

    @IBOutlet weak var btnModificar: UIButton!
    
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var mensaje: UILabel!
    @IBOutlet weak var cancelar: UIButton!
    @IBOutlet weak var claveRepetir: UITextField!
    @IBOutlet weak var claveNueva: UITextField!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func definirClave(_ sender: Any) {
       
        var datosCorrectos:boolean_t
        datosCorrectos=1
       
        
       if   !self.claveRepetir.text!.isEmpty  && !self.claveNueva.text!.isEmpty  && datosCorrectos==1 {
        if self.claveNueva.text != self.claveRepetir.text{
            Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_CLAVES, view: self)
        }else{
            if (!(Utilidades.tieneNumeros(string: self.claveNueva.text!))){
                
                Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_NUM, view: self)
                
            }else{
                if(Utilidades.tieneCaracteresEspeciales(string: self.claveNueva.text!)){
                    Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_CARACTERES_ESPECIALES, view: self)
                }else{
                if ((self.claveNueva.text?.count)! >= 0 && (self.claveNueva.text?.count)!<=6){
                    DispatchQueue.main.async {
                        self.objActivity.showActivityIndicator(uiView: self.view)
                        }
                    
                        definirClaveBimo(identificacion: usuario.strIdentificacion, token: usuario.strToken, nuevaClave: self.claveNueva.text!)
                   
                }else{
                    Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_CARACTERES_MINIMOS_BIMO, view: self)
                }
            }
            }
        }
        }else{
            Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.ERR_C_VACIOS, view: self)
        }
    }
    
    @IBAction func cancelar(_ sender: Any) {
       self.dismiss(animated: true, completion: nil)
    }
    
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      var tipoCaracter: CharacterSet = CharacterSet()
      var characterSet: CharacterSet = CharacterSet()
      var retorno: Bool = false
      
         tipoCaracter = CharacterSet.decimalDigits
          characterSet = CharacterSet(charactersIn: string)
          retorno = tipoCaracter.isSuperset(of: characterSet)
          if(retorno){
              guard let text = textField.text else { return true }
              let count = text.count + string.count - range.length
              retorno = count <= 6
          }
     
      return retorno
  }
    
    //Para ocultar el teclado al hacer touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func definirClaveBimo(identificacion: String, token: String, nuevaClave: String)-> Void{
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        let version: String = UIDevice.current.model+"_"+UIDevice.current.systemName+"_" + UIDevice.current.systemVersion+"|"+appVersion
        var firma: String = ""
        var jwt:String = ""
        for _ in 1...7 {
            firma = firma + usuario.strTelefono
        }
        jwt = JsonWebToken.crearJWT(contenido: nuevaClave, firma: firma)
       
        if recuperar == "true" { strAction = "MRC"}else{ strAction = "MC"}
        
        let json: [String: Any] = ["strAccion": strAction,
                                   "strNombreUser": self.usuario.strTelefono,
                                "strPassword": jwt,
                                "strCanalSeguridad": _CONST.CANAL_SEG_IOS_BIMO,
                                "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                "strOrigenIdentificador": macAddress?.description ?? "",
                                "strDetalleDispositivo": version,
                                "strToken":token,
                                "strSistema": _CONST.SISTEMA_BIMO_EXTERNO,
                                "strIdentificacion": identificacion,
                                "strObservacion": ""]
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/bimo/generalexterno")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { data, response, error in
            
            if let data = data, let _ = response as? HTTPURLResponse
            {
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(ResultadoRestful.self , from: data)
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_OK{
                           DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                             Utilidades.mensajeDefinirClaveBimoExterno(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                    
                }
            }else if let error = error{
                print("Error al consumir definirClaveBimo:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }

 
}
