//
//  IfisViewController.swift
//  DaquiApp
//
//  Created by Fredy on 25/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class IfisViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating  {

    var xmlDataSource = [IfisModel]()
    var ifisRes = [IfisModel]()
    var ifiData = IfisData()
    var popUpDelegate: PopUpDelegate?

    
    @IBOutlet weak var lstIFisTbl: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        xmlDataSource = ifiData.inicializar()
        
       self.lstIFisTbl.delegate = self
        self.lstIFisTbl.dataSource = self
        
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Filtre entidad financiera"
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        lstIFisTbl.tableHeaderView = searchController.searchBar
        
        ifisRes = xmlDataSource
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ifisRes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "country")
        
        if cell == nil {
            
            cell = UITableViewCell(style: .default, reuseIdentifier: "country")
        }
        cell?.textLabel?.text = ifisRes[indexPath.row].descripcion
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        popUpDelegate?.seleccionString(value: ifisRes[indexPath.row].descripcion, tipoSelection: ifisRes[indexPath.row].cuenta, tipoCuenta: ifisRes[indexPath.row].idifi, saldoDisponible: "")
        dismiss(animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true;
    }
    
    
    
    func updateSearchResults(for searchController: UISearchController) {
       
            if searchController.searchBar.text! == "" {
                
                
            } else {
                ifisRes = xmlDataSource.filter {$0.descripcion.lowercased().contains(searchController.searchBar.text!.lowercased() )
                }
            }
        
        self.lstIFisTbl.reloadData()
        
    }
}


