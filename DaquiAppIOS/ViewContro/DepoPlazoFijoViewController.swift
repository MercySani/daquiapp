//
//  DPFViewController.swift
//  DaquiApp
//
//  Created by Fredy on 30/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class DepoPlazoFijoViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var monto: UITextField!
    @IBOutlet weak var textBoxPlazosDias: UITextField!
    @IBOutlet weak var segFrecuencia: UISegmentedControl!
 
    @IBOutlet weak var interesAnual: UILabel!
    @IBOutlet weak var interesDiarios: UILabel!
    @IBOutlet weak var interesMensual: UILabel!
    @IBOutlet weak var interesTotal: UILabel!
    @IBOutlet weak var impuesto2: UILabel!
    @IBOutlet weak var totalRecibir: UILabel!
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(UIView.endEditing(_:)))
    
    //calculos
    var interes_dpf: Double = 0.0
    var retencion_dpf: Double = 0.02
    var isVencimiento: Bool = false
    // fin variables
    
    
    @IBOutlet weak var btnBorrar: UIButton!
    @IBOutlet weak var btnCalcular: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        monto.delegate = self
        textBoxPlazosDias.delegate = self
        btnCalcular.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnBorrar.layer.cornerRadius = _CONST.CORNER_RADIUS
        
    }
    

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var retorno: Bool = false
        if textField.tag == 1{
            if(Utilidades.validarNumero(textField: textField, range: range,string: string)){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 9
            }
        }
        
        return retorno
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        monto.resignFirstResponder()
        textBoxPlazosDias.resignFirstResponder()
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func limpiar(_ sender: Any) {
        monto.text = ""
        textBoxPlazosDias.text = ""
    }
    @IBAction func selecPlazos(_ sender: Any) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
                let pickerPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickerPopUpViewController") as! PickerPopUpViewController
        pickerPopupViewController.popUpDelegate = self
        pickerPopupViewController.listaOpciones = _CONST.LISTA_PLAZO_DIAS
        self.present(pickerPopupViewController, animated: true)
    }
    
    @IBAction func calcularDPF(_ sender: UIButton) {
        
        
        if monto.text!.isEmpty {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_MONTO, view: self)
            }
        
        }else {
                if textBoxPlazosDias.text!.isEmpty{
                    DispatchQueue.main.async {
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_PLAZO, view: self)
                    }
                }else{
                    let montoDpf = Double(monto.text!)
                    if (montoDpf?.isLess(than: 100.0))! != true  {
                        let tiempo_dpf = self.frecunciaPago(frecuencia: segFrecuencia.selectedSegmentIndex)
                        let plazoDpf = self.plazoDias(plazoDias: textBoxPlazosDias.text!)
                        let interesDpf = self.calculaDpf(montoDpf: Double(montoDpf!), plazoDpf: plazoDpf)
                        
                        let  interesAnual = interes_dpf*100
                        let auxInteresAnual = NSMutableAttributedString()
                            auxInteresAnual
                                .bold(" INTERES ANUAL:  %")
                                .normal(String(format: "%.2f", interesAnual))
                        self.interesAnual.layer.borderWidth = _CONST.BORDER_WIDTH
                        self.interesAnual.layer.cornerRadius = _CONST.CORNER_RADIUS
                        self.interesAnual.attributedText = auxInteresAnual
                        
                        let interesesDiarios = (montoDpf! * interes_dpf) / Double(tiempo_dpf)
                        let auxInteresDiario = NSMutableAttributedString()
                            auxInteresDiario
                                .bold(" INTERESES DIARIOS:  $")
                                .normal(String(format: "%.2f", interesesDiarios))
                        self.interesDiarios.layer.borderWidth = _CONST.BORDER_WIDTH
                        self.interesDiarios.layer.cornerRadius = _CONST.CORNER_RADIUS
                        self.interesDiarios.attributedText = auxInteresDiario
                        
                        if (!isVencimiento){
                            let interesMensu = (montoDpf!*interesDpf*30)/Double(tiempo_dpf)
                            let auxInteresMensual = NSMutableAttributedString()
                            auxInteresMensual
                                .bold(" INTERESES MENSUALES:  $")
                                .normal(String(format: "%.2f", interesMensu))
                            self.interesMensual.layer.borderWidth = _CONST.BORDER_WIDTH
                            self.interesMensual.layer.cornerRadius = _CONST.CORNER_RADIUS
                            self.interesMensual.attributedText = auxInteresMensual
                        }else{
                                self.interesMensual.text = ""
                        }
                        
                        
                        let interesTot = (montoDpf! * interes_dpf * Double(plazoDpf) ) / Double(tiempo_dpf)
                        
                        let auxInteresTotal = NSMutableAttributedString()
                        auxInteresTotal
                            .bold(" INTERES TOTAL:  $")
                            .normal(String(format: "%.2f", interesTot))
                        self.interesTotal.layer.borderWidth = _CONST.BORDER_WIDTH
                        self.interesTotal.layer.cornerRadius = _CONST.CORNER_RADIUS
                        self.interesTotal.attributedText = auxInteresTotal
                        
                        
                        let impuestoDos = (montoDpf! * interes_dpf * Double(plazoDpf) * retencion_dpf) / Double(tiempo_dpf)
                        let auxImpuestoDos = NSMutableAttributedString()
                        auxImpuestoDos
                            .bold(" IMPUESTO 2%:  $")
                            .normal(String(format: "%.2f", impuestoDos))
                        self.impuesto2.layer.borderWidth = _CONST.BORDER_WIDTH
                        self.impuesto2.layer.cornerRadius = _CONST.CORNER_RADIUS
                        self.impuesto2.attributedText = auxImpuestoDos
                        
                        let totalReci = (montoDpf! + ((montoDpf! * interes_dpf * Double(plazoDpf)) / Double(tiempo_dpf)))-((montoDpf! * interes_dpf * Double(plazoDpf) * retencion_dpf)/Double(tiempo_dpf))
                        
                        let auxTotalRecibir = NSMutableAttributedString()
                        auxTotalRecibir
                            .bold(" TOTAL A RECIBIR:  $")
                            .normal(String(format: "%.2f", totalReci))
                        self.totalRecibir.layer.borderWidth = _CONST.BORDER_WIDTH
                        self.totalRecibir.layer.cornerRadius = _CONST.CORNER_RADIUS
                        self.totalRecibir.attributedText = auxTotalRecibir
                
                    }else{
                        DispatchQueue.main.async {
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.DFP_MSJ_MONTO_MAYOR_CIEN , view: self)
                        }
                    }
                    
                }
            
        }
    }
    
    func calculaDpf(montoDpf: Double, plazoDpf: Int) -> Double {
        
         
         if plazoDpf == 30 {
            retencion_dpf = 0.02
            if(montoDpf >= 100 && montoDpf <= 2000){
                interes_dpf = 0.0575
         }
         if(montoDpf >= 2001 && montoDpf <= 5000 ){
         interes_dpf = 0.06
         }
            if(montoDpf >= 5001 && montoDpf <= 10000 ){
         interes_dpf = 0.0625
         }
         if(montoDpf > 10000 ){
         interes_dpf = 0.065
         }
         }
         if plazoDpf == 60 {
            retencion_dpf = 0.02
         if(montoDpf >= 100 && montoDpf <= 2000){
         interes_dpf = 0.0725
         }
         if(montoDpf >= 2001 && montoDpf <= 5000 ){
         interes_dpf = 0.08
         }
         if(montoDpf >= 5001 && montoDpf <= 10000 ){
         interes_dpf = 0.0825
         }
         if(montoDpf > 10000 ){
         interes_dpf = 0.085
         }
         }
         if plazoDpf == 90 {
            retencion_dpf = 0.02
         if(montoDpf >= 100 && montoDpf <= 2000){
         interes_dpf = 0.08
         }
         if(montoDpf >= 2001 && montoDpf <= 5000 ){
         interes_dpf = 0.0825
         }
         if(montoDpf >= 5001 && montoDpf <= 10000 ){
         interes_dpf = 0.085
         }
         if(montoDpf > 10000 ){
         interes_dpf = 0.09
         }
         }
         if (plazoDpf == 120 || plazoDpf == 150 || plazoDpf == 180 || plazoDpf == 210) {
            retencion_dpf = 0.02
         if(montoDpf >= 100 && montoDpf <= 2000){
         interes_dpf = 0.0825
         }
         if(montoDpf >= 2001 && montoDpf <= 5000 ){
         interes_dpf = 0.085
         }
         if(montoDpf >= 5001 && montoDpf <= 10000 ){
         interes_dpf = 0.0875
         }
         if(montoDpf > 10000 ){
         interes_dpf = 0.095
         }
         }
         if (plazoDpf == 240 || plazoDpf == 270 || plazoDpf == 330) {
            retencion_dpf = 0.02
         if(montoDpf >= 100 && montoDpf <= 2000){
         interes_dpf = 0.085
         }
         if(montoDpf >= 2001 && montoDpf <= 5000 ){
         interes_dpf = 0.0875
         }
         if(montoDpf >= 5001 && montoDpf <= 10000 ){
         interes_dpf = 0.09
         }
         if(montoDpf > 10000 ){
         interes_dpf = 0.1
         }
         }
         if plazoDpf == 365 {
             retencion_dpf = 0
             if(montoDpf >= 100 && montoDpf <= 2000){
             interes_dpf = 0.09
             }
             if(montoDpf >= 2001 && montoDpf <= 5000 ){
             interes_dpf = 0.0925
             }
             if(montoDpf >= 5001 && montoDpf <= 10000 ){
             interes_dpf = 0.095
             }
             if(montoDpf > 10000 ){
             interes_dpf = 0.105
             }
         }
        return interes_dpf
        
    }
    func frecunciaPago(frecuencia: Int) -> Int {
        var aux: Int = 0
        if frecuencia == _CONST.SIM_DPF_FRECUENCIA_MENSUAL{
           aux = 360
        }
        
        if frecuencia == _CONST.SIM_DPF_FRECUENCIA_VENCIMIENTO {
            aux = 360
            isVencimiento = true
        }
        return aux
    }
    
    func plazoDias(plazoDias: String) -> Int {
        var plazoDiasAux: Int = 0
      
        switch plazoDias {
        case "30 Dias":
            plazoDiasAux = 30
            break
        case "60 Dias":
            plazoDiasAux = 60
            break
        case "90 Dias":
            plazoDiasAux = 90
            break
        case "120 Dias":
            plazoDiasAux = 120
            break
        case "150 Dias":
            plazoDiasAux = 150
            break
        case "180 Dias":
            plazoDiasAux = 180
            break
        case "210 Dias":
            plazoDiasAux = 210
            break
        case "240 Dias":
            plazoDiasAux = 240
            break
        case "270 Dias":
            plazoDiasAux = 270
            break
        case "330 Dias":
            plazoDiasAux = 330
            break
        case "MAYOR A 365 DIAS":
            plazoDiasAux = 365
            break
            
        default:
            break
        }
        return plazoDiasAux
    }
}

extension DepoPlazoFijoViewController : PopUpDelegate {
    func seleccionString(value: String, tipoSelection: String, tipoCuenta: String, saldoDisponible: String) {
        textBoxPlazosDias.text = value
    }
    
}

