//
//  TransferenciaInternaViewController.swift
//  DaquiApp
//
//  Created by Desarrollador on 29/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class TransferenciaViewController: UIViewController, UITextFieldDelegate {
    var usuario: UsuarioModel = UsuarioModel()
    var esInternaExternaTarjeta: Int = 0
   
    var cuentaInternaCredito: CuentaInternaModel = CuentaInternaModel()
    var cuentaExternaCredito: CuentaExternaModel = CuentaExternaModel()
    
    var otp: MensajeModel = MensajeModel()
    var auxSaldoDisponible: Double = 0
    let objActivity = activityIndicatorVC()
    var posicionConsolidada: [PosicionConsolidadaModel] = []
    var posicionConsolidadaAuxiliar: [PosicionConsolidadaModel] = []
    var posicionConsolidadaString: PosicionConsolidadaModel = PosicionConsolidadaModel ()
    
    @IBOutlet weak var btnTransferir: UIButton!
    @IBOutlet weak var nroLabel: UILabel!
    @IBOutlet weak var nroValorLabel: UILabel!
    @IBOutlet weak var descripcionLabel: UILabel!
    @IBOutlet weak var descripcionValorLabel: UILabel!
    @IBOutlet weak var titularLabel: UILabel!
    @IBOutlet weak var titularValorLabel: UILabel!
    @IBOutlet weak var tipoLabel: UILabel!
    
    @IBOutlet weak var saldoDisponibleLabel: UILabel!
    @IBOutlet weak var cuentaTxt: UITextField!
    @IBOutlet weak var montoTxt: UITextField!
    @IBOutlet weak var motivoTxt: UITextField!
    @IBOutlet weak var viewUno: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        usuario = (tabBarController as! TabBarViewController).usuario
        if esInternaExternaTarjeta == 1 {
            self.navigationItem.title = _CONST.TRANS_INT
        }
        if esInternaExternaTarjeta == 2
        {
            self.navigationItem.title = _CONST.TRANS_EXT
        }
        if esInternaExternaTarjeta == 3{
            self.navigationItem.title = _CONST.PAGO_TARJETA
        }
        definirCuentaCredito()
        
        cuentaTxt.delegate = self
        motivoTxt.delegate = self
        montoTxt.delegate = self
        
        btnTransferir.layer.cornerRadius = _CONST.CORNER_RADIUS
        
        //para border del view
        self.viewUno.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.viewUno.layer.borderWidth = _CONST.BORDER_WIDTH
        self.viewUno.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        // fin borde
        //para la vista con el scroll view
        
        // Observe keyboard change
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide3(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // Add touch gesture for contentView
        self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(returnTextView(gesture:))))
        // fin scroll view
    }
    
   //auto viewController
    
    @IBOutlet weak var constraintContentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    var activeField: UITextField?
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    
    @objc func returnTextView(gesture: UIGestureRecognizer) {
        guard activeField != nil else {
            return
        }
        
        activeField?.resignFirstResponder()
        activeField = nil
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        lastOffset = self.scrollView.contentOffset
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeField?.resignFirstResponder()
        activeField = nil
        return true
    }
  

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var retorno: Bool = false
        
        switch textField.tag {
        case 2:
            guard let text = textField.text else { return true }
            let count = text.count + string.count - range.length
            retorno = count <= 80
            break
        case 3:
            if(Utilidades.validarNumero(textField: textField, range: range,string: string)){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 6
            }
            break
        
        default:
            retorno = true
        }
        
        return retorno
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if keyboardHeight != nil {
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            
            // so increase contentView's height by keyboard height
            UIView.animate(withDuration: 0.3, animations: {
                self.constraintContentHeight.constant += self.keyboardHeight
            })
            
            // move if keyboard hide input field
            if(activeField == nil){
                return
            }
            let distanceToBottom = self.scrollView.frame.size.height - (activeField?.frame.origin.y)! - (activeField?.frame.size.height)!
            let collapseSpace = keyboardHeight - distanceToBottom
            if collapseSpace < 0 {
                // no collapse
                return
            }
            
            // set new offset for scroll view
            UIView.animate(withDuration: 0.3, animations: {
                // scroll to the position above keyboard 10 points
                self.scrollView.contentOffset = CGPoint(x: self.lastOffset.x, y: collapseSpace + 10)
            })
        }
    }
    
    
    @objc func keyboardWillHide3(notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
            if self.constraintContentHeight != nil && self.keyboardHeight != nil {
                self.constraintContentHeight.constant -= self.keyboardHeight
                self.scrollView.contentOffset = self.lastOffset
            }
            
        }
        
        keyboardHeight = nil
    }
    
    
  
    //Fin auto viewController
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func selecCuenta(_ sender: Any) {
        //Es la ocultar el teclado
        DispatchQueue.main.async {
                   self.view.endEditing(true)
               }
        //fin ocultar
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        self.posicionConsolidadaAuxiliar.removeAll()
        self.posicionConsolidada(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_CEDULA, usuario: usuario.strIdentificacion, token: usuario.strToken)
    }
    
    
    @IBAction func tranferir(_ sender: Any) {
        guard !(cuentaTxt.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SER_BAS_MENSA_CUENTA, view: self)
            }
            return
        }
        guard !(motivoTxt.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_MONTIVO, view: self)
            }
            return
        }
        guard !(Utilidades.tieneCaracteresEspeciales(string: motivoTxt.text!))else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje:_CONST.MSJ_CARACTERES_ESPECIALES , view: self)
            }
            return
        }
        guard !(montoTxt.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_MONTO , view: self)
            }
            return
        }
        guard !(Double(montoTxt.text!)?.isLess(than: 1.00))! else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "El monto minimo es un 1 dólar" , view: self)
            }
            return
        }
        
        //verifica si tiene saldo suficiente para realizar la transaccion
        if (Double(montoTxt.text!)?.isLess(than: auxSaldoDisponible - Double(_CONST.MONTO_MINIMO)) )!{
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }

            procesoEnvioOTP(userName: usuario.strNombreUser, identificacion: usuario.strIdentificacion, token: usuario.strToken)
        }else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_FONDOS, view: self)
            }
        }
        
    }
    
    
    //para Buscar la cuenta
    func posicionConsolidada( identificacion: String, tipoIdentificacion: String, usuario: String, token: String){
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/posicionConsolidada/\(_CONST.CANAL_DES)/\(identificacion)/\(tipoIdentificacion)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.posicionConsolidada = try JSONDecoder().decode([PosicionConsolidadaModel].self , from: data)
                    
                    if  self.posicionConsolidada.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            for item in self.posicionConsolidada{
                                if(item.Producto == _CONST.CNTA_DEBITO_VISTA  || item.Producto == _CONST.CNTA_DEBITO_KULLKIMIRAK || item.Producto == _CONST.CNTA_DEBITO_BASICA){
                                    self.posicionConsolidadaString.Cuenta = item.Cuenta
                                    self.posicionConsolidadaString.Descripcion = item.Descripcion
                                    self.posicionConsolidadaString.GProducto = item.GProducto
                                    self.posicionConsolidadaString.Producto = item.Producto
                                    self.posicionConsolidadaString.Saldoaportacion = item.Saldoaportacion
                                    self.posicionConsolidadaString.Saldobloqueado = item.Saldobloqueado
                                    self.posicionConsolidadaString.Saldocontable = item.Saldocontable
                                    self.posicionConsolidadaString.Saldoefectivo = item.Saldoefectivo
                                    self.posicionConsolidadaAuxiliar.append(self.posicionConsolidadaString)
                                }
                            }
                            
                            
                            if self.posicionConsolidadaAuxiliar.count > 1 {
                                let popUpCuentaViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpCuentaViewController") as!  PopUpCuentaViewController
                                
                                popUpCuentaViewController.popUpDelegate = self
                                popUpCuentaViewController.tipoSelec = _CONST.SELEC_CUENTA
                                popUpCuentaViewController.listaOpciones = self.posicionConsolidadaAuxiliar
                                self.present(popUpCuentaViewController, animated: true)
                            }else{
                                self.cuentaTxt.text = self.posicionConsolidadaString.Cuenta
                                let auxTipoCuenta = NSMutableAttributedString()
                                auxTipoCuenta
                                    .bold("Tipo: ")
                                    .normal("\(self.posicionConsolidadaString.Descripcion)")
                                self.tipoLabel.attributedText = auxTipoCuenta
                                let auxSaldoDisponible = NSMutableAttributedString()
                                auxSaldoDisponible
                                    .bold("Saldo Disponible: $ ")
                                    .normal("\(self.posicionConsolidadaString.Saldoefectivo)")
                                if self.posicionConsolidadaString.Saldoefectivo != ""{
                                    self.auxSaldoDisponible = Double(self.posicionConsolidadaString.Saldoefectivo)!
                                }
                                self.saldoDisponibleLabel.attributedText = auxSaldoDisponible
                            }
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                        }
                    }
                    
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                  Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
                }
            }else if let error = error{
                print("Error al consumir Posicion Consolidada:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    
    func definirCuentaCredito(){
        if esInternaExternaTarjeta == 1 {
            nroLabel.text = "Nro.:"
            nroValorLabel.text = cuentaInternaCredito.numCuenta
            descripcionLabel.text = "Tipo:"
            descripcionValorLabel.text = Utilidades.descripcionTipoCuentaLocal(nroCuentaLocal: cuentaInternaCredito.numCuenta)
            titularLabel.text = "Titular:"
            titularValorLabel.text = cuentaInternaCredito.nombre
        }
        if esInternaExternaTarjeta == 2 || esInternaExternaTarjeta == 3
        {
            nroLabel.text = "Nro.:"
            nroValorLabel.text = cuentaExternaCredito.numerocuenta
            descripcionLabel.text = "Entidad Financiera:"
            descripcionValorLabel.text = cuentaExternaCredito.ifiCE.descripcion
            titularLabel.text = "Titular:"
            titularValorLabel.text = cuentaExternaCredito.nombre
        }
        
    }
    
    func procesoEnvioOTP(userName: String, identificacion: String, token: String)   {
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = [ "strUsername": userName,
                                    "strIdentificacion": identificacion,
                                    "strToken": token,
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                    "strObservacion": _CONST.CANAL_DES
                                    ]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/procesoEnvioOTP")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData

        session.dataTask(with: request) { (data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    self.otp = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  self.otp.strCodigoError  == _CONST.CODIGO_RETORNO_OK {
                        
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
                            otpViewController.popUpOTPDelegate = self
                            self.present(otpViewController, animated: true)
                        }
                    }else{
                        if self.otp.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                  Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: self.otp.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_OTP_NOK , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
            }else if let error = error{
                print("Error al consumir Envio OTP :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }.resume()
        
    }
    
    func transferenciaInterna(identificacion: String, montoTransferir: String, codCuentaCredito:String, codCuentaDebito: String, descripcionAdicional: String, codigoOtp: String, token:String ) {
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let json: [String: Any] = [ "strIdentificacion": identificacion,
                                    "strMontotransferir": montoTransferir,
                                    "strCodCuentaCredito": codCuentaCredito,
                                    "strCodCuentaDebito": codCuentaDebito,
                                    "strDescripcionAdicional": descripcionAdicional,
                                    "strCodigoOtp": codigoOtp,
                                    "strToken": token,
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS
                                    ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/procesoTransferenciaInterna")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let transInternaRespuestaData = try JSONDecoder().decode([RespuestaTransferenciaModel].self , from: data)
                    let transferenciaAux = self.evaluarTransferenciaInterna(transaccionAux: transInternaRespuestaData)
                    
                    if  transferenciaAux.codigoError  == _CONST.CODIGO_RETORNO_OK {
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            self.cuentaTxt.text = ""
                            self.motivoTxt.text = ""
                            self.montoTxt.text = ""
                            self.tipoLabel.text = "Tipo:"
                            self.saldoDisponibleLabel.text = "Saldo Disponible:"
                            
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: transferenciaAux.descripcionError , view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                             self.objActivity.hideActivityIndicator(uiView: self.view)
                            if(transferenciaAux.codigoError == _CONST.CODIGO_ERROR_SEG){
                                DispatchQueue.main.async {
                                    let alertController = UIAlertController(title: _CONST.AVISO, message: transferenciaAux.descripcionError, preferredStyle: .alert)
                                    let actionAceptar = UIAlertAction(title: _CONST.AVISO, style: .default) { (action:UIAlertAction) in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    alertController.addAction(actionAceptar)
                                    self.present(alertController, animated: true, completion: nil)
                                }
                                
                            }else{
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: transferenciaAux.descripcionError , view: self)
                            }
                           
                            
                            }
                        }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                  Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else
                        {
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }
                catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                    }
                }
            }else if let error = error{
                print("Error al consumir Transferencia Interna :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    func evaluarTransferenciaInterna(transaccionAux: [RespuestaTransferenciaModel]) -> RespuestaTransferenciaModel{
        var aux: RespuestaTransferenciaModel = RespuestaTransferenciaModel.init(idtransaccion: 0, codigoerror: "", estado: 0, tipotransaccion: 0, descdescripcionerror: "")
        for item in transaccionAux{
            aux = item
        }
        return aux
    }
    
    func transferenciaExterna(identificacion: String, montoTrasferir: String, codCuentaCredito: String, codCuentaDebito: String, descripcionAdicional: String, idIfi: String, codOtp: String, idConcepto: String, tipoCuentaExterna: String, token:String) {
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let json: [String: Any] = [ "strIdentificacion": identificacion,
                                    "strMontotransferir": montoTrasferir,
                                    "strCodCuentaCredito": codCuentaCredito,
                                    "strCodCuentaDebito": codCuentaDebito,
                                    "strDescripcionAdicional": descripcionAdicional,
                                    "stridIfi": idIfi,
                                    "strCodigoOtp": codOtp,
                                    "strIdconcepto": idConcepto,
                                    "strToken": token,
                                   "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                   "intTipoCuentaExterna" : tipoCuentaExterna]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/procesoTransferenciaExterna")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { (data, response, error) in
            
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let transExternaRespuestaData = try JSONDecoder().decode([RespuestaTransferenciaModel].self , from: data)
                    let transferenciaAux = self.evaluarTransaccionExterna(transaccionAux: transExternaRespuestaData)
                    
                    if  transferenciaAux.codigoError  == _CONST.CODIGO_RETORNO_OK {
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            self.cuentaTxt.text = ""
                            self.motivoTxt.text = ""
                            self.montoTxt.text = ""
                            self.tipoLabel.text = "Tipo:"
                            self.saldoDisponibleLabel.text = "Saldo Disponible:"
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: transferenciaAux.descripcionError , view: self)
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            if(transferenciaAux.codigoError == _CONST.CODIGO_ERROR_SEG){
                                DispatchQueue.main.async {
                                    let alertController = UIAlertController(title: _CONST.AVISO, message: transferenciaAux.descripcionError, preferredStyle: .alert)
                                    let actionAceptar = UIAlertAction(title: _CONST.AVISO, style: .default) { (action:UIAlertAction) in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    alertController.addAction(actionAceptar)
                                    self.present(alertController, animated: true, completion: nil)
                                }
                                
                            }else{ Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: transferenciaAux.descripcionError , view: self) }
                            
                            
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                  Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_SEG{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir Transferencia Externa :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    
    func evaluarTransaccionExterna(transaccionAux: [RespuestaTransferenciaModel]) -> RespuestaTransferenciaModel{
        var aux: RespuestaTransferenciaModel = RespuestaTransferenciaModel.init(idtransaccion: 0, codigoerror: "", estado: 0, tipotransaccion: 0, descdescripcionerror: "")
        for item in transaccionAux{
            aux = item
        }
        return aux
    }
}

extension TransferenciaViewController: PopUpDelegate{
    func seleccionString(value: String, tipoSelection: String, tipoCuenta: String, saldoDisponible: String) {
        
        
        if (tipoSelection == _CONST.SELEC_CUENTA){
            self.cuentaTxt.text = value
            let auxTipoCuenta = NSMutableAttributedString()
            auxTipoCuenta
                .bold("Tipo: ")
                .normal("\(tipoCuenta)")
            self.tipoLabel.attributedText = auxTipoCuenta
            let auxSaldoDisponible = NSMutableAttributedString()
            auxSaldoDisponible
                .bold("Saldo Disponible: $ ")
                .normal("\(saldoDisponible)")
            if saldoDisponible != ""{
                self.auxSaldoDisponible = Double(saldoDisponible)!
            }
            self.saldoDisponibleLabel.attributedText = auxSaldoDisponible
            
        }
        
    }
    
}

extension TransferenciaViewController: OTPDelegate{
    func inserOTP(value: String) {
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        if esInternaExternaTarjeta == 1{
            transferenciaInterna(identificacion: usuario.strIdentificacion, montoTransferir: montoTxt.text!, codCuentaCredito: cuentaInternaCredito.numCuenta , codCuentaDebito: cuentaTxt.text!, descripcionAdicional: motivoTxt.text!, codigoOtp: value, token: usuario.strToken)
        }
        if esInternaExternaTarjeta == 2{
            transferenciaExterna(identificacion: usuario.strIdentificacion, montoTrasferir: montoTxt.text!, codCuentaCredito: cuentaExternaCredito.numerocuenta, codCuentaDebito: cuentaTxt.text!, descripcionAdicional: motivoTxt.text!, idIfi: "\(cuentaExternaCredito.ifiCE.idifi)", codOtp: value, idConcepto: cuentaExternaCredito.tipocuentaCE.idtipocuenta,tipoCuentaExterna: _CONST.CE_TC_AHORROS ,token: usuario.strToken)
        }
        if esInternaExternaTarjeta == 3{
            transferenciaExterna(identificacion: usuario.strIdentificacion, montoTrasferir: montoTxt.text!, codCuentaCredito: cuentaExternaCredito.numerocuenta, codCuentaDebito: cuentaTxt.text!, descripcionAdicional: motivoTxt.text!, idIfi: "\(cuentaExternaCredito.ifiCE.idifi)", codOtp: value, idConcepto: cuentaExternaCredito.tipocuentaCE.idtipocuenta, tipoCuentaExterna: _CONST.CE_TC_TARJETA_CREDITO ,token: usuario.strToken)
        }
        
        
        
    }
    
    
}

