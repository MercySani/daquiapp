//
//  SBFAguaViewController.swift
//  DaquiApp
//
//  Created by Fredy on 15/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class ServiciosBasicosFacilitoViewController: UIViewController, UITextFieldDelegate {
    
    var usuario: UsuarioModel = UsuarioModel()
    
    var listaServicios: [ServicioSRModel] = []
    var auxIdCategoria: Int = 0
    var aucIdServicio: String = ""
    var auxIdOTP: String = ""
    var auxServicioNombre: String = ""
    var auxInformacionAdicional: String = ""
    var count: Int = 0
    var comisionTotal: Double = 0
    var numeroRubros: Int = 0
    var valorTotalPagar: Double = 0
    var saldoDisponible: Double = 0
    var auxlisValoresPendientes: [ListaValoresPendientes] = []
    var posicionConsolidada: [PosicionConsolidadaModel] = []
    var posicionConsolidadaAuxiliar: [PosicionConsolidadaModel] = []
    var posicionConsolidadaString: PosicionConsolidadaModel = PosicionConsolidadaModel ()
   let objActivity = activityIndicatorVC()
    
    @IBOutlet weak var btnContinuar: UIButton!
    @IBOutlet weak var btnConsulta: UIButton!
    @IBOutlet weak var viewUno: UIView!
    @IBOutlet weak var viewDos: UIView!
    @IBOutlet weak var viewTres: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usuario = (tabBarController as! TabBarViewController).usuario
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        self.continuarBtn.isHidden = true
        self.viewUno.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.viewUno.layer.borderWidth = _CONST.BORDER_WIDTH
        self.viewUno.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        
        self.viewDos.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.viewDos.layer.borderWidth = _CONST.BORDER_WIDTH
        self.viewDos.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        
        self.viewTres.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.viewTres.layer.borderWidth = _CONST.BORDER_WIDTH
        self.viewTres.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        
        btnContinuar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnConsulta.layer.cornerRadius = _CONST.CORNER_RADIUS
        
        txtCuentaDebito.delegate = self
        txtReferencia.delegate = self
        
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var tipoCaracter: CharacterSet = CharacterSet()
        var characterSet: CharacterSet = CharacterSet()
        var retorno: Bool = false
        
        switch textField.tag {
        case 2:
            tipoCaracter = CharacterSet.alphanumerics
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 25
            }
            break
        case 3:
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 15
            }
            
            break
        
        default:
            retorno = true
        }
        
        return retorno
    }
    
    
    @IBAction func continuarSBF(_ sender: UIButton) {
        
        
        guard !(txtServicio.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SBF_MEN_SERVICIOS , view: self)
            }
            return
        }
        
        guard !(txtCuentaDebito.text!.isEmpty) else {
         DispatchQueue.main.async {
         Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SER_BAS_MENSA_CUENTA , view: self)
         }
         return
         }
        
        guard !(txtReferencia.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SBF_MEN_REFERENCIA , view: self)
            }
            return
        }
        
        
        let auxVlorTotal = self.valorTotalPagar + self.comisionTotal
        
        if (auxVlorTotal.isLess(than: self.saldoDisponible - Double(_CONST.MONTO_MINIMO))){
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
             self.enviarCodigoOPT(userName: usuario.strNombreUser, identificacion: usuario.strIdentificacion, token: usuario.strToken)
        }else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_FONDOS, view: self)
            }
        }
        
        
    }
    //VARIABLES DE VISTA
    
    @IBOutlet weak var identificacionLbl: UILabel!
    @IBOutlet weak var direccionLbl: UILabel!
    @IBOutlet weak var nombreLbl: UILabel!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var comisionLbl: UILabel!
    @IBOutlet weak var valorPagarLbl: UILabel!
    @IBOutlet weak var txtCuentaDebito: UITextField!
    @IBOutlet weak var continuarBtn: UIButton!
   
    
    @IBAction func selecCuentaDebito(_ sender: Any) {
       
        DispatchQueue.main.async {
            self.view.endEditing(true)
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        self.posicionConsolidadaAuxiliar.removeAll()
        self.posicionConsolidada(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_SOCIO, usuario: usuario.strIdentificacion, token: usuario.strToken)
    }
   
    @IBOutlet weak var saldoDisponibleLbl: UILabel!
    @IBOutlet weak var numeroRubroLbl: UILabel!
    
    
    @IBOutlet weak var txtServicio: UITextField!
    
    @IBAction func selecServicio(_ sender: Any) {
        
        print("La categoria  \(self.auxIdCategoria)")
        
            DispatchQueue.main.async {
                self.view.endEditing(true)
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            listaServicioSR(usuario: usuario.strIdentificacion, parametroCategoria: self.auxIdCategoria, token: usuario.strToken)
   
    }
    
    @IBOutlet weak var txtReferencia: UITextField!
    
    //FIN

    var responseConsulta: ConsultaServicioFacilitoModel = ConsultaServicioFacilitoModel()
    
   
    @IBAction func consultarSB(_ sender: UIButton) {
        
        guard !(txtServicio.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SBF_MEN_SERVICIOS , view: self)
            }
            return
        }
        
        guard !(txtReferencia.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SBF_MEN_REFERENCIA , view: self)
            }
            return
        }
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        
        let mac: String = macAddress!.description
        
        consultaServicioBasico(referencia: txtReferencia.text!, informacionAdicional: _CONST.DATO_ADICIONAL, idServicio: "\(aucIdServicio)", idPagoProgramado: _CONST.ID_PAGO_PROGRAMADO_SB, operador: _CONST.OPERADOR_SB , canal: _CONST.INTERNET_BANKING , agencia: _CONST.ID_AGENCIA_SB, usuarioFIT: usuario.strIdentificacion, claveFIT: usuario.strIdentificacion, canalInterno: _CONST.CANAL_DES, ip: mac, token: usuario.strToken)
        
        
       
    
    }
    
    
    
    func consultaServicioBasico(referencia: String, informacionAdicional: String, idServicio: String, idPagoProgramado: String, operador: Int,  canal: Int, agencia: String, usuarioFIT: String, claveFIT: String ,canalInterno: String, ip: String, token:String)->Void{
        
        self.valorTotalPagar = 0
        self.numeroRubros = 0
        self.comisionTotal = 0
        self.saldoDisponible = 0
        self.txtCuentaDebito.text = ""
        self.numeroRubroLbl.text = "Número rubros:"
        self.saldoDisponibleLbl.text = "Saldo Disponible: "
        self.auxlisValoresPendientes.removeAll()
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/consultaDeudas/\(referencia)/\(informacionAdicional)/\(idServicio)/\(idPagoProgramado)/\(operador)/\(canal)/\(agencia)/\(usuarioFIT)/\(claveFIT)/\(canalInterno)/\(ip)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let _ = response as? HTTPURLResponse{
                do{
                    //let responseData1 = String(data: data, encoding: String.Encoding.utf8)
                    //print("Resultado---->", responseData1)
                    
                    self.responseConsulta =  try JSONDecoder().decode(ConsultaServicioFacilitoModel.self , from: data)
                    
                    if  self.responseConsulta.id != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            if self.responseConsulta.servicio.tipopago == _CONST.TIPO_PAGO_T{
                                if self.responseConsulta.servicio.comisionxrubro == _CONST.COMISION_RUBRO_S {
                                    for items in self.responseConsulta.lisValoresPendientes{
                                        if items.afectacionDetalleRecibo.rubroTipoAfectacion.rubro.esComision == false {
                                            self.numeroRubros = self.numeroRubros + 1
                                            self.valorTotalPagar = self.valorTotalPagar + items.valorUnitario
                                            
                                        }
                                    }
                                    self.comisionTotal = self.responseConsulta.comision * Double(self.numeroRubros)
                                    
                                    // para asignar el valor total de la comision en un struct
                                    if self.comisionTotal != 0 {
                                        for var auxComision in self.responseConsulta.lisValoresPendientes{
                                            if auxComision.afectacionDetalleRecibo.rubroTipoAfectacion.rubro.esComision == true{
                                                auxComision.valorUnitario = self.comisionTotal
                                                
                                            }
                                            self.auxlisValoresPendientes.append(auxComision)
                                        }
                                        
                                    }
                                    
                                }else{
                                    for item in self.responseConsulta.lisValoresPendientes{
                                        if item.afectacionDetalleRecibo.rubroTipoAfectacion.rubro.esComision == false{
                                            self.valorTotalPagar = self.valorTotalPagar + item.valorUnitario
                                            self.numeroRubros = self.numeroRubros + 1
                                        }else{
                                            self.comisionTotal = self.responseConsulta.comision
                                        }
                                    }
                                    self.auxlisValoresPendientes = self.responseConsulta.lisValoresPendientes
                                }
                            }else{
                                var detalleRecibo: ListaValoresPendientes = ListaValoresPendientes();
                                
                                for items in self.responseConsulta.lisValoresPendientes{
                                    if items.afectacionDetalleRecibo.rubroTipoAfectacion.rubro.esComision == false {
                                        self.numeroRubros = self.numeroRubros + 1
                                        self.valorTotalPagar = self.valorTotalPagar + items.valorUnitario
                                        detalleRecibo = items
                                        break
                                    }
                                }
                                self.auxlisValoresPendientes.append(detalleRecibo)
                                self.comisionTotal = self.responseConsulta.comision * Double(self.numeroRubros)
                                // para asignar el valor total de la comision en un struct
                                if self.comisionTotal != 0 {
                                    for var auxComision in self.responseConsulta.lisValoresPendientes{
                                        if auxComision.afectacionDetalleRecibo.rubroTipoAfectacion.rubro.esComision == true{
                                            auxComision.valorUnitario = self.comisionTotal
                                            detalleRecibo = auxComision
                                            break
                                        }
                                    }
                                    self.auxlisValoresPendientes.append(detalleRecibo)
                                    
                                }
                            }
                            
                            
                            
                            self.subTotalLbl.text = "            \(String(format: "%.2f", self.valorTotalPagar))"
                            self.comisionLbl.text = "           \(String(format: "%.2f", self.comisionTotal))"
                            self.valorPagarLbl.text = "         \(String(format: "%.2f", (self.valorTotalPagar + self.comisionTotal)))"
                            let auxNom = NSMutableAttributedString()
                            auxNom
                                .bold("Nombres: ")
                                .normal("\(self.responseConsulta.nombres)")
                            self.nombreLbl.attributedText = auxNom
                            
                            let auxDir = NSMutableAttributedString()
                            auxDir
                                .bold("Dirección: ")
                                .normal("\(self.responseConsulta.direccion)")
                            self.direccionLbl.attributedText = auxDir
                            
                            let auxIden = NSMutableAttributedString()
                            auxIden
                                .bold("Identificación: ")
                                .normal("\(self.responseConsulta.identificacion)")
                            self.identificacionLbl.attributedText = auxIden
                            self.continuarBtn.isHidden = false
                            let auxNumeroRubro = NSMutableAttributedString()
                            auxNumeroRubro
                                .bold("Número rubros: ")
                                .normal("\(self.numeroRubros)")
                            self.numeroRubroLbl.attributedText = auxNumeroRubro
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SB_NO_DISPONIBLES  , view: self)
                        }
                    }
                }catch{
                        do {
                            let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                            
                            if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                                }
                            }else{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                                }
                            }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                    
                    
                }
            }else if let error = error{
                print("Error al consumir consultar Servicios facilito:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    
    
    
    
    //servicio
    func listaServicioSR(usuario: String, parametroCategoria: Int, token: String)->Void{
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/servicioPorParametrosSR/\(_CONST.TODO)/\(parametroCategoria)/\(_CONST.FRECUENCIA_DE_PAGO)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let _ = response as? HTTPURLResponse{
                do{
                    //let responseData1 = String(data: data, encoding: String.Encoding.utf8)
                    //print("Resultado---->", responseData1)
                    
                    self.listaServicios =  try JSONDecoder().decode([ServicioSRModel].self , from: data)
                    if  self.listaServicios.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            let pickerPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickerPopUpViewController") as! PickerPopUpViewController
                            pickerPopupViewController.popUpDelegate = self
                            pickerPopupViewController.tipoSelec = "SERVICIOS"
                            pickerPopupViewController.listaServicio = self.listaServicios
                            self.present(pickerPopupViewController, animated: true)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "No existe servicios disponibles " , view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir lista Servicios SR:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    
    
    
    func enviarCodigoOPT(userName: String, identificacion: String, token: String)   {
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let json: [String: Any] = [ "strUsername": userName,
                                    "strIdentificacion": identificacion,
                                    "strToken": token,
                                    
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                    "strObservacion": _CONST.CANAL_DES
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/procesoEnvioOTP")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { (data, response, error) in
            
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let otpRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  otpRespuestaData.strCodigoError  == _CONST.CODIGO_RETORNO_OK{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
                            otpViewController.popUpOTPDelegate = self
                            self.present(otpViewController, animated: true)
                        }
                    }else{
                        if otpRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: otpRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_OTP_NOK , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir Envio OTP :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    
    
    
    //pagar Servicios BAsico FAcilito
    func pagarSBF(idSolicitud: Int, operador: Int ,  idCanal: Int, idAgencia:String, usuarioFIT: String, claveFIT: String, canalInterno: String, ip: String, identificacio: String, lisValoresPendientes: [ListaValoresPendientes], codigoOTP: String, numCuenta: String, referencia: String , idServicio: String, nombreServicio: String , informacionAdicional: String, token: String)   {
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let codificadoJSON = try! encoder.encode(lisValoresPendientes)
        
        
        let json: [String: Any] = [ "idSolicitud": idSolicitud ,
                                    "operador": operador,
                                    "idCanal": idCanal,
                                    "idAgencia":  idAgencia,
                                    "usuarioFIT": usuarioFIT,
                                    "claveFIT": claveFIT,
                                    "canalInterno": canalInterno,
                                    "ip": ip,
                                    "identificacion": identificacio,
                                    "lisValoresPendientes": String(data: codificadoJSON, encoding: .utf8)! ,
                                    "strCodigoOtp": codigoOTP,
                                    "numcuenta": numCuenta,
                                    "referencia": referencia,
                                    "idServicio": idServicio,
                                    "nombreServicio":nombreServicio,
                                    "informacionAdicional": _CONST.DATO_ADICIONAL,
                                    "strToken": token,
                                    "strTipoIdentificacion": _CONST.CI_TIPO_IDENTIFICACION,
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS
                                    ]
        
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/pagoServicio")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { (data, response, error) in
            
            if let data = data, let _ = response as? HTTPURLResponse
            {
                do{
                    
                    
                    let mensajeResponse = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  mensajeResponse.strCodigoError  == _CONST.CODIGO_RETORNO_OK{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            self.continuarBtn.isHidden = true
                            
                            self.txtServicio.text = ""
                            self.txtReferencia.text = ""
                            self.nombreLbl.text = "Nombres:"
                            self.direccionLbl.text = "Dirección:"
                            self.identificacionLbl.text = "Identificación:"
                            self.subTotalLbl.text = "            00.00"
                            self.comisionLbl.text = "            00.00"
                            self.valorPagarLbl.text = "          00.00"
                            self.txtCuentaDebito.text = ""
                            self.numeroRubroLbl.text = "Número rubro:"
                            self.saldoDisponibleLbl.text = "Saldo Disponible: "
                            
                             Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeResponse.strMensajeError , view: self)
                            
                        }
                    }else{
                        if mensajeResponse.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                  Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeResponse.strMensajeError , view: self)
                            }
                        }else if mensajeResponse.strCodigoError == _CONST.CODIGO_ERROR_SEG{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeResponse.strMensajeError , view: self)
                            }
                        }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeResponse.strMensajeError , view: self)
                            }

                        }
                    }
                    
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir Pagar Servicios Basicos Facilitp :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    
    
    
    //para Buscar la cuenta
    func posicionConsolidada( identificacion: String, tipoIdentificacion: String, usuario: String, token: String){
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/posicionConsolidada/\(_CONST.CANAL_DES)/\(identificacion)/\(tipoIdentificacion)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.posicionConsolidada = try JSONDecoder().decode([PosicionConsolidadaModel].self , from: data)
                    
                    if  self.posicionConsolidada.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            for item in self.posicionConsolidada{
                                if(item.Producto == _CONST.CNTA_DEBITO_VISTA  || item.Producto == _CONST.CNTA_DEBITO_KULLKIMIRAK || item.Producto == _CONST.CNTA_DEBITO_BASICA){
                                    self.posicionConsolidadaString.Cuenta = item.Cuenta
                                    self.posicionConsolidadaString.Descripcion = item.Descripcion
                                    self.posicionConsolidadaString.GProducto = item.GProducto
                                    self.posicionConsolidadaString.Producto = item.Producto
                                    self.posicionConsolidadaString.Saldoaportacion = item.Saldoaportacion
                                    self.posicionConsolidadaString.Saldobloqueado = item.Saldobloqueado
                                    self.posicionConsolidadaString.Saldocontable = item.Saldocontable
                                    self.posicionConsolidadaString.Saldoefectivo = item.Saldoefectivo
                                                                       self.posicionConsolidadaAuxiliar.append(self.posicionConsolidadaString)
                                }
                            }
                           
                            
                            if self.posicionConsolidadaAuxiliar.count > 1 {
                                let popUpCuentaViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpCuentaViewController") as!  PopUpCuentaViewController
                                
                                popUpCuentaViewController.popUpDelegate = self
                                popUpCuentaViewController.tipoSelec = _CONST.SELEC_CUENTA
                                popUpCuentaViewController.listaOpciones = self.posicionConsolidadaAuxiliar
                                self.present(popUpCuentaViewController, animated: true)
                            }else{
                                self.txtCuentaDebito.text = self.posicionConsolidadaString.Cuenta
                                //variable para comparacion si la deuda no excede es saldo disponible
                                self.saldoDisponible =  Double(self.posicionConsolidadaString.Saldoefectivo)!

                                let auxSaldoDisponible = NSMutableAttributedString()
                                
                                auxSaldoDisponible
                                    .bold("Saldo Disponible: $ ")
                                    .normal("\(self.posicionConsolidadaString.Saldoefectivo)")
                                self.saldoDisponibleLbl.attributedText = auxSaldoDisponible
                            }
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir Posicion Consolidada:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
}

extension ServiciosBasicosFacilitoViewController: PopUpDelegate{
    func seleccionString(value: String, tipoSelection: String, tipoCuenta: String, saldoDisponible: String) {
        
        if (tipoSelection == "SERVICIOS"){
            self.txtServicio.text = value
            for items in listaServicios{
                if value == items.nombre{
                    self.aucIdServicio = items.id
                    self.auxServicioNombre = items.nombre
                }
            }
        }
        if (tipoSelection == _CONST.SELEC_CUENTA){
            
            self.txtCuentaDebito.text = value
            
            self.saldoDisponible =  Double(saldoDisponible)!
            let auxSaldoDisponible = NSMutableAttributedString()
            auxSaldoDisponible
                .bold("Saldo Disponible: $ ")
                .normal("\(saldoDisponible)")
            self.saldoDisponibleLbl.attributedText = auxSaldoDisponible
            
        }
        
    }
    
}

extension ServiciosBasicosFacilitoViewController: OTPDelegate{
    func inserOTP(value: String) {
       
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        
        
        pagarSBF(idSolicitud: self.responseConsulta.id, operador: _CONST.OPERADOR_SB, idCanal: _CONST.INTERNET_BANKING, idAgencia: _CONST.ID_AGENCIA_SB, usuarioFIT: self.usuario.strIdentificacion, claveFIT: self.usuario.strIdentificacion, canalInterno: _CONST.CANAL_DES, ip: macAddress?.description ?? "" , identificacio: self.usuario.strIdentificacion, lisValoresPendientes: self.auxlisValoresPendientes, codigoOTP: value, numCuenta: txtCuentaDebito.text! , referencia: txtReferencia.text!, idServicio: self.aucIdServicio, nombreServicio: auxServicioNombre, informacionAdicional: self.auxInformacionAdicional, token: self.usuario.strToken)
       
       
    }
    
    
}
