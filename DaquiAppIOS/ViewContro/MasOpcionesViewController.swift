//
//  MasOpcionesViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 3/7/18.
//  Copyright © 2018 CoopDaquilema. All rights reserved.
//


import SwiftyXMLParser
import UIKit

class MasOpcionesViewController: UIViewController {

    @IBOutlet weak var lblBienvenida: UILabel!
    var usuario: UsuarioModel = UsuarioModel()
    let objActivity = activityIndicatorVC()
    
    @IBOutlet weak var btnBilleteraMovil: UIButton!
    var tieneBilleteraBimo = 0
    var cuentaBimo = ""
    var saldoCuentaBimo = ""
    
    @IBOutlet weak var btnPagoServicio: UIButton!
    @IBOutlet weak var btnPagoProgramado: UIButton!
    var posicionConsolidada: [PosicionConsolidadaModel] = []
    var posicionConsolidadaAuxiliar: [PosicionConsolidadaModel] = []
    var posicionConsolidadaString: PosicionConsolidadaModel = PosicionConsolidadaModel()
    var listaHistoricos: [ListaDePagosBimoModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad\\\\")
         usuario = (tabBarController as! TabBarViewController).usuario
        
        print("usuario \(usuario)")
        lblBienvenida.text = usuario.strNombreCompleto
        
        btnPagoServicio.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnPagoProgramado.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnBilleteraMovil.layer.cornerRadius = _CONST.CORNER_RADIUS
        
        if usuario.strIdentificacion.count > 10{
            print("es cc====")
            self.btnBilleteraMovil.isHidden = true
        }else{
            /*
            DispatchQueue.main.async {
               self.objActivity.showActivityIndicator(uiView: self.view)
            }
             self.accionBimo(strTrama: _CONST.VALIDA_CAMPO_RQ, telefonoOrigen: usuario.strTelefono, identificacion: usuario.strIdentificacion, strFiltro: "", strCriterio:"" ,token: usuario.strToken)
            */
            
            
            
        }
        
    }

    @IBAction func salir(_ sender: Any) {
        print("cerrar sesioooo-------")
        if(usuario.strNombreUser == _CONST.USUARIO_DEMO){
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: _CONST.AVISO, message: "HA CERRADO CON EXITO SU SESION" , preferredStyle: .alert)
                let actionAceptar = UIAlertAction(title: _CONST.ACEPTAR, style: .default) { (action:UIAlertAction) in
                    self.performSegue(withIdentifier: "unwindInicioSesion", sender: self)
                    //self.dismiss(animated: true, completion: nil)
                //self.navigationController?.popToRootViewController(animated: true)
                }
                alertController.addAction(actionAceptar)
                self.present(alertController, animated: true, completion: nil)
            }
        }else{
            self.objActivity.hideActivityIndicator(uiView: self.view)
            cerrarSesion(identificacion: usuario.strIdentificacion, token: usuario.strToken)
        }
    
    }
    func cerrarSesion(identificacion: String , token: String)-> Void{
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let json: [String: Any] = ["strIdentificacion": identificacion,
                                   "strToken": token,
                                   "strCanalSeguridad": _CONST.CANAL_SEG_IOS]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/cerrarSesion")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { data, response, error in
            
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK{
                        
                        
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: _CONST.AVISO, message: mensajeRespuestaData.strMensajeError , preferredStyle: .alert)
                            let actionAceptar = UIAlertAction(title: _CONST.ACEPTAR, style: .default) { (action:UIAlertAction) in
                                //self.dismiss(animated: true, completion: nil)
                                //self.navigationController?.popToRootViewController(animated: true)
                               // _ = self.navigationController?.popViewController(animated: true)
                           //self.performSegue(withIdentifier: "unwindToLogin", sender: self)
                                self.performSegue(withIdentifier: "unwindSeguePortal", sender: self)
                                
                            }
                            alertController.addAction(actionAceptar)
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }else{
                        if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                 Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir cerrar sesion:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
    }
    
    
    @IBAction func direccionarBimo(_ sender: Any) {
        print("direccionarBimo-----")
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
            self.accionBimo(strTrama: _CONST.OBTENER_CELULAR, telefonoOrigen: self.usuario.strTelefono, identificacion: self.usuario.strIdentificacion, strFiltro: _CONST.TELEFONO_BIMO, strCriterio:self.usuario.strTelefono, token: self.usuario.strToken)
        }
    }
    
    func abrirBimo(){
        print(self.tieneBilleteraBimo)
                print(self.tieneBilleteraBimo)
            if self.tieneBilleteraBimo == 0{
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_BIMO_ERROR_GENERICO, view: self)
                     
            }
        
            if self.tieneBilleteraBimo == 1{
                var cuentaEncontrada: Bool
                cuentaEncontrada = false
                print("self.cuentaBimo \(self.cuentaBimo)")
                for item in self.posicionConsolidadaAuxiliar{
                   if item.Cuenta == self.cuentaBimo{
                        cuentaEncontrada = true
                    }
                }
               
                print("cuentaEncontrada \(cuentaEncontrada)")
                if cuentaEncontrada {
                    performSegue(withIdentifier:"seguePrincipalBimo", sender: self)
                }else {
                    performSegue(withIdentifier:"segueAdminUsuarioBimo", sender: self)
                }
               
            }
            if self.tieneBilleteraBimo == 2{
                performSegue(withIdentifier:"segueAdminUsuarioBimo", sender: self)
            }
            if self.tieneBilleteraBimo == 3{
                performSegue(withIdentifier:"seguePrincipalBimo", sender: self)
            }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           
           if segue.identifier == "segueAdminUsuarioBimo" {
               
               if let destino = segue.destination as?  AdministracionUsuarioBimoViewController {
                destino.titulo = "Afiliación a Billetera Móvil"
                destino.posicionConsolidadaAuxiliar = self.posicionConsolidadaAuxiliar
                destino.origen = _CONST.BIMO_ORIGEN_INTERNO
             }
           }
            if segue.identifier == "seguePrincipalBimo"{
                if let destino = segue.destination as?  BillteraMovilViewController {
                    destino.estadoUsuario = tieneBilleteraBimo
                    destino.posicionConsolidadaAuxiliar = self.posicionConsolidadaAuxiliar
                    destino.cuentaBimo = self.cuentaBimo
                    destino.usuario = usuario
                    destino.listaHistoricos = self.listaHistoricos
                    destino.origen = _CONST.BIMO_ORIGEN_INTERNO
                }
                
            }
       }
    
    func buscarCuentaBimo() -> Void{
        self.accionBimo(strTrama: _CONST.CONSULTA_TEL_CUENTA_RQ, telefonoOrigen: usuario.strTelefono, identificacion: usuario.strIdentificacion, strFiltro: _CONST.TELEFONO_BIMO, strCriterio:usuario.strTelefono, token: usuario.strToken)
        
    }
    
    func accionBimo(strTrama: String, telefonoOrigen: String, identificacion: String,strFiltro: String, strCriterio: String , token: String)-> Void{
        let session = URLSession(
                   configuration: URLSessionConfiguration.ephemeral,
                   delegate: NSURLSessionPinningDelegate(),
                   delegateQueue: nil)
        
        let json: [String: Any] = [ "strIdentificacion": identificacion,
                                    "strToken": token,
                                    "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                    "strTrama": strTrama,
                                    "strTelefonoOrigen": telefonoOrigen,
                                    "strTelefonoDestino": "",
                                    "strDescripcion": "",
                                    "strMonto": "",
                                    "strAccion": "",
                                    "strFiltro": strFiltro,
                                    "strCriterio": strCriterio,
                                    "strNumeroCuenta": "",
                                    "strEndToEnd": ""
                                    
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/bimo/general")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { data, response, error in
        if let data = data, let response = response as? HTTPURLResponse, response.statusCode==202{
            do{
                let mensajeRespuestaData = try JSONDecoder().decode(BimoResultado.self , from: data)
                if  mensajeRespuestaData.strCodigo == _CONST.CODIGO_OK{
                    
                    DispatchQueue.main.sync {
                        let xml = try! XML.parse(mensajeRespuestaData.strXML)
                        print("mensajeRespuestaData----> \(mensajeRespuestaData)")
                        if let codResultado = xml["BimoXml", "codResultado"].text {
                            if strTrama == _CONST.VALIDA_CAMPO_RQ {
                                DispatchQueue.main.async {
                                       self.objActivity.hideActivityIndicator(uiView: self.view)
                                    
                                    if codResultado == _CONST.CODIGO_OK{
                                        print("tiene billera ")
                                        self.tieneBilleteraBimo = 1
                                        self.buscarCuentaBimo()
                                    }else  if codResultado == _CONST.BIMO_500{
                                        print("NO tiene billera ")
                                        self.posicionConsolidada(identificacion: identificacion, tipoIdentificacion: _CONST.TI_CEDULA, usuario: identificacion, token: token)
                                        self.tieneBilleteraBimo = 2
                                        
                                    }else  if codResultado == _CONST.BIMO_501{
                                        print("billera bloqueada")
                                        self.buscarCuentaBimo()
                                        self.tieneBilleteraBimo = 3
                                        
                                    }else{
                                        let mensajeError = xml["BimoXml", "mensajeResultado"].text
                                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeError! , view: self)
                                    }
                                }
                            }
                            if strTrama == _CONST.CONSULTA_TEL_CUENTA_RQ {
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    let cadenaCuenta: [String] = xml["BimoXml", "codResultadoAdicional"].text!.components(separatedBy: "|")
                                    self.cuentaBimo  = cadenaCuenta[3]
                                    self.posicionConsolidada(identificacion: identificacion, tipoIdentificacion: _CONST.TI_CEDULA, usuario: identificacion, token: token)
                                }
                                
                            }
                            if strTrama == _CONST.LISTA_HISTORICOS {
                                do {
                                    if codResultado == _CONST.CODIGO_OK{
                                         let jsonData = Data(xml["BimoXml", "mensajeResultado"].text!.utf8)
                                        self.listaHistoricos =  try JSONDecoder().decode([ListaDePagosBimoModel].self , from: jsonData)
                                       
                                    }else{
                                        let mensajeError = xml["BimoXml", "mensajeResultado"].text
                                        DispatchQueue.main.async {
                                            self.objActivity.hideActivityIndicator(uiView: self.view)
                                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeError! , view: self)
                                        }
                                    }
                                    
                                } catch {
                                    print(error.localizedDescription)
                                }
                                
                            }
                            if strTrama == _CONST.OBTENER_CELULAR {
                                
                                    if codResultado == _CONST.CODIGO_OK{
                                        self.tieneBilleteraBimo = 1
                                        let celular = xml["BimoXml", "mensajeResultado"].text
                                        self.usuario.strBilleteraMovil = celular!
                                        self.accionBimo(strTrama: _CONST.VALIDA_CAMPO_RQ, telefonoOrigen: self.usuario.strBilleteraMovil, identificacion: self.usuario.strIdentificacion, strFiltro: "", strCriterio:"" ,token: self.usuario.strToken)
                                    }else  if codResultado == _CONST.BIMO_1012{
                                        print("NO tiene billera ")
                                        self.posicionConsolidada(identificacion: identificacion, tipoIdentificacion: _CONST.TI_CEDULA, usuario: identificacion, token: token)
                                        print("NO ----")
                                        self.tieneBilleteraBimo = 2
                                     
                                    }else{
                                        let mensajeError = xml["BimoXml", "mensajeResultado"].text
                                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeError! , view: self)
                                    }
                                    
                            }
                        }
                    }
                }else{
                    if mensajeRespuestaData.strCodigo == _CONST.CODIGO_ERROR_TOKEN{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                             Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strCodigo , view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensaje , view: self)
                        }
                    }
                }
            }catch let error as NSError{
                print("Error al leer JSON: \(error)")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }else if let error = error{
            print("Error al consumir cerrar sesion:error: \(error)")
            if let error = error as NSError?, error.domain == NSURLErrorDomain{
                if error.code == -1001 {
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                    }
                } else if error.code == -1009 {
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }else{
            //No se obtubo ningun dato
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
        }
        }.resume()
    
    }
    
    //para Buscar la cuenta
    func posicionConsolidada( identificacion: String, tipoIdentificacion: String, usuario: String, token: String){
        print("posicionConsolidada--------->>")
        self.posicionConsolidadaAuxiliar.removeAll()
        self.posicionConsolidada.removeAll()
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/posicionConsolidada/\(_CONST.CANAL_DES)/\(identificacion)/\(tipoIdentificacion)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
            session.dataTask(with: url){(data, response, error) in
            let response = response as? HTTPURLResponse

            if let data = data,
                response?.statusCode==200{
                do{
                    self.posicionConsolidada = try JSONDecoder().decode([PosicionConsolidadaModel].self , from: data)
                    print("self.posicionConsolidada.count--------->> \(self.posicionConsolidada.count)")
                    if  self.posicionConsolidada.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.sync {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            for item in self.posicionConsolidada{
                                if(item.Producto == _CONST.CNTA_DEBITO_VISTA  || item.Producto == _CONST.CNTA_DEBITO_KULLKIMIRAK || item.Producto == _CONST.CNTA_DEBITO_BASICA){
                                    if (item.Trelacion=="PRI"){
                                    self.posicionConsolidadaString.Cuenta = item.Cuenta
                                    self.posicionConsolidadaString.Descripcion = item.Descripcion
                                    self.posicionConsolidadaString.GProducto = item.GProducto
                                    self.posicionConsolidadaString.Producto = item.Producto
                                    self.posicionConsolidadaString.Saldoaportacion = item.Saldoaportacion
                                    self.posicionConsolidadaString.Saldobloqueado = item.Saldobloqueado
                                    self.posicionConsolidadaString.Saldocontable = item.Saldocontable
                                    self.posicionConsolidadaString.Saldoefectivo = item.Saldoefectivo
                                    self.posicionConsolidadaAuxiliar.append(self.posicionConsolidadaString)
                                        
                                        print("self.posicionConsolidadaAuxiliar.count--------->> \(self.posicionConsolidadaAuxiliar.count)")
                                    }
                                }
                            }
                            
                            self.abrirBimo()
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                        }
                    }
                    
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                  Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
                }
            }else if let error = error{
                print("Error al consumir Posicion Consolidada:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
    }
    
    @IBAction func unwindOpciones(_ sender: UIStoryboardSegue) {
       /* print("unwindOpciones--")
        print("self.tieneBilleteraBimo) -- \(self.tieneBilleteraBimo)")
        
        performSegue(withIdentifier:"seguePrincipalBimo", sender: self)
        */
        print("usuario.strBilleteraMovil-- \(usuario.strBilleteraMovil)")
        self.viewDidLoad()
        
        
       /*
        if sender.source is AlertBimoViewController {
            print("viene de AlertBimoViewController")
                self.tieneBilleteraBimo = 2
                    viewDidLoad()
      
            }
        if sender.source is AlertAfiliacionBimoViewController {
            print("viene de AlertAfiliacionBimoViewController")
            performSegue(withIdentifier:"seguePrincipalBimo", sender: self)
        }
        */
       }
}
