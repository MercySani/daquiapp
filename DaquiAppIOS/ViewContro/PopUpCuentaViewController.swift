//
//  PopUpCuentaViewController.swift
//  DaquiApp
//
//  Created by Fredy on 7/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class PopUpCuentaViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource  {
    var listaOpciones: [PosicionConsolidadaModel] = []
    var listaCuentasSocio: [ListaCuentas] = []
    var auxOpciones: String = ""
    var tipoSelec: String = ""
    //vista de seleccion de cuenta
    var tipoCuenta: String = ""
    var saldoDisponible: String = ""
    //fin
    
    
    @IBOutlet weak var btnCancelar: UIButton!
    @IBOutlet weak var btnAceptar: UIButton!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var pickerOpciones: UIPickerView!
    
    var popUpDelegate: PopUpDelegate?
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var auxCount = 0
        if(tipoSelec == _CONST.SELEC_CUENTA)
        {
                auxCount = listaOpciones.count
        }
        if (tipoSelec == _CONST.SELEC_CUENTA_SOCIO) {
            auxCount = listaCuentasSocio.count
        }
        return auxCount
    }
    
    // cambiar para la vista
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
     return 100
     }
     
     func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
         let view = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 100))
        if tipoSelec == _CONST.SELEC_CUENTA{
           
            
            let topLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 15) )
            let auxCuenta = NSMutableAttributedString()
            auxCuenta
                .bold("Cuenta: ")
                .normal("\(self.listaOpciones[row].Cuenta)")
            topLabel.attributedText = auxCuenta
            topLabel.textAlignment = .justified
            view.addSubview(topLabel)
            
            let middleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 100))
            let auxTipo = NSMutableAttributedString()
            auxTipo
                .bold("Tipo: ")
                .normal("\(self.listaOpciones[row].Descripcion)")
            middleLabel.attributedText = auxTipo
            middleLabel.textAlignment = .justified
            view.addSubview(middleLabel)
            
            let bottonLabel = UILabel(frame: CGRect(x: 0, y: 78, width: 300, height: 15))
            let auxSaldoDisponible = NSMutableAttributedString()
            auxSaldoDisponible
                .bold("Saldo Disponible: $ ")
                .normal("\(self.listaOpciones[row].Saldoefectivo)")
            bottonLabel.attributedText = auxSaldoDisponible
            bottonLabel.textAlignment = .justified
            view.addSubview(bottonLabel)
        }
        if tipoSelec == _CONST.SELEC_CUENTA_SOCIO {
            let topLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 350, height: 30) )
            let auxCuenta = NSMutableAttributedString()
            auxCuenta
                .bold("Cuenta: ")
                .normal("\(self.listaCuentasSocio[row].numCuenta)")
            
            topLabel.attributedText = auxCuenta
            topLabel.textAlignment = .justified
            view.addSubview(topLabel)
            
            let middleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 350, height: 150))
            let auxTipo = NSMutableAttributedString()
            auxTipo
                .bold("Tipo: ")
                .normal("\(self.listaCuentasSocio[row].Descripcion)")
            
            middleLabel.attributedText = auxTipo
            middleLabel.textAlignment = .justified
            view.addSubview(middleLabel)
        }
        
     
     return view
     }
     // fin cambiar para la vista
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if tipoSelec == _CONST.SELEC_CUENTA{
            self.auxOpciones = self.listaOpciones[row].Cuenta
            self.tipoCuenta = self.listaOpciones[row].Descripcion
            self.saldoDisponible = self.listaOpciones[row].Saldoefectivo
        }
        if tipoSelec == _CONST.SELEC_CUENTA_SOCIO{
            self.auxOpciones = self.listaCuentasSocio[row].numCuenta
            self.tipoCuenta = self.listaCuentasSocio[row].Descripcion
        }
    }
    
    @IBAction func aceptarBtn(_ sender: Any) {
        var row = pickerOpciones.selectedRow(inComponent: 0)
        
        pickerView(pickerOpciones, didSelectRow: row, inComponent: 0)
        popUpDelegate?.seleccionString(value: self.auxOpciones, tipoSelection: self.tipoSelec, tipoCuenta: self.tipoCuenta, saldoDisponible: self.saldoDisponible )
        dismiss(animated: true)
    }
    
    @IBAction func cancelarBtn(_ sender: Any) {
        dismiss(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnAceptar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnCancelar.layer.cornerRadius = _CONST.CORNER_RADIUS
        
    }

 
}
