//
//  AgenciasContactosViewController.swift
//  DaquiApp
//
//  Created by Fredy on 24/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit
import MapKit

class AgenciasContactosViewController: UIViewController, MKMapViewDelegate {

    
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        mapView.delegate = self
        //AGENCIA LIBERTAD
        let libertad = self.localizacion(latitud: -2.2249312582148923, longitud: -80.90765945126674)
        var annotation = MKPointAnnotation() //marcadorMapa()
        annotation.coordinate = libertad
        annotation.title = "AGENCIA LIBERTAD"
        annotation.subtitle = "Avenida 7ma. entre las calles 21 y 22 Frente al mercado 'Unidos Venceremos'. Telf: 032962706 ext 1001-0993004830 - 09988710077 "
        
        //annotation.image = "isotipo coac daquilema.png"
        
        mapView.addAnnotation(annotation)
        
        
        
        //Agencia Guayaquil Norte
        let guayaquilN = self.localizacion(latitud: -2.1001970, longitud: -79.9373450)
        annotation = MKPointAnnotation() //marcadorMapa()
        annotation.coordinate = guayaquilN
        annotation.title = "AGENCIA GUAYAQUIL NORTE"
        annotation.subtitle = "AGENCIA GUAYAQUIL NORTE Km 11.5 - Via a Daule Parque California. Local R7-8. Telf: 042103049 - 0988937911 "
        mapView.addAnnotation(annotation)
        
        //Agencia Guayaquil Centro
        let guayaquilC = self.localizacion(latitud: -2.191107, longitud: -79.887511)
        annotation = MKPointAnnotation()//marcadorMapa()
        annotation.coordinate = guayaquilC
        annotation.title = "AGENCIA GUAYAQUIL CENTRO"
        annotation.subtitle = "Jose Velez entre Lorenzo de Garaicoa y 6 de Marzo, Frente al parque Centenario Telf: 042516794 - 0988432690 "
        mapView.addAnnotation(annotation)
        
        //Agencia Machala
        let machala = self.localizacion(latitud: -3.2596620, longitud: -79.9590360)
        annotation = MKPointAnnotation()//marcadorMapa()
        annotation.coordinate = machala
        annotation.title = "AGENCIA MACHALA"
        annotation.subtitle = "Rocafuerte N. 904 - C, entre  Montalvo y 9 de Mayo, a media cuadra del 'Parque Central' Telf: 072923730 Ext 1300 "
        mapView.addAnnotation(annotation)
        
        //Agencia Santo Domingo
        let stoDomingo = self.localizacion(latitud: -0.2558010, longitud: -79.1722570)
        annotation = MKPointAnnotation()//marcadorMapa()
        annotation.coordinate = stoDomingo
        annotation.title = "AGENCIA STO DOMINGO"
        annotation.subtitle = "Av. Galapagos y Ambato Sector Centro Diagonal a la parada de Buses 'SANTA MARTHA' Telf: 022761682 - 022768223 - 0990980929"
        mapView.addAnnotation(annotation)
        
        //Agencia QUITO NORTE
        let quitoN = self.localizacion(latitud: -0.1175860, longitud: -78.4910710)
        annotation = MKPointAnnotation()//marcadorMapa()
        annotation.coordinate = quitoN
        annotation.title = "AGENCIA QUITO NORTE"
        annotation.subtitle = "Av. Diego de Vásquez  de Cepeda N 508 y Nazacota Puento 2 cuadras al sur del MERCADO COTOCOLLAO Telf: 022291507 Cel:0988313869"
        mapView.addAnnotation(annotation)
        
        //Agencia QUITO CENTRO
        let quitoC = self.localizacion(latitud: -0.2154760, longitud: -78.5161930)
        annotation = MKPointAnnotation()//marcadorMapa()
        annotation.coordinate = quitoC
        annotation.title = "AGENCIA QUITO CENTRO"
        annotation.subtitle = "José López 5-97 entre Hno. Miguel y Mejia frente al C.C. NUEVO AMANECER Telf: 022954724  Cel: 0990808947"
        mapView.addAnnotation(annotation)
        
        //Agencia QUITO SUR
        let quitoS = self.localizacion(latitud: -0.2628140, longitud: -78.5488170)
        annotation = MKPointAnnotation()//marcadorMapa()
        annotation.coordinate = quitoS
        annotation.title = "AGENCIA QUITO SUR"
        annotation.subtitle = "Av. Mariscal Sucre 2351 y Las Pampas Sector LA GATAZO Telf: 022845932  Cel: 09981941128"
        mapView.addAnnotation(annotation)
        
        //Agencia LATACUNGA
        let latacunga = self.localizacion(latitud: -0.931704, longitud: -78.615670)
        annotation = MKPointAnnotation()//marcadorMapa()
        annotation.coordinate = latacunga
        annotation.title = "AGENCIA LATACUNGA"
        annotation.subtitle = "Calle Quito y Guayaquil N 16-158  Telf: 032962706  Ext 1600"
        mapView.addAnnotation(annotation)
        
        //Agencia AMBATO
        let ambato = self.localizacion(latitud: -1.2439720, longitud: -78.6267150)
        annotation = MKPointAnnotation() //marcadorMapa()
        annotation.coordinate = ambato
        annotation.title = "AGENCIA AMBATO"
        annotation.subtitle = "12 de noviembre 16-41 entre Mera y Montalvo Telf: 032962706  Ext 1500"
        mapView.addAnnotation(annotation)
        
        //Agencia Riobamba Condamine
        let riobambaConda = self.localizacion(latitud: -1.6712690, longitud: -78.6562450)
        annotation = MKPointAnnotation() //marcadorMapa()
        annotation.coordinate = riobambaConda
        annotation.title = "AGENCIA RIOBAMBA LA CONDAMINE"
        annotation.subtitle = "Juan Lavalle y Chile. Sector C.C LA CONDAMINE Telf: 032962706  Ext 602"
        mapView.addAnnotation(annotation)
        
        //Agencia Riobamba Matriz
        let riobambaMatriz = self.localizacion(latitud: -1.6710740, longitud: -78.6495590)
        annotation = MKPointAnnotation()//marcadorMapa()
        annotation.coordinate = riobambaMatriz
        annotation.title = "AGENCIA MATRIZ"
        annotation.subtitle = " Veloz N 24 - 16 entre Larrea y España. Telf: 032962706 - 2947092 - 2951452"
        mapView.addAnnotation(annotation)
        
        //Agencia Guamote
        let guamote = self.localizacion(latitud: -1.9344080, longitud: -78.7071030)
        annotation = MKPointAnnotation()//marcadorMapa()
        annotation.coordinate = guamote
        annotation.title = "AGENCIA GUAMOTE"
        annotation.subtitle = "Av. Panamericana Sur y Av. Macas frente a la Gasolina Petrocomercial. Telf: 032962706 Ext 1100"
        mapView.addAnnotation(annotation)
        
        //Agencia Alausi
        let alausi = self.localizacion(latitud: -2.20531110, longitud: -78.8475630)
        annotation = MKPointAnnotation()//marcadorMapa()
        annotation.coordinate = alausi
        annotation.title = "AGENCIA ALAUSI"
        annotation.subtitle = "Av. 5 de Junio y Carlos Catanni  Telf: 032962706 Ext 1200"
        mapView.addAnnotation(annotation)
        
        //Agencia Cañar
        let canar = self.localizacion(latitud: -2.5594380, longitud: -78.9376000)
        annotation = MKPointAnnotation()//marcadorMapa()
        annotation.coordinate = canar
        annotation.title = "AGENCIA CANAR"
        annotation.subtitle = "Pichincha y Borrero  Telf: 032962706 Ext 1400"
        mapView.addAnnotation(annotation)
        
        
        //Redirecciones
        
    }
    
    
    func localizacion(latitud: CLLocationDegrees , longitud: CLLocationDegrees) -> CLLocationCoordinate2D {
        var localizacion: CLLocationCoordinate2D
        localizacion = CLLocationCoordinate2DMake(latitud, longitud)
        return localizacion
    }
    
   // @available(iOS 10.0, *)
    @IBAction func redireccionarPaginaWeb(_ sender: UIButton) {
        
        
        switch sender.tag {
        case 1:
            let paginaWeb: NSURL = NSURL(string: "https://es-la.facebook.com/coopdaquilema/")!
            UIApplication.shared.openURL(paginaWeb as URL)
            break
        case 2:
            let paginaWeb: NSURL = NSURL(string: "https://twitter.com/coopdaquilema?lang=es")!
            UIApplication.shared.openURL(paginaWeb as URL)
            break
        case 3:
            let paginaWeb: NSURL = NSURL(string: "https://www.youtube.com/channel/UCClky-kMi42UPOgw3SgHsew/videos")!
            UIApplication.shared.openURL(paginaWeb as URL)
            break
        case 4:
            let paginaWeb: NSURL = NSURL(string: "http://www.coopdaquilema.com")!
            UIApplication.shared.openURL(paginaWeb as URL)
            break
            
        default:
            break
        }
        
    }
    
    
    
    
   

   
    
    
   

}
