//
//  HabilitacionCorrectaViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 18/12/2019.
//  Copyright © 2019 CoopDaquilema. All rights reserved.
//

import Foundation
import UIKit
class HabilitacionCorrectaViewController: UIViewController{
    var mensajeCorreo :String = ""
    
    @IBOutlet weak var btnAceptar: UIButton!
    
    
    @IBOutlet weak var lblMensajeCorreo: UILabel!
    
    @IBAction func btnAceptar(_ sender: Any) {
        self.performSegue(withIdentifier: "unwindSeguePortal", sender: self)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblMensajeCorreo.text="Te enviamos un correo a \(mensajeCorreo) con las credenciales para que puedas acceder a tu banca electrónica"
        self.navigationController?.navigationItem.hidesBackButton = true
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
}
