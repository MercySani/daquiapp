//
//  CategoriaFacilitoSRViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 11/10/18.
//  Copyright © 2018 CoopDaquilema. All rights reserved.
//

import UIKit

class CategoriaFacilitoSRViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    //variables
    let objActivity = activityIndicatorVC()
    var usuario: UsuarioModel = UsuarioModel()
    var listaCategoria: [CategoriaSRModel] = []
    var listaCategoriaRes: [CategoriaSRModel] = []
    var isRecarga: Bool = false
    @IBOutlet weak var categoriaTableView: UITableView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        usuario = (tabBarController as! TabBarViewController).usuario
        self.navigationController?.navigationBar.tintColor =
        UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        listaCategoriaSR(usuario: usuario.strIdentificacion, token: usuario.strToken)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func listaCategoriaSR(usuario: String, token: String)->Void{
        
        self.listaCategoria.removeAll()
        self.listaCategoriaRes.removeAll()
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let urlString = "\(_CONST.DIRECCION)/listaCategorias/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)/\(_CONST.FRECUENCIA_DE_PAGO)"
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let _ = response as? HTTPURLResponse{
                do{
                    
                    self.listaCategoria =  try JSONDecoder().decode([CategoriaSRModel].self , from: data)
                    if  self.listaCategoria.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            self.listaCategoriaRes = self.listaCategoria
                            
                            self.categoriaTableView.reloadData()
                            
                            
                            /*let pickerPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickerPopUpViewController") as! PickerPopUpViewController
                            pickerPopupViewController.popUpDelegate = self
                            pickerPopupViewController.tipoSelec = "CATEGORIA"
                            pickerPopupViewController.listaCategoria = self.listaCategoria
                            self.present(pickerPopupViewController, animated: true)*/
                            
                        }
                    }else{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "No existe categorias " , view: self)
                        }
                    }
                }catch{
                    do {
                        
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                        
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "No existe categorias", view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir lista Categoria SR:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listaCategoriaRes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoriaCell") as! GenericoTableViewCell
        let auxDescripcion = NSMutableAttributedString()
        auxDescripcion  .normal(self.listaCategoriaRes[indexPath.row].descripcion)
        cell.detalle1.attributedText = auxDescripcion
    return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.listaCategoriaRes[indexPath.row].id == _CONST.ID_RECARGAS){
            
            self.isRecarga = true
            performSegue(withIdentifier: "recargasSegue", sender: self)
            
        }else{
            self.isRecarga = false
            performSegue(withIdentifier: "categoriaSegue", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if(isRecarga){
            if let destination =  segue.destination as? RecargasSRViewController {
                destination.auxIdCategoria = listaCategoriaRes[(categoriaTableView.indexPathForSelectedRow?.row)!].id
                
            }
        }else{
            if let destination =  segue.destination as? ServiciosBasicosFacilitoViewController {
                destination.auxIdCategoria = listaCategoriaRes[(categoriaTableView.indexPathForSelectedRow?.row)!].id
                
            }
        }
        
    }
    

}
