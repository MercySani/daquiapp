//
//  AfiliacionBimoExternoViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 03/06/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import Foundation
import UIKit
import SwiftyXMLParser


class AfiliacionBimoExternoViewController: UIViewController, UITextFieldDelegate {
    
    
    // variables
    var usuario: ResultadoRestful = ResultadoRestful()
    let objActivity = activityIndicatorVC()
    
    var posicionConsolidada: [PosicionConsolidadaModel] = []
    var posicionConsolidadaAuxiliar: [PosicionConsolidadaModel] = []
    var posicionConsolidadaString: PosicionConsolidadaModel = PosicionConsolidadaModel()
    
    var cuentaSeleccionada = ""
    var auxCount = 0
    
    @IBOutlet weak var lblIdentificacion: UILabel!
    @IBOutlet weak var lblCelular: UILabel!
    @IBOutlet weak var txtCuenta: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // consulta posicion consolidada
        lblIdentificacion.text = usuario.strIdentificacion
        lblCelular.text = usuario.strTelefono
        self.navigationItem.setHidesBackButton(true, animated: true)
         self.posicionConsolidada(identificacion: usuario.strTelefono, tipoIdentificacion: _CONST.TI_CEDULA, usuario: usuario.strTelefono, token: usuario.strToken)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           txtCuenta.resignFirstResponder()
           self.view.endEditing(true)
    }
    
    @IBAction func selectCuenta(_ sender: Any) {
        print("seleccionar cuenta")
            DispatchQueue.main.async {
                             self.view.endEditing(true)
                         }
                  guard !(posicionConsolidadaAuxiliar.count == 0 && auxCount == 0) else {
                      auxCount = 1
                      DispatchQueue.main.async {
                          Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "Debe realizar la busqueda de la cuenta" , view: self)
                      }
                      return
                  }
                  guard posicionConsolidadaAuxiliar.count == 0 else {
                      let popUpCuentaViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpCuentaViewController") as!  PopUpCuentaViewController
                      
                      popUpCuentaViewController.popUpDelegate = self
                      popUpCuentaViewController.tipoSelec = _CONST.SELEC_CUENTA
                      popUpCuentaViewController.listaOpciones = self.posicionConsolidadaAuxiliar
                      self.present(popUpCuentaViewController, animated: true)
                      return
                  }
    }
    
    @IBAction func accionAfiliar(_ sender: Any) {
        //control q exista cuenta
        guard !(txtCuenta.text!.isEmpty) else {
               DispatchQueue.main.async {
                  Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_BIMO_ERROR_CUENTA_AFILIACION , view: self)
            }
            return
        }
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        self.accionBimo(strTrama: _CONST.AFILIACION_IFI_RQ, telefonoOrigen: usuario.strTelefono, identificacion: usuario.strTelefono , strNumeroCuenta: cuentaSeleccionada,token: usuario.strToken)
         
    
    }
    
    
    func accionBimo(strTrama: String, telefonoOrigen: String, identificacion: String , strNumeroCuenta: String,token: String)-> Void{
           let session = URLSession(
                      configuration: URLSessionConfiguration.ephemeral,
                      delegate: NSURLSessionPinningDelegate(),
                      delegateQueue: nil)
           
           let json: [String: Any] = [ "strIdentificacion": identificacion,
                                       "strToken": token,
                                       "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                       "strCanalSeguridad": _CONST.CANAL_SEG_IOS_BIMO,
                                       "strTrama": strTrama,
                                       "strTelefonoOrigen": telefonoOrigen,
                                       "strTelefonoDestino": "",
                                       "strDescripcion": "",
                                       "strMonto": "",
                                       "strAccion": "",
                                       "strFiltro": "",
                                       "strCriterio": "",
                                       "strNumeroCuenta": strNumeroCuenta,
                                       "strEndToEnd": ""
                                       
           ]
           
           let jsonData = try? JSONSerialization.data(withJSONObject: json)
           let url = URL(string: "\(_CONST.DIRECCION)/bimo/general")!
           var request = URLRequest(url: url)
           request.httpMethod = "POST"
           request.httpBody = jsonData
           session.dataTask(with: request) { data, response, error in
           if let data = data, let response = response as? HTTPURLResponse, response.statusCode==202{
               do{
                   let mensajeRespuestaData = try JSONDecoder().decode(BimoResultado.self , from: data)
                   if  mensajeRespuestaData.strCodigo == _CONST.CODIGO_OK{
                        DispatchQueue.main.sync {
                            let xml = try! XML.parse(mensajeRespuestaData.strXML)
                                if let codResultado = xml["BimoXml", "codResultado"].text {
                                     if codResultado == "0"{
                                        print("afiliacion exitosa")
                                        DispatchQueue.main.async {
                                        self.objActivity.hideActivityIndicator(uiView: self.view)
                                        //self.dismiss(animated: true, completion: nil)
                                        //self.performSegue(withIdentifier:"segueDefinirClave", sender: self)
                                            
                                        let definirClaveBimoExternoViewController = self.storyboard?.instantiateViewController(withIdentifier: "DefinirClaveBimoExternoViewController") as!  DefinirClaveBimoExternoViewController
                                            definirClaveBimoExternoViewController.usuario = self.usuario
                                            definirClaveBimoExternoViewController.recuperar = "false"; self.present(definirClaveBimoExternoViewController, animated: true)
                                        
                                        }
                                      }else{
                                          DispatchQueue.main.async {
                                            self.objActivity.hideActivityIndicator(uiView: self.view)
                                            let mensajeError = xml["BimoXml", "mensajeResultado"].text
                                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeError!, view: self)
                                          }
                                   }               
                                 }
                            }
                   }else{
                       if mensajeRespuestaData.strCodigo == _CONST.CODIGO_ERROR_TOKEN{
                           DispatchQueue.main.async {
                               self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strCodigo , view: self)
                           }
                       }else{
                           DispatchQueue.main.async {
                               self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensaje , view: self)
                           }
                       }
                   }
               }catch let error as NSError{
                   print("Error al leer JSON: \(error)")
                   DispatchQueue.main.async {
                       self.objActivity.hideActivityIndicator(uiView: self.view)
                       Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                   }
               }
           }else if let error = error{
               print("Error al consumir cerrar sesion:error: \(error)")
               if let error = error as NSError?, error.domain == NSURLErrorDomain{
                   if error.code == -1001 {
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                       }
                   } else if error.code == -1009 {
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                       }
                   }else{
                       DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                       }
                   }
               }else{
                   DispatchQueue.main.async {
                       self.objActivity.hideActivityIndicator(uiView: self.view)
                       Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                   }
               }
           }else{
               //No se obtubo ningun dato
               DispatchQueue.main.async {
                   self.objActivity.hideActivityIndicator(uiView: self.view)
                   Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
               }
           }
           }.resume()
       
       }
       
      //para Buscar la cuenta
      func posicionConsolidada( identificacion: String, tipoIdentificacion: String, usuario: String, token: String){
          let session = URLSession(
              configuration: URLSessionConfiguration.ephemeral,
              delegate: NSURLSessionPinningDelegate(),
              delegateQueue: nil)
          
          let urlString = "\(_CONST.DIRECCION)/posicionConsolidada/\(_CONST.CANAL_DES)/\(identificacion)/\(tipoIdentificacion)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS_BIMO)"
          guard let url = URL(string: urlString) else {
              
              DispatchQueue.main.async {
                  self.objActivity.hideActivityIndicator(uiView: self.view)
                  Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
              }
              return}
              session.dataTask(with: url){(data, response, error) in
              let response = response as? HTTPURLResponse

              if let data = data,
                  response?.statusCode==200{
                  do{
                      self.posicionConsolidada = try JSONDecoder().decode([PosicionConsolidadaModel].self , from: data)
                      if  self.posicionConsolidada.count != _CONST.LISTA_VACIA{
                          DispatchQueue.main.async {
                              self.objActivity.hideActivityIndicator(uiView: self.view)
                              for item in self.posicionConsolidada{
                                  if(item.Producto == _CONST.CNTA_DEBITO_VISTA  || item.Producto == _CONST.CNTA_DEBITO_KULLKIMIRAK || item.Producto == _CONST.CNTA_DEBITO_BASICA){
                                    if (item.Trelacion=="PRI"){
                                      self.posicionConsolidadaString.Cuenta = item.Cuenta
                                      self.posicionConsolidadaString.Descripcion = item.Descripcion
                                      self.posicionConsolidadaString.GProducto = item.GProducto
                                      self.posicionConsolidadaString.Producto = item.Producto
                                      self.posicionConsolidadaString.Saldoaportacion = item.Saldoaportacion
                                      self.posicionConsolidadaString.Saldobloqueado = item.Saldobloqueado
                                      self.posicionConsolidadaString.Saldocontable = item.Saldocontable
                                      self.posicionConsolidadaString.Saldoefectivo = item.Saldoefectivo
                                      self.posicionConsolidadaAuxiliar.append(self.posicionConsolidadaString)
                                    }
                                    print("item----> \(item)")
                                  }
                              }
                            
                            if self.posicionConsolidadaAuxiliar.count > 1 {
                                let popUpCuentaViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpCuentaViewController") as!  PopUpCuentaViewController
                                
                                popUpCuentaViewController.popUpDelegate = self
                                popUpCuentaViewController.tipoSelec = _CONST.SELEC_CUENTA
                                popUpCuentaViewController.listaOpciones = self.posicionConsolidadaAuxiliar
                                self.present(popUpCuentaViewController, animated: true)
                            }else{
                                self.txtCuenta.text = self.posicionConsolidadaString.Cuenta
                                let auxTipoCuenta = NSMutableAttributedString()
                                auxTipoCuenta
                                    .bold("Tipo: ")
                                    .normal("\(self.posicionConsolidadaString.Descripcion)")
                                self.cuentaSeleccionada = self.posicionConsolidadaString.Cuenta
                                self.txtCuenta.isUserInteractionEnabled = false
                            }
                          }
                      }else{
                          DispatchQueue.main.async {
                              self.objActivity.hideActivityIndicator(uiView: self.view)
                              Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                          }
                      }
                      
                  }catch{
                      do {
                          let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                          
                          if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                              DispatchQueue.main.async {
                                  self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSesionBimo(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                              }
                          }else{
                              DispatchQueue.main.async {
                                  self.objActivity.hideActivityIndicator(uiView: self.view)
                                  Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                              }
                          }
                      }catch let error as NSError{
                      print("Error al leer JSON: \(error)")
                      DispatchQueue.main.async {
                          self.objActivity.hideActivityIndicator(uiView: self.view)
                          Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                      }
                  }
                  }
              }else if let error = error{
                  print("Error al consumir Posicion Consolidada:error: \(error)")
                  if let error = error as NSError?, error.domain == NSURLErrorDomain{
                      if error.code == -1001 {
                          DispatchQueue.main.async {
                              self.objActivity.hideActivityIndicator(uiView: self.view)
                              Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                          }
                      } else if error.code == -1009 {
                          DispatchQueue.main.async {
                              self.objActivity.hideActivityIndicator(uiView: self.view)
                              Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                          }
                      }else{
                          DispatchQueue.main.async {
                              self.objActivity.hideActivityIndicator(uiView: self.view)
                              Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                          }
                      }
                  }else{
                      DispatchQueue.main.async {
                          self.objActivity.hideActivityIndicator(uiView: self.view)
                          Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                      }
                  }
              }else{
                  //No se obtubo ningun dato
                  DispatchQueue.main.async {
                      self.objActivity.hideActivityIndicator(uiView: self.view)
                      Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                  }
              }
              }.resume()
      }
    
    
}

extension AfiliacionBimoExternoViewController: PopUpDelegate{
    func seleccionString(value: String, tipoSelection: String, tipoCuenta: String, saldoDisponible: String) {
        if (tipoSelection == _CONST.SELEC_CUENTA){
            self.txtCuenta.text = value
            self.cuentaSeleccionada = value
        }
        
    }
}
    
   
    



