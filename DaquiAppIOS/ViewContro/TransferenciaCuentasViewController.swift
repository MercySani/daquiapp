//
//  TransferenciaViewController.swift
//  DaquiApp
//
//  Created by Desarrollador on 26/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

//class TransferenciaCuentasViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating {
class TransferenciaCuentasViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var usuario: UsuarioModel = UsuarioModel()
    var cuentasInternas: [CuentaInternaModel] = []
    var cuentasExternas: [CuentaExternaModel] = []
    var cuentasTarjetas: [CuentaExternaModel] = []
    var cuentasInternasRes: [CuentaInternaModel] = []
    var cuentasExternasRes: [CuentaExternaModel] = []
    var cuentasTarjetaRes: [CuentaExternaModel] = []
    
    var auxCuentasExternas:[CuentaExternaModel] = []
    var esInternaExternaTarjeta: Int = 1;
    
    let searchController = UISearchController(searchResultsController: nil)
    let objActivity = activityIndicatorVC()
    @IBOutlet weak var cuentasTransTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        usuario = (tabBarController as! TabBarViewController).usuario
        
        guard usuario.strNombreUser == _CONST.USUARIO_DEMO else {
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            consultaCuentasInternas(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_CEDULA, token: usuario.strToken)
            return
        }
        
        setUpSearchBar()
        /*searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Filtre sus cuentas"
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        cuentasTransTableView.tableHeaderView = searchController.searchBar*/
    }
    
    
    
    
    private func setUpSearchBar() {
        searchBar.delegate = self
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var auxCount: Int = 0
        if esInternaExternaTarjeta == 1{
            auxCount = self.cuentasInternasRes.count
        }
        if esInternaExternaTarjeta == 2{
            auxCount = self.cuentasExternasRes.count
        }
        if esInternaExternaTarjeta == 3 {
            auxCount = self.cuentasTarjetaRes.count
        }
        
        return auxCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cuentaTransCell") as! GenericoTableViewCell
        
        if esInternaExternaTarjeta == 1 {
            let auxNumCuenta = NSMutableAttributedString()
            auxNumCuenta .bold("Cuenta: ") .normal(self.cuentasInternasRes[indexPath.row].numCuenta)
            cell.titulo.attributedText = auxNumCuenta
            
            let auxNombre = NSMutableAttributedString()
            auxNombre .bold("Nombre: ") .normal(self.cuentasInternasRes[indexPath.row].nombre)
            cell.detalle1.attributedText = auxNombre
            
            let auxIdentificacion = NSMutableAttributedString()
            auxIdentificacion .bold("Identificación: ") .normal(self.cuentasInternasRes[indexPath.row].identificacion)
            cell.detalle2.attributedText = auxIdentificacion
            
            let auxEmail = NSMutableAttributedString()
            auxEmail .bold("Email: ") .normal(self.cuentasInternasRes[indexPath.row].email)
            cell.detalle3.attributedText = auxEmail
            
            let auxComentario = NSMutableAttributedString()
            auxComentario .bold("Comentarios: ") .normal(self.cuentasInternasRes[indexPath.row].comentario)
            cell.detalle4.attributedText = auxComentario
            cell.detalle5.isHidden = true
            cell.detalle6.isHidden = true
        }
        if esInternaExternaTarjeta == 2
        {
            cell.detalle5.isHidden = false
            cell.detalle6.isHidden = false
            cell.titulo.text = self.cuentasExternasRes[indexPath.row].ifiCE.descripcion
            
            let auxNombre = NSMutableAttributedString()
            auxNombre .bold("Nombre: ") .normal(self.cuentasExternasRes[indexPath.row].nombre)
            cell.detalle1.attributedText = auxNombre
            
            let auxNumCuenta = NSMutableAttributedString()
            auxNumCuenta .bold("Cuenta: ") .normal(self.cuentasExternasRes[indexPath.row].numerocuenta)
            cell.detalle2.attributedText = auxNumCuenta
            
            let auxIdentificacion = NSMutableAttributedString()
            auxIdentificacion .bold("Identificación: ") .normal(self.cuentasExternasRes[indexPath.row].identificacion)
            cell.detalle3.attributedText = auxIdentificacion
            
            let auxEmail = NSMutableAttributedString()
            auxEmail .bold("Email: ") .normal(self.cuentasExternasRes[indexPath.row].email)
            cell.detalle4.attributedText = auxEmail
            
            let auxDescripcion = NSMutableAttributedString()
            auxDescripcion .bold("Descripción: ") .normal(self.cuentasExternasRes[indexPath.row].tipocuentaCE.descripcion)
            cell.detalle5.attributedText = auxDescripcion
            
            let auxComentario = NSMutableAttributedString()
            auxComentario .bold("Comentario: ") .normal(self.cuentasExternasRes[indexPath.row].comentario)
            cell.detalle6.attributedText = auxComentario
        }
        if esInternaExternaTarjeta == 3{
            
            cell.detalle5.isHidden = false
            cell.detalle6.isHidden = false
            cell.titulo.text = self.cuentasTarjetaRes[indexPath.row].ifiCE.descripcion
            
            let auxNombre = NSMutableAttributedString()
            auxNombre .bold("Nombre: ") .normal(self.cuentasTarjetaRes[indexPath.row].nombre)
            cell.detalle1.attributedText = auxNombre
            
            let auxNumCuenta = NSMutableAttributedString()
            auxNumCuenta .bold("Cuenta: ") .normal(self.cuentasTarjetaRes[indexPath.row].numerocuenta)
            cell.detalle2.attributedText = auxNumCuenta
            
            let auxIdentificacion = NSMutableAttributedString()
            auxIdentificacion .bold("Identificación: ") .normal(self.cuentasTarjetaRes[indexPath.row].identificacion)
            cell.detalle3.attributedText = auxIdentificacion
            
            let auxEmail = NSMutableAttributedString()
            auxEmail .bold("Email: ") .normal(self.cuentasTarjetaRes[indexPath.row].email)
            cell.detalle4.attributedText = auxEmail
            
            let auxDescripcion = NSMutableAttributedString()
            auxDescripcion .bold("Descripción: ") .normal(self.cuentasTarjetaRes[indexPath.row].tipocuentaCE.descripcion)
            cell.detalle5.attributedText = auxDescripcion
            
            
            
            let auxComentario = NSMutableAttributedString()
            auxComentario .bold("Comentario: ") .normal(self.cuentasTarjetaRes[indexPath.row].comentario)
            cell.detalle6.attributedText = auxComentario
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "mostrarTransferencia", sender: self)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    
    // Search Bar
       func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if esInternaExternaTarjeta == 1 {
        cuentasInternasRes = cuentasInternas.filter({ CuentaInternaModel -> Bool in
            
                           //cuentasInternasRes = cuentasInternas.filter {
                            if searchText.isEmpty {
                                cuentasInternasRes = cuentasInternas
                                return true
                            }
            return CuentaInternaModel.nombre.lowercased().contains(searchText.lowercased()) ||
                CuentaInternaModel.numCuenta.lowercased().contains(searchText.lowercased()) ||
                CuentaInternaModel.identificacion.lowercased().contains(searchText.lowercased())
                           //}
        })}
            if esInternaExternaTarjeta == 2
            {
                    cuentasExternasRes = auxCuentasExternas.filter({CuentaExternaModel -> Bool in
                        if searchText.isEmpty {
                            cuentasInternasRes = cuentasInternas
                            return true
                        }
                        return CuentaExternaModel.nombre.lowercased().contains(searchText.lowercased()) ||
                            CuentaExternaModel.numerocuenta.lowercased().contains(searchText.lowercased()) ||
                            CuentaExternaModel.identificacion.lowercased().contains(searchText.lowercased())
                    })
            }
            if esInternaExternaTarjeta == 3 {
      
                    cuentasTarjetaRes = cuentasTarjetas.filter({ CuentaExternaModel -> Bool in
                            if searchText.isEmpty {
                                                       cuentasInternasRes = cuentasInternas
                                                       return true
                                                   }
                        return CuentaExternaModel.nombre.lowercased().contains(searchText.lowercased()) ||
                            CuentaExternaModel.numerocuenta.lowercased().contains(searchText.lowercased()) ||
                            CuentaExternaModel.identificacion.lowercased().contains(searchText.lowercased())
                    })
              
                
            }
        self.cuentasTransTableView.reloadData()
       }
       
       
    
    
    /*func updateSearchResults(for searchController: UISearchController) {
        if esInternaExternaTarjeta == 1 {
            if searchController.searchBar.text! == "" {
                cuentasInternasRes = cuentasInternas
            } else {
                cuentasInternasRes = cuentasInternas.filter {
                    $0.nombre.lowercased().contains(searchController.searchBar.text!.lowercased()) ||
                    $0.numCuenta.lowercased().contains(searchController.searchBar.text!.lowercased()) ||
                    $0.identificacion.lowercased().contains(searchController.searchBar.text!.lowercased())
                }
            }
        }
        if esInternaExternaTarjeta == 2
        {
            if searchController.searchBar.text! == "" {
                cuentasExternasRes = auxCuentasExternas
            } else {
                cuentasExternasRes = auxCuentasExternas.filter {
                    $0.nombre.lowercased().contains(searchController.searchBar.text!.lowercased()) ||
                    $0.numerocuenta.lowercased().contains(searchController.searchBar.text!.lowercased()) ||
                    $0.identificacion.lowercased().contains(searchController.searchBar.text!.lowercased())
                }
            }
        }
        if esInternaExternaTarjeta == 3 {
            
            if searchController.searchBar.text! == "" {
                cuentasTarjetaRes = cuentasTarjetas
                
            } else {
                
                
                cuentasTarjetaRes = cuentasTarjetas.filter{
                        $0.nombre.lowercased().contains(searchController.searchBar.text!.lowercased()) ||
                        $0.numerocuenta.lowercased().contains(searchController.searchBar.text!.lowercased()) ||
                        $0.identificacion.lowercased().contains(searchController.searchBar.text!.lowercased())
                }
            }
            
        }
        self.cuentasTransTableView.reloadData()
    }*/
    
    @IBAction func sc_consultaCuentasTrans(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            searchBar.text = nil
            searchBar.endEditing(true);
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            self.esInternaExternaTarjeta = 1
            self.consultaCuentasInternas(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_CEDULA, token: usuario.strToken)
        case 1:
            searchBar.text = nil
            searchBar.endEditing(true);
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            self.esInternaExternaTarjeta = 2
            
            self.consultaCuentasExternas(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_CEDULA, token: usuario.strToken, tipoCuentaE:_CONST.CE_GENERAL)
           
            
        case 2:
            searchBar.text = nil
            searchBar.endEditing(true);
            self.esInternaExternaTarjeta = 3
            
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            self.consultaCuentasExternas(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_CEDULA, token: usuario.strToken, tipoCuentaE:_CONST.CE_TARJETA)
            
            
        default: break;
        }
    }
    
    @IBAction func nuevaBtn(_ sender: UIBarButtonItem) {
        if esInternaExternaTarjeta == 1 {
            performSegue(withIdentifier: "mostrarNuevaInterna", sender: self)
        }
        
        if esInternaExternaTarjeta == 2 {
                    performSegue(withIdentifier: "mostrarNuevaExterna", sender: self)
        }
        if esInternaExternaTarjeta == 3{
           performSegue(withIdentifier: "mostrarNuevaExterna", sender: self)
        }
            
        
    }
    
    @IBAction func unwindSegueCInternas(_ sender: UIStoryboardSegue) {
        cuentasInternas.removeAll()
        cuentasInternasRes.removeAll()
        cuentasTransTableView.reloadData()
        consultaCuentasInternas(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_CEDULA, token: usuario.strToken)
        
    }
    
    @IBAction func unwindSegueCExterna(_ sender: UIStoryboardSegue) {
        cuentasExternas.removeAll()
        cuentasExternasRes.removeAll()
        cuentasTarjetaRes.removeAll()
        cuentasTarjetas.removeAll()
        auxCuentasExternas.removeAll()
        cuentasTransTableView.reloadData()
        var tipo: Int = 0;
        if esInternaExternaTarjeta == 2 {
            tipo = _CONST.CE_GENERAL
        }
        if esInternaExternaTarjeta == 3{
            tipo = _CONST.CE_TARJETA
        }
        consultaCuentasExternas(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_CEDULA, token: usuario.strToken, tipoCuentaE: tipo)
    }
    
    
    func consultaCuentasInternas(identificacion: String, tipoIdentificacion: String, token:String)->Void{
        
        self.cuentasInternas.removeAll()
        self.cuentasInternasRes.removeAll()
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/listaCuentasInternasSocio/\(_CONST.CANAL_DES)/\(identificacion)/\(tipoIdentificacion)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    
                    self.cuentasInternas = try JSONDecoder().decode([CuentaInternaModel].self , from: data)
                    if  self.cuentasInternas.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            self.cuentasInternasRes = self.cuentasInternas
                            self.cuentasTransTableView.reloadData();
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_CNTA_INT_SIN , view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }
                    catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir Consultar Cuentas Internas:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }.resume()
        
    }
    
    func consultaCuentasExternas(identificacion: String, tipoIdentificacion: String, token:String, tipoCuentaE: Int){
        
        self.cuentasExternas.removeAll()
        self.cuentasExternasRes.removeAll()
        self.cuentasTarjetaRes.removeAll()
        self.cuentasTarjetas.removeAll()
        self.auxCuentasExternas.removeAll()
        self.cuentasTransTableView.reloadData()
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let urlString = "\(_CONST.DIRECCION)/listaCuentasExternasSocio/\(_CONST.CANAL_DES)/\(identificacion)/\(tipoIdentificacion)/\(tipoCuentaE)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    //print(" Data ----> \(data)")
                    self.cuentasExternas = try JSONDecoder().decode([CuentaExternaModel].self , from: data)
                    if  self.cuentasExternas.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            for item in self.cuentasExternas{
                                if item.tipocuentaCE.idtipocuenta == _CONST.CNTA_TARJETA_CREDITO{
                                    self.cuentasTarjetas.append(item)
                                }else{
                                    self.auxCuentasExternas.append(item)
                                    
                                    
                                }
                            }
                            
                            self.cuentasExternasRes = self.auxCuentasExternas
                            self.cuentasTarjetaRes = self.cuentasTarjetas
                            
                            if self.esInternaExternaTarjeta == 3 && self.cuentasTarjetaRes.count == _CONST.LISTA_VACIA{
                                DispatchQueue.main.async {
                                    
                                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_TARJETA, view: self)
                                }
                            }else{
                                if self.esInternaExternaTarjeta == 2 && self.cuentasExternasRes.count == _CONST.LISTA_VACIA{
                                    DispatchQueue.main.async {
                                        
                                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_CNTA_EXT_SIN , view: self)
                                    }
                                    
                                }
                            }
                            self.cuentasTransTableView.reloadData()
                           
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            if self.esInternaExternaTarjeta == 2{
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_CNTA_EXT_SIN , view: self)
                            }else{
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_TARJETA , view: self)
                            }
                            
                        }
                    }
                    
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                  Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                    }
                }
            }else if let error = error{
                print("Error al consumir Consultar Cuentas Externas:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }.resume()
        
    }
    
    //Para que el teclado de la pantalla desaparesca al buscar
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
           searchBar.endEditing(true);
       }
       
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
           searchBar.endEditing(true);
       }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true);
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Atras"
        navigationItem.backBarButtonItem = backItem
        //Ocultar el teclado del filtro searchBar
        searchBar.endEditing(true);
        
        
        if let destination =  segue.destination as? TransferenciaViewController {
            if self.esInternaExternaTarjeta == 1 {
                destination.cuentaInternaCredito = self.cuentasInternasRes[(cuentasTransTableView.indexPathForSelectedRow?.row)!]
            }
            if self.esInternaExternaTarjeta == 2{
                destination.cuentaExternaCredito = self.cuentasExternasRes[(cuentasTransTableView.indexPathForSelectedRow?.row)!]
            }
            if self.esInternaExternaTarjeta == 3 {
                
                destination.cuentaExternaCredito = self.cuentasTarjetaRes[(cuentasTransTableView.indexPathForSelectedRow?.row)!]
            }
            destination.esInternaExternaTarjeta = self.esInternaExternaTarjeta
        }
        if self.esInternaExternaTarjeta == 3{
            if let dest = segue.destination as? RegistrarCuentaExternaViewController {
                dest.esInternaExternaTarjeta = 3
            }
        }
        
    }
}
