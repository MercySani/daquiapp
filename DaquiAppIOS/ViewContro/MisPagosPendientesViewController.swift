//
//  MisPagosPendientes.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 07/05/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import UIKit
import SwiftyXMLParser

class MisPagosPendientesViewController: UIViewController, alerts {
    
    var tituloAlerta: String?
    var mensajeAlerta: String?
    var monto: String?
    var descripcion: String?
    var endToEnd: String?
    var telefonoDestino: String?
    var listaPagos: [PagoBimoModel] = []
    var usuario: UsuarioModel = UsuarioModel()
    let objActivity = activityIndicatorVC()
    
    var origen: String?
    var canalAux: String?
    var identificacionAux: String?
    @IBOutlet var tableView: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(MisPagosPendientesTableViewCell.nib(),forCellReuseIdentifier: MisPagosPendientesTableViewCell.identificador)
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 600
        if origen == _CONST.BIMO_ORIGEN_EXTERNO{
            identificacionAux = usuario.strTelefono
            canalAux = _CONST.CANAL_SEG_IOS_BIMO
        }else{
            identificacionAux = usuario.strIdentificacion
            canalAux = _CONST.CANAL_SEG_IOS
        }
        
        self.accionBimo(strTrama: _CONST.CONSULTA_COBROS_PENDIENTES, telefonoOrigen: usuario.strTelefono, identificacion: self.identificacionAux!, strFiltro: _CONST.PAGO_PENDIENTE , strCriterio:_CONST.PAGO_BIMO, token: usuario.strToken)
        
        self.tableView.reloadData()
    }
    
    func mostrarAlerta(title: String, message: String,monto: String, descripcion: String, endToEnd:String, telefonoDestino: String) {
           self.tituloAlerta = title
           self.mensajeAlerta = message
           self.monto = monto
           self.descripcion = descripcion
           self.endToEnd = endToEnd
           self.telefonoDestino = telefonoDestino
          performSegue(withIdentifier:"segueModal", sender: self)
         
       }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueModal" {
            if let destino = segue.destination as? AlertBimoViewController{
                destino.variableMensaje = self.mensajeAlerta
                destino.usuario = self.usuario
                destino.strMonto = self.monto
                destino.strDescripcion = self.descripcion
                destino.strEndToEnd = self.endToEnd
                destino.strTelefonoDestino = self.telefonoDestino
                destino.origen = self.origen
                if tituloAlerta == "PAGAR"{
                    destino.strTrama = _CONST.TRANSFER_RQ
                    destino.strCriterio = "INDIRECTO"
                }else{
                    destino.strTrama = _CONST.RECHAZO_COBRO_RQ
                    destino.strCriterio = "CANCEL"
                }
            }
        }
    }
}
    
extension MisPagosPendientesViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaPagos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MisPagosPendientesTableViewCell.identificador, for: indexPath) as! MisPagosPendientesTableViewCell
        cell.configure(witch: listaPagos[indexPath.row])
        cell.alertsDelegate = self
        cell.delegate = self
        return cell
    }
   
    func accionBimo(strTrama: String, telefonoOrigen: String, identificacion: String,strFiltro: String, strCriterio: String , token: String)-> Void{

        print(strTrama)
        print(telefonoOrigen)
        print(identificacion)
        print(strFiltro)
        print(strCriterio)
        print(canalAux)
        print("token -> \(token)")

        let session = URLSession(
                   configuration: URLSessionConfiguration.ephemeral,
                   delegate: NSURLSessionPinningDelegate(),
                   delegateQueue: nil)
        
        let json: [String: Any] = [ "strIdentificacion": identificacion,
                                    "strToken": token,
                                    "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                    "strCanalSeguridad": canalAux,
                                    "strTrama": strTrama,
                                    "strTelefonoOrigen": telefonoOrigen,
                                    "strTelefonoDestino": "",
                                    "strDescripcion": "",
                                    "strMonto": "",
                                    "strAccion": "",
                                    "strFiltro": strFiltro,
                                    "strCriterio": strCriterio,
                                    "strNumeroCuenta": "",
                                    "strEndToEnd": ""
                                    
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/bimo/general")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { data, response, error in
        if let data = data, let response = response as? HTTPURLResponse, response.statusCode==202{
            do{
                let mensajeRespuestaData = try JSONDecoder().decode(BimoResultado.self , from: data)
                  print("mensajeRespuestaData-----")
                print(mensajeRespuestaData)
                if  mensajeRespuestaData.strCodigo == _CONST.CODIGO_OK{
                    
                    DispatchQueue.main.async {
                        print("mensajeRespuestaData-----")
                        print(mensajeRespuestaData.strXML)
                        
                        let xml = try! XML.parse(mensajeRespuestaData.strXML)
                        print(mensajeRespuestaData)
                              if strTrama == _CONST.CONSULTA_COBROS_PENDIENTES {
                                  do {
                                     if let codResultado = xml["BimoXml", "codResultado"].text {
                                        if codResultado == _CONST.CODIGO_OK{
                                            let jsonData = Data(xml["BimoXml", "mensajeResultado"].text!.utf8)
                                             self.listaPagos =  try JSONDecoder().decode([PagoBimoModel].self , from: jsonData)
                                           DispatchQueue.main.async {
                                                 self.objActivity.hideActivityIndicator(uiView: self.view)
                                                 self.tableView.reloadData()
                                            }
                                        }else{
                                            let mensajeError = xml["BimoXml", "mensajeResultado"].text
                                            DispatchQueue.main.async {
                                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeError! , view: self)
                                            }
                                        }
                                        
                                    }else{
                                        
                                    }
                                  } catch {
                                      print(error.localizedDescription)
                                  }
                                  
                              }
     
                    }
                }else{
                    if mensajeRespuestaData.strCodigo == _CONST.CODIGO_ERROR_TOKEN{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                             Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strCodigo , view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensaje , view: self)
                        }
                    }
                }
            }catch let error as NSError{
                print("Error al leer JSON: \(error)")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }else if let error = error{
            print("Error al consumir cerrar sesion:error: \(error)")
            if let error = error as NSError?, error.domain == NSURLErrorDomain{
                if error.code == -1001 {
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                    }
                } else if error.code == -1009 {
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }else{
            //No se obtubo ningun dato
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
        }
        }.resume()
    
    }

    
}
    
extension MisPagosPendientesViewController: MisPagosPendientesDelegate{
    func didTapButton(with title: String){
        print("\(title)")
       
    }
}

protocol  alerts: class {
    func mostrarAlerta(title: String , message: String, monto: String, descripcion: String, endToEnd:String, telefonoDestino: String)
    
}

