//
//  DPFSWController.swift
//  DaquiApp
//
//  Created by Fredy on 17/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
import UIKit
class DPFViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var usuario: UsuarioModel = UsuarioModel()
    var dpfs: [DPFModel] = []
    
    @IBOutlet weak var dpfTableView: UITableView!
    let objActivity = activityIndicatorVC()
    
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refresControl = UIRefreshControl()
        refresControl.addTarget(self, action: #selector(DPFViewController.actualizarDatos(_:)), for: .valueChanged)
        refresControl.tintColor = UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        return refresControl
    }()
    
    @objc func actualizarDatos (_ refresControl: UIRefreshControl){
        
        dpfs.removeAll()
        refresControl.endRefreshing()
        consultaDPF(identificacion: usuario.strIdentificacion, idTipoIdentificacion: _CONST.TI_CEDULA, token: usuario.strToken)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usuario = (tabBarController as! TabBarViewController).usuario
        
        if(usuario.strNombreUser == _CONST.USUARIO_DEMO){
            self.dpfs = _DEMO.dpfs()
            self.dpfTableView.reloadData()
            
        }else{
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            consultaDPF(identificacion: usuario.strIdentificacion, idTipoIdentificacion: _CONST.TI_CEDULA, token: usuario.strToken)
            
            dpfTableView.addSubview(self.refreshControl)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dpfs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dpfCell") as! GenericoTableViewCell
        if self.dpfs[indexPath.row].ccuenta == ""{
            cell.titulo.text = _CONST.MSJ_DPF_SIN
            cell.detalle1.text = ""
            cell.detalle2.text = ""
            cell.detalle3.text = ""
            cell.detalle4.text = ""
            cell.detalle5.text = ""
            cell.detalle6.text = ""
            cell.detalle7.text = ""
        }else{
        cell.titulo.text = self.dpfs[indexPath.row].descripcionproducto
        
        let auxCuenta = NSMutableAttributedString()
        auxCuenta.bold("Cuenta: ") .normal(self.dpfs[indexPath.row].ccuenta)
        cell.detalle1.attributedText = auxCuenta
        
        let auxMonto = NSMutableAttributedString()
        auxMonto .bold("Monto: ") .normal(self.dpfs[indexPath.row].monto)
        cell.detalle2.attributedText = auxMonto
        
        let auxFApertura = NSMutableAttributedString()
        auxFApertura .bold("Fecha apertura: ") .normal(self.dpfs[indexPath.row].fapertura)
        cell.detalle3.attributedText = auxFApertura
        
        let auxFVen = NSMutableAttributedString()
        auxFVen .bold("Fecha vencimiento: ") .normal(self.dpfs[indexPath.row].fvencimiento)
        cell.detalle4.attributedText = auxFVen
        
        let auxEstado = NSMutableAttributedString()
        auxEstado .bold("Estado: ") .normal(self.dpfs[indexPath.row].descripcionestatuscuenta)
        cell.detalle5.attributedText = auxEstado
        
        let auxTasa  = NSMutableAttributedString()
        auxTasa .bold("Tasa: ") .normal(self.dpfs[indexPath.row].tasa)
        cell.detalle6.attributedText = auxTasa
        
        let auxFreInte = NSMutableAttributedString()
        auxFreInte .bold("Frecuencia interés: ") .normal(self.dpfs[indexPath.row].cfrecuencia_interes)
        cell.detalle7.attributedText = auxFreInte
        }
        return cell
    }
    
    func consultaDPF(identificacion: String, idTipoIdentificacion: String, token: String) -> Void  {
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/listaDepositosPlazoFijo/\(_CONST.CANAL_DES)/\(identificacion)/\(idTipoIdentificacion)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return
            
        }
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.dpfs = try JSONDecoder().decode([DPFModel].self , from: data)
                    
                    if  self.evaluarLista(dpfsAux: self.dpfs) != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            self.dpfTableView.reloadData()
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            self.dpfTableView.reloadData()
                            
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }
                catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
                }else if let error = error{
                print("Error al consumir DPF :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
                }else{
                    
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }.resume()
        
    }
    
    func evaluarLista(dpfsAux: [DPFModel] )-> Int{
        for item in dpfsAux{
            if item.ccuenta == ""{
                return _CONST.LISTA_VACIA
            }
        }
       return 1
    }
}
