//
//  OpcionesGeneralesViewController.swift
//  DaquiApp
//
//  Created by Desarrollador on 17/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class OpcionesGeneralesViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var btnIniciarSesion: UIButton!
    @IBOutlet weak var coopLinea: UIButton!
    @IBOutlet weak var btnBimo: UIButton!
    @IBOutlet weak var btnSimulador: UIButton!
    @IBOutlet weak var btnAgencias: UIButton!
    
    @IBOutlet weak var viewUsuario: UIView!
    @IBOutlet weak var viewAgencias: UIView!
    @IBOutlet weak var viewSimulador: UIView!
    @IBOutlet weak var itemMenu: UINavigationItem!
    
    
    let objActivity = activityIndicatorVC()
       var celularUsuario :String = ""
    
    @IBAction func btnIniciarSesion(_ sender: Any) {
    }
    
    
    @IBAction func coopLinea(_ sender: Any) {
        /*
        let habilitarUsuarioViewController =  self.storyboard?.instantiateViewController(withIdentifier: "HabilitarUsuarioViewController") as! HabilitarUsuarioViewController
        self.present(habilitarUsuarioViewController, animated: true)*/
    }
    
    var imageArray = [UIImage]()
    var currentIndex = 0
    var timer : Timer?
    override func viewDidLoad() {
        super.viewDidLoad()
        imageArray = [ #imageLiteral(resourceName: "slider1") , #imageLiteral(resourceName: "slider3"), #imageLiteral(resourceName: "slider2"), #imageLiteral(resourceName: "slider5"), #imageLiteral(resourceName: "slider4")]
        pageControl.numberOfPages = imageArray.count
        startTimer()
        applyRounCorner()
        //Borde Redondeado botones
        btnIniciarSesion.layer.cornerRadius = _CONST.CORNER_RADIUS
        viewUsuario.layer.cornerRadius = 10
        viewAgencias.layer.cornerRadius = 10
        viewSimulador.layer.cornerRadius = 10
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func applyRounCorner() {
        coopLinea.layer.cornerRadius =  coopLinea.frame.size.width / 2
        coopLinea.layer.masksToBounds = true
        
        btnBimo.layer.cornerRadius = btnBimo.frame.size.width / 2
        btnBimo.layer.masksToBounds = true
        
        btnSimulador.layer.cornerRadius = btnSimulador.frame.size.width / 2
        btnSimulador.layer.masksToBounds = true
    }
 
    func startTimer(){
        
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction(){
        
        let desiredScrollPosition = (currentIndex < imageArray.count - 1) ? currentIndex + 1 : 0
        collectionView.scrollToItem(at: IndexPath(item: desiredScrollPosition, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
   
    
    //son alerts con text
    func showInputDialog() {
        //Creating UIAlertController and
        //Setting title and message for the alert dialog
        let alertController = UIAlertController(title: "Enter details?", message: "Enter your name and email", preferredStyle: .alert)
        
        //the confirm action taking the inputs
        let confirmAction = UIAlertAction(title: "Enter", style: .default) { (_) in
            
            //getting the input values from user
            let name = alertController.textFields?[0].text
            let email = alertController.textFields?[1].text
            
            
            
            return
        }
        
        //the cancel action doing nothing
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        //adding textfields to our dialog box
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter Name"
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter Email"
        }
        
        //adding the action to dialogbox
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        //finally presenting the dialog box
        self.present(alertController, animated: true, completion: nil)
    }
 
    @IBAction func unwindSeguePortal(_ sender: UIStoryboardSegue) {
        self.view.layoutIfNeeded()
        self.title = "Menú"
        self.navigationController?.navigationBar.tintColor =
                     UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        
                     let backItem = UIBarButtonItem()
                     backItem.title = "Menú"
                     navigationItem.backBarButtonItem = backItem
        
    }
    
    @IBAction func AperturarCuenta(_ sender: Any) {
        let url = _CONST.URL_CREACION_CUENTA
        if !url.isEmpty {
           openUrl(urlString: url)
        }
        
    }
 
    func openUrl(urlString:String!) {
        let url = URL(string: urlString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
         } else {
                   UIApplication.shared.openURL(url)
         }
   }
    
    
    //BIMO
    
    @IBAction func btnBimoExterno(_ sender: Any) {
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        self.bimoExternoConsulta()

    }
    
    func bimoExternoConsulta()-> Void{
        print("bimoExternoConsulta---")
        let strAction="Q"
        let session = URLSession(
        configuration: URLSessionConfiguration.ephemeral,
        delegate: NSURLSessionPinningDelegate(),
        delegateQueue: nil)
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        let version: String = UIDevice.current.model+"_"+UIDevice.current.systemName+"_" + UIDevice.current.systemVersion+"|"+appVersion
        
        let json: [String: Any] = ["strAccion": strAction,
                                "strCanalSeguridad": _CONST.CANAL_SEG_IOS_BIMO,
                                "strCanalDescripcionAdicional": _CONST.CANAL_DES ,
                                "strDetalleDispositivo": version ,
                                "strOrigenIdentificador": macAddress?.description ?? "",
                                "strToken":"",
                                "strNombreUser": "",
                                "strSistema": _CONST.SISTEMA_BIMO_EXTERNO,
                                "strPassword": "",
                                "strIdentificacion":"",
                                "strObservacion":""
                                    ]
    
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/bimo/generalexterno")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
    
        session.dataTask(with: request) { data, response, error in
            if let data = data, let _ = response as? HTTPURLResponse
            {
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(ResultadoRestful.self , from: data)
                    self.celularUsuario=mensajeRespuestaData.strTelefono
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                    }
                    print("mensajeRespuestaData.strCodigoError --> \(mensajeRespuestaData.strCodigoError)")
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_OK{
                        
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "segueLoginBimo", sender: self)
                            //self.present(loginBimoViewController, animated: true)
                        }
                    }else if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_BIMO_MAC_NO_REGISTRADO{
                        DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "segueRegistroBimoExterno", sender: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir Bimo externo consulta :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                print("no se obtuno nigun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueLoginBimo"{
            let loginBimoExternoViewController = segue.destination as! LoginBimoExternoViewController
                      loginBimoExternoViewController.celularUsuario =  self.celularUsuario
            
        }
    }
    
}

extension OpcionesGeneralesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCell", for: indexPath) as! SliderCell
        
        cell.image = imageArray[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        currentIndex = Int(scrollView.contentOffset.x / collectionView.frame.size.width)
        pageControl.currentPage = currentIndex
    }
    
}

