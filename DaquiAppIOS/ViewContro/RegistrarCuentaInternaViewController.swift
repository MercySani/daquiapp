//
//  swCuentasInternas.swift
//  DaquiApp
//
//  Created by Fredy on 14/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
import UIKit

class RegistrarCuentaInternaViewController: UIViewController, UITextFieldDelegate  {
    var usuario: UsuarioModel = UsuarioModel()
    var listaCuentaSocio: ListaCuentasSocioModel = ListaCuentasSocioModel()
    var listaCuentasSo: [ListaCuentas] = []
    var mensajeModel: MensajeModel = MensajeModel()
    var auxCount: Int = 0
    var tipIdentificacion: String = ""
    var tipoIdent: String = ""
    let objActivity = activityIndicatorVC()
    @IBOutlet weak var lblTipoCuenta: UILabel!
    @IBOutlet weak var txtComentario: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var btnRegistrar: UIButton!
    @IBOutlet weak var lblIdentificador: UILabel!
    @IBOutlet weak var txtCuenta: UITextField!
    @IBOutlet weak var segControlCriterioBusqueda: UISegmentedControl!
    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var btnBuscar: UIButton!
    
    
    @IBOutlet weak var txtIdentificador: UITextField!
    @IBOutlet weak var constraintContentHeight: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    var activeField: UITextField?
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    
    @objc func returnTextView(gesture: UIGestureRecognizer) {
        guard activeField != nil else {
            return
        }
        
        activeField?.resignFirstResponder()
        activeField = nil
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        lastOffset = self.scrollView.contentOffset
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeField?.resignFirstResponder()
        activeField = nil
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var tipoCaracter: CharacterSet = CharacterSet()
        var characterSet: CharacterSet = CharacterSet()
        var retorno: Bool = false
        
        switch textField.tag {
        case 1:
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 15
            }
            break
        case 2:
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 15
            }
            break
        case 3:
            guard let text = textField.text else { return true }
            let count = text.count + string.count - range.length
            retorno = count <= 100
            break
        case 4:
            guard let text = textField.text else { return true }
            let count = text.count + string.count - range.length
            retorno = count <= 250
            break
        default:
            retorno = true
        }
        
        return retorno
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if keyboardHeight != nil {
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            
            // so increase contentView's height by keyboard height
            UIView.animate(withDuration: 0.3, animations: {
                self.constraintContentHeight.constant += self.keyboardHeight
            })
            
            
            // move if keyboard hide input field
            if(activeField == nil){
                return
            }
            print("ActiveField ---> \(activeField)")
            let distanceToBottom = self.scrollView.frame.size.height - (activeField?.frame.origin.y)! - (activeField?.frame.size.height)!
            print("Distance ---> \(distanceToBottom)")
            
            
            let collapseSpace = keyboardHeight - distanceToBottom
            if collapseSpace < 0 {
                // no collapse
                return
            }
            
            // set new offset for scroll view
            UIView.animate(withDuration: 0.3, animations: {
                // scroll to the position above keyboard 10 points
                self.scrollView.contentOffset = CGPoint(x: self.lastOffset.x, y: collapseSpace + 10)
            })
        }
    }
    
    
    @objc func keyboardWillHide1(notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
            if self.constraintContentHeight != nil && self.keyboardHeight != nil {
                self.constraintContentHeight.constant -= self.keyboardHeight
                self.scrollView.contentOffset = self.lastOffset
            }
            
        }
        
        keyboardHeight = nil
    }
    // fin metodo de vizualizacion
    
    @IBAction func selecCuenta(_ sender: Any) {
        DispatchQueue.main.async {
                   self.view.endEditing(true)
               }
        guard !(listaCuentasSo.count == 0 && auxCount == 0) else {
            auxCount = 1
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "Debe realizar la busqueda de la cuenta" , view: self)
            }
            return
        }
        guard listaCuentasSo.count == 0 else {
            let popUpCuentaViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpCuentaViewController") as!  PopUpCuentaViewController
            
            popUpCuentaViewController.popUpDelegate = self
            popUpCuentaViewController.tipoSelec = _CONST.SELEC_CUENTA_SOCIO
            popUpCuentaViewController.listaCuentasSocio = listaCuentasSo
            self.present(popUpCuentaViewController, animated: true)
            return
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        usuario = (tabBarController as! TabBarViewController).usuario
        self.btnRegistrar.isHidden = true
        
        btnBuscar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnRegistrar.layer.cornerRadius = _CONST.CORNER_RADIUS
        
        // textField delegate para el boton Done
        txtIdentificador.delegate = self
        txtCuenta.delegate = self
        txtEmail.delegate = self
        txtComentario.delegate = self
    
        //para el borde de los content view
        self.viewBorder.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.viewBorder.layer.borderWidth = _CONST.BORDER_WIDTH
        self.viewBorder.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        
        //para la vista con el scroll view
        
        // Observe keyboard change
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide1(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // reconoce el touch de la pantalla
        self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(returnTextView(gesture:))))
        // fin scroll view
        
    }
 
    @objc func keyboardWillChange(notificaction: Notification){
        guard let keyboardRect = (notificaction.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        if notificaction.name == Notification.Name.UIKeyboardWillShow || notificaction.name == Notification.Name.UIKeyboardWillChangeFrame{
             view.frame.origin.y = -keyboardRect.height
        }else{
            view.frame.origin.y = 0
        }
    }
    
    @IBAction func registroCuentaInterna(_ sender: Any) {
        guard !(txtCuenta.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje:_CONST.SER_BAS_MENSA_CUENTA , view: self)
            }
            return
        }
        guard !(txtEmail.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_CI_EMAIL , view: self)
            }
            return
        }
        
        let isEmailAddressValid = Utilidades.isValidEmailAddress(emailAddressString: txtEmail.text!)
        guard (isEmailAddressValid) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_EMAIL_INC, view: self)
            }
            return
        }
        guard !(txtComentario.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje:_CONST.MEN_CI_COMENTARIO , view: self)
            }
            return
        }
        guard !(Utilidades.tieneCaracteresEspeciales(string: txtComentario.text!))else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje:_CONST.MSJ_CARACTERES_ESPECIALES , view: self)
            }
            return
        }
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        
       
        if(self.tipIdentificacion == _CONST.TI_SOCIO || self.tipIdentificacion == _CONST.TI_CEDULA){
            self.tipoIdent = _CONST.CI_TIPO_IDENTIFICACION
        }else{
            self.tipoIdent = _CONST.TI_RUC
        }
        insertarCuentaInterna(idUsuarioSocio: self.usuario.strIdentificacion, identificacionCI: self.listaCuentaSocio.Identificacion, idTipoIdentificacionCI: self.tipoIdent, numCuentaCI: self.txtCuenta.text!, emailCI: txtEmail.text!, comentarioCI: txtComentario.text!, nombreCI: self.listaCuentaSocio.NombreLegal, numSocioCI: self.listaCuentaSocio.SeguroSocial , codigoOtp: _CONST.CODIGO_OK, token: self.usuario.strToken)
        
    }
    @IBAction func buscarSocio(_ sender: Any) {
        guard !txtIdentificador.text!.isEmpty else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_CI_IDENTIFICADOR , view: self)
            }
            return
        }
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        
        if segControlCriterioBusqueda.selectedSegmentIndex == 1 {
            self.tipIdentificacion = _CONST.TI_SOCIO
        }else if ((self.txtIdentificador.text!.count)>10){
            self.tipIdentificacion = _CONST.TI_RUC
        }else{
            self.tipIdentificacion = _CONST.TI_CEDULA
        }
        
        listarCuentasSocio(identificacionUsuario: usuario.strIdentificacion, identificacionBusqueda: txtIdentificador.text!, tipoIdentificacion: tipIdentificacion, token: usuario.strToken)
    }
    func tipoCedulaoSocio(dato: Int) -> String {
        var auxDato: String = ""
        if segControlCriterioBusqueda.selectedSegmentIndex == 0 {
            auxDato = _CONST.TI_CEDULA
        }
        if segControlCriterioBusqueda.selectedSegmentIndex == 1{
            auxDato = _CONST.TI_SOCIO
        }
        return auxDato
    }
    
    func listarCuentasSocio(identificacionUsuario: String, identificacionBusqueda: String, tipoIdentificacion: String, token: String ){
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        txtCuenta.text = ""
        lblTipoCuenta.text = ""
        let urlString = "\(_CONST.DIRECCION)/listaCuentasSocio/\(_CONST.CANAL_DES)/\(identificacionUsuario)/\(identificacionBusqueda)/\(tipoIdentificacion)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
            self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.listaCuentaSocio = try JSONDecoder().decode(ListaCuentasSocioModel.self , from: data)
                    if  !self.listaCuentaSocio.cPersona.isEmpty{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            self.listaCuentasSo = self.listaCuentaSocio.lstCuentas
                            self.btnRegistrar.isHidden = false
                            
                            self.lblIdentificador.text = self.listaCuentaSocio.Identificacion
                            self.lblNombre.text = self.listaCuentaSocio.NombreLegal
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                        }
                    }
                    
                }catch{
                    do {
                        self.mensajeModel = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if self.mensajeModel.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: self.mensajeModel.strMensajeError , view: self)
                            }
                        }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: self.mensajeModel.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir Lista Cuentas Socio:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    func insertarCuentaInterna( idUsuarioSocio: String , identificacionCI: String , idTipoIdentificacionCI: String , numCuentaCI: String, emailCI: String , comentarioCI: String, nombreCI: String, numSocioCI: String, codigoOtp: String, token: String)-> Void{
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let json: [String: Any] = ["idUsuario": idUsuarioSocio,
                                   "identificacion": identificacionCI,
                                   "idTipoIdentificacion": idTipoIdentificacionCI,
                                   "numCuenta": numCuentaCI,
                                   "email": emailCI,
                                   "comentario": comentarioCI,
                                   "nombre": nombreCI,
                                   "strNumSocio": numSocioCI,
                                   "strOTP": codigoOtp,
                                   "strToken": token,
                                   "strCanalSeguridad": _CONST.CANAL_SEG_IOS]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/insertarCuentaInterna")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { data, response, error in
            if let data = data{
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_OK{
                        
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            let alertControler = UIAlertController(title: _CONST.AVISO, message: mensajeRespuestaData.strMensajeError, preferredStyle: .alert)
                            let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
                            { (action:UIAlertAction!) in
                                
                                
                                self.lblNombre.text = ""
                                self.lblIdentificador.text = ""
                                self.txtComentario.text = ""
                                self.txtEmail.text = ""
                                self.txtCuenta.text = ""
                                self.txtIdentificador.text = ""
                                self.btnRegistrar.isHidden = true
                                self.performSegue(withIdentifier: "unwindSegueCInternas", sender: self)
                            }
                            alertControler.addAction(aceptarAccion)
                            self.present(alertControler, animated: true, completion: nil)
                        }
                    }else{
                        if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }else if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_SEG{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                        }
                    }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir Insertar Cuentas Internas:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    //enviar OTP
    //esta funcion solo envia el OTP, para utilizar este funcion cualquiera de las dos variables deben estar estar correctos.
    func enviarCodigoOPT(userName: String, identificacion: String, token:String)   {
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let json: [String: Any] = [ "strUsername": userName,
                                    "strIdentificacion": identificacion,
                                    "strToken": token,
                                    
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                    "strObservacion": _CONST.CANAL_DES
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/procesoEnvioOTP")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { (data, response, error) in
            
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let otpRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  otpRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK {
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
                            otpViewController.popUpOTPDelegate = self
                            self.present(otpViewController, animated: true)
                            }
                        }else{
                            if otpRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: otpRespuestaData.strMensajeError , view: self)
                                }
                            }else{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    
                                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_OTP_NOK , view: self)
                                }
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                }
            }else if let error = error{
                print("Error al consumir Envio OTP :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
extension RegistrarCuentaInternaViewController: PopUpDelegate{
    func seleccionString(value: String, tipoSelection: String, tipoCuenta: String, saldoDisponible: String) {
        if (tipoSelection == _CONST.SELEC_CUENTA_SOCIO){
            self.txtCuenta.text = value
            self.lblTipoCuenta.text = tipoCuenta
            
            
        }
        
    }
}

extension RegistrarCuentaInternaViewController: OTPDelegate{
    func inserOTP(value: String) {
        //Se retiro por que no existe registro de cuentas con otp
        
    }
}



