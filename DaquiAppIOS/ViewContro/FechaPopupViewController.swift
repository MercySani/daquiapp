//
//  FechaPopupViewController.swift
//  DaquiApp
//
//  Created by Desarrollador on 23/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class FechaPopupViewController: UIViewController {
    
    @IBOutlet weak var fechaPicker: UIDatePicker!
    @IBOutlet weak var fechaView: UIView!
    @IBOutlet weak var btnAceptar: UIButton!
    @IBOutlet weak var btnCancelar: UIButton!
    
    var popUpDelegate: PopUpDelegate?
    
    
    var tipoSeleccion: String = ""
    var tipoCuenta: String = ""
    var saldoDisponible: String = ""
    var fechaInicial: String = ""
    var date = Date()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCancelar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnAceptar.layer.cornerRadius = _CONST.CORNER_RADIUS
        
        if tipoSeleccion == "FechaTransaccion"{
            
            let dateStringFormatter = DateFormatter()
            
            dateStringFormatter.dateStyle = .short
            dateStringFormatter.dateFormat = "yyyy-MM-dd"
            let dateFormato = dateStringFormatter.date(from: dateStringFormatter.string(from: self.date))
    
            let monthsToAdd = _CONST.T_MAXIMO_CONSULTA
            
           
            let newDate = Calendar.current.date(byAdding: .month, value: monthsToAdd, to: dateFormato!)
            fechaPicker.minimumDate = newDate
            fechaPicker.maximumDate = self.date
        }
        
        if tipoSeleccion == "FINICIO"{
            fechaPicker.minimumDate = self.date
        }
        if tipoSeleccion == "FFIN"{
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateFormato = dateFormatter.date(from: self.fechaInicial)
            let monthsToAdd = _CONST.T_MINIMO_PP
            let newDate = Calendar.current.date(byAdding: .month, value: monthsToAdd, to: dateFormato!)
            
            fechaPicker.minimumDate = newDate
            
        }
        
    }

    
    @IBAction func aceptarBtn(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = _CONST.YYYY_MM_DD
        popUpDelegate?.seleccionString(value: dateFormatter.string(from: fechaPicker.date), tipoSelection: tipoSeleccion, tipoCuenta: tipoCuenta, saldoDisponible: saldoDisponible)
        dismiss(animated: true)
    }
    
    @IBAction func cancelarBtn(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
}
