//
//  CambioClaveViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 10/9/18.
//  Copyright © 2018 CoopDaquilema. All rights reserved.
//

import UIKit

class CambioClaveViewController: UIViewController {
    var claveLogin: String = ""
    var userLogin: String = ""
    var usuario: UsuarioModel = UsuarioModel()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let objActivity = activityIndicatorVC()
    
    @IBOutlet weak var lblConsejo: UILabel!
    @IBOutlet weak var btnCancelar: UIButton!
    @IBOutlet weak var btnModificar: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.nombreCompleto.text = userLogin
        self.view.endEditing(true)
        btnCancelar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnModificar.layer.cornerRadius = _CONST.CORNER_RADIUS
        
        if(usuario.strIdentificacion == userLogin){
            nombreUsuario.isEnabled=true
            titulo.text="Cambiar nombre de usuario y contraseña"
            mensaje.text="Por seguridad usted debe cambiar su nombre de usuario y  clave temporal"
            self.nombreUsuario.text=""
        }else{
            nombreUsuario.text = userLogin
            nombreUsuario.isEnabled=false
            self.lblConsejo.isHidden=true
            titulo.text="Cambiar contraseña"
             mensaje.text="Por seguridad usted debe cambiar su clave temporal"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBOutlet weak var nombreUsuario: UITextField!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var mensaje: UILabel!
    @IBOutlet weak var claveRepetir: UITextField!
    @IBOutlet weak var claveNueva: UITextField!

    @IBOutlet weak var cancelar: UIButton!
    @IBOutlet weak var modificarClave: UIButton!
    @IBAction func modificarClave(_ sender: Any) {
        print("valor del disable", self.nombreUsuario.isEnabled==true)
        var datosCorrectos:boolean_t
        datosCorrectos=1
        if  (self.nombreUsuario.isEnabled==true && self.nombreUsuario.text!.isEmpty){
            datosCorrectos=0
            Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.ERR_C_VACIOS, view: self)
        }
        if (self.nombreUsuario.isEnabled==true && !(Utilidades.tieneNumeros(string: self.nombreUsuario.text!) && Utilidades.tieneMayusculas(string: self.nombreUsuario.text!) && Utilidades.tieneMinusculas(string: self.nombreUsuario.text!))){
            datosCorrectos=0
            Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_NUM_LETRAS_NICKNAME, view: self)
        }
        if(self.nombreUsuario.isEnabled==true  && Utilidades.tieneCaracteresEspeciales(string: self.nombreUsuario.text!)){
            datosCorrectos=0
            Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_CARACTERES_ESPECIALES+" el nickname", view: self)
        }
        if(self.nombreUsuario.isEnabled==true  && Utilidades.tieneEspaciosEnBlanco(string: self.nombreUsuario.text!)){
            datosCorrectos=0
            Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_ESPACIOS_BLANCO_NICKNAME, view: self)
        }
        if (self.nombreUsuario.isEnabled==true  && (self.nombreUsuario.text?.count)! < 8){
            datosCorrectos=0
             Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_CARACTERES_MINIMOS_NICKNAME, view: self)
        }
        
       if   !self.claveRepetir.text!.isEmpty  && !self.claveNueva.text!.isEmpty  && datosCorrectos==1 {
        if self.claveNueva.text != self.claveRepetir.text{
            Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_CLAVES, view: self)
        }else{
            if (!(Utilidades.tieneNumeros(string: self.claveNueva.text!) && Utilidades.tieneMayusculas(string: self.claveNueva.text!) && Utilidades.tieneMinusculas(string: self.claveNueva.text!))){
                
                Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_NUM_LETRAS, view: self)
                
            }else{
                if(Utilidades.tieneCaracteresEspeciales(string: self.claveNueva.text!)){
                    Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_CARACTERES_ESPECIALES, view: self)
                }else{
                if ((self.claveNueva.text?.count)! >= 8 && (self.claveNueva.text?.count)!<=18){
                    DispatchQueue.main.async {
                        self.objActivity.showActivityIndicator(uiView: self.view)
                        }
                    
                    if self.nombreUsuario.isEnabled==true{
                        cambiarClave(identificacion: usuario.strIdentificacion, token: usuario.strToken, nuevaClave: self.claveNueva.text!, cambioNickname:_CONST.APLICA_CAMBIO,nuevoNickname:self.nombreUsuario.text!)
                    }else{
                        cambiarClave(identificacion: usuario.strIdentificacion, token: usuario.strToken, nuevaClave: self.claveNueva.text!, cambioNickname: _CONST.NO_APLICA_CAMBIO, nuevoNickname: self.nombreUsuario.text!)
                    }
                }else{
                    Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_CARACTERES_MINIMOS, view: self)
                }
            }
            }
        }
        }else{
            Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.ERR_C_VACIOS, view: self)
        }
    }
    
    @IBAction func cancelar(_ sender: Any) {
       self.dismiss(animated: true, completion: nil)
        // let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        //self.present(loginViewController, animated: true)
    }
    //Para ocultar el teclado al hacer touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func cambiarClave(identificacion: String, token: String, nuevaClave: String, cambioNickname: String, nuevoNickname:String)-> Void{
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        var firma: String = ""
        var jwt:String = ""
        for _ in 1...7 {
            firma = firma + usuario.strIdentificacion
        }
        jwt = JsonWebToken.crearJWT(contenido: HASH.MD5(string: nuevaClave), firma: firma)
        
        let json: [String: Any] = ["strToken": token,
                                   "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                   "strNuevaClave": jwt , "strIdentificacion":identificacion,
                                   "strConfirmarCambioNickName":cambioNickname,
                                   "strNuevoNickName":nuevoNickname
        ]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/cambiarClave")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { data, response, error in
            
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSesionRecuperarClave(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            
                        //self.performSegue(withIdentifier: "unwindInicioSesion", sender: self)
                            
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir cambiarClave:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }

}
