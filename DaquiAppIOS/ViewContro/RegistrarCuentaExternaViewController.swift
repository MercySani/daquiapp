//
//  swCuentasExternas.swift
//  DaquiApp
//
//  Created by Fredy on 14/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
import UIKit

class RegistrarCuentaExternaViewController: UIViewController, UITextFieldDelegate {
    var usuario: UsuarioModel = UsuarioModel()
    var auxIfisCuenta: String = ""
    var auxIdIfi: String = ""
   let objActivity = activityIndicatorVC()
    @IBOutlet weak var txtIfis: UITextField!
    @IBOutlet weak var txtComentario: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtCuenta: UITextField!
    @IBOutlet weak var segConCuenta: UISegmentedControl!
    @IBOutlet weak var txtIdentificador: UITextField!
    @IBOutlet weak var segConIdentificador: UISegmentedControl!
    @IBOutlet weak var txtBuscarIfis: UITextField!
    @IBOutlet weak var viewUno: UIView!
    @IBOutlet weak var viewDos: UIView!
    @IBOutlet weak var btnRegistrar: UIButton!
    var esInternaExternaTarjeta: Int = 0
    @IBAction func selecBuscarIfis(_ sender: Any) {
        //Es la ocultar el teclado
        DispatchQueue.main.async {
                   self.view.endEditing(true)
               }
        //fin ocultar
        let pickerPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "IfisViewController") as! IfisViewController
        pickerPopupViewController.popUpDelegate = self
        self.present(pickerPopupViewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        usuario = (tabBarController as! TabBarViewController).usuario
        txtIdentificador.delegate = self
        txtCuenta.delegate = self
        txtNombre.delegate = self
        txtEmail.delegate = self
        txtComentario.delegate = self
        txtIfis.delegate = self
        
        
        if esInternaExternaTarjeta == 3{
            self.navigationItem.title = "Tarjetas"
            segConCuenta.removeSegment(at: 1, animated: true)
            segConCuenta.removeSegment(at: 0, animated: true)
            
        }else{
            segConCuenta.removeSegment(at: 2, animated: true)
        }
        
        btnRegistrar.layer.cornerRadius = _CONST.CORNER_RADIUS
        //para el borde de los views
        self.viewUno.layer.borderWidth = _CONST.BORDER_WIDTH
        self.viewUno.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        
        self.viewDos.layer.borderWidth = _CONST.BORDER_WIDTH
        self.viewDos.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        //fin borde
        
        //para la vista con el scroll view
        // Observe keyboard change
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide2(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // Add touch gesture for contentView
        self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(returnTextView(gesture:))))
        // fin scroll view
       
    }
    
    // para el scroll view autoloyut
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var constraintContentHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    var activeField: UITextField?
    var lastOffset: CGPoint!
    var keyboardHeight: CGFloat!
    
    @objc func returnTextView(gesture: UIGestureRecognizer) {
        guard activeField != nil else {
            return
        }
        
        activeField?.resignFirstResponder()
        activeField = nil
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        lastOffset = self.scrollView.contentOffset
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        activeField?.resignFirstResponder()
        activeField = nil
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var tipoCaracter: CharacterSet = CharacterSet()
        var characterSet: CharacterSet = CharacterSet()
        var retorno: Bool = false
        
        switch textField.tag {
        case 2:
            guard let text = textField.text else { return true }
            let count = text.count + string.count - range.length
            retorno = count <= 15
            break
        case 3:
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 20
            }
            break
        case 4:
            guard let text = textField.text else { return true }
            let count = text.count + string.count - range.length
            retorno = count <= 50
            break
        case 5:
            guard let text = textField.text else { return true }
            let count = text.count + string.count - range.length
            retorno = count <= 100
            break
        case 6:
            guard let text = textField.text else { return true }
            let count = text.count + string.count - range.length
            retorno = count <= 250
            break
        default:
            retorno = true
        }
        
        return retorno
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if keyboardHeight != nil {
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            
            // so increase contentView's height by keyboard height
            UIView.animate(withDuration: 0.3, animations: {
                self.constraintContentHeight.constant += self.keyboardHeight
            })
            
            // move if keyboard hide input field
            if(activeField == nil){
                return
            }
            let distanceToBottom = self.scrollView.frame.size.height - (activeField?.frame.origin.y)! - (activeField?.frame.size.height)!
            let collapseSpace = keyboardHeight - distanceToBottom
            if collapseSpace < 0 {
                // no collapse
                return
            }
            
            // set new offset for scroll view
            UIView.animate(withDuration: 0.3, animations: {
                // scroll to the position above keyboard 10 points
                self.scrollView.contentOffset = CGPoint(x: self.lastOffset.x, y: collapseSpace + 10)
            })
        }
    }
    
    @objc func keyboardWillHide2(notification: NSNotification) {
        UIView.animate(withDuration: 0.3) {
            if self.constraintContentHeight != nil && self.keyboardHeight != nil {
                self.constraintContentHeight.constant -= self.keyboardHeight
                self.scrollView.contentOffset = self.lastOffset
            }
            
        }
        
        keyboardHeight = nil
    }
    
    
    //fin scroll view autolayout
    
    
    func hideKeyboard()  {
        txtComentario.resignFirstResponder()
        txtIdentificador.resignFirstResponder()
        txtCuenta.resignFirstResponder()
        txtNombre.resignFirstResponder()
        txtEmail.resignFirstResponder()
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func registroCuentaExterna(_ sender: Any) {
        guard !(txtBuscarIfis.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_ENTIDAD_FINANCIERA , view: self)
            }
            return
        }
        guard !(txtIdentificador.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_CI_IDENTIFICADOR , view: self)
            }
            return
        }
        guard !(txtCuenta.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_CUENTA , view: self)
            }
            return
        }
        let isSoloNum = Utilidades.SoloNumeros(string: txtCuenta.text!)
        guard (isSoloNum) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_SOLO_NUM, view: self)
            }
            return
        }
        guard !(txtNombre.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_NOMBRE , view: self)
            }
            return
        }
        
        guard !(Utilidades.tieneCaracteresEspeciales(string: txtNombre.text!))else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje:_CONST.MSJ_CARACTERES_ESPECIALES , view: self)
            }
            return
        }
        guard !(Utilidades.tieneCaracteresEspeciales(string: txtIdentificador.text!))else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje:_CONST.MSJ_CARACTERES_ESPECIALES , view: self)
            }
            return
        }
        if(segConIdentificador.selectedSegmentIndex != 2){
            guard (Identificador.validar(identificador: txtIdentificador.text!))else{
             DispatchQueue.main.async {
             Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje:_CONST.MSJ_IDENTIFICACION , view: self)
             }
             return
             }
        }
        
        guard !(txtEmail.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_CI_EMAIL , view: self)
            }
            return
        }
        let isEmailAddressValid = Utilidades.isValidEmailAddress(emailAddressString: txtEmail.text!)
        guard (isEmailAddressValid) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_EMAIL_INC , view: self)
            }
            return
        }
        guard !(txtComentario.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_CI_COMENTARIO , view: self)
            }
            return
        }
        guard !(Utilidades.tieneCaracteresEspeciales(string: txtComentario.text!))else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje:_CONST.MSJ_CARACTERES_ESPECIALES , view: self)
            }
            return
        }
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        insertarCuentaExterna(idUsuario: usuario.strIdentificacion, numCuenta: txtCuenta.text!, idIfi: auxIdIfi, descripcionIfi: txtIfis.text!, idTipoCuenta: selecTipoCuenta(auxSC_TipoCuenta: segConCuenta.selectedSegmentIndex), idTipoIdentificacion: selecTipoIdentificacion(auxSegmenControll: segConIdentificador.selectedSegmentIndex), identificacion: txtIdentificador.text!, nombre: txtNombre.text!, email: txtEmail.text!, comentario: txtComentario.text!, token: usuario.strToken, codigoOtp: _CONST.CODIGO_OK)
        
    }
    
    func selecTipoIdentificacion(auxSegmenControll: Int) -> String {
        var strTipoIdentificacion: String = ""
        switch auxSegmenControll {
        case 0:
            strTipoIdentificacion = _CONST.CE_TI_CEDULA
            break
        case 1:
            strTipoIdentificacion = _CONST.CE_TI_RUC
            break
        case 2:
            strTipoIdentificacion = _CONST.CE_TI_PASAPORTE
            break
        default:
            break
        }
        return strTipoIdentificacion
    }
    
    func selecTipoCuenta(auxSC_TipoCuenta: Int) -> String {
        var auxTipoCuenta: String = ""
        switch auxSC_TipoCuenta {
        case 0:
            auxTipoCuenta = _CONST.CE_TC_AHORROS
            break
        case 1:
            auxTipoCuenta = _CONST.CE_TC_CORRIENTE
            break
        case 2:
            auxTipoCuenta = _CONST.CE_TC_TARJETA_CREDITO
            break
        default:
            break
        }
        
        if self.esInternaExternaTarjeta == 3{
            auxTipoCuenta = _CONST.CE_TC_TARJETA_CREDITO
        }
        return auxTipoCuenta
    }
    
    func insertarCuentaExterna( idUsuario: String , numCuenta: String , idIfi: String ,descripcionIfi: String, idTipoCuenta: String, idTipoIdentificacion: String , identificacion: String, nombre: String, email: String, comentario: String, token: String, codigoOtp: String) -> Void{
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = ["idUsuario":idUsuario,
                                   "numCuenta":numCuenta,
                                   "idIfi":idIfi,
                                   "strIfiCEDescripcion": descripcionIfi,
                                   "idTipoCuenta":idTipoCuenta,
                                   "idTipoIdentificacion":idTipoIdentificacion,
                                   "identificacion":identificacion,
                                   "nombre":nombre,
                                   "email":email,
                                   "comentario":comentario,
                                   "strToken": token,
                                   "strOTP": codigoOtp,
                                   "strCanalSeguridad": _CONST.CANAL_SEG_IOS]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/insertarCuentaExterna")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { (data, response, error) in
            if let data = data
                {
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    
                    if  mensajeRespuestaData.strCodigoError  == _CONST.CODIGO_OK{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            let alertControler = UIAlertController(title: _CONST.AVISO, message: mensajeRespuestaData.strMensajeError, preferredStyle: .alert)
                            
                            let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
                            { (action:UIAlertAction!) in
                                
                                self.txtBuscarIfis.text = ""
                                self.txtIdentificador.text = ""
                                self.txtCuenta.text = ""
                                self.txtNombre.text = ""
                                self.txtComentario.text = ""
                                self.txtEmail.text = ""
                                self.performSegue(withIdentifier: "unwindSegueCExterna", sender: self)
                                
                            }
                            alertControler.addAction(aceptarAccion)
                            self.present(alertControler, animated: true, completion: nil)
                        }
                    }else{
                        if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }else if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_SEG{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                        }
                    }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir Insertar Cuentas Externa:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        

        
    }
    //enviar OTP
    //esta funcion solo envia el OTP, para utilizar este funcion cualquiera de las dos variables deben estar estar correctos.
    func enviarCodigoOPT(userName: String, identificacion:String, token:String)   {
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        let json: [String: Any] = [ "strUsername": userName,
                                    "strIdentificacion": identificacion,
                                    "strToken": token,
                                   
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                    "strObservacion": _CONST.CANAL_DES
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/procesoEnvioOTP")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { (data, response, error) in
            
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let otpRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  otpRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK {
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
                            otpViewController.popUpOTPDelegate = self
                            self.present(otpViewController, animated: true)
                        }
                    }else{
                        if otpRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: otpRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_OTP_NOK , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir Envio OTP :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }

    
}
extension RegistrarCuentaExternaViewController: PopUpDelegate{
    func seleccionString(value: String, tipoSelection: String, tipoCuenta: String, saldoDisponible: String) {
        
        txtBuscarIfis.text = value
        self.auxIfisCuenta = tipoSelection // en esta variable viene la cuenta de IFIS
        self.auxIdIfi = tipoCuenta //en esta variable viene un IDIFIs
        
    }
    
}
extension RegistrarCuentaExternaViewController: OTPDelegate{
    func inserOTP(value: String) {
        
        
        
    }
}
