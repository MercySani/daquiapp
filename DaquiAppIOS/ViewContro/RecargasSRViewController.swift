//
//  SBTiempoAireViewController.swift
//  DaquiApp
//
//  Created by Fredy on 9/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class RecargasSRViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtEmpresa: UITextField!
    @IBOutlet weak var txtCuenta: UITextField!
    @IBOutlet weak var txtCelular: UITextField!
    @IBOutlet weak var txtValor: UITextField!
    @IBOutlet weak var saldoDisponibleLbl: UILabel!
    @IBOutlet weak var tipoCuentaLbl: UILabel!
    var auxTituloReferencia: String = ""
    
    var usuario: UsuarioModel = UsuarioModel()
    let objActivity = activityIndicatorVC()
    var listaServicios: [ServicioSRModel] = []
    var aucIdServicio: String = ""
    var auxServicioNombre: String = ""
    var saldoDisponible: Double = 0
    var auxIdCategoria: Int = 0
    var posicionConsolidada: [PosicionConsolidadaModel] = []
    var posicionConsolidadaAuxiliar: [PosicionConsolidadaModel] = []
    var posicionConsolidadaString: PosicionConsolidadaModel = PosicionConsolidadaModel ()
    var responseConsulta: ConsultaServicioFacilitoModel = ConsultaServicioFacilitoModel()
    var auxlisValoresPendientes: [ListaValoresPendientes] = []
    var auxSWC_Valor: [SWC_Valor] = []
    var auxInformacionAdicional: String = ""
    
    @IBOutlet weak var btnContinuar: UIButton!
    @IBOutlet weak var viewUno: UIView!
    @IBOutlet weak var viewDos: UIView!
    var cuentaDebitoCliente: String = ""

    @IBAction func consultarEmpresa(_ sender: Any) {
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        
        listaServicioSR(usuario: usuario.strIdentificacion, parametroCategoria: self.auxIdCategoria, token: usuario.strToken)
    }
    
    
    @IBAction func consultarCuenta(_ sender: Any) {
        self.posicionConsolidadaAuxiliar.removeAll()
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        self.posicionConsolidada(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_CEDULA, usuario: usuario.strIdentificacion, token: usuario.strToken)
    }
    
    
    @IBAction func selecValorRecarga(_ sender: Any) {
        DispatchQueue.main.async {
        self.view.endEditing(true)
        }
        if(auxSWC_Valor.count > 0){
            let pickerPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickerPopUpViewController") as! PickerPopUpViewController
            pickerPopupViewController.popUpDelegate = self
            pickerPopupViewController.tipoSelec = _CONST.RECARGAS
            pickerPopupViewController.lisValoresRecarga = auxSWC_Valor
            self.present(pickerPopupViewController, animated: true)
        }
    }
    @IBAction func pagarRecarga(_ sender: Any) {
        guard !(txtEmpresa.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SBF_MEN_SERVICIOS , view: self)
            }
            return
        }
        
        guard !(txtCelular.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SBF_MEN_DINAMICO+""+self.auxTituloReferencia , view: self)
            }
            return
        }
        
        let isSoloNum = Utilidades.SoloNumeros(string: txtCelular.text!)
        guard (isSoloNum) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_SOLO_NUM, view: self)
            }
            return
        }
        guard !(txtValor.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SB_MEN_VALOR , view: self)
            }
            return
        }
        guard !(txtCuenta.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SER_BAS_MENSA_CUENTA , view: self)
            }
            return
        }
        let auxVlorTotal: Double = Double(txtValor.text!)!
        
        if (auxVlorTotal.isLess(than: self.saldoDisponible - Double(_CONST.MONTO_MINIMO))){
            DispatchQueue.main.async {
                
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            self.enviarCodigoOPT(userName: usuario.strNombreUser, identificacion: usuario.strIdentificacion, token: usuario.strToken)
        }else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MEN_FONDOS, view: self)
            }
        }
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usuario = (tabBarController as! TabBarViewController).usuario
        
        self.viewUno.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.viewUno.layer.borderWidth = _CONST.BORDER_WIDTH
        self.viewUno.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        
        self.viewDos.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.viewDos.layer.borderWidth = _CONST.BORDER_WIDTH
        self.viewDos.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        
       txtCuenta.delegate = self
        txtCelular.delegate = self
        txtValor.delegate = self
        
        btnContinuar.layer.cornerRadius = _CONST.CORNER_RADIUS
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var tipoCaracter: CharacterSet = CharacterSet()
        var characterSet: CharacterSet = CharacterSet()
        var retorno: Bool = false
        
        switch textField.tag {
        case 2:
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 25
            }
            break
        case 3:
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 4
            }
            break
        case 4:
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 15
            }
            
            break
            
        default:
            retorno = true
        }
        
        return retorno
    }
    
    //servicio
    func listaServicioSR(usuario: String, parametroCategoria: Int, token: String)->Void{
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/servicioPorParametrosSR/\(_CONST.TODO)/\(parametroCategoria)/\(_CONST.FRECUENCIA_DE_PAGO)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==202 || response.statusCode==200 {
                do{
                    self.listaServicios =  try JSONDecoder().decode([ServicioSRModel].self , from: data)
                    if  self.listaServicios.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            let pickerPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickerPopUpViewController") as! PickerPopUpViewController
                            pickerPopupViewController.popUpDelegate = self
                            pickerPopupViewController.tipoSelec = "SERVICIOS"
                            pickerPopupViewController.listaServicio = self.listaServicios
                            self.present(pickerPopupViewController, animated: true)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "No existe servicios disponibles " , view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir lista Servicios SR:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir lista Servicios SR: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    //fin servicio
    
    
    //para Buscar la cuenta
    func posicionConsolidada( identificacion: String, tipoIdentificacion: String, usuario: String,  token: String ){
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/posicionConsolidada/\(_CONST.CANAL_DES)/\(identificacion)/\(tipoIdentificacion)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.posicionConsolidada = try JSONDecoder().decode([PosicionConsolidadaModel].self , from: data)
                    
                    if  self.posicionConsolidada.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            for item in self.posicionConsolidada{
                                if(item.Producto == _CONST.CNTA_DEBITO_VISTA  || item.Producto == _CONST.CNTA_DEBITO_KULLKIMIRAK || item.Producto == _CONST.CNTA_DEBITO_BASICA){
                                    self.posicionConsolidadaString.Cuenta = item.Cuenta
                                    self.posicionConsolidadaString.Descripcion = item.Descripcion
                                    self.posicionConsolidadaString.GProducto = item.GProducto
                                    self.posicionConsolidadaString.Producto = item.Producto
                                    self.posicionConsolidadaString.Saldoaportacion = item.Saldoaportacion
                                    self.posicionConsolidadaString.Saldobloqueado = item.Saldobloqueado
                                    self.posicionConsolidadaString.Saldocontable = item.Saldocontable
                                    self.posicionConsolidadaString.Saldoefectivo = item.Saldoefectivo
                                    self.posicionConsolidadaAuxiliar.append(self.posicionConsolidadaString)
                                }
                            }
                            
                            if self.posicionConsolidadaAuxiliar.count > 1 {
                                let popUpCuentaViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpCuentaViewController") as!  PopUpCuentaViewController
                                popUpCuentaViewController.popUpDelegate = self
                                popUpCuentaViewController.tipoSelec = _CONST.SELEC_CUENTA
                                popUpCuentaViewController.listaOpciones = self.posicionConsolidadaAuxiliar
                                self.present(popUpCuentaViewController, animated: true)
                            }else{
                                if(self.posicionConsolidadaString.Cuenta==""){
                               self.txtCuenta.text = "No tiene cuentas Vista o Kullkimirak"
                                                                 }else{
                                    self.txtCuenta.text = self.posicionConsolidadaString.Cuenta
                                    let auxTipoCuenta = NSMutableAttributedString()
                                    auxTipoCuenta
                                        .bold("Tipo: ")
                                        .normal("\(self.posicionConsolidadaString.Descripcion)")
                                    self.tipoCuentaLbl.attributedText = auxTipoCuenta
                                    
                                    //variable para comparacion si la deuda no excede es saldo disponible
                                    self.saldoDisponible =  Double(self.posicionConsolidadaString.Saldoefectivo)!
                                    let auxSaldoDisponible = NSMutableAttributedString()
                                    auxSaldoDisponible
                                        .bold("Saldo Disponible: $ ")
                                        .normal("\(self.posicionConsolidadaString.Saldoefectivo)")
                                    self.saldoDisponibleLbl.attributedText = auxSaldoDisponible
                                }
                                
                            }
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir Posicion Consolidada:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    //pagar Servicios BAsico FAcilito
    func pagarSBF(idSolicitud: Int, operador: Int ,  idCanal: Int, idAgencia:String, usuarioFIT: String, claveFIT: String, canalInterno: String, ip: String, identificacio: String, lisValoresPendientes: [ListaValoresPendientes], codigoOTP: String, numCuenta: String, referencia: String , idServicio: String, nombreServicio: String , informacionAdicional: String, token: String )   {
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let codificadoJSON = try! encoder.encode(lisValoresPendientes)
        
        
        let json: [String: Any] = [ "idSolicitud": idSolicitud ,
                                    "operador": operador,
                                    "idCanal": idCanal,
                                    "idAgencia":  idAgencia,
                                    "usuarioFIT": usuarioFIT,
                                    "claveFIT": claveFIT,
                                    "canalInterno": canalInterno,
                                    "ip": ip,
                                    "identificacion": identificacio,
                                    "lisValoresPendientes": String(data: codificadoJSON, encoding: .utf8)! ,
                                    "strCodigoOtp": codigoOTP,
                                    "numcuenta": numCuenta,
                                    "referencia": referencia,
                                    "idServicio": idServicio,
                                    "nombreServicio":nombreServicio,
                                    "informacionAdicional": informacionAdicional,
                                    "strToken": token,
                                    "strTipoIdentificacion": _CONST.CI_TIPO_IDENTIFICACION,
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS
        ]
        
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/pagoServicio")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { (data, response, error) in
            
            if let data = data, let _ = response as? HTTPURLResponse
            {
                do{
                    
                    
                    let mensajeResponse = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  mensajeResponse.strCodigoError  == _CONST.CODIGO_RETORNO_OK{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            self.txtEmpresa.text = ""
                            self.txtCelular.text = ""
                         
                            self.txtValor.text = ""
                            self.txtCuenta.text = ""
                            self.tipoCuentaLbl.text = "Tipo:"
                            self.saldoDisponibleLbl.text = "Saldo Disponible: "
                            
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeResponse.strMensajeError , view: self)
                            
                        }
                    }else{
                        if mensajeResponse.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeResponse.strMensajeError , view: self)
                            }
                        }else if mensajeResponse.strCodigoError == _CONST.CODIGO_ERROR_SEG{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeResponse.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeResponse.strMensajeError , view: self)
                            }
                            
                        }
                    }
                    
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir Pagar Servicios Basicos Facilitp :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir Servicios Basicos: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    
    
    
    
    //enviar OTP
    //esta funcion solo envia el OTP, para utilizar este funcion cualquiera de las dos variables deben estar estar correctos.
    func enviarCodigoOPT(userName: String, identificacion: String, token:String)   {
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = [ "strUsername": userName,
                                    "strIdentificacion": identificacion,
                                    "strToken": token,
                                   
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                    "strObservacion": _CONST.CANAL_DES
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/procesoEnvioOTP")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { (data, response, error) in
            
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let otpRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  otpRespuestaData.strCodigoError  == _CONST.CODIGO_RETORNO_OK {
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
                            otpViewController.popUpOTPDelegate = self
                            self.present(otpViewController, animated: true)
                        }
                    }else{
                        if otpRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: otpRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_OTP_NOK , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir Envio OTP :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir Envio OTP: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    
    

}



extension RecargasSRViewController: PopUpDelegate{
    func seleccionString(value: String, tipoSelection: String, tipoCuenta: String, saldoDisponible: String) {
        
        if (tipoSelection == "SERVICIOS"){
            self.txtEmpresa.text = value
            
            for items in listaServicios{
                if value == items.nombre{
                    self.aucIdServicio = items.id
                    self.auxServicioNombre = items.nombre
                    if(items.tituloreferencia != ""){
                    self.txtCelular.placeholder = "Ingrese el  \(items.tituloreferencia.lowercased())"
                    self.auxTituloReferencia = items.tituloreferencia.lowercased()
                    }else{
                    self.auxTituloReferencia = "referencia"
                    }
                    if(items.valoresrecarga.count > 0){
                        self.auxSWC_Valor = items.valoresrecarga
                    }else{
                        self.auxSWC_Valor.removeAll()
                    }
                }
            }
        }
        
        if(tipoSelection == _CONST.RECARGAS)
        {
           self.txtValor.text = value
        
        }
        if (tipoSelection == _CONST.SELEC_CUENTA){
            txtCuenta.text = value
            let auxTipoCuenta = NSMutableAttributedString()
            auxTipoCuenta
                .bold("Tipo: ")
                .normal("\(tipoCuenta)")
            self.tipoCuentaLbl.attributedText = auxTipoCuenta
            self.saldoDisponible =  Double(saldoDisponible)!
            let auxSaldoDisponible = NSMutableAttributedString()
            auxSaldoDisponible
                .bold("Saldo Disponible: $ ")
                .normal("\(saldoDisponible)")
            self.saldoDisponibleLbl.attributedText = auxSaldoDisponible
        }
        
        
    }
    
}

extension RecargasSRViewController: OTPDelegate{
    func inserOTP(value: String) {
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        
    self.auxlisValoresPendientes.removeAll()
        
        let date = Date()
        let dateStringFormatter = DateFormatter()
        
        dateStringFormatter.dateStyle = .short
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        
                    // para asignar el valor total de la comision en un struct
        
        var auxValoUnitario: ListaValoresPendientes = ListaValoresPendientes()
        auxValoUnitario.valorUnitario = Double(txtValor.text!)!
        auxValoUnitario.fActualizacion = dateStringFormatter.string(from: date)
        auxValoUnitario.fCreacion = dateStringFormatter.string(from: date)
        auxValoUnitario.afectacionDetalleRecibo.fActualizacion = dateStringFormatter.string(from: date)
        auxValoUnitario.afectacionDetalleRecibo.fCreacion = dateStringFormatter.string(from: date)
        auxValoUnitario.afectacionDetalleRecibo.rubroTipoAfectacion.fActualizacion = dateStringFormatter.string(from: date)
        auxValoUnitario.afectacionDetalleRecibo.rubroTipoAfectacion.fCreacion = dateStringFormatter.string(from: date)
        auxValoUnitario.afectacionDetalleRecibo.rubroTipoAfectacion.rubro.fCreacion = dateStringFormatter.string(from: date)
        auxValoUnitario.afectacionDetalleRecibo.rubroTipoAfectacion.rubro.fActualizacion = dateStringFormatter.string(from: date)
        self.auxlisValoresPendientes.append(auxValoUnitario)
        
                    
                
        
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
         pagarSBF(idSolicitud: 0, operador: _CONST.OPERADOR_SB, idCanal: _CONST.INTERNET_BANKING, idAgencia: _CONST.ID_AGENCIA_SB, usuarioFIT: self.usuario.strIdentificacion, claveFIT: self.usuario.strIdentificacion, canalInterno: _CONST.CANAL_DES, ip: macAddress?.description ?? "" , identificacio: self.usuario.strIdentificacion, lisValoresPendientes: self.auxlisValoresPendientes, codigoOTP: value, numCuenta: txtCuenta.text! , referencia: txtCelular.text!, idServicio: self.aucIdServicio, nombreServicio: auxServicioNombre, informacionAdicional: _CONST.DATO_ADICIONAL, token: self.usuario.strToken)
        
        
    }
    
    
}


