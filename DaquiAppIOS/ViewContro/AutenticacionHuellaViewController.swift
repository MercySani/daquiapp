//
//  AutenticacionHuellaViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 03/01/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import UIKit
import LocalAuthentication
class AutenticacionHuellaViewController: UIViewController {
   
    @IBAction func authWithTouchID(_ sender: Any) {
        // 1
        let context = LAContext()
        var error: NSError?
        
        // 2
        // check if Touch ID is available
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            // 3
            let reason = "Authenticate with Touch ID"
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply:
                {(success, error) in
                    // 4
                    if success {
                        self.showAlertController("Touch ID Authentication Succeeded")
                    }
                    else {
                        self.showAlertController("Touch ID Authentication Failed")
                    }
            })
        }
            // 5
        else {
            showAlertController("Touch ID not available")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func showAlertController(_ message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func cancelar(_ sender: Any) {
        dismiss(animated: true)
        //DispatchQueue.main.async {
        // self.performSegue(withIdentifier: "menuPrincipal", sender: self)
        //}
         
    }

}

