//
//  TransaccionBimoViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 08/05/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import UIKit
import ContactsUI

class TransaccionBimoViewController: UIViewController , UITextFieldDelegate, CNContactPickerDelegate {

    var lblTitulo: String?
    var lblMonto: String?
    var monto: Double?
    
    @IBOutlet weak var view1: UIView!
    
    @IBOutlet weak var txtMonto: UITextField!
    @IBOutlet weak var lblAccion: UILabel!
    @IBOutlet weak var btnContinuar: UIButton!
    @IBOutlet weak var referencia: UITextField!
    @IBOutlet weak var receptorDelCobro: UITextField!
    
    var usuario: UsuarioModel = UsuarioModel()
    var strCriterio: String?
    var origen: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblAccion.text = lblTitulo
        self.txtMonto.text = lblMonto
        print(usuario)
        self.view1.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.view1.layer.borderWidth = _CONST.BORDER_WIDTH
        self.view1.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        self.btnContinuar.layer.cornerRadius = _CONST.CORNER_RADIUS
    
        self.txtMonto.delegate = self
        self.receptorDelCobro.delegate = self
        self.referencia.delegate = self
    }
    
    @IBAction func accionContinuar(_ sender: Any) {
        guard  !(Utilidades.tieneCaracteresEspeciales(string: self.referencia.text!)) else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_CARACTERES_ESPECIALES, view: self)
            }
            return
        }
        guard !(txtMonto.text!.isEmpty) else {
        DispatchQueue.main.async {
           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_MONTO , view: self)
         }
        return
        }
        guard !(receptorDelCobro.text!.isEmpty) else {
        DispatchQueue.main.async {
           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_BIMO_ERROR_RECEPTOR , view: self)
         }
        return
        }
        guard !(referencia.text!.isEmpty) else {
        DispatchQueue.main.async {
           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_BIMO_ERROR_REFERENCIA , view: self)
         }
        return
        }
       
        guard (receptorDelCobro.text!.count == 10) else {
        DispatchQueue.main.async {
           Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_BIMO_ERROR_RECEPTOR_LONGITUD , view: self)
         }
        return
        }
        guard (Utilidades.SoloNumeros(string: self.receptorDelCobro.text!)) else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_BIMO_ERROR_RECEPTOR_CARACTERES, view: self)
            }
            return
        }
        monto = Double(txtMonto.text!)
        
        guard  (self.monto! >= 1.00 && self.monto! <= 50.00) else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje:  _CONST.MSJ_BIMO_MONTO_MIN_MAX, view: self)
            }
            return
        }
       performSegue(withIdentifier:"segueDialogBimo", sender: self)
    }
    
   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var retorno: Bool = false
        if textField.tag == 0{
            if(Utilidades.validarNumero(textField: textField, range: range,string: string)){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 5
            }
        }
        if textField.tag == 1{
            if(Utilidades.validarNumero(textField: textField, range: range,string: string)){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 10
            }
        }
        if textField.tag == 2{
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 35
        }
        return retorno
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.txtMonto.resignFirstResponder()
            self.receptorDelCobro.resignFirstResponder()
            self.referencia.resignFirstResponder()
            self.view.endEditing(true)
       }

    @IBAction func accederContactos(_ sender: Any) {
        let picker = CNContactPickerViewController()
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact){
        //contacts.forEach{ (contact) in
            for data in contact.phoneNumbers{
                let contacts = data.value
                let cadena = contacts.stringValue
                var nuevaCadena = cadena.replacingOccurrences(of: _CONST.CODIGO_ECUADOR, with: "0", options: .literal, range: nil)
                nuevaCadena = nuevaCadena.replacingOccurrences(of: "-", with: "", options: .literal, range: nil)
                nuevaCadena = nuevaCadena.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                receptorDelCobro.text = nuevaCadena
                
            }
       // }
    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("contactPickerDidCancel---------")
    }
    
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
       if segue.identifier == "segueDialogBimo" {
           if let destino = segue.destination as? AlertBimoViewController{
            destino.usuario = usuario
            destino.strTelefonoDestino = self.receptorDelCobro.text
            destino.strMonto = self.txtMonto.text
            destino.strDescripcion = self.referencia.text
            destino.origen = self.origen
               if self.lblAccion.text == "Pagar" {
                   destino.strTrama = _CONST.TRANSFER_RQ
                   destino.variableMensaje = "¿Desea hacer este pago? "
                   destino.strCriterio = strCriterio
               }else{
                destino.strTrama = _CONST.SOLICITUD_COBRO_RQ
                destino.variableMensaje = "¿Desea hacer este cobro? "
            }
               
           }
       }
   }
    
    @IBAction func unwindTransaccionBimo(_ sender: UIStoryboardSegue) {
        if sender.source is LectorQRViewController {
            if let senderVC = sender.source as? LectorQRViewController{
                receptorDelCobro.text = senderVC.numeroTelefono
            }
        }
        
       }
}
