//
//  PickerPopUpViewController.swift
//  DaquiApp
//
//  Created by Fredy on 4/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class PickerPopUpViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    var popUpDelegate: PopUpDelegate?
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var cancelarBtn: UIButton!
    @IBOutlet weak var AceptarBtn: UIButton!
    @IBOutlet weak var pickerOpciones: UIPickerView!
    var lisValoresRecarga: [SWC_Valor] = []
    var listaOpciones: [String] = []
    var auxOpciones: String = ""
    var tipoSelec: String = ""
    
    //pago Programado
    var listaCategoria:[CategoriaSRModel] = []
    var listaServicio: [ServicioSRModel] = []
    
    
    //fin pago Programado
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelarBtn.layer.cornerRadius = _CONST.CORNER_RADIUS
        AceptarBtn.layer.cornerRadius = _CONST.CORNER_RADIUS
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var auxCount: Int  = 0
        if(lisValoresRecarga.count != _CONST.LISTA_VACIA){
            auxCount = lisValoresRecarga.count
        }
        if listaOpciones.count != _CONST.LISTA_VACIA{
                auxCount = listaOpciones.count
        }
        if listaCategoria.count != _CONST.LISTA_VACIA{
            auxCount = listaCategoria.count
        }
        if listaServicio.count != _CONST.LISTA_VACIA{
            auxCount = listaServicio.count
        }
        return auxCount
    }
    
   func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
    var titleRow: String = ""
    
    if(lisValoresRecarga.count != _CONST.LISTA_VACIA ){
        titleRow = lisValoresRecarga[row].valor
    }
    if listaOpciones.count != _CONST.LISTA_VACIA{
                titleRow = listaOpciones[row]
            }
            if listaCategoria.count != _CONST.LISTA_VACIA{
                titleRow  = listaCategoria[row].descripcion
            }
            if listaServicio.count != _CONST.LISTA_VACIA{
                titleRow = listaServicio[row].nombre
            }
        return titleRow
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(lisValoresRecarga.count != _CONST.LISTA_VACIA){
            self.auxOpciones = self.lisValoresRecarga[row].valor
        }
        if listaOpciones.count != _CONST.LISTA_VACIA{
            self.auxOpciones = self.listaOpciones[row]
        }
        if listaCategoria.count != _CONST.LISTA_VACIA{
            self.auxOpciones = self.listaCategoria[row].descripcion
        }
        if listaServicio.count != _CONST.LISTA_VACIA{
            self.auxOpciones = self.listaServicio[row].nombre
        }
        
    }
  
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func aceptarBtn(_ sender: UIButton) {
        let row = pickerOpciones.selectedRow(inComponent: 0)
        
        
        pickerView(pickerOpciones, didSelectRow: row, inComponent: 0)
        
        popUpDelegate?.seleccionString(value: self.auxOpciones, tipoSelection: self.tipoSelec, tipoCuenta: "", saldoDisponible: "" )
        dismiss(animated: true)
    }
    
    @IBAction func cancelarBtn(_ sender: UIButton) {
        dismiss(animated: true)
    }
}
