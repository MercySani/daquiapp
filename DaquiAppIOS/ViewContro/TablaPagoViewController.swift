//
//  PagosSWController.swift
//  DaquiApp
//
//  Created by Fredy on 17/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
import UIKit

class TablaPagoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var numeroCuenta: String = String()
    var usuario: UsuarioModel = UsuarioModel()
    var tablaPagosTotal: [TablaPagoModel] = []
    var tablaPagosRealizados: [TablaPagoModel] = []
    var tablaPagos: [TablaPagoModel] = []
    let objActivity = activityIndicatorVC()
    @IBOutlet weak var pagosTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        usuario = (tabBarController as! TabBarViewController).usuario
        if(usuario.strNombreUser == _CONST.USUARIO_DEMO){
            self.tablaPagosTotal = _DEMO.tablaPagos()
            self.tablaPagosRealizados = _DEMO.pagosRealizados()
            self.tablaPagos.removeAll()
            self.tablaPagos = self.tablaPagosRealizados
            self.pagosTableView.reloadData();
        }else{
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            consultaTablaPagosRealizados(identificacion: usuario.strIdentificacion, idTipoIdentificacion: _CONST.TI_CEDULA, numeroCuenta: self.numeroCuenta, token: usuario.strToken)
            consultaTablaPagos(identificacion: usuario.strIdentificacion, idTipoIdentificacion: _CONST.TI_CEDULA, numeroCuenta: self.numeroCuenta, token: usuario.strToken)
        }
    }

    func consultaTablaPagos(identificacion: String, idTipoIdentificacion: String, numeroCuenta: String, token: String) -> Void {
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/consultaTablaPagos/\(_CONST.CANAL_DES)/\(identificacion)/\(idTipoIdentificacion)/\(numeroCuenta)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.tablaPagosTotal = try JSONDecoder().decode([TablaPagoModel].self , from: data)
                    
                    if  self.tablaPagosTotal.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje:_CONST.MSJ_TBL_P_SIN, view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir Tabla de pagos :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    
    func consultaTablaPagosRealizados( identificacion: String, idTipoIdentificacion: String, numeroCuenta: String, token: String) -> Void {
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/consultaPagosRealizados/\(_CONST.CANAL_DES)/\(identificacion)/\(idTipoIdentificacion)/\(numeroCuenta)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.tablaPagosRealizados = try JSONDecoder().decode([TablaPagoModel].self , from: data)
                    
                    if  self.tablaPagosRealizados.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            self.tablaPagos.removeAll()
                            self.tablaPagos = self.tablaPagosRealizados
                            self.pagosTableView.reloadData()
                            
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_TBL_PR_SIN, view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    } catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir Tabla de Pagos realizados:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tablaPagos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tablaPagoCell") as! GenericoTableViewCell
        cell.detalle1.text = self.tablaPagos[indexPath.row].NumeroCouta
        cell.detalle2.text = self.tablaPagos[indexPath.row].FVence
        cell.detalle3.text = "$\(self.tablaPagos[indexPath.row].Total)"
        return cell
    }
    @IBAction func sc_actualizarTablaPagos(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.tablaPagos.removeAll()
            self.tablaPagos = self.tablaPagosRealizados
            self.pagosTableView.reloadData();
        case 1:
            self.tablaPagos.removeAll()
            self.tablaPagos = self.tablaPagosTotal
            self.pagosTableView.reloadData();
        default: break;
        }
    }
}
