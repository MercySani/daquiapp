//
//  SimuladorCreditosViewController.swift
//  DaquiApp
//
//  Created by Fredy on 3/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit
import Foundation

class SimuladorCreditosViewController: UIViewController, UITextFieldDelegate {
    //INI VARIABLES
    
    var saldo: Double = 0.0
    var rate: Double = 0.0
    var var_tipo_cred: Double = 0
    var var_frec_pago: Double = 0
    var var_cuota_cred: Double = 0
    var auxTotal:Double = 0.0
    var seguro: Double = 0.000302
    var payment: Double = 0.0
    var payment2: Double = 0.0
    var band: Double = 0.0
    var calculoseguroIncendio:Double = 0.0
    var valorSeguroIncendio:Double = 0.0
    var monthly: Double = 0.0
    var frase: String = ""
    var interest: Double = 0.0
    var monto_cred: Double = 0 //amount
    var plazo_cred: Double = 0 //numpay
    var valorbien:Double = 0.0 //amountBien
    var newPrincipal: Double = 0.0
    var newPrincipal2: Double = 0.0
    var newInterest2: Double = 0.0
    var newSeguro2: Double = 0.0
    var reduction2: Double = 0.0
    var newInteresWmo: Double = 0.0
    var newSeguro: Double = 0.0
    var reduction: Double = 0.0
    var frecuenciaSelect: Double = 0.0
    var seleccionCredito: Int = 0
 
    //FIN VARIABLE
    
    @IBOutlet weak var textField: NoCopyPasteUITextField!
    
    
    @IBOutlet weak var txtFrecuencia: UITextField!
    @IBOutlet weak var txtValorBien: UITextField!
    @IBOutlet weak var txtMonto: UITextField!
    @IBOutlet weak var txtPalzos: UITextField!
    @IBOutlet weak var segCTipoCuota: UISegmentedControl!
    //@IBOutlet weak var segCFrecuenciaPago: UISegmentedControl!
    @IBOutlet weak var api: UILabel!
    @IBOutlet weak var certificado: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var interesAnual: UILabel!
    @IBOutlet weak var capital: UILabel!
    @IBOutlet weak var numeroPagos: UILabel!
    @IBOutlet weak var totalPagos: UILabel!
    @IBOutlet weak var totalInteres: UILabel!
   
    var listOpciones: [String] = []
    
    @IBOutlet weak var btnCalcular: UIButton!
    @IBOutlet weak var btnBorrar: UIButton!
    
    override func viewDidLoad() {
      super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = "Creditos"
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        
        txtMonto.keyboardType = UIKeyboardType.numberPad
        
        btnBorrar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnCalcular.layer.cornerRadius = _CONST.CORNER_RADIUS
        txtMonto.delegate = self
        txtPalzos.delegate = self
        txtValorBien.delegate = self
        txtValorBien.isHidden = true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var tipoCaracter: CharacterSet = CharacterSet()
        var characterSet: CharacterSet = CharacterSet()
        var retorno: Bool = false
        if textField.tag == 1{
            if(Utilidades.validarNumero(textField: textField, range: range,string: string)){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 7
            }
        }
        
        if textField.tag == 2{
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 3
            }
        }
        
        if textField.tag == 4{
            if(Utilidades.validarNumero(textField: textField, range: range,string: string)){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 9
            }
        }
        
        return retorno
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func btnCalcularCredito(_ sender: Any) {
        guard !textField.text!.isEmpty else {
                          DispatchQueue.main.async {
                              Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_TIPO_CREDITO , view: self)
                          }
                          return
        }
        
        guard !txtMonto.text!.isEmpty else {
                   DispatchQueue.main.async {
                       Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_MONTO , view: self)
                   }
                   return
        }
        
        guard (Double(txtMonto.text!)! <= _CONST.MONTO_MAX_CREDITO) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_MONTO_MAX_CREDITO , view: self)
            }
            return
        }
        
        guard (Double(txtMonto.text!)! > 0) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_MONTO , view: self)
                self.txtMonto.text = ""
                self.txtMonto.reloadInputViews()
            }
            return
        }
        
        guard !txtPalzos.text!.isEmpty else {
                   DispatchQueue.main.async {
                       Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_PLAZO , view: self)
                   }
                   return
        }
        
        guard (Double(txtPalzos.text!)! > 0) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_PLAZO , view: self)
                self.txtPalzos.text = ""
                self.txtPalzos.reloadInputViews()
            }
            return
        }
       
        guard !txtFrecuencia.text!.isEmpty else {
               DispatchQueue.main.async {
                   Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_FRECUENCIA , view: self)
               }
          return
        }
        
        self.calcular_credito()
        
    }
    
    
    @IBAction func selecFrecuencia(_ sender: UITextField) {
        DispatchQueue.main.async {
            self.view.endEditing(true);
        }
        print("self.seleccionCredito   \(self.seleccionCredito) ")
        let pickerPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickerPopUpViewController") as! PickerPopUpViewController
        pickerPopupViewController.popUpDelegate = self
        if  self.seleccionCredito == 0 {
           pickerPopupViewController.listaOpciones = _CONST.LISTA_FRECUENCIA_SQM
        }else{
           pickerPopupViewController.listaOpciones = _CONST.LISTA_FRECUENCIA_MBTS
        }
        pickerPopupViewController.tipoSelec = _CONST.FRECUENCIA
        self.present(pickerPopupViewController, animated: true)
    }
    
    @IBAction func selecTipoCredito(_ sender: UITextField) {
        DispatchQueue.main.async {
            self.view.endEditing(true);
        }
        
        let pickerPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickerPopUpViewController") as! PickerPopUpViewController
        pickerPopupViewController.popUpDelegate = self
        pickerPopupViewController.listaOpciones = _CONST.LISTA_TIPO_CREDITO
        pickerPopupViewController.tipoSelec = _CONST.TIPOCREDITO
        self.present(pickerPopupViewController, animated: true)
    }
   
    //Metodo CREDITO

    func calcular_credito()-> Void {
       print("calcular_credito-----")
        
     if txtMonto.text!.isEmpty
     {
        DispatchQueue.main.async {
            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_MONTO , view: self)
        }
     
     }else{
        
        if txtPalzos.text!.isEmpty {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje:_CONST.SIM_CRE_MSJ_PLAZO , view: self)
            }
        }else{
            if textField.text!.isEmpty {
                DispatchQueue.main.async {
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_TIPO_CREDITO, view: self)
                }
            }else{
                self.var_tipo_cred  = self.tipoCredito(auxTipoCredito: textField.text!)
                self.var_frec_pago = self.frecuencia(auxFrecuencia: txtFrecuencia.text!)
                print("self.var_tipo_cred \(self.var_tipo_cred)")
                print("self.var_frec_pago \(self.var_frec_pago)")
                self.monto_cred = Double(txtMonto.text!)!
                self.var_cuota_cred = Double(segCTipoCuota.selectedSegmentIndex)
                self.plazo_cred = Double(txtPalzos.text!)!
                if(var_tipo_cred==0)
                {
                    //sentencias para control de tasa segun capital  y frecuencia de pagos
                    //Microcredito
                    if (var_frec_pago==0) {
                        if (monto_cred >= 50 && monto_cred <= 1000){
                            rate = 20.00;
                        }else if (monto_cred >= 1001 && monto_cred <= 6000){
                            rate = 20.00;
                        }else if (monto_cred >= 6001 && monto_cred <= 60000){
                            rate = 20.00;
                        }else if (monto_cred >= 60001 && monto_cred <= 100000){
                            rate = 18.90;
                        }else{
                            DispatchQueue.main.async {
                                 Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERROR_MONTO, view: self)
                            }
                        }
                        rate=rate/100;
                        monthly=rate/12;
                        frase = _CONST.MENSUAL;
                    }else if (var_frec_pago==1) {
                        if (monto_cred >= 50 && monto_cred <= 5000){
                            rate = 20.00;
                        }else if (monto_cred >= 1001 && monto_cred <= 5000){
                            rate = 20.00;
                        }else{
                            DispatchQueue.main.async {
                                 Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERROR_MONTO, view: self)
                            }
                        }
                        
                        rate=rate/100;
                        monthly=rate/24;
                        frase = _CONST.QUINCENAL;
                        
                    }else if (var_frec_pago==2) {
                        if (monto_cred >= 50 && monto_cred <= 1000){
                            rate = 20.00;
                        }else if (monto_cred >= 1001 && monto_cred <= 5000){
                            rate = 20.00;
                        }else {
                            DispatchQueue.main.async {
                                 Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                            }
                        }
                        
                        rate=rate/100;
                        monthly=rate/52;
                        frase = _CONST.SEMANAL;
                        
                    }else{
                        DispatchQueue.main.async {
                             Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_QUINCENAL_SEMANAL, view: self)
                        }
                    }
                    
                    
                }else if (var_tipo_cred==1){
                    //Consumo
                    if (var_frec_pago==0) {
                        if (monto_cred >= 100 && monto_cred <= 40000){
                            rate = 15.50;
                        }else{
                            DispatchQueue.main.async {
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO , view: self)
                            }
                        }
                        rate=rate/100;
                        monthly=rate/12;
                        frase = _CONST.MENSUAL;
                        
                    }else{
                        DispatchQueue.main.async {
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_QUINCENAL_SEMANAL, view: self)
                        }

                    }
               }else if (var_tipo_cred==2){
                    //iglesias
                    if (var_frec_pago==0) {
                        if (monto_cred >= 1000 && monto_cred <= 200000){
                            rate = 12.00
                        }else if (monto_cred >= 200001 && monto_cred <= 1000000){
                            rate = 10.50
                        }else
                        {
                            DispatchQueue.main.async {
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje:_CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                            }
                            
                        }
                        
                        rate=rate/100;
                        monthly=rate/12;
                        frase = _CONST.MENSUAL;
                        
                    }else{
                        DispatchQueue.main.async {
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_QUINCENAL_SEMANAL, view: self)
                        }
                    }
                    
                }else if (var_tipo_cred==3){
                    //agricola
                    if (var_frec_pago==0) {
                        if (monto_cred >= 50 && monto_cred <= 40000){
                            rate = 15.00
                        }else{
                            DispatchQueue.main.async {
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                            }
                        }
                        rate=rate/100;
                        monthly=rate/12;
                        frase = _CONST.MENSUAL;
                    }else if (var_frec_pago==1) { // BIMENSUAL
                            if (monto_cred >= 50 && monto_cred <= 40000){
                                rate = 15.00
                            }else{
                                DispatchQueue.main.async {
                                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                                }
                                
                            }
                            
                            rate=rate/100;
                            monthly=rate/6;
                            frase = _CONST.BIMENSUAL;
                    }else if (var_frec_pago==2) { // TRIMESTRAL
                               if (monto_cred >= 50 && monto_cred <= 40000){
                                      rate = 15.00
                               }else {
                                      DispatchQueue.main.async {
                                          Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                                       }
                                                   
                                }
                                               
                                rate=rate/100;
                                monthly=rate/4;
                                frase = _CONST.TRIMESTRAL;
                                               
                        }else if (var_frec_pago==3) { // SEMESTRAL
                              if (monto_cred >= 50 && monto_cred <= 40000){
                                  rate = 15.00
                              }else {
                                  DispatchQueue.main.async {
                                       Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                                  }
                                                                   
                               }
                                                               
                               rate=rate/100;
                               monthly=rate/2;
                               frase = _CONST.SEMESTRAL;
                        }else{
                            DispatchQueue.main.async {
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                            }
                            
                        }
                        
                    } else if (var_tipo_cred==4){//Vivienda
                    
                    if (var_frec_pago==0) {
                        if (monto_cred >= 5000 && monto_cred <= 80000){
                            rate = 10.50
                        }else
                        {
                            DispatchQueue.main.async {
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                            }
                            
                        }
                        
                        rate=rate/100;
                        monthly=rate/12;
                        frase = _CONST.MENSUAL;
                    }else{
                        DispatchQueue.main.async {
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_QUINCENAL_SEMANAL, view: self)
                        }

                    }
                    
                }else if (var_tipo_cred==5)
                {//Daqui Esperanza
                    
                    if (var_frec_pago==0) { // MENSUAL
                        if (monto_cred >= 50 && monto_cred <= 100000){
                            rate = 18.00
                        }else
                        {
                            DispatchQueue.main.async {
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                            }
                            
                        }
                        
                        rate=rate/100;
                        monthly=rate/12;
                        frase = _CONST.MENSUAL;
                    }else  if (var_frec_pago==1) { // BIMENSUAL
                        if (monto_cred >= 50 && monto_cred <= 100000){
                            rate = 18.00
                        }else
                        {
                            DispatchQueue.main.async {
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                            }
                            
                        }
                        
                        rate=rate/100;
                        monthly=rate/6;
                        frase = _CONST.BIMENSUAL;
                     }else if (var_frec_pago==2) { // TRIMESTRAL
                           if (monto_cred >= 50 && monto_cred <= 100000){
                                  rate = 18.00
                           }else {
                                  DispatchQueue.main.async {
                                      Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                                   }
                                               
                            }
                                           
                            rate=rate/100;
                            monthly=rate/4;
                            frase = _CONST.TRIMESTRAL;
                                           
                    }else if (var_frec_pago==3) { // SEMESTRAL
                          if (monto_cred >= 50 && monto_cred <= 100000){
                              rate = 18.00
                          }else {
                              DispatchQueue.main.async {
                                   Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_RANGO_TIPO_CREDITO, view: self)
                              }
                                                               
                           }
                                                           
                           rate=rate/100;
                           monthly=rate/2;
                           frase = _CONST.SEMESTRAL;
                    }else{
                        DispatchQueue.main.async {
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SIM_CRE_MSJ_QUINCENAL_SEMANAL, view: self)
                        }

                    }
                    
                    
                }
                
                
                
                
                let rat = rate * 100
                let auxInteresAnual = NSMutableAttributedString()
                auxInteresAnual
                    .bold("INTERES ANUAL: %")
                    .normal(String(format: "%.2f", rat))
                
                self.interesAnual.layer.borderWidth = _CONST.BORDER_WIDTH
                self.interesAnual.layer.cornerRadius = _CONST.CORNER_RADIUS
                self.interesAnual.attributedText = auxInteresAnual
                
                
                auxTotal = 0.0
                seguro = 0.000302
                payment = 0.0
                payment2 = 0.0
                band = 0.0
                calculoseguroIncendio = 0.00018444
                valorSeguroIncendio = 0.0
                
                
                
                if (var_tipo_cred == 4)
                {
                    if !(txtValorBien.text!.isEmpty) {
                        valorbien  = Double(txtValorBien.text!)!
                        valorSeguroIncendio = calculoseguroIncendio*valorbien
                       
                    }
                }else{
                    valorSeguroIncendio = 0.0
                }
                
                
                
                if (var_cuota_cred==0) {
                    payment = ((monto_cred * monthly) / (1 - (pow((1+monthly), -plazo_cred))))
                    auxTotal = payment*plazo_cred
                }
                else if (var_cuota_cred==1) {
                    payment2=((monto_cred*monthly)/(1 - (pow((1+monthly),-plazo_cred))));
                    payment=monto_cred/plazo_cred;
                    band=payment;
                    auxTotal = payment2*plazo_cred;
                }
                
                interest=auxTotal-monto_cred;
                
                
                if (rate > 0){
                    
                    
                    let auxTotalPagos = NSMutableAttributedString()
                    auxTotalPagos
                        .bold(" TOTAL PAGOS \(frase):  $")
                        .normal(String(format: "%.2f",payment))
                    self.totalPagos.layer.borderWidth = _CONST.BORDER_WIDTH
                    self.totalPagos.layer.cornerRadius = _CONST.CORNER_RADIUS
                    totalPagos.attributedText = auxTotalPagos
                 
                    
                    let auxTotalInteres = NSMutableAttributedString()
                    auxTotalInteres
                        .bold(" TOTAL INTERESES:  $")
                        .normal(String(format: "%.2f",interest))
                    self.totalInteres.layer.borderWidth = _CONST.BORDER_WIDTH
                    self.totalInteres.layer.cornerRadius = _CONST.CORNER_RADIUS
                    self.totalInteres.attributedText = auxTotalInteres
                    
                    let auxCapital = NSMutableAttributedString()
                    auxCapital
                        .bold(" CAPITAL:  $")
                        .normal(String(format: "%.2f",monto_cred))
                    self.capital.layer.borderWidth = _CONST.BORDER_WIDTH
                    self.capital.layer.cornerRadius = _CONST.CORNER_RADIUS
                    self.capital.attributedText = auxCapital
                    
                    newPrincipal = monto_cred;
                    newPrincipal2 = monto_cred;
                    
                    
                    let auxNumPagos = NSMutableAttributedString()
                    auxNumPagos
                        .bold(" NUMERO DE PAGOS:  ")
                        .normal(String(format: "%.0f",plazo_cred))
                    self.numeroPagos.layer.borderWidth = _CONST.BORDER_WIDTH
                    self.numeroPagos.layer.cornerRadius = _CONST.CORNER_RADIUS
                    self.numeroPagos.attributedText = auxNumPagos
                   
                    self.calculoApi(monto: monto_cred, montoplazo: Int(plazo_cred), frecuencia: frase)
                    
                    
                }
                
            }
            
        }
        
    
     }
     
   
     }
     
    
     
     func calculoApi(monto: Double, montoplazo: Int, frecuencia: String){
     
        let B8:Double = 0.50 // certificado
        let B9:Double = 2.00; // api
     
        let B3:Double = monto; //monto
     var B4:Int = montoplazo;    //plazo meses
        let B5:Int = montoplazo;    //plazo semanas
        let B6:Int = montoplazo;    //plazo quincenal
        let B7:Int = montoplazo;    //plazo diario
     var Api: Double = 0.0;
     var Certificado:Double = 0.0;
     
     switch(frecuencia) {
     case _CONST.MENSUAL:
     Api =  self.mensualApi(B3: B3, B4: Double(B4), B9: B9)
     Certificado = self.mensualCerti(B3: B3, B8: B8)
     break
     case _CONST.SEMANAL:
         Api =  self.semanalApi(B3: B3, B5: Double(B5), B9: B9)
         Certificado = self.semanalCerti(B3: B3, B8: B8)
         break
     case _CONST.QUINCENAL:
         Api =  self.quincenalApi(B3: B3, B6: Double(B6), B9: B9)
         Certificado = self.quincenalCerti(B3: B3, B8: B8)
         break
     case _CONST.DIARIO:
         Api =  self.diariolApi(B3: B3, B7: Double(B7), B9: B9)
         Certificado = self.diarioCerti(B3: B3, B8: B8)
         break
     case _CONST.BIMENSUAL:
         B4 = 2
         Api = self.mensualApi(B3: B3, B4:Double(B4), B9: B9)
         Certificado = self.mensualCerti(B3: B3, B8: B8)
         break;
     case _CONST.TRIMESTRAL:
         B4 = 3
         Api = self.mensualApi(B3: B3, B4: Double(B4), B9: B9)
         Certificado = self.mensualCerti(B3: B3, B8: B8)
         break
    case _CONST.SEMESTRAL:
            B4 = 6
            Api = self.mensualApi(B3: B3, B4: Double(B4), B9: B9)
            Certificado = self.mensualCerti(B3: B3, B8: B8)
            break
     default:
     break;
     }
     
     let total:Double = Api  + Certificado
     let auxApi = NSMutableAttributedString()
        auxApi
            .bold(" API: $ ")
            .normal(String(format: "%.2f", Api))
        api.layer.borderWidth = _CONST.BORDER_WIDTH
        api.layer.cornerRadius = _CONST.CORNER_RADIUS
        api.attributedText = auxApi
     
     
     
        let auxCertificado = NSMutableAttributedString()
        auxCertificado
            .bold(" Aportación SOLCA:  $ ")
            .normal(String(format: "%.2f", Certificado))
        certificado.layer.borderWidth = _CONST.BORDER_WIDTH
        certificado.layer.cornerRadius = _CONST.CORNER_RADIUS
        certificado.attributedText = auxCertificado
    
     
        let auxTotal = NSMutableAttributedString()
        auxTotal
            .bold(" TOTAL: $")
            .normal(String(format: "%.2f", total))
        self.total.layer.borderWidth = _CONST.BORDER_WIDTH
        self.total.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.total.attributedText = auxTotal
     
     }
     
     func mensualApi(B3: Double, B4: Double, B9: Double)-> Double{
     var resultApi:Double =  0.0
     
     if ( B4 < 12 ) {
     resultApi =  ((B3 * (B9/100)/12)*B4);
     }
     else{
     resultApi =  (B3*(B9/100));
     }
     return resultApi;
     
     }
     
     func mensualCerti(B3: Double, B8:Double )->Double{
     
        let resultCert:Double = ((B3 * (B8/100) ))
     return resultCert
     
     }
     
     func semanalApi(B3:Double, B5: Double, B9: Double) -> Double {
     var resultApi:Double = 0.0
     
     if ( B5<48 ){
     resultApi =  ((B3 * (B9/100))/48)*B5
     }else{
     resultApi = B3 * (B9/100)
     
     }
     
     return resultApi
     
     }
     
     func semanalCerti(B3: Double, B8: Double) -> Double {
        let resultCert:Double = ((B3 * (B8/100) ))
     return resultCert
     
     }
     
     func quincenalApi(B3: Double, B6: Double, B9: Double) -> Double {
     var resultApi:Double = 0.0
     
     if ( B6<24 ){
     resultApi =   ((B3 * (B9 / 100))/24) * B6
     }else{
     resultApi = B3 * (B9/100)
     }
     
     return resultApi
     
     }
     
     func quincenalCerti(B3: Double, B8: Double) -> Double {
     
     
        let resultCert: Double =  ((B3 * (B8/100) ))
     return resultCert
     
     }
     
     func diariolApi(B3:Double, B7: Double, B9: Double  )-> Double{
     
     var resultApi: Double = 0.0
     
     if ( B7<365 ){
     resultApi =   ((B3 * (B9 / 100))/360) * B7
     }else{
     resultApi = B3 * ( B9 / 100)
     }
     return resultApi
     
     }
     
     func  diarioCerti(B3:Double, B8:Double) -> Double {
     
     
        let resultCert:Double =   ((B3 * (B8/100) ))
     return resultCert;
     
     }
    
    func tipoCredito(auxTipoCredito: String) -> Double {
        var auxTC: Double = 5
        switch auxTipoCredito {
        case _CONST.MICROCREDITO:
            auxTC = 0
            break
        case  _CONST.CONSUMO :
            auxTC = 1
            break
        case _CONST.IGLESIAS :
            auxTC = 2
            break
        case _CONST.AGRICOLA :
            auxTC = 3
            break
        case _CONST.VIVIENDA:
            auxTC = 4
            break
        case _CONST.VIVIENDA:
            auxTC = 5
            break
        default:
            break
        }
        return auxTC
    }
   
    func frecuencia(auxFrecuencia: String) -> Double {
        var auxF: Double = -1
        switch auxFrecuencia {
            case _CONST.MENSUAL :
                auxF = 0
                break
            case  _CONST.QUINCENAL :
                auxF = 1
                break
            case _CONST.SEMANAL:
                auxF = 2
                break
            
            // DAQUI ESPERANZA
            case _CONST.BIMENSUAL:
                auxF = 1
                break
            case _CONST.TRIMESTRAL:
                auxF = 2
                break
            case _CONST.SEMESTRAL:
                auxF = 3
                break
            default:
                break
        }
        return auxF
    }
 
    @IBAction func limpiar(_ sender: Any) {
        txtMonto.text = _CONST.VACIO
        txtValorBien.text = _CONST.VACIO
        txtPalzos.text = _CONST.VACIO
        textField.text = _CONST.VACIO
    }
}
extension SimuladorCreditosViewController: PopUpDelegate {
    func seleccionString(value: String, tipoSelection: String, tipoCuenta: String, saldoDisponible: String) {
        print("SimuladorCreditosViewController value \(value)")
        if tipoSelection == _CONST.FRECUENCIA{
            txtFrecuencia.text = value
        }
        
        if tipoSelection == _CONST.TIPOCREDITO{
            textField.text = value
            if value == _CONST.ESPERANZA || value == _CONST.AGRICOLA{
                self.seleccionCredito = 1
            }else{
                self.seleccionCredito = 0
            }
            txtValorBien.isHidden = true
            if value == _CONST.VIVIENDA{
                txtValorBien.isHidden = false
                self.seleccionCredito = 1
            }
        }
        
    }
   
}

