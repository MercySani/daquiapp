//
//  PageItemController.swift
//  DaquiApp
//
//  Created by Fredy on 23/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class PageItemController: UIViewController {
    var itemIndex: Int = 0 // ***
    var imageName: String = "" {
        
        didSet {
            
            if let imageView = contentImageView {
                imageView.image = UIImage(named: imageName)
            }
            
        }
    }
     @IBOutlet var contentImageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        contentImageView!.image = UIImage(named: imageName)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
