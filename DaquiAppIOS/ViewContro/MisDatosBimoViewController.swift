//
//  MisDatosBimoViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 05/06/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import UIKit

class MisDatosBimoViewController: UIViewController, UITextFieldDelegate{

    // variables
    var usuario: UsuarioModel = UsuarioModel()
    var posicionConsolidadaAuxiliar: [PosicionConsolidadaModel] = []
    var auxCount: Int = 0
    var titulo: String?
    var cuenta: String?
  
    // outlet
    
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblCelular: UILabel!
    @IBOutlet weak var lblCuenta: UILabel!
    @IBOutlet weak var btnCerrar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Usuario --> \(usuario)")
        btnCerrar.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.lblNombre.text = usuario.strNombreCompleto
        self.lblCelular.text = usuario.strTelefono
        self.lblCuenta.text = cuenta
        
    }

    @IBAction func accionSalir(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
