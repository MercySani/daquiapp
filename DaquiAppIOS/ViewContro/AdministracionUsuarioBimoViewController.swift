//
//  AfiliacionBimoViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 08/05/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import UIKit

class AdministracionUsuarioBimoViewController: UIViewController, UITextFieldDelegate{

    var usuario: UsuarioModel = UsuarioModel()
    var posicionConsolidadaAuxiliar: [PosicionConsolidadaModel] = []
    var auxCount: Int = 0
    var titulo: String?
    
    let objActivity = activityIndicatorVC()
   
    @IBOutlet weak var btnAceptar: UIButton!
    @IBOutlet weak var contenedor: UIView!
    
    //lable
    
    @IBOutlet weak var txtCuenta: UITextField!
    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var lblCelular: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    var cuentaSeleccionada: String?
    var origen: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usuario = (tabBarController as! TabBarViewController).usuario
        print("Usuario --> \(usuario)")
       
        btnAceptar.layer.cornerRadius = _CONST.CORNER_RADIUS
    
        self.contenedor.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.contenedor.layer.borderWidth = _CONST.BORDER_WIDTH
        self.contenedor.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        
        self.lblNombre.text = usuario.strNombreCompleto
        self.lblCelular.text = usuario.strTelefono
        self.lblEmail.text = usuario.strEMail
       
        self.txtCuenta.delegate = self
        
    }

    @IBAction func accionAfiliar(_ sender: Any) {
        if cuentaSeleccionada == nil{
            cuentaSeleccionada = posicionConsolidadaAuxiliar[0].Cuenta
        }
        performSegue(withIdentifier:"segueModalAdmin", sender: self)

    }
    
    @IBAction func seleccionarCuenta(_ sender: Any) {
        accionSelecionarCuenta()
    }
    
    
    @IBAction func selectCuenta(_ sender: Any) {
       accionSelecionarCuenta()
    }
    
    func accionSelecionarCuenta(){
        DispatchQueue.main.async {
                                self.view.endEditing(true)
                            }
                     guard !(posicionConsolidadaAuxiliar.count == 0 && auxCount == 0) else {
                         auxCount = 1
                         DispatchQueue.main.async {
                             Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "Debe realizar la busqueda de la cuenta" , view: self)
                         }
                         return
                     }
                     guard posicionConsolidadaAuxiliar.count == 0 else {
                         let popUpCuentaViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpCuentaViewController") as!  PopUpCuentaViewController
                         
                         popUpCuentaViewController.popUpDelegate = self
                         popUpCuentaViewController.tipoSelec = _CONST.SELEC_CUENTA
                         popUpCuentaViewController.listaOpciones = self.posicionConsolidadaAuxiliar
                         self.present(popUpCuentaViewController, animated: true)
                         return
                     }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueModalAdmin" {
            if let destino = segue.destination as? AlertBimoViewController{
                destino.strNumeroCuenta = cuentaSeleccionada
                destino.strIdentificacion = usuario.strIdentificacion
                destino.strTrama = _CONST.AFILIACION_IFI_RQ
                destino.usuario = usuario
                destino.variableMensaje = "¿Desea afiliar su cuenta Bimo?"
                destino.origen = self.origen
            }
        }
    }
    
}



extension AdministracionUsuarioBimoViewController: PopUpDelegate{
    func seleccionString(value: String, tipoSelection: String, tipoCuenta: String, saldoDisponible: String) {
        if (tipoSelection == _CONST.SELEC_CUENTA){
            self.txtCuenta.text = value
            self.cuentaSeleccionada = value
        }
        
    }
}
