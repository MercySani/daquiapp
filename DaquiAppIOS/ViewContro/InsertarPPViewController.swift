//
//  InsertarPPViewController.swift
//  DaquiApp
//
//  Created by Fredy on 13/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit
import Foundation

class InsertarPPViewController: UIViewController, UITextFieldDelegate {
    var usuario: UsuarioModel = UsuarioModel()
    var listaCategoria: [CategoriaSRModel] = []
    var listaServicios: [ServicioSRModel] = []
    var objInsertarPagoProgramado: LisCueXNoSocioSRModel = LisCueXNoSocioSRModel ()
    var auxIdCategoria: Int = 0
    var aucIdServicio: String = ""
    var auxIdOTP: String = ""
    var count: Int = 0
    var countCategoria:Int = 0
    var countCuenta: Int = 0
    var auxFinicio: String  = ""
    var auxFecha: Int = 0
    let objActivity = activityIndicatorVC()
    var posicionConsolidada: [PosicionConsolidadaModel] = []
    var posicionConsolidadaAuxiliar: [PosicionConsolidadaModel] = []
    var posicionConsolidadaString: PosicionConsolidadaModel = PosicionConsolidadaModel ()
    
    @IBOutlet weak var btnContinuar: UIButton!
    @IBOutlet weak var viewDos: UIView!
    
    
    
    
    @IBAction func diaPago(_ sender: Any) {
        DispatchQueue.main.async {
        self.view.endEditing(true)
        }
        
        
        var dia:[String]  = []
        for i in 1...28{
            dia.append("\(i)")
        }
        let pickerPopUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickerPopUpViewController") as! PickerPopUpViewController
        pickerPopUpViewController.popUpDelegate = self
        pickerPopUpViewController.listaOpciones = dia
        pickerPopUpViewController.tipoSelec = "DiaPago"
        self.present(pickerPopUpViewController, animated: true)
    }
    //variables vista
    
    @IBOutlet weak var saldoDisponibleLbl: UILabel!
    @IBOutlet weak var tipoCuentaLbl: UILabel!
    @IBOutlet weak var txtCategoria: UITextField!
    
    @IBAction func selecCategoria(_ sender: Any) {
        DispatchQueue.main.async {
        self.view.endEditing(true)
        }
        guard !(usuario.strNombreUser == _CONST.USUARIO_DEMO) else {
            if(self.countCategoria == 0){
                countCategoria = countCategoria + 1
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_USUARIO_DEMO  , view: self)
                }
            }
            return
        }
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        listaCategoriaSR(usuario: usuario.strIdentificacion, token: usuario.strToken)
    }
    
    @IBOutlet weak var txtServicio: UITextField!
    
    @IBAction func selecServicio(_ sender: Any) {
        DispatchQueue.main.async {
        self.view.endEditing(true)
        }
        if txtCategoria.text!.isEmpty && count == 0 {
            
            DispatchQueue.main.async {
                self.count = 1
                let alertControler = UIAlertController(title: _CONST.AVISO, message: _CONST.SBF_MEN_CATEGORIA, preferredStyle: .alert)
                let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
                { (action:UIAlertAction!) in
                    
                }
                alertControler.addAction(aceptarAccion)
                self.present(alertControler, animated: true, completion: nil)
                
            }
        }
        
        
        guard txtCategoria.text!.isEmpty else {
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            listaServicioSR(usuario: usuario.strIdentificacion, parametroCategoria: self.auxIdCategoria, token: usuario.strToken)
            return
        }
    }
    
    @IBOutlet weak var txtCuentaDebito: UITextField!
    
    @IBAction func selecCuentaDebito(_ sender: Any) {
        DispatchQueue.main.async {
        self.view.endEditing(true)
        }
        guard !(usuario.strNombreUser == _CONST.USUARIO_DEMO) else {
            if(self.countCuenta == 0){
                countCuenta = countCuenta + 1
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_USUARIO_DEMO  , view: self)
                }
            }
            return
        }
        
        self.posicionConsolidadaAuxiliar.removeAll()
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        self.posicionConsolidada(identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_SOCIO, usuario: usuario.strIdentificacion, token: usuario.strToken)
    }
    
    @IBOutlet weak var txtReferencia: UITextField!
    
    @IBOutlet weak var txtMontoMaximo: UITextField!
    
    @IBOutlet weak var txtDiaPago: UITextField!
    
    @IBOutlet weak var txtFInicio: UITextField!
    
    @IBAction func selecFInicio(_ sender: Any) {
        DispatchQueue.main.async {
        self.view.endEditing(true)
        }
        let fechaPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "FechaPopupViewController") as! FechaPopupViewController
        fechaPopupViewController.popUpDelegate = self
        fechaPopupViewController.tipoSeleccion = _CONST.PP_TS_FINICIO
        self.present(fechaPopupViewController, animated: true)
        
    }
    
    @IBOutlet weak var txtFFin: UITextField!
    @IBAction func selecFFIN(_ sender: Any) {
        DispatchQueue.main.async {
        self.view.endEditing(true)
        }
        if txtFInicio.text!.isEmpty && auxFecha == 0 {
            self.auxFecha = 1
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.PP_MEN_FINICIO , view: self)
            }
        }
        
        
        guard txtFInicio.text!.isEmpty else {
            let fechaPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "FechaPopupViewController") as! FechaPopupViewController
            fechaPopupViewController.popUpDelegate = self
            fechaPopupViewController.fechaInicial = self.auxFinicio
            fechaPopupViewController.tipoSeleccion = _CONST.PP_TS_FFIN
            self.present(fechaPopupViewController, animated: true)
            return
        }
        
        
    }
    
    @IBAction func ingresarPP(_ sender: Any) {
        guard !(txtCategoria.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SBF_MEN_CATEGORIA , view: self)
            }
            return
        }
        
        guard !(txtServicio.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SBF_MEN_SERVICIOS , view: self)
            }
            return
        }
        
        guard !(txtCuentaDebito.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SER_BAS_MENSA_CUENTA , view: self)
            }
            return
        }
        guard !(txtReferencia.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.SBF_MEN_REFERENCIA , view: self)
            }
            return
        }
        
        guard !(txtMontoMaximo.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.PP_MEN_MONTO_MAXIMO , view: self)
            }
            return
        }
        guard !(txtMontoMaximo.text == "0") else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "El monto minimo debe ser mayor a cero" , view: self)
            }
            return
        }
        
        guard !(txtDiaPago.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.PP_MEN_DIA_DEBITO , view: self)
            }
            return
        }
        
        guard !(txtFInicio.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.PP_MEN_FINICIO , view: self)
            }
            return
        }
        
        guard !(txtFFin.text!.isEmpty) else {
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.PP_MEN_FFIN , view: self)
            }
            return
        }
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        
        self.enviarCodigoOPT(userName: usuario.strNombreUser, identificacion: usuario.strIdentificacion, token: usuario.strToken)
        
        
    }
    
    
    
    //fin variables vista
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        usuario = (tabBarController as! TabBarViewController).usuario
        
        self.viewDos.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.viewDos.layer.borderWidth = _CONST.BORDER_WIDTH
        self.viewDos.layer.borderColor = UIColor.init(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA).cgColor
        
       
        
        btnContinuar.layer.cornerRadius = _CONST.CORNER_RADIUS
        txtCuentaDebito.delegate = self
        txtReferencia.delegate = self
        txtMontoMaximo.delegate = self
        txtDiaPago.delegate = self
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var tipoCaracter: CharacterSet = CharacterSet()
        var characterSet: CharacterSet = CharacterSet()
        var retorno: Bool = false
        
        switch textField.tag {
        case 3:
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 15
            }
            break
        case 4:
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 25
            }
            
            break
        case 5:
            tipoCaracter = CharacterSet.decimalDigits
            characterSet = CharacterSet(charactersIn: string)
            retorno = tipoCaracter.isSuperset(of: characterSet)
            if(retorno){
                guard let text = textField.text else { return true }
                let count = text.count + string.count - range.length
                retorno = count <= 4
            }
            
            break
        default:
            retorno = true
        }
        
        return retorno
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func listaCategoriaSR(usuario: String, token:String)->Void{
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/listaCategorias/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)/\(_CONST.FRECUENCIA_DE_PAGO)"
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let _ = response as? HTTPURLResponse{
                do{
                    self.listaCategoria =  try JSONDecoder().decode([CategoriaSRModel].self , from: data)
                    if  self.listaCategoria.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            let pickerPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickerPopUpViewController") as! PickerPopUpViewController
                            pickerPopupViewController.popUpDelegate = self
                            pickerPopupViewController.tipoSelec = "CATEGORIA"
                            pickerPopupViewController.listaCategoria = self.listaCategoria
                            self.present(pickerPopupViewController, animated: true)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "No existe categorias " , view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "No existe categorias", view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir lista Categoria SR:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                }
            }
            }.resume()
        
    }
    
    
    //servicio
    func listaServicioSR(usuario: String, parametroCategoria: Int, token: String)->Void{
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/servicioPorParametrosSR/\(_CONST.TODO)/\(parametroCategoria)/\(_CONST.FRECUENCIA_DE_PAGO)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let _ = response as? HTTPURLResponse{
                do{
                    self.listaServicios =  try JSONDecoder().decode([ServicioSRModel].self , from: data)
                    if  self.listaServicios.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            let pickerPopupViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickerPopUpViewController") as! PickerPopUpViewController
                            pickerPopupViewController.popUpDelegate = self
                            pickerPopupViewController.tipoSelec = "SERVICIOS"
                            pickerPopupViewController.listaServicio = self.listaServicios
                            self.present(pickerPopupViewController, animated: true)
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: "No existe servicios disponibles " , view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir lista Servicios SR:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir lista Servicios SR: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    //fin servicio
    
    //para Buscar la cuenta
    func posicionConsolidada( identificacion: String, tipoIdentificacion: String, usuario: String, token: String){
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/posicionConsolidada/\(_CONST.CANAL_DES)/\(identificacion)/\(tipoIdentificacion)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.posicionConsolidada = try JSONDecoder().decode([PosicionConsolidadaModel].self , from: data)
                    
                    if  self.posicionConsolidada.count != _CONST.LISTA_VACIA{
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            for item in self.posicionConsolidada{
                                if(item.Producto == _CONST.CNTA_DEBITO_VISTA  || item.Producto == _CONST.CNTA_DEBITO_KULLKIMIRAK || item.Producto == _CONST.CNTA_DEBITO_BASICA){
                                    self.posicionConsolidadaString.Cuenta = item.Cuenta
                                    self.posicionConsolidadaString.Descripcion = item.Descripcion
                                    self.posicionConsolidadaString.GProducto = item.GProducto
                                    self.posicionConsolidadaString.Producto = item.Producto
                                    self.posicionConsolidadaString.Saldoaportacion = item.Saldoaportacion
                                    self.posicionConsolidadaString.Saldobloqueado = item.Saldobloqueado
                                    self.posicionConsolidadaString.Saldocontable = item.Saldocontable
                                    self.posicionConsolidadaString.Saldoefectivo = item.Saldoefectivo
                                    self.posicionConsolidadaAuxiliar.append(self.posicionConsolidadaString)
                                }
                            }
                            
                            
                            if self.posicionConsolidadaAuxiliar.count > 1 {
                                let popUpCuentaViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpCuentaViewController") as!  PopUpCuentaViewController
                                
                                popUpCuentaViewController.popUpDelegate = self
                                popUpCuentaViewController.tipoSelec = _CONST.SELEC_CUENTA
                                popUpCuentaViewController.listaOpciones = self.posicionConsolidadaAuxiliar
                                self.present(popUpCuentaViewController, animated: true)
                            }else{
                                self.txtCuentaDebito.text = self.posicionConsolidadaString.Cuenta
                                let auxTipoCuenta = NSMutableAttributedString()
                                auxTipoCuenta
                                    .bold("Tipo: ")
                                    .normal("\(self.posicionConsolidadaString.Descripcion)")
                                self.tipoCuentaLbl.attributedText = auxTipoCuenta
                                let auxSaldoDisponible = NSMutableAttributedString()
                                auxSaldoDisponible
                                    .bold("Saldo Disponible: $ ")
                                    .normal("\(self.posicionConsolidadaString.Saldoefectivo)")
                                self.saldoDisponibleLbl.attributedText = auxSaldoDisponible
                            }
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL , view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir Posicion Consolidada:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir Posicion Consolidada: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    //
    
    func mergePagoProgramado( id: Int , servicioID: String, noSocio: String, idAgencia: String, fInicio: String, fFinalizacion: String, dia: Int, cuentaDebito: String, montoMaximo: Int, referencia: String, datoAdicional: String, codigoCanalInterno: String, estado: Int, usuarioCreacion: String, usuarioActualizacion: String, identificacion: String, tipoIdentificacion: String, codigoOTP: String, token: String )->Void{
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = [ "id": id,
                                    "servicio": [
                                        "id": servicioID],
                                    "persona": ["identificacion": identificacion, "idTipoIdentificacion": tipoIdentificacion, "socio" : noSocio],
                                    
            "idAgencia": idAgencia,
            "fInicio": fInicio,
            "fFinalizacion": fFinalizacion,
            "dia": dia,
            "cuentaDebito": cuentaDebito,
            "montoMaximo": montoMaximo,
            "referencia": referencia,
            "datoAdicional": datoAdicional,
            "codigoCanalInterno": codigoCanalInterno,
            "estado": estado,
            "usuarioCreacion": usuarioCreacion,
            "usuarioActualizacion": usuarioActualizacion,
            "identificacion": identificacion,
            
            "strCodigoOtp": codigoOTP,
            "strToken": token,
            "strCanalSeguridad": _CONST.CANAL_SEG_IOS]

        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/combinarPagoProgramadoSR")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        
        session.dataTask(with: request) { data, response, error in
            if let data = data{
                do{
                    let responseInsertarPP = try JSONDecoder().decode(LisCueXNoSocioSRModel.self , from: data)
                    
                    if  responseInsertarPP.id != _CONST.LISTA_VACIA {
                        
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            let alertControler = UIAlertController(title: _CONST.AVISO, message: _CONST.PP_MEN_REGISTRO_OK, preferredStyle: .alert)
                            
                            let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
                            { (action:UIAlertAction!) in
                                
                                self.performSegue(withIdentifier: "unwindSegueInsertar", sender: self)
                                
                            }
                            alertControler.addAction(aceptarAccion)
                            self.present(alertControler, animated: true, completion: nil)
                            
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_SEG{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir Insertar Pago Programado :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir Insertar Pago Programado")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    
    //enviar OTP
    //esta funcion solo envia el OTP, para utilizar este funcion cualquiera de las dos variables deben estar estar correctos.
    func enviarCodigoOPT(userName: String, identificacion: String, token: String)   {
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = [ "strUsername": userName,
                                    "strIdentificacion": identificacion,
                                    "strToken": token,
                                    
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                    "strObservacion": _CONST.CANAL_DES
        ]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/procesoEnvioOTP")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let otpRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  otpRespuestaData.strCodigoError  == _CONST.CODIGO_RETORNO_OK {
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
                            otpViewController.popUpOTPDelegate = self
                            self.present(otpViewController, animated: true)
                        }
                    }else{
                        if otpRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: otpRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_OTP_NOK , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir Envio OTP :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir Envio OTP: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        countCategoria = 0
        count = 0
        countCuenta = 0
        self.view.endEditing(true)
    }
    
}
extension InsertarPPViewController: PopUpDelegate{
    func seleccionString(value: String, tipoSelection: String, tipoCuenta: String, saldoDisponible: String) {
        
        if(tipoSelection == "CATEGORIA")
        {
            self.txtCategoria.text = value
            for items in listaCategoria{
                if value == items.descripcion{
                    self.auxIdCategoria = items.id
                    self.txtServicio.text = ""
                }
            }
            
        }
        
        if (tipoSelection == "SERVICIOS"){
            self.txtServicio.text = value
            for items in listaServicios{
                if value == items.nombre{
                    self.aucIdServicio = items.id
                }
            }
        }
        if (tipoSelection == _CONST.SELEC_CUENTA){
            
            self.txtCuentaDebito.text = value
            let auxTipoCuenta = NSMutableAttributedString()
            auxTipoCuenta
                .bold("Tipo: ")
                .normal("\(tipoCuenta)")
            self.tipoCuentaLbl.attributedText = auxTipoCuenta
            let auxSaldoDisponible = NSMutableAttributedString()
            auxSaldoDisponible
                .bold("Saldo Disponible: $ ")
                .normal("\(saldoDisponible)")
            self.saldoDisponibleLbl.attributedText = auxSaldoDisponible
            
        }
        
        if (tipoSelection == "FINICIO"){
            self.txtFInicio.text = value
            self.auxFinicio = value
        }
        if tipoSelection == "FFIN"{
            self.txtFFin.text = value
        }
        
        if tipoSelection == "DiaPago"{
            self.txtDiaPago.text = value
        }
        
        
    }
    
}

extension InsertarPPViewController: OTPDelegate{
    func inserOTP(value: String) {
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        mergePagoProgramado( id: _CONST.ID_PAGO_PROGRAMADO_MERGE, servicioID: self.aucIdServicio , noSocio: "" , idAgencia: _CONST.ID_AGENCIA_SB , fInicio: txtFInicio.text!, fFinalizacion: txtFFin.text!, dia: Int(txtDiaPago.text!)!, cuentaDebito: txtCuentaDebito.text!, montoMaximo: Int(txtMontoMaximo.text!)!, referencia: txtReferencia.text!, datoAdicional: _CONST.DATO_ADICIONAL , codigoCanalInterno: _CONST.CANAL_DES  , estado: _CONST.PP_ESTADO_INGRESO , usuarioCreacion: usuario.strIdentificacion, usuarioActualizacion: usuario.strIdentificacion, identificacion: usuario.strIdentificacion, tipoIdentificacion: _CONST.TI_CEDULA, codigoOTP: value, token: usuario.strToken)
    }
    
    
}

