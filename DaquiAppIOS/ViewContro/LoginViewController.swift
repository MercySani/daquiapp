//
//  LoginViewController.swift
//  DaquiApp
//
//  Created by fjaneta on 14/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit
import Foundation

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var usuarioField: UITextField!
    @IBOutlet weak var contrasenaField: UITextField!
    
    @IBOutlet weak var btnSalir: UIButton!
    @IBOutlet weak var btnAcceder: UIButton!

    
    var usuario = UsuarioModel()
    
    //variables de indicador de actividades
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let objActivity = activityIndicatorVC()
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        guard !Seguridad.verifyJailbreak() else {
            DispatchQueue.main.async {
                
                let alertController = UIAlertController(title: _CONST.ALERTA, message: _CONST.MSJ_SEGURIDAD , preferredStyle: .alert)
                let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
                { (action:UIAlertAction!) in
                    exit(0)
                }
                alertController.addAction(aceptarAccion)
                self.present(alertController, animated: true, completion: nil)
            }
            return
        }
        
        self.usuarioField.text = ""
        self.contrasenaField.text = ""
        
        usuarioField.delegate = self
        contrasenaField.delegate = self
        
        //Borde Redondeado botones
        btnSalir.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnAcceder.layer.cornerRadius = _CONST.CORNER_RADIUS
        
        //pruebas
        let modelName = UIDevice.modelName
        //print("Modelo Name -> \(modelName)")
        
        let version = UIDevice.current.systemVersion
        let sistema = UIDevice.current.systemName
        
        //print("version \(version) -> \(sistema)")
        
    }
    
    func hideKeyboard()  {
        usuarioField.resignFirstResponder()
        contrasenaField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyboard()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var retorno: Bool = false
        if textField.tag == 1{
           guard let text = textField.text else { return true }
            let count = text.count + string.count - range.length
            retorno = count <= 16
        
        }
        if textField.tag == 2 {
            guard let text = textField.text else { return true }
            let count = text.count + string.count - range.length
            retorno = count <= 25
            
        }
        
        return retorno
    }
    
    @IBAction func recuperarClave(_ sender: Any) {
       // dismiss(animated: true)
        
       let recuperarClaveViewController = self.storyboard?.instantiateViewController(withIdentifier: "RecuperarClaveViewController") as! RecuperarClaveViewController
        //dismiss(animated: true)
        present(recuperarClaveViewController, animated: true)
    }
    

    
    @IBAction func salir(_ sender: Any) {
        dismiss(animated: true)
        //DispatchQueue.main.async {
        // self.performSegue(withIdentifier: "menuPrincipal", sender: self)
        //}
         
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Para ocultar el teclado al hacer touch
       override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           self.view.endEditing(true)
       }
       
    
    
    @IBAction func accederBtn(_ sender: UIButton) {
        
        guard !((self.usuarioField.text == _CONST.USUARIO_DEMO) && (self.contrasenaField.text == _CONST.PASSWORD_DEMO)) else {
            DispatchQueue.main.async {
                self.usuario.strCodigoError = _CONST.CODIGO_OK
                self.usuario.strMensajeError = _CONST.MSJ_DEFAULT_OK
                self.usuario.strEMail = _CONST.EMAIL_DEMO
                self.usuario.strNombreCompleto = _CONST.NOMBRECOMPLETO_DEMO
                self.usuario.strNombreUser = _CONST.USUARIO_DEMO
                self.usuario.strTelefono = _CONST.TELEFONO_DEMO
                self.usuario.strIdentificacion = "0000000000"
                self.performSegue(withIdentifier: "segueTabBar", sender: nil)
  
                self.usuarioField.text = ""
                self.contrasenaField.text = ""
                
            }
            
            
            
            return
        }
        
        guard  !(Utilidades.tieneCaracteresEspeciales(string: self.usuarioField.text!)) else{
            DispatchQueue.main.async {
                Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_CARACTERES_ESPECIALES, view: self)
            }
            return
        }
        
        if !self.usuarioField.text!.isEmpty && !self.contrasenaField.text!.isEmpty {
        //indicator de actividades
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
            
        // fin indicador de actividades
          //logeoUsuarioTEST(usuario: self.usuarioField.text!, contrasena: self.contrasenaField.text!)
        logeoUsuario(usuario: self.usuarioField.text!, contrasena: self.contrasenaField.text!)
        }else{
            Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.ERR_C_VACIOS, view: self)
        }
 
 
    }
    
    //@IBAction func unwindToLogin(segue:UIStoryboardSegue) {
        
    //}
    
    func logeoUsuario(usuario: String, contrasena: String) -> Void{
        
        //https://stackoverflow.com/questions/34223291/ios-certificate-pinning-with-swift-and-nsurlsession
       
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let version: String = UIDevice.current.model+"_"+UIDevice.current.systemName+"_" + UIDevice.current.systemVersion+"|"+appVersion
        
        
        
        var firma: String = ""
        var jwt:String = ""
        for _ in 1...7 {
            firma = firma + usuario
        }
        jwt = JsonWebToken.crearJWT(contenido: HASH.MD5(string: contrasena), firma: firma)
        
        let json: [String: Any] = ["strNombreUser": usuario,
                                   "strPassword": jwt ,
                                   "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                   "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                   "strOrigenIdentificador": macAddress?.description ?? "",
                                   "strDetalleDispositivo": version]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/logeoUsuario")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, let _ = response as? HTTPURLResponse {
                    do {
                        
                        let usuarioData = try JSONDecoder().decode(UsuarioModel.self, from: data)
                        self.usuario = usuarioData
                        
                    
                        
                        if self.usuario.strCodigoError == _CONST.CODIGO_LOGUEO_OK{
                             DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            }
                            if(self.usuario.strVerificarExistenciaUCSOrigen){
                                if self.usuario.strUserEstado == _CONST.COD_ESTADO_CAMIBIO_CLAVE{
                                  /*  if(self.usuario.strIdentificacion == self.usuarioField.text){
                                        DispatchQueue.main.async {
                                            self.objActivity.hideActivityIndicator(uiView: self.view)
                                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_CEDULA_IGUAL_USUARIO, view: self)
                                        }
                                    }else{*/
                                        DispatchQueue.main.async {
                                        let cambioClaveViewController = self.storyboard?.instantiateViewController(withIdentifier: "CambioClaveViewController") as!  CambioClaveViewController
                                        cambioClaveViewController.usuario = self.usuario
                                        cambioClaveViewController.claveLogin = self.contrasenaField.text!
                                        cambioClaveViewController.userLogin = self.usuarioField.text!
                                        self.present(cambioClaveViewController, animated: true)
                                    }
                                //}
                                }else{
                                    
                                    //el mensaje va en vacio xq se modifica en el metodo el mensaje BOLD
                                    //Utilidades.mensajeSugerencia(titulo: "", mensaje: "", view: self)
                                   
                                   
                                    DispatchQueue.main.async {
                                        
                                         self.performSegue(withIdentifier: "segueTabBar", sender: self)
                                        self.usuarioField.text = ""
                                        self.contrasenaField.text = ""
                                    }
                                }
                            }else{
                            self.enviarCodigoOPT(userName: self.usuario.strNombreUser, identificacion: self.usuario.strIdentificacion, token: self.usuario.strToken)
                            }
                            
                        }else{
                            if(self.usuario.strCodigoError == _CONST.CODIGO_ACTULIZACION){
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeActualizacion(titulo: "", mensaje: "", view: self)
                                }
                             
                            }else{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: self.usuario.strMensajeError, view: self)
                                }
                            }
                            }
                            
                    }  catch let error as NSError {
                        print("Error al leer JSON: \(error)")
                        
                        do {
                            var mensajeMode: MensajeModel = MensajeModel()
                            let mensajeData = try JSONDecoder().decode(MensajeModel.self, from: data)
                             mensajeMode = mensajeData
                            
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeMode.strMensajeError, view: self)
                            }
                        } catch _ as NSError {
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                            }
                        }
                    }
            }else if let error = error{
                print("Error al consumir logeoUsuario:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                           self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }                    
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir logeoUsuario: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
        }.resume()
        
    }
    
    func logeoUsuarioTEST(usuario: String, contrasena: String) -> Void{

        //https://stackoverflow.com/questions/34223291/ios-certificate-pinning-with-swift-and-nsurlsession
        
         let session = URLSession(
         configuration: URLSessionConfiguration.ephemeral,
         delegate: NSURLSessionPinningDelegate(),
         delegateQueue: nil)
            
        
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        let version = UIDevice.current.model
        var firma: String = ""
        var jwt:String = ""
        for _ in 1...7 {
            firma = firma + usuario
        }
        jwt = JsonWebToken.crearJWT(contenido: HASH.MD5(string: contrasena), firma: firma)
        
        
         let json: [String: Any] = ["strNombreUser": usuario,
         "strPassword": HASH.MD5(string: contrasena) ,
         "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
         "strCanalDescripcionAdicional": _CONST.CANAL_DES,
         "strOrigenIdentificador": macAddress?.description ?? ""]
         
         let jsonData = try? JSONSerialization.data(withJSONObject: json)
         let url = URL(string: "\(_CONST.DIRECCION)/logeoUsuario")!
        var request = URLRequest(url: url)
         request.httpMethod = "POST"
         request.httpBody = jsonData
         
    
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, let _ = response as? HTTPURLResponse {
                do {
                    let usuarioData = try JSONDecoder().decode(UsuarioModel.self, from: data)
                    self.usuario = usuarioData
                    
                    if self.usuario.strCodigoError == _CONST.CODIGO_LOGUEO_OK{
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        if(self.usuario.strVerificarExistenciaUCSOrigen){
                            if self.usuario.strUserEstado == _CONST.COD_ESTADO_CAMIBIO_CLAVE{
                                
                                if(self.usuario.strIdentificacion == self.usuarioField.text){
                                    DispatchQueue.main.async {
                                        self.objActivity.hideActivityIndicator(uiView: self.view)
                                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_CEDULA_IGUAL_USUARIO, view: self)
                                    }
                                }else{
                                    DispatchQueue.main.async {
                                        
                                        let cambioClaveViewController = self.storyboard?.instantiateViewController(withIdentifier: "CambioClaveViewController") as!  CambioClaveViewController
                                        cambioClaveViewController.usuario = self.usuario
                                        cambioClaveViewController.claveLogin = self.contrasenaField.text!
                                        cambioClaveViewController.userLogin = self.usuarioField.text!
                                        
                                        self.present(cambioClaveViewController, animated: true)
                                    }}
                            }else{
                                
                                //el mensaje va en vacio xq se modifica en el metodo el mensaje BOLD
                                Utilidades.mensajeSugerencia(titulo: "", mensaje: "", view: self)
                                DispatchQueue.main.async {
                                    
                                    self.usuarioField.text = ""
                                    self.contrasenaField.text = ""
                                }
                            }
                        }else{
                            self.enviarCodigoOPT(userName: self.usuario.strNombreUser, identificacion: self.usuario.strIdentificacion, token: self.usuario.strToken)
                        }
                        
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: self.usuario.strMensajeError, view: self)
                        }
                    }
                }  catch let error as NSError {
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir logeoUsuario:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir logeoUsuario: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueTabBar"{
            let tabBarViewController = segue.destination as! TabBarViewController
            tabBarViewController.usuario = self.usuario
            
        }
    }

    //enviar OTP
    //esta funcion solo envia el OTP, para utilizar este funcion cualquiera de las dos variables deben estar estar correctos.
    func enviarCodigoOPT(userName: String, identificacion: String, token:String)   {
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = [ "strUsername": userName,
                                    "strIdentificacion": identificacion,
                                    "strToken": token,
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                    "strObservacion": _CONST.CANAL_DES]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/procesoEnvioOTP")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { (data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let otpRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    
                    if  otpRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK {
                        
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                            let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
                            otpViewController.popUpOTPDelegate = self
                            self.present(otpViewController, animated: true)
                        }
                    }else{
                        if otpRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: otpRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_OTP_NOK , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir Envio OTP :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    func verificarOTP(identificacion: String , codigoOtp: String, token: String)-> Void{
        
        let macAddress = UIDevice.current.identifierForVendor?.uuidString
        
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = ["strIdentificacion": identificacion,
                                   "strOTP": codigoOtp,
                                   "strToken": token,
                                   "strCanal": _CONST.CANAL_DES,
                                   "strCanalSeguridad": _CONST.CANAL_SEG_IOS,
                                   "strOrigenIdentificador": macAddress?.description ?? ""]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/verificarOTP")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        session.dataTask(with: request) { data, response, error in
            if let data = data{
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK{
                        
                        if self.usuario.strUserEstado == _CONST.COD_ESTADO_CAMIBIO_CLAVE{
                           /* if(self.usuario.strIdentificacion == self.usuarioField.text){
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                     Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_CEDULA_IGUAL_USUARIO, view: self)
                                }
                            }else{*/
                            DispatchQueue.main.async {
                                 self.objActivity.hideActivityIndicator(uiView: self.view)
                                let cambioClaveViewController = self.storyboard?.instantiateViewController(withIdentifier: "CambioClaveViewController") as!  CambioClaveViewController
                                cambioClaveViewController.usuario = self.usuario
                                cambioClaveViewController.claveLogin = self.contrasenaField.text!
                                cambioClaveViewController.userLogin = self.usuarioField.text!
                                
                                self.present(cambioClaveViewController, animated: true)
                                }
                            //}
                        }else{
                        
                        if mensajeRespuestaData.strValorResultado != "" {
                             self.usuario.strToken = mensajeRespuestaData.strValorResultado
                        }
                        
                            DispatchQueue.main.async {
                                                   self.objActivity.hideActivityIndicator(uiView: self.view)
                                                   self.usuarioField.text = ""
                                                   self.contrasenaField.text = ""
                                                   
                                                   }
                    
                        Utilidades.mensajeSugerencia(titulo: "", mensaje: "", view: self)
                            
                       
                        }
                    }else{
                        if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }else if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_USERNAME{
                            
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                               Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir verificar OTP:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir verificarOTP: No se obtubo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }

    func cerrarSesion(identificacion: String , token: String, opcion: Int)-> Void{
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let json: [String: Any] = ["strIdentificacion": identificacion,
                                   "strToken": token]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(_CONST.DIRECCION)/cerrarSesion")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        
        session.dataTask(with: request) { data, response, error in
            
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200
            {
                do{
                    let mensajeRespuestaData = try JSONDecoder().decode(MensajeModel.self , from: data)
                    if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_RETORNO_OK{
                        
                        
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                        if (opcion == _CONST.OP_CERRAR_ENVIO){
                            
                            DispatchQueue.main.async{
                                let alertControl = UIAlertController(title: _CONST.AVISO, message: mensajeRespuestaData.strMensajeError , preferredStyle: .alert)
                                let actionM = UIAlertAction(title: _CONST.ACEPTAR, style: .default) { (action:UIAlertAction) in
                                    self.enviarCodigoOPT(userName: self.usuario.strNombreCompleto, identificacion: self.usuario.strIdentificacion, token: self.usuario.strToken)
                                }
                                alertControl.addAction(actionM)
                                self.present(alertControl, animated: true, completion: nil)
                            }
                        }else{
                             DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }
                    }else{
                        if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                            }
                        }
                    }
                }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else if let error = error{
                print("Error al consumir cerrar sesion:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                print("Error al consumir cerrar sesion: No se obtuvo ningun dato")
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
        
    }
    /*
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          navigationController?.setNavigationBarHidden(true, animated: animated)
      }

      override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          navigationController?.setNavigationBarHidden(false, animated: animated)
      }*/
    
    
    
    
    
    
}
extension LoginViewController: OTPDelegate{
    func inserOTP(value: String) {
        
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        verificarOTP(identificacion: usuario.strIdentificacion, codigoOtp: value, token: usuario.strToken)
    }
}


