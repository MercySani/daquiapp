//
//  SpinnerViewController.swift
//  DaquiApp
//
//  Created by fjaneta on 18/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class SpinnerViewController: UIViewController {
    var spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
    var label: UILabel = UILabel()
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        label.frame = CGRect(x: 0, y: 100, width: 200, height: 50)
        label.textColor = UIColor.white
        label.font = UIFont(name: _CONST.FONT, size: 20)
        label.text = _CONST.ESPERA
        label.textAlignment = NSTextAlignment.center
        view.addSubview(label)
        
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)
        
    }
    
    func show(uiViewController: UIViewController) {
        uiViewController.addChildViewController(self)
        self.view.frame = uiViewController.view.frame
        uiViewController.view.addSubview(self.view)
        self.didMove(toParentViewController: self)
    }
    
    func hide(){
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
}
