//
//  CreditoSWController.swift
//  DaquiApp
//
//  Created by Fredy on 17/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
import UIKit

class CreditoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    @IBOutlet weak var creditoTableView: UITableView!
    let objActivity = activityIndicatorVC()
    var usuario: UsuarioModel = UsuarioModel()
    var creditos: [CreditoModel] = []
    
    lazy var refreshControl: UIRefreshControl = {
        let refresControl = UIRefreshControl()
        refresControl.addTarget(self, action: #selector(CreditoViewController.actualizarDatos(_:)), for: .valueChanged)
        refresControl.tintColor = UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        return refresControl
    }()
    
    @objc func actualizarDatos (_ refresControl: UIRefreshControl){
        
        creditos.removeAll()
        refresControl.endRefreshing()
        consultaCreditos(identificacion: usuario.strIdentificacion, idTipoIdentificacion: _CONST.TI_CEDULA, usuario: usuario.strIdentificacion , token: usuario.strToken)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        creditoTableView.reloadData()
        usuario = (tabBarController as! TabBarViewController).usuario
        
        if(usuario.strNombreUser == _CONST.USUARIO_DEMO){
            creditos = _DEMO.creditos()
            self.creditoTableView.reloadData()
        }else{
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            consultaCreditos(identificacion: usuario.strIdentificacion, idTipoIdentificacion: _CONST.TI_CEDULA, usuario: usuario.strIdentificacion , token: usuario.strToken)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.creditos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "creditoCell") as! GenericoTableViewCell
        if (self.creditos[indexPath.row].ccuenta == "" ){
            cell.titulo.text = _CONST.MSJ_CRED_SIN
            cell.detalle1.text = ""
            cell.detalle2.text = ""
            cell.detalle3.text = ""
            cell.detalle4.text = ""
            cell.detalle5.text = ""
            cell.detalle6.text = ""
            cell.detalle7.text = ""
            cell.detalle8.text = ""
        }else{
        cell.titulo.text = self.creditos[indexPath.row].descripcionproducto
        
        let auxEstado = NSMutableAttributedString()
        auxEstado .bold("Estado: ") .normal(self.creditos[indexPath.row].descripcionestatuscuenta)
        cell.detalle1.attributedText = auxEstado
        
        let auxMonto = NSMutableAttributedString()
        auxMonto .bold("Monto: ") .normal("$\(self.creditos[indexPath.row].montoprestamo)")
        cell.detalle2.attributedText = auxMonto
        
        let auxSaldo = NSMutableAttributedString()
        auxSaldo .bold("Saldo: ") .normal(self.creditos[indexPath.row].saldoPendienteVigente)
        cell.detalle3.attributedText = auxSaldo
        
        let auxPlazo = NSMutableAttributedString()
        auxPlazo .bold("Plazo: ") .normal(self.creditos[indexPath.row].plazo)
        cell.detalle4.attributedText = auxPlazo
        
        let auxFrecuencia = NSMutableAttributedString()
        auxFrecuencia .bold("Frecuencia: ") .normal(self.creditos[indexPath.row].frecuencias)
        cell.detalle5.attributedText = auxFrecuencia
        
        let auxFProxiPago = NSMutableAttributedString()
        auxFProxiPago .bold("Fecha prox pago: ") .normal(self.creditos[indexPath.row].fechaProximoPago)
        cell.detalle6.attributedText = auxFProxiPago
        
        let auxSaldoPendienteVige = NSMutableAttributedString()
        auxSaldoPendienteVige .bold("Cuota: ") .normal(self.creditos[indexPath.row].saldoPendiente)
        cell.detalle7.attributedText = auxSaldoPendienteVige
        
        let auxDiasVen = NSMutableAttributedString()
        auxDiasVen .bold("Días Vencidos:") .normal(String(self.creditos[indexPath.row].diasVencidos))
        cell.detalle8.attributedText = auxDiasVen
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.evaluarLista(creditosAux: self.creditos) == _CONST.LISTA_VACIA{
            
        }else{
                performSegue(withIdentifier: "mostrarCreditoDetalles", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if self.evaluarLista(creditosAux: self.creditos) == _CONST.LISTA_VACIA{
            
        }else{
            if let destination =  segue.destination as? TablaPagoViewController {
            destination.numeroCuenta = creditos[(creditoTableView.indexPathForSelectedRow?.row)!].ccuenta
            }
        }
    }
    
    //para esta funcion se retiro la condicion if del data.count por que devuelve valores en cero, por lo cual se controlara a nivel de vista.    
    func consultaCreditos(identificacion: String, idTipoIdentificacion: String, usuario: String,  token: String )  {
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/listaCreditos/\(_CONST.CANAL_DES)/\(identificacion)/\(idTipoIdentificacion)/\(usuario)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return
            
        }
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.creditos = try JSONDecoder().decode([CreditoModel].self , from: data)
                    if  self.evaluarLista(creditosAux: self.creditos) != _CONST.LISTA_VACIA{
                        
                       
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            self.creditoTableView.reloadData()
                        }
                    }else{
                        DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                            self.creditoTableView.reloadData()
                            
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                    print("Error al leer JSON: \(error)")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
                }
            }else if let error = error{
                print("Error al consumir Creditos :error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                }
            }
            }.resume()
        
    }
    
    func evaluarLista(creditosAux: [CreditoModel] )-> Int{
        for item in creditosAux{
            if item.ccuenta == ""{
                return _CONST.LISTA_VACIA
            }
        }
        return 1
    }
    
}
