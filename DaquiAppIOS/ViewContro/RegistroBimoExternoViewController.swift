//
//  RegistroBimoExternoViewController.swift
//  DaquiAppIOS
//
//  Created by MacBookPro on 6/1/20.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import Foundation
import UIKit

class RegistroBimoExternoViewController: UIViewController,UITextFieldDelegate{
     let objActivity = activityIndicatorVC()
    var existeBimo : String = ""
    var usuarioRespuesta = ResultadoRestful()
    @IBOutlet weak var btnContinuar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        identificacionField.delegate = self
        celularField.delegate=self
        self.navigationController?.navigationBar.tintColor =
               UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: _CONST.ALPHA)
        btnContinuar.layer.cornerRadius = _CONST.CORNER_RADIUS
              
    }
 
    @IBOutlet weak var celularField: UITextField!
    @IBOutlet weak var identificacionField: UITextField!
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          var tipoCaracter: CharacterSet = CharacterSet()
          var characterSet: CharacterSet = CharacterSet()
          var retorno: Bool = false
          
              tipoCaracter = CharacterSet.decimalDigits
              characterSet = CharacterSet(charactersIn: string)
              retorno = tipoCaracter.isSuperset(of: characterSet)
              if(retorno){
                  guard let text = textField.text else { return true }
                  let count = text.count + string.count - range.length
                  retorno = count <= 10
              }
         
          return retorno
      }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           identificacionField.resignFirstResponder()
           celularField.resignFirstResponder()
           self.view.endEditing(true)
    }
    
    @IBAction func registrar(_ sender: Any) {
        
        guard !(identificacionField.text!.isEmpty) else {
         DispatchQueue.main.async {
            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_C_VACIOS , view: self)
          }
         return
         }
        
         guard (identificacionField.text!.count == 10) else {
         DispatchQueue.main.async {
            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_BIMO_ERROR_IDENTIFICACION_LONGITUD , view: self)
          }
         return
         }
        
        guard !(celularField.text!.isEmpty) else {
         DispatchQueue.main.async {
            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_C_VACIOS , view: self)
          }
         return
         }
        
         guard (celularField.text!.count == 10) else {
         DispatchQueue.main.async {
            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_BIMO_ERROR_CELULAR_LONGITUD , view: self)
          }
         return
         }
        
           if !self.identificacionField.text!.isEmpty  || !self.celularField.text!.isEmpty{
                if(Utilidades.SoloNumeros(string: identificacionField.text!) && Utilidades.SoloNumeros(string: celularField.text!) ){
                    DispatchQueue.main.async { self.objActivity.showActivityIndicator(uiView: self.view)
                    }
                    registroBimoExterno(identificacion: self.identificacionField.text!, celular: self.celularField.text!)
                }else{
                   Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.MSJ_SOLO_NUMEROS, view: self)
                }
                
            }else{
                Utilidades.mensajeSimple(titulo: _CONST.ALERTA, mensaje: _CONST.ERR_C_VACIOS, view: self)
            }
        
    }
    
      func registroBimoExterno(identificacion: String,celular: String)-> Void{
            
            let session = URLSession(
                configuration: URLSessionConfiguration.ephemeral,
                delegate: NSURLSessionPinningDelegate(),
                delegateQueue: nil)
            let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
            let macAddress = UIDevice.current.identifierForVendor?.uuidString
                  let version: String = UIDevice.current.model+"_"+UIDevice.current.systemName+"_" + UIDevice.current.systemVersion+"|"+appVersion
            let strAction="M"
            let json: [String: Any] = ["strAccion": strAction,
                                    "strNombreUser": celular,
                                    "strPassword": "" ,
                                    "strCanalSeguridad": _CONST.CANAL_SEG_IOS_BIMO,
                                    "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                    "strOrigenIdentificador": macAddress?.description ?? "",
                                    "strDetalleDispositivo": version,
                                    "strToken":"",
                                    "strSistema": _CONST.SISTEMA_BIMO_EXTERNO,
                                    "strIdentificacion": identificacion,
                                    "strObservacion": _CONST.CANAL_DES]
            
            
            let jsonData = try? JSONSerialization.data(withJSONObject: json)
            let url = URL(string: "\(_CONST.DIRECCION)/bimo/generalexterno")!
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.httpBody = jsonData
        
            session.dataTask(with: request) { data, response, error in
                if let data = data, let _ = response as? HTTPURLResponse {
                    do{
                        print("data---->> \(data)")
                        let mensajeRespuestaData = try JSONDecoder().decode(ResultadoRestful.self , from: data)
                        print("mensajeRespuestaData.strCodigoError---->> \(mensajeRespuestaData)")
                        self.usuarioRespuesta = mensajeRespuestaData
                        if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_OK{
                               print(mensajeRespuestaData.strCodigoError)
                            self.existeBimo = mensajeRespuestaData.strVerificarExistenciaUCSOrigen.description
                            
                            DispatchQueue.main.async { self.objActivity.hideActivityIndicator(uiView: self.view)
                                let otpViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopUpOTPViewController") as! PopUpOTPViewController
                                otpViewController.popUpOTPDelegate = self
                                self.present(otpViewController, animated: true)
                            }
                            
                        }else if mensajeRespuestaData.strCodigoError=="-2"{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSimpleUrl(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError,
                                        url: mensajeRespuestaData.strEMail, view: self)
                                }
                        }else{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                                }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else if let error = error{
                    print("Error al consumir buscarUsuario:error: \(error)")
                    if let error = error as NSError?, error.domain == NSURLErrorDomain{
                        if error.code == -1001 {
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                            }
                        } else if error.code == -1009 {
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                            }
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    //No se obtubo ningun dato
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
                }.resume()
            
            
        }
        
       func verificarOTP(celular: String , codigoOtp: String, token: String)-> Void{
            
            let macAddress = UIDevice.current.identifierForVendor?.uuidString
            
            let session = URLSession(
                configuration: URLSessionConfiguration.ephemeral,
                delegate: NSURLSessionPinningDelegate(),
                delegateQueue: nil)
            
           let strAction="OTP"
                  let json: [String: Any] = ["strAccion": strAction,
                                          "strNombreUser": celular,
                                          "strPassword": "" ,
                                          "strCanalSeguridad": _CONST.CANAL_SEG_IOS_BIMO,
                                          "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                          "strOrigenIdentificador": macAddress?.description ?? "",
                                          "strDetalleDispositivo": "",
                                          "strToken":token,
                                          "strSistema": _CONST.SISTEMA_BIMO_EXTERNO,
                                          "strIdentificacion": "",
                                          "strObservacion": _CONST.CANAL_DES,
                                          "strOTP": codigoOtp]
            
              
               let jsonData = try? JSONSerialization.data(withJSONObject: json)
               let url = URL(string: "\(_CONST.DIRECCION)/bimo/generalexterno")!
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.httpBody = jsonData
            session.dataTask(with: request) { data, response, error in
                if let data = data{
                    do{
                        let mensajeRespuestaData = try JSONDecoder().decode(ResultadoRestful.self , from: data)
                        self.usuarioRespuesta.strToken = mensajeRespuestaData.strToken
                        if  mensajeRespuestaData.strCodigoError == _CONST.CODIGO_OK{
                            if self.usuarioRespuesta.strVerificarExistenciaUCSOrigen.description == "true"{
                                DispatchQueue.main.async {
                                     self.objActivity.hideActivityIndicator(uiView: self.view)
                                    let definirClaveBimoExternoViewController = self.storyboard?.instantiateViewController(withIdentifier: "DefinirClaveBimoExternoViewController") as!  DefinirClaveBimoExternoViewController
                                    definirClaveBimoExternoViewController.usuario = self.usuarioRespuesta;
                                    definirClaveBimoExternoViewController.recuperar = "false";
                                    self.present(definirClaveBimoExternoViewController, animated: true)
                                }
                            }else{
                           DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "segueAfiliarBimoExterno", sender: self)
                            }
                            }
                        }else{
                            if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                                
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                                }
                            }else if mensajeRespuestaData.strCodigoError == _CONST.CODIGO_ERROR_USERNAME{
                                
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                                }
                            }else{
                                DispatchQueue.main.async {
                                    self.objActivity.hideActivityIndicator(uiView: self.view)
                                   Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensajeError , view: self)
                                }
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else if let error = error{
                    print("Error al consumir verificar OTP:error: \(error)")
                    if let error = error as NSError?, error.domain == NSURLErrorDomain{
                        if error.code == -1001 {
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                            }
                        } else if error.code == -1009 {
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                            }
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    print("Error al consumir verificarOTP: No se obtubo ningun dato")
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
                }.resume()
            
        }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "segueAfiliarBimoExterno"{
               let afiliacionBimoExternoViewController = segue.destination as! AfiliacionBimoExternoViewController
               afiliacionBimoExternoViewController.usuario =  self.usuarioRespuesta
               
           }
       }
        
    }
    extension RegistroBimoExternoViewController: OTPDelegate{
        func inserOTP(value: String) {
            
            DispatchQueue.main.async {
                self.objActivity.showActivityIndicator(uiView: self.view)
            }
            verificarOTP(celular: self.celularField.text!, codigoOtp: value, token: usuarioRespuesta.strToken)
        }
    }


