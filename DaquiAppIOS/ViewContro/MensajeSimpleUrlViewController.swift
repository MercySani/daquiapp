//
//  MensajeSimpleUrlViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 13/06/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import UIKit

class MensajeSimpleUrlViewController: UIViewController, UITextFieldDelegate {

    var mensaje: String?
    var url: String?
    
    @IBOutlet weak var lblMensaje: UILabel!
    
    @IBOutlet weak var btnAceptar: UIButton!
    @IBOutlet weak var btnCancelar: UIButton!
    @IBOutlet weak var viewContenedor: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblMensaje.text = mensaje
        btnAceptar.layer.cornerRadius = _CONST.CORNER_RADIUS
        btnCancelar.layer.cornerRadius = _CONST.CORNER_RADIUS
        viewContenedor.layer.cornerRadius = 10
        if url!.isEmpty {
            btnCancelar.isHidden = true
        }
        
    }

    
    @IBAction func btnAccionAceptar(_ sender: Any) {
        if url!.isEmpty {
           self.dismiss(animated: true, completion: nil)
        }else{
             openUrl(urlString: url)
        }
    }
    
    @IBAction func btnAccionCancelar(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

        func openUrl(urlString:String!) {
            let url = URL(string: urlString)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    
    
}
