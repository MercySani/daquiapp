//
//  AnalisisEgresoViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 8/3/19.
//  Copyright © 2019 CoopDaquilema. All rights reserved.
//

import UIKit
import Charts
class AnalisisEgresoViewController: UIViewController {
    
    @IBOutlet weak var barChartView: BarChartView!
    var usuario: UsuarioModel = UsuarioModel()
    var transaccionesCuenta: [TransaccionCuentaModel] = []
    let objActivity = activityIndicatorVC()
    var numeroCuenta: String = String()
    let fechaDatePicker = UIDatePicker()
    let date = Date()
    
    override func viewDidLoad() {
        usuario = (tabBarController as! TabBarViewController).usuario
        
        barChartView.noDataText = "No existe datos para mostrar."
        //consultar las ultimas transacciones
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateStyle = .short
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        let dateFormato = dateStringFormatter.date(from: dateStringFormatter.string(from: self.date))
        let monthsToAdd = -3
        
        let newDate = Calendar.current.date(byAdding: .month, value: monthsToAdd, to: dateFormato!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = _CONST.YYYY_MM_DD
        
        //fin consultar
        consultaTransaccionesCuentas(identificacion: usuario.strIdentificacion, idTipoIdentificacion: _CONST.TI_CEDULA, numeroCuenta: numeroCuenta, fecha: dateFormatter.string(from: newDate!), token: usuario.strToken)
        
        
        
    }
    
    func buscarMes(mes:Int) -> String {
        switch mes {
        case 1:
            return "Enero"
        case 2:
            return "Febrero"
        case 3:
            return "Marzo"
            
        case 4:
            return "Abril"
            
        case 5:
            return "Mayo"
            
        case 6:
            return "Junio"
            
        case 7:
            return "Julio"
            
        case 8:
            return "Agosto"
            
        case 9:
            return "Septiembre"
            
        case 10:
            return "Octubre"
            
        case 11:
            return "Noviembre"
            
        case 12:
            return "Diciembre"
        default:
            return ""
        }
    }
    
    
    
    func legenChart(meses:[String]) -> Void {
        barChartView.noDataText = "No existe dato para mostrar."
        let legend = barChartView.legend
        legend.enabled = true
        legend.horizontalAlignment = .right
        legend.verticalAlignment = .top
        legend.orientation = .vertical
        legend.drawInside = true
        legend.yOffset = 10.0;
        legend.xOffset = 10.0;
        legend.yEntrySpace = 0.0;
        
        
        let xaxis = barChartView.xAxis
        xaxis.drawGridLinesEnabled = true
        xaxis.labelPosition = .bottom
        xaxis.centerAxisLabelsEnabled = true
        xaxis.valueFormatter = IndexAxisValueFormatter(values: meses)
        xaxis.granularity = 1
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.maximumFractionDigits = 1
        
        
        let yaxis = barChartView.leftAxis
        yaxis.spaceTop = 0.35
        yaxis.granularity = 1
        yaxis.valueFormatter = formateadorDeValores()
        yaxis.axisMinimum = 0
        yaxis.drawGridLinesEnabled = false
        barChartView.rightAxis.enabled = false
    }
    
    //BarChartAgrupado
    func setChart(ingreso:[(Int, Double)], egreso: [(Int, Double)] ){
        barChartView.noDataText = "No existe datos para mostrar."
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries1: [BarChartDataEntry] = []
        for (mes, valor) in ingreso {
           
            let dataEntry = BarChartDataEntry(x: Double(mes) , y: valor)
            
            dataEntries.append(dataEntry)
        }
        for (mes, valor ) in egreso {
            let dataEntry1 = BarChartDataEntry(x: Double(mes) , y: valor)
            dataEntries1.append(dataEntry1)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Ingresos")
        chartDataSet.valueFormatter = formtValor()
        chartDataSet.colors = [UIColor(red:0.60, green:0.00, blue:0.00, alpha:1.0)]
        chartDataSet.valueFont = UIFont(name: "Verdana", size: 15)!
        
        let chartDataSet1 = BarChartDataSet(values: dataEntries1, label: "Egresos")
        chartDataSet1.valueFormatter = formtValor()
        chartDataSet1.valueFont = UIFont(name: "Verdana", size: 15)!
        chartDataSet1.colors = [UIColor(red:0.30, green:0.30, blue:0.30, alpha:1.0)]
        
        
        let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet1]

        let chartData = BarChartData(dataSets: dataSets)
        
        
        let groupSpace = 0.3
        let barSpace = 0.05
        let barWidth = 0.3
        
        var groupCount:Int = 0
        if(ingreso.count>egreso.count){
          groupCount = ingreso.count
        }else{
          groupCount = egreso.count
        }
        
        let startYear = 0
        
        
        chartData.barWidth = barWidth;
        
        barChartView.xAxis.axisMinimum = Double(startYear)
        let spaceGroup = chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        barChartView.xAxis.axisMaximum = Double(startYear) + spaceGroup * Double(groupCount)
        
        chartData.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
        
        barChartView.notifyDataSetChanged()
        barChartView.data = chartData

        //chart animation
        barChartView.animate(xAxisDuration: 1.5, yAxisDuration: 1.5, easingOption: .linear)
        
        
    }
    
    func calcularValoresXMeses(lisValoresXmes:[(Int,Double)]) -> [(Int,Double)] {
     
        var auxTotal:[(Int,Double)] = []
        var subtotal1:Double = 0
        var subtotal2:Double = 0
        var subtotal3:Double = 0
        var subtotal4:Double = 0
        var subtotal5:Double = 0
        var subtotal6:Double = 0
        var subtotal7:Double = 0
        var subtotal8:Double = 0
        var subtotal9:Double = 0
        var subtotal10:Double = 0
        var subtotal11:Double = 0
        var subtotal12:Double = 0
        if(lisValoresXmes.count != _CONST.LISTA_VACIA){
            for (mes,valor) in lisValoresXmes{
                switch (mes){
                case 1:
                    subtotal1 = subtotal1 + valor
                    
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal1
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal1))
                        }
                        
                    }else{
                        auxTotal.append((mes,subtotal1))
                    }
                    break
                case 2:
                    subtotal2 = subtotal2 + valor
                   
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal2
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal2))
                        }
                        
                    }else{
                        auxTotal.append((mes,subtotal2))
                    }
                    break
                case 3:
                    subtotal3 = subtotal3 + valor
                    
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal3
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal3))
                        }
                        
                    }else{
                        auxTotal.append((mes,subtotal3))
                    }
                    break
                case 4:
                    subtotal4 = subtotal4 + valor
                    
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal4
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal4))
                        }
                        
                    }else{
                        auxTotal.append((mes,subtotal4))
                    }
                    break
                case 5:
                    subtotal5 = subtotal5 + valor
                    
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal5
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal5))
                        }
                        
                    }else{
                        auxTotal.append((mes,subtotal5))
                    }
                    break
                case 6:
                    subtotal6 = subtotal6 + valor
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal6
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal6))
                        }
                        
                    }else{
                        auxTotal.append((mes,subtotal6))
                    }
                    break
                case 7:
                    subtotal7 = subtotal7 + valor
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal7
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal7))
                        }
                        
                    }else{
                        auxTotal.append((mes,subtotal7))
                    }
                    break
                case 8:
                    subtotal8 = subtotal8 + valor
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal8
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal8))
                        }
                        
                    }else{
                        auxTotal.append((mes,subtotal8))
                    }
                    break
                case 9:
                    subtotal9 = subtotal9 + valor
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal9
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal9))
                        }
                        
                    }else{
                        auxTotal.append((mes,subtotal9))
                    }
                    break
                case 10:
                    subtotal10 = subtotal10 + valor
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal10
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal10))
                        }
                        
                    }else{
                        auxTotal.append((mes,subtotal10))
                    }
                    break
                case 11:
                    subtotal11 = subtotal11 + valor
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal11
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal11))
                        }
                        
                    }else{
                        auxTotal.append((mes,subtotal11))
                    }
                    break
                case 12:
                    subtotal12 = subtotal12 + valor
                    var isNuevoRegistro: Bool = false
                    if(auxTotal.count != _CONST.LISTA_VACIA){
                        for i in 0..<auxTotal.count{
                            if(mes == auxTotal[i].0 ){
                                auxTotal[i].1 = subtotal12
                                isNuevoRegistro = false
                            }else{
                                isNuevoRegistro = true
                            }
                        }
                        if(isNuevoRegistro){
                            auxTotal.append((mes,subtotal12))
                        }
                    }else{
                        auxTotal.append((mes,subtotal12))
                    }
                    break
                default:
                    auxTotal.append((0,0))
                    break
                }
                
                
            }
            return auxTotal
        }
        return auxTotal
    }
    
    func consultaTransaccionesCuentas( identificacion: String, idTipoIdentificacion: String, numeroCuenta: String, fecha: String, token:String){
        let session = URLSession(
            configuration: URLSessionConfiguration.ephemeral,
            delegate: NSURLSessionPinningDelegate(),
            delegateQueue: nil)
        
        let urlString = "\(_CONST.DIRECCION)/consultaTransaccionesCuentas/\(_CONST.CANAL_DES)/\(identificacion)/\(idTipoIdentificacion)/\(numeroCuenta)/\(fecha)/\(token)/\(_CONST.CANAL_SEG_IOS)"
        
        guard let url = URL(string: urlString) else {
            
            DispatchQueue.main.async {
                self.objActivity.hideActivityIndicator(uiView: self.view)
                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
            }
            return}
        session.dataTask(with: url){(data, response, error) in
            if let data = data, let response = response as? HTTPURLResponse,
                response.statusCode==200{
                do{
                    self.transaccionesCuenta = try JSONDecoder().decode([TransaccionCuentaModel].self , from: data)
                    if  self.transaccionesCuenta.count != _CONST.LISTA_VACIA{
                        
                        
                        var lisValorYMesIngresos:[(Int,Double)]=[]
                        var lisValorYMesEgresos:[(Int,Double)]=[]
                        
                        for transaccion in self.transaccionesCuenta{
                            var components: DateComponents = DateComponents()
                            if(!transaccion.FContable.isEmpty){
                                let fecha = transaccion.FContable
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy-MM-dd"
                                dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
                                let date = dateFormatter.date(from:fecha)!
                                let calendar = Calendar.current
                                components = calendar.dateComponents([.month], from: date)
                                
                            }
                            if transaccion.Depositos == "0.00"{
                                lisValorYMesEgresos.append((components.month!,Double(transaccion.Retiros)!))
                            }else{
                                lisValorYMesIngresos.append((components.month!,Double(transaccion.Depositos)!))
                            }
                        }
                        
                        var totalXmesIngreso:[(Int,Double)] = []
                        var totalXmesEgreso:[(Int,Double)] = []
                        
                        if(lisValorYMesIngresos.count != _CONST.LISTA_VACIA){
                            totalXmesIngreso = self.calcularValoresXMeses(lisValoresXmes: lisValorYMesIngresos)
                        }
                        if(lisValorYMesEgresos.count != _CONST.LISTA_VACIA){
                            totalXmesEgreso = self.calcularValoresXMeses(lisValoresXmes: lisValorYMesEgresos)
                        }
                        if(totalXmesIngreso.count != _CONST.LISTA_VACIA || totalXmesEgreso.count != _CONST.LISTA_VACIA){
                            var lisMeses: [String] = []
                            if(totalXmesIngreso.count > totalXmesEgreso.count ){
                                for item in totalXmesIngreso{
                                    lisMeses.append(self.buscarMes(mes: item.0))
                                }
                            }else {
                                for item in totalXmesEgreso{
                                    lisMeses.append(self.buscarMes(mes: item.0))
                                }
                            }
                            DispatchQueue.main.async {
                                self.legenChart(meses: lisMeses)
                                self.setChart(ingreso: totalXmesIngreso, egreso: totalXmesEgreso)
                            }
                            
                        }
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                        }
                    }
                }catch{
                    do {
                        let mensajeRespuesta = try JSONDecoder().decode(MensajeModel.self , from: data)
                        
                        if mensajeRespuesta.strCodigoError == _CONST.CODIGO_ERROR_TOKEN{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }else{
                            DispatchQueue.main.async {
                                self.objActivity.hideActivityIndicator(uiView: self.view)
                                Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuesta.strMensajeError , view: self)
                            }
                        }
                    }catch let error as NSError{
                        print("Error al leer JSON: \(error)")
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }
            }else if let error = error{
                print("Error al consumir Transaccion de Cuentas:error: \(error)")
                if let error = error as NSError?, error.domain == NSURLErrorDomain{
                    if error.code == -1001 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                        }
                    } else if error.code == -1009 {
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.objActivity.hideActivityIndicator(uiView: self.view)
                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.objActivity.hideActivityIndicator(uiView: self.view)
                        Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                    }
                }
            }else{
                //No se obtubo ningun dato
                DispatchQueue.main.async {
                    self.objActivity.hideActivityIndicator(uiView: self.view)
                    Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.MSJ_TRAN_CNTA_SIN, view: self)
                }
            }
            }.resume()
        
    }
    
    
}

final class formateadorDeValores: IAxisValueFormatter{
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let intVal = value
        return "$\(intVal)"
    }
}
final class formtValor: IValueFormatter{
    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        
        let intVal = String(format: "%.2f", value)
        
        return "$\(intVal)"
    }
    
    
}

