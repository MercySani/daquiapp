//
//  MenuSimuladorViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 10/7/18.
//  Copyright © 2018 CoopDaquilema. All rights reserved.
//

import UIKit

class MenuSimuladorViewController: UIViewController {

    @IBOutlet weak var btnCreditos: UIButton!
    @IBOutlet weak var btnPlazoFijo: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor =
            UIColor(red: _CONST.RED, green: _CONST.GREEN, blue: _CONST.BLUE, alpha: 1)
        
        //Botones redondeados
        btnPlazoFijo.layer.cornerRadius =  _CONST.CORNER_RADIUS
        btnCreditos.layer.cornerRadius = _CONST.CORNER_RADIUS
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let backItem = UIBarButtonItem()
        backItem.title = "Simulador"
        navigationItem.backBarButtonItem = backItem 
    }

}
