//
//  AlertBimoViewController.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 12/05/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import SwiftyXMLParser
import UIKit

class AlertBimoViewController: UIViewController{

    var variableMensaje: String?
    let objActivity = activityIndicatorVC()
    var usuario: UsuarioModel = UsuarioModel()
    
    @IBOutlet weak var lblMensaje: UILabel!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnSi: UIButton!
    
    // VARIABLES
    var strIdentificacion: String? = ""
    var strNumeroCuenta: String? = ""
    var strTrama: String? = ""
    var strAccion: String? = ""
    var strTelefonoOrigen: String? = ""
    var strTelefonoDestino: String? = ""
    var strDescripcion: String? = ""
    var strMonto: String? = ""
    var strCriterio: String? = ""
    var strEndToEnd: String? = ""
    var estadoUsuario = 0
    var origen: String?
    var identificacionAux: String?
    var canalAux: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblMensaje.text = variableMensaje
        self.btnNo.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.btnSi.layer.cornerRadius = _CONST.CORNER_RADIUS
        self.strIdentificacion = usuario.strIdentificacion
        self.strTelefonoOrigen = usuario.strBilleteraMovil
        
        if origen == _CONST.BIMO_ORIGEN_EXTERNO{
            identificacionAux = usuario.strTelefono
            canalAux = _CONST.CANAL_SEG_IOS_BIMO
        }else{
            identificacionAux = usuario.strIdentificacion
            canalAux = _CONST.CANAL_SEG_IOS
        }
    }
   
    @IBAction func accionSi(_ sender: Any) {
        print(strTrama!)
        print(strTelefonoOrigen!)
        print(identificacionAux!)
        print(strNumeroCuenta!)
        print(strAccion!)
        print(strTelefonoDestino!)
        print(strDescripcion!)
        print(strMonto!)
        print(strCriterio!)
        print(strEndToEnd!)
        print(self.origen!)
        DispatchQueue.main.async {
            self.objActivity.showActivityIndicator(uiView: self.view)
        }
        
        self.accionBimo(strTrama: strTrama!, telefonoOrigen: strTelefonoOrigen!, identificacion: identificacionAux! , strNumeroCuenta: strNumeroCuenta!,strAccion: strAccion!,
                strTelefonoDestino: strTelefonoDestino!, strDescripcion: strDescripcion!, strMonto: strMonto!, strCriterio: strCriterio! , strEndToEnd: strEndToEnd! ,token: usuario.strToken)

    }
    
    
    @IBAction func accionNo(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func accionBimo(strTrama: String, telefonoOrigen: String, identificacion: String, strNumeroCuenta: String, strAccion: String, strTelefonoDestino: String, strDescripcion: String, strMonto: String, strCriterio: String, strEndToEnd: String, token: String)-> Void{

              let session = URLSession(
                         configuration: URLSessionConfiguration.ephemeral,
                         delegate: NSURLSessionPinningDelegate(),
                         delegateQueue: nil)
              
              let json: [String: Any] = [ "strIdentificacion": identificacion,
                                          "strToken": token,
                                          "strCanalDescripcionAdicional": _CONST.CANAL_DES,
                                          "strCanalSeguridad": canalAux,
                                          "strTrama": strTrama,
                                          "strTelefonoOrigen": telefonoOrigen,
                                          "strTelefonoDestino": strTelefonoDestino,
                                          "strDescripcion": strDescripcion,
                                          "strMonto": strMonto,
                                          "strAccion": strAccion,
                                          "strFiltro": "",
                                          "strCriterio": strCriterio,
                                          "strNumeroCuenta": strNumeroCuenta,
                                          "strEndToEnd": strEndToEnd
                                          
              ]
              
              let jsonData = try? JSONSerialization.data(withJSONObject: json)
              let url = URL(string: "\(_CONST.DIRECCION)/bimo/general")!
              var request = URLRequest(url: url)
              request.httpMethod = "POST"
              request.httpBody = jsonData
              session.dataTask(with: request) { data, response, error in
              if let data = data, let response = response as? HTTPURLResponse, response.statusCode==202{
                  do{
                      let mensajeRespuestaData = try JSONDecoder().decode(BimoResultado.self , from: data)
                      if  mensajeRespuestaData.strCodigo == _CONST.CODIGO_OK{
                          
                          DispatchQueue.main.async {
                             let xml = try! XML.parse(mensajeRespuestaData.strXML)
                                if let codResultado = xml["BimoXml", "codResultado"].text {
                                  if codResultado == "0"{
                                    if strTrama == _CONST.AFILIACION_IFI_RQ{
                                        print("afiliacion exitosa")
                                        DispatchQueue.main.async {
                                          self.objActivity.hideActivityIndicator(uiView: self.view)
                                          
                                          Utilidades.mensajeAfiliacionBimo(titulo: _CONST.AVISO, mensaje:"AFILIADO EXITOSAMENTE" , view: self)
                                        }
                                    }else if strTrama == _CONST.BLOCK_USER_RQ{
                                        if strAccion == "DESENROLAMIENTO"{
                                           
                                            DispatchQueue.main.async {
                                              self.objActivity.hideActivityIndicator(uiView: self.view)
                                              
                                                Utilidades.mensajeDesafiliarBimo(titulo: _CONST.AVISO, mensaje:"DESAFILIADO EXITOSAMENTE" ,origen: self.origen!, view: self)
                                            }
                                        }else if strAccion == "BLOQUEO"{
                                            print("administracion usuario \(strAccion)")
                                            
                                            DispatchQueue.main.async {
                                              self.objActivity.hideActivityIndicator(uiView: self.view)
                                              
                                              Utilidades.mensajeAccionBimo(titulo: _CONST.AVISO, mensaje:"BLOQUEADO EXITOSAMENTE" , view: self)
                                            }
                                        }else {
                                            print("administracion usuario \(strAccion)")
                                            DispatchQueue.main.async {
                                              self.objActivity.hideActivityIndicator(uiView: self.view)
                                              
                                               Utilidades.mensajeAccionBimo(titulo: _CONST.AVISO, mensaje:"DESBLOQUEADO EXITOSAMENTE" , view: self)
                                            }
                                           
                                        }
                                        
                                    }else if strTrama == _CONST.TRANSFER_RQ{
                                         print("tranferencia \(strAccion)")
                                        DispatchQueue.main.async {
                                            self.objActivity.hideActivityIndicator(uiView: self.view)
                                            
                                            Utilidades.mensajeAccionBimo(titulo: _CONST.AVISO, mensaje:"PAGO EXITOSO" , view: self)
                                          }
                                     }else if strTrama == _CONST.SOLICITUD_COBRO_RQ{
                                        print("solicitud de cobro  \(strAccion)")
                                        DispatchQueue.main.async {
                                            self.objActivity.hideActivityIndicator(uiView: self.view)
                                             
                                             Utilidades.mensajeAccionBimo(titulo: _CONST.AVISO, mensaje:"SOLICITUD DE COBRO EXITOSO" , view: self)
                                        }
                                    }else if strTrama == _CONST.RECHAZO_COBRO_RQ{
                                        DispatchQueue.main.async {
                                            self.objActivity.hideActivityIndicator(uiView: self.view)
                                             
                                            Utilidades.mensajeAccionBimo(titulo: _CONST.AVISO, mensaje:"RECHAZO EXITOSO" , view: self)
                                        }
                                    }else {
                                        DispatchQueue.main.async {
                                            self.objActivity.hideActivityIndicator(uiView: self.view)
                                            Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                                         }
                                    }
                                  }else{
                                    DispatchQueue.main.async {
                                        self.objActivity.hideActivityIndicator(uiView: self.view)
                                         let mensajeError = xml["BimoXml", "mensajeResultado"].text
                                         Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeError!, view: self)
                                    }
                                    
                                }
                                 
                              }else{
                                DispatchQueue.main.async {
                                   self.objActivity.hideActivityIndicator(uiView: self.view)
                                   Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                                }
                            }
                          }
                      }else{
                          if mensajeRespuestaData.strCodigo == _CONST.CODIGO_ERROR_TOKEN{
                              DispatchQueue.main.async {
                                  self.objActivity.hideActivityIndicator(uiView: self.view)
                                   Utilidades.mensajeSesion(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensaje , view: self)
                              }
                          }else{
                              DispatchQueue.main.async {
                                  self.objActivity.hideActivityIndicator(uiView: self.view)
                                  Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: mensajeRespuestaData.strMensaje , view: self)
                              }
                          }
                      }
                  }catch let error as NSError{
                      print("Error al leer JSON: \(error)")
                      DispatchQueue.main.async {
                          self.objActivity.hideActivityIndicator(uiView: self.view)
                          Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                      }
                  }
              }else if let error = error{
                  print("Error al consumir cerrar sesion:error: \(error)")
                  if let error = error as NSError?, error.domain == NSURLErrorDomain{
                      if error.code == -1001 {
                          DispatchQueue.main.async {
                              self.objActivity.hideActivityIndicator(uiView: self.view)
                              Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1001, view: self)
                          }
                      } else if error.code == -1009 {
                          DispatchQueue.main.async {
                              self.objActivity.hideActivityIndicator(uiView: self.view)
                              Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_NT_1009, view: self)
                          }
                      }else{
                          DispatchQueue.main.async {
                              self.objActivity.hideActivityIndicator(uiView: self.view)
                              Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                          }
                      }
                  }else{
                      DispatchQueue.main.async {
                          self.objActivity.hideActivityIndicator(uiView: self.view)
                          Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                      }
                  }
              }else{
                  //No se obtubo ningun dato
                  DispatchQueue.main.async {
                      self.objActivity.hideActivityIndicator(uiView: self.view)
                      Utilidades.mensajeSimple(titulo: _CONST.AVISO, mensaje: _CONST.ERR_GENERAL, view: self)
                  }
              }
              }.resume()
          
          }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if strTrama == _CONST.BLOCK_USER_RQ{
            print("prepare administracion usuario ")
            
            if strAccion == "DESBLOQUEO" {
                estadoUsuario = 1
            }
            if strAccion == "BLOQUEO" {
                estadoUsuario = 3
            }
            if strAccion == "DESENROLAMIENTO" {
                estadoUsuario = 2
            }
        }else{
            
            estadoUsuario = 1
        }  
        
    }
    
}
