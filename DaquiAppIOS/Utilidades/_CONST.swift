//
//  _CONST.swift
//  DaquiApp
//
//  Created by fjaneta on 15/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
import UIKit

struct _CONST {

    static let URL: String = "http://192.168.122.253:8090"
    //static let URL: String = "http://172.17.24.98:8082"
    static let SERVIDOR: String = "/servicioswebhb/"
    
    //contingencia
    //static let URL: String = "http://daquionline.coopdaquilema.com:4040"
    //static let SERVIDOR: String = "/servicioswebhbmovil/"
    //fin contingencia
    
    //static let URL: String = "https://daquionline.coopdaquilema.com:9443"
    //static let SERVIDOR: String = "/servicioswebhbBeta/"
    static let SERVICIO: String = "mobile/"
    static let PAQUETE: String = "MdwServices"
    static let DIRECCION: String = "\(URL)\(SERVIDOR)\(SERVICIO)\(PAQUETE)"
    static let URL_CREACION_CUENTA : String = "https://daquionline.coopdaquilema.com:9443/serviciosBETA/"

    static let COD_ESTADO_CAMIBIO_CLAVE: Int = 2;
    static let CODIGO_ERROR_TOKEN: String = "ET001";
    static let CODIGO_ERROR: String = "E001";
    static let CODIGO_ACTULIZACION:String  = "A01"
    static let CODIGO_ERROR_USERNAME: String = "MDW0045";
    static let CODIGO_ERROR_SEG: String = "4";
    static let OP_CERRAR_ENVIO: Int = 1;
    static let OP_SOLO_CERRAR: Int = 0;
    static let CANAL_DES: String = "BVM_IOS";
    static let CODIGO_LOGUEO_OK: String = "0";
    static let TI_CEDULA: String = "C";
    static let TI_SOCIO: String = "S";
    static let TI_RUC: String = "R";

    static let LISTA_VACIA: Int = 0
    static let DEFAULT: String = "default"
    static let ACEPTO: String = "Acepto"
    static let AVISO: String = "Aviso"
    static let ALERTA: String = "Alerta"
    static let ACEPTAR: String = "Aceptar"
    static let CANCELAR: String = "Cancelar"
    static let ACTULIZACION: String = "¡DESCARGUELA AQUI!"
    static let SOLO_CERRAR: String = "Solo cerrar sesión"
    static let CERRAR_INGRESAR: String = "Cerrar sesión e ingresar"
    static let MSJ_CLAVES: String = "Las contraseñas no coinciden."
    static let MSJ_CLAVE_INCORRECTO: String = "La contraseña actual no es correcta."
    static let MSJ_INFO_MOV = "Usted visualizará sus 15 últimos movimientos, y al consultar sus movimientos de hasta 3 meses antes de la fecha actual"
    
    static let MSJ_EMAIL_INC: String = "Formato incorrecto del correo electrónico "
    static let MSJ_SOLO_NUM: String = "Debe contener solo números el número de cuenta"
    
    static let ESPERA: String = "Espere por favor"
    static let FONT: String = "Helvetica"
    
    static let TRANS_INT: String = "Transferencia interna"
    static let TRANS_EXT: String = "Transferencia externa"
    static let PAGO_TARJETA: String = "Pago de Tarjeta" 
    
    static let ERR_C_VACIOS: String = "Por favor complete los campos vacíos"
    static let ERR_GENERAL: String = "Lo sentimos, pero no podemos procesar su solicitud. inténtelo más tarde."
    static let ERR_NT_1001: String = "Por favor verifique si su punto de acceso no tiene restricciones hacia esta aplicación y vuelva a intentarlo."
    static let ERR_NT_1009: String = "Por favor verifique si su teléfono está conectado a internet y vuelva a intentarlo."
    
    static let CNTA_CERTIFICADOS: [String] = ["033"]
    
    static let CNTA_TARJETA_CREDITO:String = "04"
    
    static let N_PREF_CNTA_INI:  Int = 0;
    static let N_PREF_CNTA_FIN:  Int = 2;
    
    static let MSJ_TRAN_CNTA_SIN: String = "No ha realizado ninguna transacción a esta fecha "
    static let MSJ_CRED_SIN: String = "No tiene créditos."
    static let MSJ_DPF_SIN: String = "No cuenta con Depósitos a Plazo Fijo."
    static let MSJ_TBL_P_SIN: String = "Lo sentimos, tabla de pagos no disponible."
    static let MSJ_TBL_PR_SIN: String = "No ha realizado ningun pago."
    static let MSJ_CNTA_INT_SIN: String = "No tiene Cuentas Internas registradas."
    static let MSJ_CNTA_EXT_SIN: String = "No tiene Cuentas Externas registradas."
    static let MSJ_TARJETA: String = "No tiene tarjetas registradas"
    
    static let YYYY_MM_DD: String = "yyyy-MM-dd"
    
    
    static let SIM_CRE_MSJ_MONTO: String = "Ingrese el monto"
    static let SIM_CRE_MSJ_PLAZO: String = "Ingrese el plazo"
    static let SIM_CRE_MSJ_REPOSICION_BIEN: String = "Ingrese el valor de reposición del bien"
    static let SIM_CRE_MSJ_TIPO_CREDITO: String = "Seleccione el tipo de crédito"
    static let SIM_CRE_MSJ_FRECUENCIA: String = "Seleccione una frecuencia"
    static let SIM_CRE_MSJ_RANGO_TIPO_CREDITO:String = "El monto ingresado no corresponde al rango de este tipo de crédito"
    static let SIM_CRE_MSJ_QUINCENAL_SEMANAL : String = "Frecuencia no disponible para este tipo de crédito"
    static let MICROCREDITO: String = "MICROCREDITO"
    static let CONSUMO:String = "CONSUMO"
    static let IGLESIAS: String = "IGLESIAS"
    static let AGRICOLA: String = "AGRICOLA"
    static let VIVIENDA: String = "VIVIENDA"
    static let ESPERANZA: String = "DAQUI ESPERANZA"
    static let VACIO:String = ""
    static let VENCIMMIENTO: String = "VENCIMIENTO"
    static let MENSUAL: String = "MENSUAL"
    static let SEMANAL: String = "SEMANAL"
    static let QUINCENAL: String = "QUINCENAL"
    static let DIARIO: String = "DIARIO"
    static let BIMENSUAL: String = "BIMENSUAL"
    static let TRIMESTRAL: String = "TRIMESTRAL"
    static let SEMESTRAL: String = "SEMESTRAL"
    
    static let LISTA_TIPO_CREDITO: [String] = ["MICROCREDITO" , "CONSUMO", "IGLESIAS", "AGRICOLA", "VIVIENDA","DAQUI ESPERANZA"]
    static let LISTA_FRECUENCIA_SQM: [String] = ["SEMANAL" , "QUINCENAL", "MENSUAL"]
    static let LISTA_FRECUENCIA_MBTS: [String] = [ "MENSUAL", "BIMENSUAL", "TRIMESTRAL","SEMESTRAL"]
    static let LISTA_PLAZO_DIAS: [String] = ["30 Dias", "60 Dias", "90 Dias", "120 Dias","150 Dias", "180 Dias", "210 Dias", "240 Dias", "270 Dias","330 Dias", "MAYOR A 365 DIAS"]
    static let SIM_DPF_FRECUENCIA_MENSUAL: Int = 0
    static let SIM_DPF_FRECUENCIA_VENCIMIENTO: Int = 1
    static let BORDER_WIDTH: CGFloat = 1.0
    static let CORNER_RADIUS: CGFloat = 4
    static let DFP_MSJ_MONTO_MAYOR_CIEN : String = "El monto debe ser igual o mayor a $100"
    static let ESTADO_PP_ELIMINAR: Int = 0
    static let MMM_D_YYYY: String = "MMM d, yyyy"
    static let MSJ_PP_LISTA_VACIA = "No cuenta con registros de pago programado"
    static let TODO: Int = 0
    static let FRECUENCIA_DE_PAGO:Int = 0
    static let FRECUENCIA: String = "FRECUENCIA"
    static let TIPOCREDITO: String = "TIPOCREDITO"
    static let ERROR_FRECUENCIA: String = "La frecuencia de pagos no corresponde a ninguno de nuestros productos."
    static let ERROR_MONTO: String = "El monto no corresponde a ninguno de nuestros productos."
    
    // seguridades
    static let MSJ_OTP_SIN: String = "Lo sentimos, el código OTP no pudo ser enviado."
    static let MSJ_OTP: String = "Ingrese el OTP"
    static let MSJ_OTP_NOK: String = "Lo sentimos, el código OTP no pudo ser enviado."
    static let CANAL_SEG_IOS: String = "3"
    static let CANAL_SEG_IOS_BIMO: String = "6"
    
    //servicios basicos de facilito
    static let SBF_MEN_CATEGORIA = "Seleccione la categoría"
    static let SBF_MEN_SERVICIOS = "Seleccione el servicio"
    static let SBF_MEN_REFERENCIA = "Ingrese la referencia"
    static let PP_MEN_MONTO_MAXIMO = "Ingrese el monto máximo"
    static let PP_MEN_DIA_DEBITO = "Seleccione el día donde se realizará el débito"
    static let PP_MEN_FINICIO = "Ingrese la fecha de inicio"
    static let PP_MEN_FFIN = "Ingrese la fecha fin"
    // PP = Pago Programado  TS= Tipo Seleccion  FINICIO= Fecha INICIO
    static let PP_TS_FINICIO = "FINICIO"
    static let PP_TS_FFIN = "FFIN"
    static let PP_MEN_REGISTRO_OK = "El ingreso de pago programado es correcto"
    static let PP_MEN_ELIMINAR_OK = "Se eliminó el pago programado correctamente"
    
    static let PP_ESTADO_INGRESO:Int = 1
    static let INTERNET_BANKING: Int = 6
    static let SER_BAS_MENSA_CUENTA: String = "Seleccione una cuenta"
    static let CNTA_DEBITO_VISTA: String = "406"
    static let CNTA_DEBITO_KULLKIMIRAK: String = "403"
    static let CNTA_DEBITO_BASICA: String = "400"
    static let SELEC_CUENTA: String = "selecCuenta"
    static let SELEC_CUENTA_SOCIO: String = "CuentaSocio"
    static let CODIGO_RETORNO_OK: String = "0001"
    static let CODIGO_OK: String = "0"
    //CUENTA EXTERNA - TIPO IDENTIFICACION-
    static let CE_TI_CEDULA: String = "01"
    static let CE_TI_RUC: String = "02"
    static let CE_TI_PASAPORTE: String = "03"
    //CUENTA EXTERNA - TIPO CUENTA
    static let CE_TC_AHORROS: String = "02"
    static let CE_TC_CORRIENTE: String = "01"
    static let CE_TC_TARJETA_CREDITO: String = "04"
    static let COMISION_RUBRO_S: String = "S"
    static let TIPO_PAGO_T = "T"
    static let CE_GENERAL: Int = 1
    static let CE_TARJETA: Int = 4
    //CUENTA INTERNA
    static let CI_TIPO_IDENTIFICACION: String = "C"
    static let MEN_CERRAR_SESION: String = "Ha expirado su sesión"
    //Monto minimo  2 dolares
    static let MONTO_MINIMO: Int = 2
    static let MEN_FONDOS: String = "El monto supera el saldo mínimo disponible"
    //mensaje cuenta interna identificador
    static let MEN_CI_IDENTIFICADOR: String = "Ingrese el identificador"
    static let MEN_CI_EMAIL: String = "Ingrese el correo electrónico"
    static let MEN_CI_COMENTARIO: String = "Ingrese el Comentario"
    static let MEN_ENTIDAD_FINANCIERA: String = "Ingrese una entidad financiera"
    static let MEN_CUENTA:String = "Ingrese una cuenta"
    static let MEN_NOMBRE:String = "Ingrese el nombre"
    static let MEN_MONTIVO:String = "Ingrese el motivo"
    static let ID_PAGO_PROGRAMADO_SB:String = "0"
    static let ID_PAGO_PROGRAMADO_MERGE: Int = 0
    static let OPERADOR_SB: Int = 1
    static let ID_AGENCIA_SB: String = "0"
    static let SB_NO_DISPONIBLES:String = "No existe servicios disponibles "
    static let DATO_ADICIONAL: String = "NA"
    static let ALPHA: CGFloat = 1
    static let RED: CGFloat = 0.60
    static let GREEN: CGFloat = 0
    static let BLUE: CGFloat = 0
    static let RECARGAS: String = "recargas"
    static let SELEC_EMPRESA: String = "selecEmpresa"
    static let SER_BAS_MENSA_SELECCION_EMPRESA:String = "Seleccione una empresa"
    
    static let SBF_MEN_DINAMICO: String = "Ingrese el "
    static let SB_MEN_VALOR:String = "Ingrese el valor de la recarga"
    static let SB_MEN_RECARGA:String = "La recarga se realizó  correctamente"
    // TIEMPO MAXIMA PARA CONSULTA DE TRANSACCIONES DESDE LA FECHA ACTUAL  4 MESES ATRAS
    static let T_MAXIMO_CONSULTA:Int = -4
    // TIEMPO MINIMO PARA CREAR UN PAGO PROGRAMADO
    static let T_MINIMO_PP: Int = 6
    
    // fin servicios basicos
    
    //Ultimas transacciones realizadas
    static let ULTIMAS_TRANSACCIONES: Int = -15
    
    
    static let MSJ_CEDULA_IGUAL_USUARIO: String = "Estimado usuario, este es su primer ingreso a su Banca Electronica por lo cual debe ingresar a la aplicacion web DaquiOnline."
    static let MSJ_NUM_LETRAS : String = "La contraseña debe contener combinación de letras mayúsculas y minúsculas, y números."
    static let MSJ_SOLO_NUMEROS :String = "Ingrese solo numeros"
    static let MSJ_CARACTERES_MINIMOS: String = "Se requiere mínimo 8 caracteres en la clave."
static let MSJ_CARACTERES_MINIMOS_BIMO: String = "Se requiere mínimo 6 caracteres en la clave."
    static let ID_RECARGAS: Int = 5
    static let MSJ_NUM_LETRAS_NICKNAME : String = "El nombre de usuario debe contener combinación de letras mayúsculas y minúsculas, y números."
   static let MSJ_CARACTERES_MINIMOS_NICKNAME: String = "Se requiere mínimo 8 caracteres en el nombre de usuario.";
    static let MSJ_ESPACIOS_BLANCO_NICKNAME: String = "El nombre de usuario no puede contener espacios en blanco.";
    //MSJ de sugerencia
    static let MSJ_SUGERENCIA:String = "No entregue los datos de sus cuentas, tarjetas, contraseñas por ningún medio: ni correo electrónico, ni SMS, ni mensajería instantánea, ni redes sociales. Cooperativa Fernando Daquilema JAMÁS le pedirá esta información. No solicitamos la actualización de datos vía correo electrónico. No preguntamos su email, ni adjuntamos links a la Banca electrónica."
    static let MSJ_SUGERENCIA_COMPROMISO: String = "He leido, comprendido y me comprometo a practicar las medidas de seguridad antes expuestas, cada vez que haga uso de los canales electrónicos asociados a mi cuenta."
    static let MSJ_ACTUALIZACION: String = "Para un correcto funcionamiento te recomendamos que actualices DaquiApp. Existen nuevas funciones que te ofrecerán una gran experiencia"
    static let  MONTO_MAX_CREDITO: Double = 1000000;
    static let MSJ_MONTO_MAX_CREDITO: String = "El monto ingresado excede del monto de credito valido";
    static let MSJ_CARACTERES_ESPECIALES: String = "No debe contener caracteres especiales"
    static let MSJ_IDENTIFICACION: String = "Ingrese una identificación valida"
    static let MSJ_SEGURIDAD = "El dispositivos no cuenta con los requerimientos de seguridad necesarios."
    static let URL_APPSTORE: String = "https://apps.apple.com/ec/app/daquiapp/id1477675079"
    //SEGURIDAD
    //poner en false para probar en el simulador
    static let ACCESO_RESTRINGIDO_EMULADOR:Bool = false
    
    //LOGEO DEMO
    //static let USUARIO_DEMO: String = "D4qu1l3ma4dm1n"
    //static let PASSWORD_DEMO: String = "d4qu1l3mA71"
    static let USUARIO_DEMO: String = "Admin"
    static let PASSWORD_DEMO: String = "admin123"
    static let MSJ_DEFAULT_OK: String = "TRANSACCION REALIZADA CORRECTAMENTE"
    static let ROL_DEMO: String = ""
    static let IDENTIFICACION_DEMO: String = "0000000000"
    static let NOMBRECOMPLETO_DEMO: String = "ESCOBAR CASTRO DAQUILEMA FERNANDO"
    static let EMAIL_DEMO: String = "test@test.com "
    static let SUCURSAL_DEMO: String = "1"
    static let TELEFONO_DEMO: String = "0999999999"
    static let TOKEN_DEMO: String="asdfasdfasdfasdfasdfa"
    static let USUARIO_ESTADO_DEMO: Int = 0
    static let VERIFICAR_EXISTENCIA_UCS_DEMO: Bool = false
    static let MSJ_USUARIO_DEMO: String = "Ud no puede realizar esta accion ya que es un usuario demo"
    static let SIM_CRE_MSJ_CAMPOS: String = "Ingrese los campos solicitados"
    static let SIM_CRE_MSJ_ACEPTAR_CONDICIONES: String = "Para continuar debe leer y aceptar los términos y condiciones"
    
    static let CODIGO_BIOMETRIA_NO_REGISTRADO:String="B0001"
    static let CODIGO_BIMO_MAC_NO_REGISTRADO:String="-1"
    static let APLICA_CAMBIO:  String = "1";
    static let NO_APLICA_CAMBIO:  String = "0";
    
    // BIMO
    static let VALIDA_CAMPO_RQ = "camt.998.421"
    static let AFILIACION_IFI_RQ = "camt.998.211"
    static let CONSULTA_COBROS_PENDIENTES = "camt.998.371"
    static let TRANSFER_RQ = "camt.998.321"
    static let RECHAZO_COBRO_RQ = "camt.998.431"
    static let BLOCK_USER_RQ = "camt.998.311"
    static let SOLICITUD_COBRO_RQ = "camt.998.381"
    static let CONSULTA_TEL_CUENTA_RQ = "camt.998.441"
    static let LISTA_HISTORICOS = "LISTAHISTORICOS"
    static let OBTENER_CELULAR = "OBTENERCELULAR"
    

    static let TELEFONO_BIMO = "PHONE"
    static let CUENTA_BIMO = "CTA"
    static let COBRO_BIMO = "COLLECTIONS"
    static let PAGO_BIMO = "PAYMENTS"
    static let PAGO_PENDIENTE = "PND"
    static let BIMO_500 = "500" // no tiene cuenta bimo
    static let BIMO_501 = "501"  // cuenta bloqueada
    static let BIMO_1012 = "1012"
    
    static let MSJ_BIMO_MONTO_MIN_MAX: String = "Ingrese un monto entre 1.00 y 50.00"
    static let MSJ_BIMO_ERROR_REFERENCIA: String = "Ingrese la referencia"
    static let MSJ_BIMO_ERROR_RECEPTOR: String = "Ingrese el número receptor"
    static let MSJ_BIMO_ERROR_RECEPTOR_LONGITUD: String = "El número receptor debe tener 10 digitos"
    static let MSJ_BIMO_ERROR_RECEPTOR_CARACTERES: String = "El número receptor no debe contener caracteres"
    static let MSJ_BIMO_ERROR_IDENTIFICACION_LONGITUD: String = "La identificacion debe tener 10 digitos"
    static let MSJ_BIMO_ERROR_CELULAR_LONGITUD: String = "El celular debe tener 10 digitos"
    static let MSJ_BIMO_ERROR_CUENTA_AFILIACION: String = "Ingrese un número de cuenta para la afiliación"
    static let MSJ_BIMO_ERROR_GENERICO: String = "NO SE HA PODIDO CARGAR LA BILLETERA MÓVIL"
    
    static let CODIGO_ECUADOR = "+593"
    static let BIMO_ORIGEN_INTERNO = "INTERNO"
    static let BIMO_ORIGEN_EXTERNO = "EXTERNO"
    static let SISTEMA_BIMO_EXTERNO = "BIMOE"
    
     static let MSJ_NUM : String = "La contraseña debe contener solo números."
    
}
