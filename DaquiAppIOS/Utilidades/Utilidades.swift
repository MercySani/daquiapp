//
//  Utilidades.swift
//  DaquiApp
//
//  Created by fjaneta on 19/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
import UIKit


class Utilidades {
  
    static func mensajeActualizacion(titulo: String, mensaje:String, view: UIViewController) -> Void {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: titulo, message: mensaje , preferredStyle: .alert)
            
            let mensajeAlert = NSMutableAttributedString()
            mensajeAlert
                .bold("\n"+_CONST.MSJ_ACTUALIZACION)
            alertController.setValue(mensajeAlert, forKey: "attributedMessage")
            
            let aceptarAccion = UIAlertAction(title: _CONST.ACTULIZACION, style: .default)
            { (action:UIAlertAction!) in
                
                UIApplication.shared.openURL(NSURL(string: _CONST.URL_APPSTORE)! as URL)
                
            }
            alertController.addAction(aceptarAccion)
            view.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    static func mensajeSugerencia(titulo: String, mensaje:String, view: UIViewController) -> Void {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: titulo, message: mensaje , preferredStyle: .alert)
            
            let mensajeAlert = NSMutableAttributedString()
            mensajeAlert
                .normal(_CONST.MSJ_SUGERENCIA)
                .bold("\n"+_CONST.MSJ_SUGERENCIA_COMPROMISO)
            alertController.setValue(mensajeAlert, forKey: "attributedMessage")

            let aceptarAccion = UIAlertAction(title: _CONST.ACEPTO, style: .default)
            { (action:UIAlertAction!) in
                
                view.performSegue(withIdentifier: "segueTabBar", sender: self)
               
            }
            alertController.addAction(aceptarAccion)
            view.present(alertController, animated: true, completion: nil)
        }
    }
    static func mensajeSimple(titulo: String, mensaje:String, view: UIViewController) -> Void {
        DispatchQueue.main.async {
            let alertControler = UIAlertController(title: titulo, message: mensaje, preferredStyle: .alert)
            let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
            { (action:UIAlertAction!) in
            }
            alertControler.addAction(aceptarAccion)
            view.present(alertControler, animated: true, completion: nil)
        }
    }
    
    static func mensajeSesion(titulo: String, mensaje:String, view: UIViewController)-> Void{
        DispatchQueue.main.async {
        let alertController = UIAlertController(title: titulo, message: mensaje , preferredStyle: .alert)
        let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
        { (action:UIAlertAction!) in
            //let loginViewController = view.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            //view.present(loginViewController, animated: true)
            view.dismiss(animated: true, completion: nil)
            view.performSegue(withIdentifier: "unwindSeguePortal", sender: view)
           
        }
        alertController.addAction(aceptarAccion)
        view.present(alertController, animated: true, completion: nil)
        }
    }
    static func mensajeSesionBimo(titulo: String, mensaje:String, view: UIViewController)-> Void{
           DispatchQueue.main.async {
           let alertController = UIAlertController(title: titulo, message: mensaje , preferredStyle: .alert)
           let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
           { (action:UIAlertAction!) in
               //let loginViewController = view.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
               //view.present(loginViewController, animated: true)
               view.dismiss(animated: true, completion: nil)
               view.performSegue(withIdentifier: "unwindInicioSesion", sender: view)
              
           }
           alertController.addAction(aceptarAccion)
           view.present(alertController, animated: true, completion: nil)
           }
       }
    static func mensajeSesionRecuperarClave(titulo: String, mensaje:String, view: UIViewController)-> Void{
        DispatchQueue.main.async {
        let alertController = UIAlertController(title: titulo, message: mensaje , preferredStyle: .alert)
        let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
        { (action:UIAlertAction!) in
            view.dismiss(animated: true, completion: nil)
            view.performSegue(withIdentifier: "unwindInicioSesion", sender: view)
        //let InicioSesionViewController = view.storyboard?.instantiateViewController(withIdentifier: "InicioSesionViewController") as! InicioSesionViewController
        //view.present(InicioSesionViewController, animated: true)
            
        }
        alertController.addAction(aceptarAccion)
        view.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func descripcionTipoCuentaLocal(nroCuentaLocal: String) -> String {
        let indexStartOfText = nroCuentaLocal.index(nroCuentaLocal.startIndex, offsetBy: _CONST.N_PREF_CNTA_FIN)
        let prefijo = String(nroCuentaLocal[...indexStartOfText])
        switch prefijo {
            case "033":
                return "Certificados de aportacion"
            case "041":
                return "Ahorro programado empleados"
            case "042":
                return "Ahorro representacion"
            case "043":
                return "Ahorro kullki mirak"
            case "046":
                return "Ahorro vista"
            case "049":
                return "Ahorro basico"
            case "047":
                return "Ahorro encaje"
            case "048":
                return "Ahorro plan cesantia"
            default:
                return "NA"
        }
    }
    
    
    
    static func mensajeOTP(titulo: String, mensaje:String, view: UIViewController) -> Void {
        DispatchQueue.main.async {
            let alertControler = UIAlertController(title: titulo, message: mensaje, preferredStyle: .alert)
            let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
            { (action:UIAlertAction!) in
                
            }
            alertControler.addTextField { (textField: UITextField) in
                textField.keyboardAppearance = .dark
                textField.keyboardType = .numberPad
                textField.placeholder = "Ingrese su codigo de seguridad..."
                textField.clearButtonMode = .whileEditing
            }
            alertControler.addAction(aceptarAccion)
            view.present(alertControler, animated: true, completion: nil)
        }
    }
    static func dateFormatMmmDYyyy (fecha: String)-> String{
        let dateString:String = fecha
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy"
        dateFormatter.locale = Locale(identifier: "en_US" )
        let dateObj:Date = dateFormatter.date(from: dateString)!
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.string(from: dateObj)
    }
    
    static func evaluarServicioBasico(pagarServicioBasicoAux: [PagarServiciosBasicosModel]) -> PagarServiciosBasicosModel{
        var aux: PagarServiciosBasicosModel = PagarServiciosBasicosModel()
        for item in pagarServicioBasicoAux{
            aux = item
        }
        return aux
    }
    
    static func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0
            {
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
    static func SoloNumeros(string: String) -> Bool {
        var returnValue = true
        let numRegEx = "^[0-9]*$"
        do {
            let regex = try NSRegularExpression(pattern: numRegEx)
            let nsString = string as NSString
            let results = regex.matches(in: string, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0
            {
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
    static func tieneNumeros(string: String) -> Bool {
        var returnValue = true
        let numRegEx = "[0-9]{1,}"
        do {
            let regex = try NSRegularExpression(pattern: numRegEx)
            let nsString = string as NSString
            let results = regex.matches(in: string, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0
            {
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
    static func tieneMayusculas(string: String) -> Bool {
        var returnValue = true
        let numRegEx = "[A-Z]{1,}"
        do {
            let regex = try NSRegularExpression(pattern: numRegEx)
            let nsString = string as NSString
            let results = regex.matches(in: string, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0
            {
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    static func tieneMinusculas(string: String) -> Bool {
        var returnValue = true
        let numRegEx = "[a-z]{1,}"
        do {
            let regex = try NSRegularExpression(pattern: numRegEx)
            let nsString = string as NSString
            let results = regex.matches(in: string, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0
            {
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    static func tieneCaracteresEspeciales(string: String) -> Bool {
        var returnValue = true
        let numRegEx = ".*[^' '_.ñÑA-Za-z0-9].*"
        do {
            let regex = try NSRegularExpression(pattern: numRegEx)
            let nsString = string as NSString
            let results = regex.matches(in: string, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0
            {
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
    static func validarNumero(textField: UITextField, range:NSRange, string:String) -> Bool {
        guard let oldText = textField.text, let r = Range(range, in: oldText) else {
            return true
        }
        
        let newText = oldText.replacingCharacters(in: r, with: string)
        let isNumeric = newText.isEmpty || (Double(newText) != nil)
        let numberOfDots = newText.components(separatedBy: ".").count - 1
        
        let numberOfDecimalDigits: Int
        if let dotIndex = newText.index(of: ".") {
            numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
        } else {
            numberOfDecimalDigits = 0
        }
        
        return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
    }
    
    static func saludoDadoHora(hora: Int)-> String {
        var mensaje: String = ""
        if(hora < 12){
            mensaje = "Buenos dias"
        }else{
            if(hora >= 12 && hora < 18){
                mensaje = "Buenas tardes"
            }else{
                if(hora >= 18 && hora < 00){
                    mensaje = "Buenas noches"
                }else{
                    mensaje = "Bienvenido"
                }
            }
        }
        return mensaje
    }
    
    static func separarNombre(fullName: String)-> String{
        var nombre: String = ""
        if(fullName.count > 0){
            let fullNameArr = fullName.components(separatedBy: " ")
            
                var count: Int = 0
                for item in fullNameArr {
                    if(item.isNotEmpty){
                        if(count == 2){
                            nombre = item
                        }
                        count += 1
                    }
                }
        }
        return nombre
    }
    
    static func mensajeSugerenciaLoginHuella(titulo: String, mensaje:String, view: UIViewController, usuario:UsuarioModel) -> Void {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: titulo, message: mensaje , preferredStyle: .alert)
            
            let mensajeAlert = NSMutableAttributedString()
            mensajeAlert
                .normal(_CONST.MSJ_SUGERENCIA)
                .bold("\n"+_CONST.MSJ_SUGERENCIA_COMPROMISO)
            alertController.setValue(mensajeAlert, forKey: "attributedMessage")

            let aceptarAccion = UIAlertAction(title: _CONST.ACEPTO, style: .default)
            { (action:UIAlertAction!) in
                let autorizacionHuellaViewController = view.storyboard?.instantiateViewController(withIdentifier: "AutorizacionHuellaViewController") as! AutorizacionHuellaViewController
                
                autorizacionHuellaViewController.usuario=usuario
                view.present(autorizacionHuellaViewController, animated: true)
               
            }
            alertController.addAction(aceptarAccion)
            view.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func tieneEspaciosEnBlanco(string: String) -> Bool {
        var returnValue = true
        let numRegEx = "[:space:]"
        do {
            let regex = try NSRegularExpression(pattern: numRegEx)
            let nsString = string as NSString
            let results = regex.matches(in: string, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0
            {
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
    static func mensajeRedireccionCambioContrasenia(titulo: String, mensaje:String, view: UIViewController, usuario:UsuarioModel, contrasenia:String, nombreUsuario:String) -> Void {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: titulo, message: mensaje , preferredStyle: .alert)
            
            let mensajeAlert = NSMutableAttributedString()
            mensajeAlert
                .normal(mensaje)
            alertController.setValue(mensajeAlert, forKey: "attributedMessage")

            let aceptarAccion = UIAlertAction(title: _CONST.ACEPTO, style: .default)
            { (action:UIAlertAction!) in
                let cambioClaveViewController = view.storyboard?.instantiateViewController(withIdentifier: "CambioClaveViewController") as!  CambioClaveViewController
                cambioClaveViewController.usuario = usuario
                cambioClaveViewController.claveLogin = contrasenia
                cambioClaveViewController.userLogin = nombreUsuario
                
                view.present(cambioClaveViewController, animated: true)
            }
            alertController.addAction(aceptarAccion)
            view.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func mensajeAfiliacionBimo(titulo: String, mensaje:String, view: UIViewController)-> Void{
        DispatchQueue.main.async {
        let alertController = UIAlertController(title: titulo, message: mensaje , preferredStyle: .alert)
        let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
        { (action:UIAlertAction!) in
            view.performSegue(withIdentifier: "unwindOpciones", sender: view)
        }
        alertController.addAction(aceptarAccion)
        view.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func mensajeAccionBimo(titulo: String, mensaje:String, view: UIViewController)-> Void{
        DispatchQueue.main.async {
        let alertController = UIAlertController(title: titulo, message: mensaje , preferredStyle: .alert)
        let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
        { (action:UIAlertAction!) in
           view.performSegue(withIdentifier: "unwindBilleteraMovil", sender: view)
        }
        alertController.addAction(aceptarAccion)
        view.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func mensajeDesafiliarBimo(titulo: String, mensaje:String, origen: String, view: UIViewController)-> Void{
        DispatchQueue.main.async {
        let alertController = UIAlertController(title: titulo, message: mensaje , preferredStyle: .alert)
        let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
        { (action:UIAlertAction!) in
            if origen == _CONST.BIMO_ORIGEN_INTERNO{
                view.performSegue(withIdentifier: "unwindOpciones", sender: view)
            }else{
                view.performSegue(withIdentifier: "unwindSeguePortal", sender: view)
            }
        }
        alertController.addAction(aceptarAccion)
        view.present(alertController, animated: true, completion: nil)
        }
    }
    static func mensajeDefinirClaveBimoExterno(titulo: String, mensaje:String, view: UIViewController)-> Void{
        DispatchQueue.main.async {
        let alertController = UIAlertController(title: titulo, message: mensaje , preferredStyle: .alert)
        let aceptarAccion = UIAlertAction(title: _CONST.ACEPTAR, style: .default)
        { (action:UIAlertAction!) in
                view.performSegue(withIdentifier: "unwindSeguePortal", sender: view)
        }
        alertController.addAction(aceptarAccion)
        view.present(alertController, animated: true, completion: nil)
        }
    }
    
    static func mensajeSimpleUrl(titulo: String, mensaje: String, url: String ,view: UIViewController){
        let mensajeSimpleUrlViewController = view.storyboard?.instantiateViewController(withIdentifier: "MensajeSimpleUrlViewController") as! MensajeSimpleUrlViewController
        
        mensajeSimpleUrlViewController.mensaje = mensaje
        mensajeSimpleUrlViewController.url = url
        view.present(mensajeSimpleUrlViewController, animated: true)
    }
    
    static func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
}

extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedStringKey: Any] = [.font: UIFont(name: "HelveticaNeue-Bold", size: 16)!]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}
