//
//  SliderCell.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 31/08/2019.
//  Copyright © 2019 CoopDaquilema. All rights reserved.
//

import UIKit

class SliderCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var image: UIImage! {
        didSet {
            imageView.image = image
        }
    }
    
    
}
