//
//  _DEMO.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 12/09/2019.
//  Copyright © 2019 CoopDaquilema. All rights reserved.
//

import Foundation

class _DEMO {
    
    static func posicionConsolidad() -> [PosicionConsolidadaModel] {
        var posicionConsolidada = PosicionConsolidadaModel()
        posicionConsolidada.Cuenta = "033000170777"
        posicionConsolidada.Descripcion = "CERTIFICADOS DE APORTACION"
        posicionConsolidada.Producto = "305"
        posicionConsolidada.GProducto = "03"
        posicionConsolidada.Saldoefectivo = "279.00"
        posicionConsolidada.Saldocontable = "334.00"
        posicionConsolidada.Saldomonto = "0.00"
        posicionConsolidada.Saldoretenido = "0.00"
        posicionConsolidada.Saldobloqueado = "0.00"
        posicionConsolidada.Saldoaportacion = "55.00"
        posicionConsolidada.Saldopignorado = "0.00"
        posicionConsolidada.Trelacion = "PRI"
        
        var lisPosicionConsolidadaDemo: [PosicionConsolidadaModel] = []
        lisPosicionConsolidadaDemo.append(posicionConsolidada)
        posicionConsolidada.Cuenta = "041000170777"
        posicionConsolidada.Descripcion = "AHORROS VISTA"
        posicionConsolidada.Producto = "406"
        posicionConsolidada.GProducto = "04"
        posicionConsolidada.Saldoefectivo = "279.00"
        posicionConsolidada.Saldocontable = "334.00"
        posicionConsolidada.Saldomonto = "0.00"
        posicionConsolidada.Saldoretenido = "0.00"
        posicionConsolidada.Saldobloqueado = "0.00"
        posicionConsolidada.Saldoaportacion = "55.00"
        posicionConsolidada.Saldopignorado = "0.00"
        posicionConsolidada.Trelacion = "PRI"
        lisPosicionConsolidadaDemo.append(posicionConsolidada)
        posicionConsolidada.Cuenta = "046000170777"
        posicionConsolidada.Descripcion = "AHORRO PROGRAMADO EMPLEADOS"
        posicionConsolidada.Producto = "412"
        posicionConsolidada.GProducto = "04"
        posicionConsolidada.Saldoefectivo = "279.00"
        posicionConsolidada.Saldocontable = "334.00"
        posicionConsolidada.Saldomonto = "0.00"
        posicionConsolidada.Saldoretenido = "0.00"
        posicionConsolidada.Saldobloqueado = "0.00"
        posicionConsolidada.Saldoaportacion = "55.00"
        posicionConsolidada.Saldopignorado = "0.00"
        posicionConsolidada.Trelacion = "PRI"
        lisPosicionConsolidadaDemo.append(posicionConsolidada)
        
        
        return lisPosicionConsolidadaDemo
    }
    
    static func creditos()-> [CreditoModel]{
        var credito: CreditoModel = CreditoModel()
        credito.ccuenta = "064000000000"
        credito.montoprestamo = "11000"
        credito.descripciongrupoproducto = "LINEA MICROCREDITO"
        credito.descripcionproducto = "MICROCREDITO GENERAL"
        credito.descripcionestatuscuenta = "ACTIVO"
        credito.plazo = "36"
        credito.tasa = "20"
        credito.frecuencias = "MENSUAL"
        credito.fdesembolso = "2018-12-11"
        credito.identificacion = "0600000000"
        credito.nombrelegal = _CONST.NOMBRECOMPLETO_DEMO
        credito.fechaProximoPago = "2019-09-16"
        credito.cuotasVencidas = "0"
        credito.saldoPendiente = "416.75"
        credito.saldoPendienteVigente = "8847.56"
        credito.diasVencidos = 0
        credito.numeroSocio = 0
        credito.totalDeuda = 0
        credito.oficial = 0
        credito.agencia = 0
        
        
        
        
        
        
        
        var lisCreditDemo: [CreditoModel] = []
        lisCreditDemo.append(credito)
        
        return lisCreditDemo
        
    }
    
    static func dpfs()-> [DPFModel]{
        var dpf:DPFModel = DPFModel()
        
        dpf.ccuenta = "059000000000"
        dpf.cgrupoproducto = "01"
        dpf.cproducto = "509"
        dpf.nombrecuenta = _CONST.NOMBRECOMPLETO_DEMO
        dpf.monto = "14000"
        dpf.plazo = "120"
        dpf.tasa = "9.5"
        dpf.fapertura = "2019-08-02"
        dpf.descripcionproducto = "DEPOSITOS A PLAZO FIJO"
        dpf.descripcionestatuscuenta = "VIGENTE"
        dpf.fvencimiento = "2019-11-30"
        dpf.cfrecuencia_interes = "0"
        
        var lisDPFDemo: [DPFModel] = []
        lisDPFDemo.append(dpf)
        return lisDPFDemo
    }
    
    static func tablaPagos() ->[TablaPagoModel]{
        var tablaPagos: TablaPagoModel = TablaPagoModel()
        var lisTablaPagos: [TablaPagoModel] = []
        
        for item in 1...15 {
            tablaPagos.NumeroCouta = "\(item)"
            tablaPagos.FVence = "2019-01-16"
            tablaPagos.Capital = "193.34"
            tablaPagos.Interes = "220"
            tablaPagos.Acciones = ""
            tablaPagos.Seguro = ""
            tablaPagos.Impuestos = ""
            tablaPagos.Recargo = ""
            tablaPagos.Cargos = ""
            tablaPagos.Otros = ""
            tablaPagos.Total = "418.12"
            lisTablaPagos.append(tablaPagos)
        }
        
        
     //[DaquiAppIOS.TablaPagoModel(NumeroCouta: "1", FVence: "2019-01-16", Capital: "193.34", Interes: "220", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "418.12"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "2", FVence: "2019-02-16", Capital: "227.23", Interes: "186.11", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "417.39"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "3", FVence: "2019-03-16", Capital: "248.77", Interes: "164.57", Acciones: "", Seguro: "", Impuestos: "", Recargo: "0.09", Cargos: "", Otros: "", Total: "417.01"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "4", FVence: "2019-04-16", Capital: "235.42", Interes: "177.92", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "417.21"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "5", FVence: "2019-05-16", Capital: "245.09", Interes: "168.25", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "417"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "6", FVence: "2019-06-16", Capital: "243.7", Interes: "169.64", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "417.03"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "7", FVence: "2019-07-16", Capital: "253.23", Interes: "160.11", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "416.82"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "8", FVence: "2019-08-16", Capital: "252.26", Interes: "161.08", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "416.84"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "9", FVence: "2019-09-16", Capital: "256.6", Interes: "156.74", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "416.75"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "10", FVence: "2019-10-16", Capital: "265.93", Interes: "147.41", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "416.54"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "11", FVence: "2019-11-16", Capital: "265.6", Interes: "147.74", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "416.55"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "12", FVence: "2019-12-16", Capital: "274.79", Interes: "138.55", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "416.35"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "13", FVence: "2020-01-16", Capital: "274.91", Interes: "138.43", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "416.35"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "14", FVence: "2020-02-16", Capital: "279.64", Interes: "133.7", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "416.25"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "15", FVence: "2020-03-16", Capital: "292.77", Interes: "120.57", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "415.96"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "16", FVence: "2020-04-16", Capital: "289.5", Interes: "123.84", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "416.03"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "17", FVence: "2020-05-16", Capital: "298.32", Interes: "115.02", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "415.84"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "18", FVence: "2020-06-16", Capital: "299.62", Interes: "113.72", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "415.81"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "19", FVence: "2020-07-16", Capital: "308.29", Interes: "105.05", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "415.62"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "20", FVence: "2020-08-16", Capital: "310.09", Interes: "103.25", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "415.58"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "21", FVence: "2020-09-16", Capital: "315.43", Interes: "97.91", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "415.47"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "22", FVence: "2020-10-16", Capital: "323.85", Interes: "89.49", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "415.29"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "23", FVence: "2020-11-16", Capital: "326.44", Interes: "86.9", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "415.23"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "24", FVence: "2020-12-16", Capital: "334.69", Interes: "78.65", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "415.05"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "25", FVence: "2021-01-16", Capital: "337.83", Interes: "75.51", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "414.98"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "26", FVence: "2021-02-16", Capital: "343.65", Interes: "69.69", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "414.86"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "27", FVence: "2021-03-16", Capital: "355.74", Interes: "57.6", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "414.59"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "28", FVence: "2021-04-16", Capital: "355.69", Interes: "57.65", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "414.59"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "29", FVence: "2021-05-16", Capital: "363.48", Interes: "49.86", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "414.42"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "30", FVence: "2021-06-16", Capital: "368.08", Interes: "45.26", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "414.32"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "31", FVence: "2021-07-16", Capital: "375.67", Interes: "37.67", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "414.16"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "32", FVence: "2021-08-16", Capital: "380.89", Interes: "32.45", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "414.05"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "33", FVence: "2021-09-16", Capital: "387.45", Interes: "25.89", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "413.9"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "34", FVence: "2021-10-16", Capital: "394.74", Interes: "18.6", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "413.74"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "35", FVence: "2021-11-16", Capital: "400.92", Interes: "12.42", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "413.61"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "36", FVence: "2021-12-16", Capital: "320.35", Interes: "5.34", Acciones: "", Seguro: "", Impuestos: "", Recargo: "", Cargos: "", Otros: "", Total: "325.81")]
        return lisTablaPagos
    }
    
    static func pagosRealizados()->[TablaPagoModel]{
        var tablaPagos: TablaPagoModel = TablaPagoModel()
        var lisTablaPagos: [TablaPagoModel] = []
        
        for item in 1...8 {
            tablaPagos.NumeroCouta = "\(item)"
            tablaPagos.FVence = "2019-01-16"
            tablaPagos.Capital = "193.34"
            tablaPagos.Interes = "220"
            tablaPagos.Acciones = ""
            tablaPagos.Seguro = ""
            tablaPagos.Impuestos = ""
            tablaPagos.Recargo = ""
            tablaPagos.Cargos = ""
            tablaPagos.Otros = ""
            tablaPagos.Total = "418.12"
            lisTablaPagos.append(tablaPagos)
        }
        return lisTablaPagos
        //[DaquiAppIOS.TablaPagoModel(NumeroCouta: "1", FVence: "2019-01-16", Capital: "193.34", Interes: "220", Acciones: "0.0", Seguro: "0.0", Impuestos: "0.0", Recargo: "", Cargos: "0.0", Otros: "0.0", Total: "418.12"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "2", FVence: "2019-02-16", Capital: "227.23", Interes: "186.11", Acciones: "0.0", Seguro: "0.0", Impuestos: "0.0", Recargo: "", Cargos: "0.0", Otros: "0.0", Total: "417.39"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "3", FVence: "2019-03-16", Capital: "248.77", Interes: "164.57", Acciones: "0.0", Seguro: "0.0", Impuestos: "0.0", Recargo: "0.09", Cargos: "0.0", Otros: "0.0", Total: "417.01"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "4", FVence: "2019-04-16", Capital: "235.42", Interes: "177.92", Acciones: "0.0", Seguro: "0.0", Impuestos: "0.0", Recargo: "", Cargos: "0.0", Otros: "0.0", Total: "417.21"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "5", FVence: "2019-05-16", Capital: "245.09", Interes: "168.25", Acciones: "0.0", Seguro: "0.0", Impuestos: "0.0", Recargo: "", Cargos: "0.0", Otros: "0.0", Total: "417"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "6", FVence: "2019-06-16", Capital: "243.7", Interes: "169.64", Acciones: "0.0", Seguro: "0.0", Impuestos: "0.0", Recargo: "", Cargos: "0.0", Otros: "0.0", Total: "417.03"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "7", FVence: "2019-07-16", Capital: "253.23", Interes: "160.11", Acciones: "0.0", Seguro: "0.0", Impuestos: "0.0", Recargo: "", Cargos: "0.0", Otros: "0.0", Total: "416.82"), DaquiAppIOS.TablaPagoModel(NumeroCouta: "8", FVence: "2019-08-16", Capital: "252.26", Interes: "161.08", Acciones: "0.0", Seguro: "0.0", Impuestos: "0.0", Recargo: "", Cargos: "0.0", Otros: "0.0", Total: "416.84")]
    }
    
}
