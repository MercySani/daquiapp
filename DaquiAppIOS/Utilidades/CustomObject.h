//
//  CustomObject.h
//  DaquiAppIOS
//
//  Created by Desarrollador on 2/10/18.
//  Copyright © 2018 CoopDaquilema. All rights reserved.
//

#ifndef CustomObject_h
#define CustomObject_h

#import <Foundation/Foundation.h>

@interface CustomObject : NSObject

@property (strong, nonatomic) id someProperty;

- (void) someMethod;

@end
#endif /* CustomObject_h */
