//
//  GenericoTableViewCell.swift
//  DaquiApp
//
//  Created by Desarrollador on 26/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import UIKit

class GenericoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var descripcion: UITextView!
    @IBOutlet weak var detalle1: UILabel!
    @IBOutlet weak var detalle2: UILabel!
    @IBOutlet weak var detalle3: UILabel!
    @IBOutlet weak var detalle4: UILabel!
    @IBOutlet weak var detalle5: UILabel!
    @IBOutlet weak var detalle6: UILabel!
    @IBOutlet weak var detalle7: UILabel!
    @IBOutlet weak var detalle8: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
