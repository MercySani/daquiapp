//
//  CustomObject.m
//  DaquiAppIOS
//
//  Created by Desarrollador on 2/10/18.
//  Copyright © 2018 CoopDaquilema. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomObject.h"

@implementation CustomObject

- (void) someMethod {
    NSLog(@"SomeMethod Ran");
}

@end
