//
//  Identificador.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 28/12/18.
//  Copyright © 2018 CoopDaquilema. All rights reserved.
//


import Foundation
import UIKit

class Identificador {
    
    
    static let  RUC_JURIDICOS_Y_EXTRANJEROS: String = "9"
    static let  RUC_PUBLICOS:String = "6"
    static let  MODULO_10: Int = 10
    static let  MODULO_11: Int = 11
    static let COEFICIENTES_JURIDICOS_Y_EXTRANJEROS: [Int] = [4, 3, 2, 7, 6, 5, 4, 3, 2]
    static let COEFICIENTES_PUBLICOS: [Int] = [3, 2, 7, 6, 5, 4, 3, 2]
    static let COEFICIENTES_NATURALES: [Int] = [2, 1, 2, 1, 2, 1, 2, 1, 2]
    
    static func validar(identificador: String) -> Bool {
        do{
            if(identificador == "9999999999999"){
                return true
            }
            
            if(identificador.count==10){
                return validarRUCNaturales(cedula: identificador)
            }
            if(identificador.count>=10){
                let subString = identificador.index(identificador.startIndex, offsetBy: 10)
                let auxStr = String(identificador[subString...])
                let auxInt: Int = Int(auxStr)!
                
                if ((identificador.count==13) && (auxInt>0))
                {
                    let subStringEnd = identificador.index(identificador.endIndex, offsetBy: -3)
                    let auxIdentificador = identificador[..<subStringEnd]
                    
                    return validarRUC(RUC: String(auxIdentificador))
                }
            }
            
            
        return false
        }catch{
            return false
        }
    }
        
    
        /**
         *
         * @param RUC, size=13
         * @return
         */
    static func validarRUC(RUC: String) -> Bool {
        let pasoValidacionNaturales: Bool = validarRUCNaturales(cedula: RUC)
        if(!pasoValidacionNaturales){
            
            let template = RUC
            let indexStartOfText = template.index(template.startIndex, offsetBy: 2)
            let indexEndOfText = template.index(template.endIndex, offsetBy: -7)
            let auxIdentificador = template[indexStartOfText..<indexEndOfText]
            
            
            if(String(auxIdentificador) == RUC_JURIDICOS_Y_EXTRANJEROS){
                return validarRUCJuridicosExtranjeros(cadenaValidacion: RUC)
            }else{
                let auxRuc = RUC
                let indexStartOfText = auxRuc.index(auxRuc.startIndex, offsetBy: 2)
                let indexEndOfText = auxRuc.index(auxRuc.endIndex, offsetBy: -7)
                let auxIdentificador = auxRuc[indexStartOfText..<indexEndOfText]
                if(String(auxIdentificador) == RUC_PUBLICOS){
                    return validarRUCPublicos(cadenaValidacion: RUC)
                }else{
                    return validarRUCNaturales(cedula: RUC)
                }
            }
            
        }else{
            return pasoValidacionNaturales
        }
        
    }
    
        
        /**
         *
         * @param cedula, size=10
         * @return
         */
    static func validarRUCNaturales(cedula:String)-> Bool{
        let auxCedula = cedula
        
        let indexStartOfText = auxCedula.index(auxCedula.startIndex, offsetBy: 0)
        let indexEndOfText = auxCedula.index(auxCedula.endIndex, offsetBy: -1)
        let auxIdentificador = auxCedula[indexStartOfText..<indexEndOfText]
        
        let indexStartOfText1 = auxCedula.index(auxCedula.startIndex, offsetBy: 9)
        let auxStarTe = auxCedula[indexStartOfText1...]
        let auxStarInt : Int = Int(String(auxStarTe))!
        let auxValor = getDigitoVerificador(coeficientes: COEFICIENTES_NATURALES, digitos: String( auxIdentificador), modulo: MODULO_10, naturales: true)
        return ( auxValor == auxStarInt)
        }
        
    static func  validarRUCJuridicosExtranjeros(cadenaValidacion:String ) -> Bool {
        let auxCedula = cadenaValidacion
        let indexStartOfText = auxCedula.index(auxCedula.startIndex, offsetBy: 0)
        let indexEndOfText = auxCedula.index(auxCedula.endIndex, offsetBy: -1)
        let auxIdentificador = auxCedula[indexStartOfText..<indexEndOfText]
        
        let indexStartOfTextAux = cadenaValidacion.index(cadenaValidacion.startIndex, offsetBy: 9)
        
        let auxStarTe = auxCedula[indexStartOfTextAux...]
        let auxStarInt : Int = Int(String(auxStarTe))!
        return (getDigitoVerificador(coeficientes: COEFICIENTES_JURIDICOS_Y_EXTRANJEROS, digitos: String(auxIdentificador), modulo: MODULO_11, naturales: false) == auxStarInt)
        }
        
    static func  validarRUCPublicos( cadenaValidacion:String) -> Bool {
        let auxCedula = cadenaValidacion
        let indexStartOfText = auxCedula.index(auxCedula.startIndex, offsetBy: 0)
        let indexEndOfText = auxCedula.index(auxCedula.endIndex, offsetBy: -2)
        let auxIdentificador = auxCedula[indexStartOfText..<indexEndOfText]
        
        let auxIndentificadion = cadenaValidacion
        let aindexStartOfText = auxIndentificadion.index(auxIndentificadion.startIndex, offsetBy: 8)
        let aindexEndOfText = auxIndentificadion.index(auxIndentificadion.endIndex, offsetBy: -1)
        let auxIdentifi = auxIndentificadion[aindexStartOfText..<aindexEndOfText]
        let auxStarInt : Int = Int(String(auxIdentifi))!
        
        return (getDigitoVerificador(coeficientes: COEFICIENTES_PUBLICOS, digitos: String(auxIdentificador), modulo: MODULO_11, naturales: false) == auxStarInt)
        }
        
    static func getDigitoVerificador(coeficientes:[Int], digitos:String,  modulo: Int,  naturales: Bool)-> Int{
        var acumulador : Int = 0
        var posicionCoeficiente : Int = 0
        let characters = Array(digitos)
        for auxDigit  in characters {
            let digit:Int = Int(String(auxDigit))!
            var productoDigitoCoeficiente: Int = digit * coeficientes[posicionCoeficiente]
            posicionCoeficiente = posicionCoeficiente + 1
            
            if(naturales){
                if(productoDigitoCoeficiente > 9){
                    productoDigitoCoeficiente = (productoDigitoCoeficiente - 9)
                }
            }
            
            acumulador = acumulador + productoDigitoCoeficiente
            
            
        }
        var auxDiv = 0
        if((acumulador % modulo)==0){
           auxDiv = modulo
        }else{
           auxDiv = acumulador%modulo
        }
        
        return (modulo - auxDiv)
    }
        
    
}
