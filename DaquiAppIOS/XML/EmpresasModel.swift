//
//  EmpresasModel.swift
//  DaquiApp
//
//  Created by Fredy on 6/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation


struct EmpresasModel {
    var empresaGeneral: String = ""
    var codigo: String = ""
    var empresa: String = ""
    var nombre: String = ""
    var alias: String = ""
    var categoria:String = ""
}
