//
//  IfisModel.swift
//  DaquiApp
//
//  Created by Fredy on 25/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
struct IfisModel {
    var ifi: String = ""
    var cuenta: String = ""
    var descripcion: String = ""
    var idifi: String = ""
}
