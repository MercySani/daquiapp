//
//  EmpresasData.swift
//  DaquiApp
//
//  Created by Fredy on 8/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
import UIKit

class EmpresasData: UIViewController, XMLParserDelegate {
    //variables XML
    
    var xmlDataSource = [EmpresasModel]()
    var tipoServicioName = ""
    var codigo: String = ""
    var empresa: String = ""
    var nombre: String = ""
    var alias: String = ""
    var categoria:String = ""
    
    //Fin XML
    
    //XML parser delegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let path = Bundle.main.url(forResource: "empresas", withExtension: "xml"){
            if let parser = XMLParser(contentsOf: path){
                parser.delegate = self
                parser.parse()
            }
        }
    }
    
    func inicializar () -> [EmpresasModel] {
        if let path = Bundle.main.url(forResource: "empresas", withExtension: "xml"){
            if let parser = XMLParser(contentsOf: path){
                parser.delegate = self
                parser.parse()
            }
        }
        return self.xmlDataSource
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        self.tipoServicioName = elementName
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if data.count != 0 {
            switch tipoServicioName {
            case  "codigo":
                self.codigo = data
                break
            case "empresa":
                self.empresa = data
                break
            case "nombre":
                self.nombre = data
                break
            case "alias":
                self.alias = data
                break
            case "categoria":
                self.categoria = data
                break
            default:
                break
            }
        }
    }
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        var empresa = EmpresasModel()
        switch elementName {
        case "telefono":
            empresa.empresaGeneral = elementName
            empresa.codigo = self.codigo
            empresa.alias = self.alias
            empresa.categoria = self.categoria
            empresa.empresa = self.empresa
            empresa.nombre = self.nombre
            xmlDataSource.append(empresa)
            break
        case "transito":
            empresa.empresaGeneral = elementName
            empresa.codigo = self.codigo
            empresa.alias = self.alias
            empresa.categoria = self.categoria
            empresa.empresa = self.empresa
            empresa.nombre = self.nombre
            xmlDataSource.append(empresa)
            break
        case "agua":
            empresa.empresaGeneral = elementName
            empresa.codigo = self.codigo
            empresa.alias = self.alias
            empresa.categoria = self.categoria
            empresa.empresa = self.empresa
            empresa.nombre = self.nombre
            xmlDataSource.append(empresa)
            break
        case "luz":
            empresa.empresaGeneral = elementName
            empresa.codigo = self.codigo
            empresa.alias = self.alias
            empresa.categoria = self.categoria
            empresa.empresa = self.empresa
            empresa.nombre = self.nombre
            xmlDataSource.append(empresa)
            break
        case "recargas":
            empresa.empresaGeneral = elementName
            empresa.codigo = self.codigo
            empresa.alias = self.alias
            empresa.categoria = self.categoria
            empresa.empresa = self.empresa
            empresa.nombre = self.nombre
            xmlDataSource.append(empresa)
            break
        case "plan":
            empresa.empresaGeneral = elementName
            empresa.codigo = self.codigo
            empresa.alias = self.alias
            empresa.categoria = self.categoria
            empresa.empresa = self.empresa
            empresa.nombre = self.nombre
            xmlDataSource.append(empresa)
            break
        case "sriM":
            empresa.empresaGeneral = elementName
            empresa.codigo = self.codigo
            empresa.alias = self.alias
            empresa.categoria = self.categoria
            empresa.empresa = self.empresa
            empresa.nombre = self.nombre
            xmlDataSource.append(empresa)
            break
        case "sriT":
            empresa.empresaGeneral = elementName
            empresa.codigo = self.codigo
            empresa.alias = self.alias
            empresa.categoria = self.categoria
            empresa.empresa = self.empresa
            empresa.nombre = self.nombre
            xmlDataSource.append(empresa)
            break
        case "sri":
            empresa.empresaGeneral = elementName
            empresa.codigo = self.codigo
            empresa.alias = self.alias
            empresa.categoria = self.categoria
            empresa.empresa = self.empresa
            empresa.nombre = self.nombre
            xmlDataSource.append(empresa)
            break
        case "PACIFICO_CNELGYELR":
            empresa.empresaGeneral = elementName
            empresa.codigo = self.codigo
            empresa.alias = self.alias
            empresa.categoria = self.categoria
            empresa.empresa = self.empresa
            empresa.nombre = self.nombre
            xmlDataSource.append(empresa)
            break
        case "PACIFICO_CNTOL":
            empresa.empresaGeneral = elementName
            empresa.codigo = self.codigo
            empresa.alias = self.alias
            empresa.categoria = self.categoria
            empresa.empresa = self.empresa
            empresa.nombre = self.nombre
            xmlDataSource.append(empresa)
            break
        default:
            break
        }
    }
    
    
    //fin XML
    
}
