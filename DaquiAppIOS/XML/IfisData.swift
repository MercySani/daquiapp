//
//  IfisData.swift
//  DaquiApp
//
//  Created by Fredy on 25/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
import UIKit

class IfisData:  UIViewController, XMLParserDelegate {
    
    //variables XML
    var xmlDataSource = [IfisModel]()
    var ifi: String = ""
    var cuenta: String = ""
    var descripcion: String = ""
    var idIfi: String = ""
   
    
    //Fin XML
 
    
    func inicializar () -> [IfisModel] {
        if let path = Bundle.main.url(forResource: "IFIS_XML", withExtension: "xml"){
            if let parser = XMLParser(contentsOf: path){
                parser.delegate = self
                parser.parse()
            }
        }
        return self.xmlDataSource
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        self.ifi = elementName
        
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if data.count != 0 {
            switch ifi {
            case  "cuenta":
                self.cuenta = data
                break
            case "descripcion":
                self.descripcion = data
                break
            case "idifi":
                self.idIfi = data
                break
            default:
                break
            }
        }
    }
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        var ifi = IfisModel()
        switch elementName {
        case "ifi":
            ifi.ifi = elementName
            ifi.cuenta = self.cuenta
            ifi.descripcion = self.descripcion
            ifi.idifi = self.idIfi
            xmlDataSource.append(ifi)
            break
        default:
            break
        }
    }
    
}
