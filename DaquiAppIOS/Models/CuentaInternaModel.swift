//
//  CuentasInternasModel.swift
//  DaquiApp
//
//  Created by Fredy on 16/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
struct CuentaInternaModel: Decodable {
   var idUsuario: String = ""
    var numCuenta: String = ""
    var identificacion: String = ""
    var nombre: String = ""
    var email: String = ""
    var comentario: String = ""
    var estado: Bool = false
    var tipoIdentificacion: String = ""
    var socio: String? = nil
    var fechaRegistro: String = ""
    var cpersona: Int = 0
}

