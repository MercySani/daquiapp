//
//  CuentasExternasModel.swift
//  DaquiApp
//
//  Created by Fredy on 16/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation

struct UsuarioCE: Decodable {
    var idUsuario: String = ""
    var identificacion: String = ""
    var usuario: String = ""
    var clave: String = ""
    var nombreCompleto: String = ""
    var email: String = ""
    var celular: String = ""
    var canal: String = ""
    var sucursal: String = ""
    var tipoUsuario: TipoUsuario=TipoUsuario()
    var nickname: String = ""
    var estado: Estado=Estado()
    var intentos: Int = 0
}

struct IfiCE:Decodable {
    var idifi: Int = 0
    var cuentaBCE: String = ""
    var descripcion: String = ""
}

struct TipocuentaCE:Decodable {
    var idtipocuenta: String = ""
    var descripcion: String = ""
}

struct TipoidentificacionCE: Decodable {
    var idtipoiden: String = ""
    var descripcion: String = ""
}

struct Estado: Decodable{
    var estado: Bool=false
    var id: Int=0
}

struct TipoUsuario: Decodable{
    var tipoUsuario: String=""
}

struct CuentaExternaModel: Decodable{
    var usuarioCE: UsuarioCE = UsuarioCE()
    var numerocuenta: String = ""
    var identificacion: String = ""
    var nombre: String = ""
    var email: String = ""
    var comentario: String = ""
    var ifiCE: IfiCE = IfiCE()
    var tipocuentaCE: TipocuentaCE = TipocuentaCE()
    var tipoidentificacionCE: TipoidentificacionCE = TipoidentificacionCE()
    var fecharegistro: String = ""
    var estado: Bool = false
}
