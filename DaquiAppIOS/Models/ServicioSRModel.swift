//
//  ServicioSRModel.swift
//  DaquiApp
//
//  Created by Fredy on 13/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
import UIKit

//este struct es para lista de categoria y servicio

struct tipoEmpresaSR:Decodable {
    var estado: Int = 0
    var id: Int = 0
   
}
struct  empresaProveedoraI: Decodable {
    var id: Int = 0
    var idPersona: Int = 0
    var idEmpresaProveedora: Int = 0
    var tipoEmpresa: tipoEmpresaSR = tipoEmpresaSR ()
    var descripcion: String = ""
    var nombreClaseServicios: String = ""
    var codigoOk: String = ""
    var formatoData: String = ""
    var observacion: String = ""
    var nombreReporte: String = ""
    var estado: Int = 0
    
}
struct SWC_Valor: Decodable {
    var valor: String = ""
}

struct ServicioSRModel: Decodable {
    var id: String = ""
    var empresaProveedora: empresaProveedoraI = empresaProveedoraI()
    var idFrecuencia: Int = 0
    var idCategoria: Int = 0
    var proveedor: String = ""
    var producto: String = ""
    var nombre: String = ""
    var idauxiliar: String = ""
    var tipotrxconsulta: Int = 0
    var tipotrxpago: Int = 0
    var tipotrxreverso: Int = 0
    var tipotrxaux: Int = 0
    var comisionxrubro: String = ""
    var tituloreferencia: String = ""
    var tipodatoreferencia: String = ""
    var longitudreferencia: Int = 0
    var tipopago: String = ""
    var consultaauxiliar: String = ""
    var infoadd: String = ""
    var tipocontrol: String = ""
    var valoresrecarga: [SWC_Valor]  = []
    var valoresrecarga_str: String = ""
    var observacion: String = ""
    //var paraPagoProgramado: Bool
    var estado: Int = 0
    var fCreacion:String = ""
    var fActualizacion: String = ""
    var usuarioCreacion: String = ""
    var usuarioActualizacion: String = ""
    
}
