//
//  ConsultaServicioFacilito.swift
//  DaquiApp
//
//  Created by Fredy on 15/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
//Este struc es para la consulta de servicios basicos

struct TipoEmpresaS: Codable {
   var estado:Int = 0
    var id: Int = 0
    
}

struct Etiquetas: Codable {
    var id: Int = 0
    var idProducto: String = ""
    var nombre: String = ""
    var css: String = ""
    var identificador: Bool = false
    var idAuxiliar: Bool = false
    var descripcion: Bool = false
    var valor: Bool = false
}
struct EmpresaProveedoraS: Codable {

    var id: Int = 0
    var idPersona: Int = 0
    var idEmpresaProveedora: Int = 0
    var tipoEmpresa: TipoEmpresaS = TipoEmpresaS ()
    var descripcion: String = ""
    var nombreClaseServicios: String = ""
    var codigoOk: String = ""
    var formatoData: String = ""
    var observacion: String = ""
    var nombreReporte: String = ""
   // var etiquetas: [Etiquetas] = []
    var estado: Int = 0
    
}

struct Servicios: Codable {

    var id: String = ""
    //var empresaProveedora: EmpresaProveedoraS = EmpresaProveedoraS()
    var idFrecuencia: Int = 0
    var idCategoria: Int = 0
    var proveedor: String = ""
    var producto: String = ""
    var nombre: String = ""
    var idauxiliar: String = ""
    var tipotrxconsulta: Int = 0
    var tipotrxpago: Int = 0
    var tipotrxreverso: Int = 0
    var tipotrxaux: Int = 0
    var comisionxrubro: String = ""
    var tituloreferencia: String = ""
    var tipodatoreferencia: String = ""
    var longitudreferencia: Int = 0
    var tipopago: String = ""
    var consultaauxiliar: String = ""
    var infoadd: String = ""
    var tipocontrol: String = ""
    //var valoresrecarga: String = [] datos de la lista desconocidos por eso se comenta
    var valoresrecarga_str: String = ""
    var observacion: String = ""
    //var paraPagoProgramado: Bool = false
    var estado: Int = 0
    var fCreacion: String = ""
    var fActualizacion: String = ""
    var usuarioCreacion: String = ""
    var usuarioActualizacion: String = ""
    
}

struct Rubro:Codable {
    var id: Int = 0
    var idProducto: Int = 0
    var idEmpresaProveedora: Int = 0
    var descripcion: String = ""
    var informacionAdicional: String = ""
    var esComision: Bool = false
    var esSujetoIva: Bool = false
    var valorBase: Float = 0//var valorBase: Double = 0
    var estado: Int = 0
    var fCreacion: String = ""
    var fActualizacion: String = ""
    var usuarioCreacion: String = ""
    var usuarioActualizacion: String = ""
 
}

struct TipoAfectacion: Codable {
    var id: Int = 0
    var estado: Int = 0
    var descripcion: String = ""
    var nombreMetodo: String = ""
    var cSubSistema: String = ""
    var cTransaccion: String = ""
}

struct  RubroTipoAfectacion: Codable {
    var id: Int = 0
    var descripcion: String = ""
    var rubro: Rubro = Rubro()
    var tipoAfectacion: TipoAfectacion = TipoAfectacion ()
    var codigoGenerico: String = ""
    var estado: Int = 0
    var fCreacion: String = ""
    var fActualizacion: String = ""
    var usuarioCreacion: String = ""
    var usuarioActualizacion: String = ""
    
}

struct AfectacionDetalleRecibo: Codable {
    var id: Int = 0
    var idDetalleRecibo: Int = 0
    var rubroTipoAfectacion: RubroTipoAfectacion = RubroTipoAfectacion()
    var fCreacion: String = ""
    var fActualizacion: String = ""
    
}

struct EtiquetasLista: Codable {
    var clave: String = ""
    var valor: String = ""
    var css: String = ""
}

struct ListaValoresPendientes: Codable {
    var id: Int = 0
    var idRecibo: Int = 0
    var cantidad: Int = 0
    var codigo: String = ""
    var codigoAuxiliar: String? = nil // este codigo preguntar si es opcional o no porque en algunas esta en otras no ..
    var descripcion: String = ""
    var valorUnitario: Double = 0
    var afectacionDetalleRecibo: AfectacionDetalleRecibo = AfectacionDetalleRecibo()
    var estado: Int = 0
    var etiquetas: [EtiquetasLista] = []
    var fCreacion: String = ""
    var fActualizacion: String = ""
  
}

struct ConsultaServicioFacilitoModel: Codable  {
    var id: Int = 0
    var servicio: Servicios = Servicios()
    var idTipoSolicitud: Int = 0
    var idEstadoSolicitud: Int = 0
    //var idCanal: Int = 0
    var idEstadoProcesoConciliacion: Int = 0
    var idPagoProgramado: Int = 0
    //var canalInterno
    var traceSW: String = ""
    var tokenData: String = ""
    var abaAdquirente: Int = 0
    var codTerminal: String = ""
    var sbdPassword: String = ""
    var sbdUsuario: String = ""
    var codTransaccion: Int = 0
    var fechaTransaccion: String = ""
    var horaTransaccion: String = ""
    var impuestos: Int = 0
    var indicadorReverso: Int = 0
    var moneda: Int = 0
    var nombres: String = ""
    var operador: Int = 0
    var producto: String = ""
    var referencia: String = ""
    var secuenciaAdquirente: Int = 0
    var secuenciaSwitch: Int = 0
    var valorPagado: Double = 0
    var valorSubTotal: Double = 0
    var valorTotal: Double = 0
    var data: String = ""
    var identificacion: String = ""
    var codResultado: String = ""
    var comision: Double = 0
    var direccion: String = ""
    var fechaCompensacion: String = ""
    var mensaje: String = ""
    var estadoConciliacion: String = ""
    var comisionConciliacion: Double = 0
    var ivaConciliacion: Double =  0
    var lisValoresPendientes: [ListaValoresPendientes] = []
    var fCreacion: String = ""
    var fActualizacion: String = ""
    var usuarioCreacion: String = ""
    var usuarioActualizacion: String = ""
}

