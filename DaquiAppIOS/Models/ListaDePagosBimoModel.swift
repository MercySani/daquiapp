//
//  ListaDePagosBimoModel.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 16/05/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import Foundation

struct ListaDePagosBimoModel:Decodable {
    var phoneOrig: String = ""
    var nameOrig: String = ""
    var phoneDst: String = ""
    var nameDst: String = ""
    var reference: String = ""
    var accountDst: String = ""
    var endToEnd: String = ""
    var estado: String = ""
    var criterio: String = ""
    var fecha: String = ""
    var date: DateBimo
    var amount: Amount
}

struct DateBimo:Decodable {
    var year: Int
    var month: Int
    var day: Int
    var timezone: Int
    var hour: Int
    var minute: Int
    var second: Int
}

struct Amount:Decodable {
    var value: Float
    var ccy: String
}
