//
//  MensajeHabiliarUsuarioModel.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 16/12/2019.
//  Copyright © 2019 CoopDaquilema. All rights reserved.
//

import Foundation
struct MensajeHabilitarUsuarioModel:Decodable {
    var strCodigoError: String = ""
    var strMensajeError: String = ""
    var strEmail: String = ""
    var strInsertarEmail: String = ""
}
