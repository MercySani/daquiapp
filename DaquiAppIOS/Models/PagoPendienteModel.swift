//
//  PagoPendienteModel.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 08/05/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import Foundation
struct PagoPendienteModel:Decodable {
    var resultadoDate: ResultadoDate
    var phoneOrig: String = ""
    var nameOrig: String = ""
    var phoneDst: String = ""
    var nameDst: String = ""
    var ResultadoAmout: ResultadoAmout
    var reference: String = ""
    var accountDst: String = ""
    var endToEnd: String = ""
}


struct ResultadoDate:Decodable {
    var year: Int
    var month: Int
    var day: Int
    var timezome: Int
    var hour: Int
    var minute: Int
    var second: Int
}

struct ResultadoAmout:Decodable {
    var value: Float
    var ccy: String
}
