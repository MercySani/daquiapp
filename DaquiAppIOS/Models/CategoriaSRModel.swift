//
//  CategoriaSRModel.swift
//  DaquiApp
//
//  Created by Fredy on 12/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
struct CategoriaSRModel: Decodable {
    var id: Int = 0
    var descripcion: String = ""
}
