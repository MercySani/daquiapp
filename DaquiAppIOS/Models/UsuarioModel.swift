//
//  UsuarioModel.swift
//  pasarDatos
//
//  Created by fjaneta on 14/5/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation

struct UsuarioModel: Decodable {
    var strRol: String = ""
    var strIdentificacion: String = ""
    var strNombreCompleto: String = ""
    var strEMail: String = ""
    var strSucursal: String = ""
    var strNombreUser: String = ""
    var strTelefono: String = ""
    var strMensajeError: String = ""
    var strCodigoError: String = ""
    var strToken: String=""
    var strUserEstado: Int=0
    var strVerificarExistenciaUCSOrigen: Bool = false
    var strBilleteraMovil: String=""
}
