//
//  TablaPagosModel.swift
//  DaquiApp
//
//  Created by Fredy on 17/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
struct TablaPagoModel: Decodable {
    var NumeroCouta: String = ""
    var FVence: String = ""
    var Capital: String = ""
    var Interes: String = ""
    var Acciones: String = ""
    var Seguro: String = ""
    var Impuestos: String = ""
    var Recargo: String = ""
    var Cargos: String = ""
    var Otros: String = ""
    var Total: String = ""
}
