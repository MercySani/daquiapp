//
//  BimoXmlModel.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 12/05/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//
import SwiftyXMLParser
import Foundation

struct BimoXmlModel:Decodable {
    var canalInterno: String = ""
    var usuario: String = ""
    var ip: String = ""
    var trama: String = ""
    var parametroTelefonoOrigen: String = ""
    var parametroTelefonoDestino: String = ""
    var parametroDescripcion: String = ""
    var parametroMonto: String = ""
    var parametroAccion: String = ""
    var parametroFiltro: String = ""
    var parametroCriterio: String = ""
    var parametroIdentificacion: String = ""
    var parametroNumeroCuenta: String = ""
    var codResultado: String = ""
    var mensajeResultado: String = ""
    var codResultadoAdicional: String = ""
    var idTransaccion: String = ""
    var endToEnd: String = ""
}

struct BimoResultado:Decodable {
    var strCodigo: String
    var strMensaje: String
    var strXML: String
}
