//
//  OtpMensajeModel.swift
//  DaquiApp
//
//  Created by Fredy on 18/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
struct OTPMensajeModel: Decodable {
    var strCodigoError: Int = 0
    var strMensajeError: String = ""
}
