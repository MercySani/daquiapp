//
//  MensajeModel.swift
//  DaquiApp
//
//  Created by Fredy on 16/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
struct MensajeModel:Decodable {
    var strCodigoError: String = ""
    var strMensajeError: String = ""
    var strValorResultado: String = ""
}
