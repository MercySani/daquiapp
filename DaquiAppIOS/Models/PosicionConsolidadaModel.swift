//
//  PosicionConsolidadaModel.swift
//  DaquiApp
//
//  Created by Fredy on 15/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation

struct PosicionConsolidadaModel: Decodable {
    var Cuenta: String = ""
    var Descripcion: String = ""
    var Producto: String = ""
    var GProducto: String = ""
    var Saldoefectivo: String = ""
    var Saldocontable: String = ""
    var Saldomonto: String = ""
    var Saldoretenido: String = ""
    var Saldobloqueado: String = ""
    var Saldoaportacion: String = ""
    var Saldopignorado: String = ""
    var Trelacion: String = ""
}
