//
//  DPFModel.swift
//  DaquiApp
//
//  Created by Fredy on 17/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
struct DPFModel:Decodable {
    var ccuenta: String = ""
    var cgrupoproducto: String = ""
    var cproducto: String = ""
    var nombrecuenta: String = ""
    var monto: String = ""
    var plazo: String = ""
    var tasa: String = ""
    var fapertura: String = ""
    var descripcionproducto: String = ""
    var descripcionestatuscuenta: String = ""
    var fvencimiento: String = ""
    var cfrecuencia_interes: String = ""
}
