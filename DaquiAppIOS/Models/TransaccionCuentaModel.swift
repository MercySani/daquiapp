//
//  TransaccionesCuentasModel.swift
//  DaquiApp
//
//  Created by Fredy on 17/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
struct TransaccionCuentaModel: Decodable {
    var valor: Int = 0
    var FContable: String = ""
    var Transaccion: String = ""
    var Depositos: String = ""
    var Retiros: String = ""
    var Contable: String = ""
}
