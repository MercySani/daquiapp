//
//  PagarServiciosBasicosModel.swift
//  DaquiApp
//
//  Created by Fredy on 28/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
struct PagarServiciosBasicosModel: Decodable {
    var strRucBanco: String = ""
    var strLocalidad: String = ""
    var strPuntoVenta: String = ""
   var strSecuencial: String = ""
    var strAutorizacionSRI: String = ""
    var strDireccionBanco: String = ""
    var strFechaVigenciaSri: String = ""
    var strFechaInicioSRI: String = ""
    var strFechaResolucionSRI: String = ""
    var strFecha_Proceso: String = ""
    var strCodigo_Retorno: String = ""
    var strDescripcion_Retorno: String = ""
    var strSecuencial_Banco: String = ""
    var strSecuencial_Banco_Comision: String = ""
    var strCodigo_Tercero: String = ""
}
