//
//  TransferenciaExternaModel.swift
//  DaquiApp
//
//  Created by Fredy on 17/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
struct RespuestaTransferenciaModel: Decodable {
    var idtransaccion: Int
    var codigoError: String
    var estado: Int
    var tipoTransaccion: Int
    var fecha: String? = nil
    var descripcionError: String
    
    init(idtransaccion: Int, codigoerror: String, estado: Int, tipotransaccion: Int, descdescripcionerror: String) {
        self.idtransaccion = idtransaccion
        self.codigoError = codigoerror
        self.estado = estado
        self.tipoTransaccion = tipotransaccion
        self.descripcionError = descdescripcionerror
    }

}
