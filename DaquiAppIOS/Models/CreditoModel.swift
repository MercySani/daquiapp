//
//  CreditosModel.swift
//  DaquiApp
//
//  Created by Fredy on 17/5/18.
//  Copyright © 2018 coopdaquilema. All rights reserved.
//

import Foundation
struct CreditoModel: Decodable{
    var ccuenta:String = ""
    var montoprestamo: String = ""
    var descripciongrupoproducto: String = ""
    var descripcionproducto: String = ""
    var descripcionestatuscuenta: String = ""
    var plazo: String = ""
    var tasa: String = ""
    var frecuencias: String = ""
    var fdesembolso: String = ""
    var identificacion: String = ""
    var nombrelegal: String = ""
    var fechaProximoPago: String = ""
    var cuotasVencidas: String = ""
    var saldoPendiente: String = ""
    var saldoPendienteVigente: String = ""
    var diasVencidos: Int = 0
    var numeroSocio: Int = 0
    var totalDeuda: Int = 0
    var oficial: Int = 0
    var agencia: Int = 0
}
