//
//  ResultadoRestful.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 03/01/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import Foundation
struct ResultadoRestful:Decodable {
    var strRol: String = ""
    var strIdentificacion: String = ""
    var strNombreCompleto: String = ""
    var strEMail: String = ""
    var strSucursal: String = ""
    var strNombreUser: String = ""
    var strToken: String = ""
    var strTelefono: String = ""
    var strVerificarExistenciaUCSOrigen: String = ""
    var strUserEstado: String = ""
    var strMensajeError: String = ""
    var strCodigoError: String = ""
}
