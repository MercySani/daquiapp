//
//  PagoBimoModel.swift
//  DaquiAppIOS
//
//  Created by Desarrollador on 18/05/2020.
//  Copyright © 2020 CoopDaquilema. All rights reserved.
//

import Foundation

struct PagoBimoModel:Decodable {
    var phoneOrig: String = ""
    var nameOrig: String = ""
    var phoneDst: String = ""
    var nameDst: String = ""
    var reference: String = ""
    var accountDst: String = ""
    var endToEnd: String = ""
    var date: DateBimo
    var amount: Amount
}
