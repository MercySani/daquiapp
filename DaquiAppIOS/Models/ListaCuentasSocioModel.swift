//
//  ListaCuentasSocioModel.swift
//  DaquiApp
//
//  Created by Fredy on 22/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
import UIKit

struct ListaCuentas: Decodable {
    var numCuenta: String = ""
    var Descripcion: String = ""
    var Producto: String = ""
    var GrupoProducto: String = ""
    var SaldoDis: String = ""
    var SaldoCon: String = ""
    var SaldoRet: String = ""
    var SaldoBloq: String = ""
    var SaldoAport: String = ""
    var SaldoPig: String = ""
    var Relacion: String = ""
}
struct ListaCuentasSocioModel: Decodable {
    var cPersona: String = ""
    var cTipoPersona: String = ""
    var cTipoIdentificacion: String = ""
    var Identificacion: String = ""
    var NombreLegal: String = ""
    var Urbanizacion: String = ""
    var Calle: String = ""
    var Direccion: String = ""
    var NumeroTelefono: String = ""
    var cPais: String = ""
    var cProvincia: String = ""
    var cCiudad: String = ""
    var CodigoZip5: String = ""
    var CodigoZip4: String = ""
    var CodigoBarras: String = ""
    var cTipoIdentificacion_Adicional: String = ""
    var IdentificacionAdicional: String = ""
    var SeguroSocial: String = ""
    var LicenciaConducir: String = ""
    var fNacimiento: String = ""
    var cEstatusPersona: String = ""
    var cTipoBanca: String = ""
    var cTipoSegmento:String = ""
    var MontoExcepcion: String =  ""
    var fCaducidadExcepcion: String = ""
    var NoConductor: String = ""
    var lstCuentas: [ListaCuentas] = []
    
}
