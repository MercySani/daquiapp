//
//  LisCueXNoSocioSRModel.swift
//  DaquiApp
//
//  Created by Fredy on 11/6/18.
//  Copyright © 2018 Coopdaquilema. All rights reserved.
//

import Foundation
import UIKit
struct EmpresaProveedora: Decodable {
    var id: Int = 0
    var idPersona: Int = 0
    var idEmpresaProveedora: Int = 0
    //var servicios: String = []
    var estado: Int = 0
    var fCreacion: String = ""
    var fActualizacion: String = ""
}

struct ServiciosSR: Decodable {
    var id: String = ""
    var empresaProveedora: EmpresaProveedora = EmpresaProveedora()
    var idFrecuencia: Int = 0
    var idCategoria: Int = 0
    var proveedor: String = ""
    var producto: String = ""
    var nombre: String = ""
    var idauxiliar: String = ""
    var tipotrxconsulta: Int = 0
    var tipotrxpago: Int = 0
    var tipotrxreverso: Int = 0
    var tipotrxaux: Int = 0
    var comisionxrubro: String = ""
    var tituloreferencia: String = ""
    var tipodatoreferencia: String = ""
    var longitudreferencia:Int = 0
    var tipopago: String = ""
    var consultaauxiliar: String = ""
    var infoadd: String = ""
    var tipocontrol: String = ""
    //var valoresrecarga: String = [] //datos no determinados
    var valoresrecarga_str: String = ""
    var observacion: String = ""
    //var paraPagoProgramado: Bool = false
    var estado: Int = 0
    var fCreacion: String = ""
    var fActualizacion: String = ""
    var usuarioCreacion: String = ""
    var usuarioActualizacion: String = ""
    
}

struct Persona: Decodable {
    var id: Int = 0
    var idTipoPersona: String = ""
    var idTipoIdentificacion: String = ""
    var cpersona: Int = 0
    var identificacion: String = ""
    var nombre: String = ""
    var socio: String = ""
    var email: String = ""
    var callePrincipal: String = ""
    //var calleSecundaria: String = ""
    var celular: String = ""
    //var autorizacionSRI: String = ""
    var fEmision: String = ""
    var fCaducidad: String = ""
    //var comentario: String = ""
    var fCreacion: String = ""
    var fActualizacion: String = ""
    var usuarioCreacion: String = ""
    var usuarioActualizacion: String = ""
    
}

struct LisCueXNoSocioSRModel: Decodable {
    var id: Int = 0
    var servicio: ServiciosSR = ServiciosSR()
    var persona: Persona = Persona()
    var idAgencia:Int = 0
    var fInicio: String = ""
    var fFinalizacion: String = ""
    var dia: Int = 0
    var cuentaDebito: String = ""
    var montoMaximo: Int = 0
    var referencia: String = ""
    var datoAdicional: String? = nil
    var codigoCanalInterno: String = ""
    var estado: Int = 0
    var fCreacion: String = ""
    var fActualizacion: String = ""
    var usuarioCreacion: String = ""
    var usuarioActualizacion: String = ""
    
}

